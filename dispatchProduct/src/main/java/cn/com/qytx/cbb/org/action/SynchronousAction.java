package cn.com.qytx.cbb.org.action;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;

import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.org.domain.GroupInfo;
import cn.com.qytx.platform.org.domain.UserInfo;
import cn.com.qytx.platform.org.service.IGroup;
import cn.com.qytx.platform.org.service.IUser;
import cn.com.qytx.platform.utils.encrypt.MD5;
import cn.com.qytx.platform.utils.pinyin.FormatPinyinTo9Data;
import cn.com.qytx.platform.utils.pinyin.Pinyin4jUtil;

import com.google.gson.Gson;

public class SynchronousAction extends BaseActionSupport{
	private final static Logger logger = Logger.getLogger(SynchronousAction.class.getName());
	/**
	 * 
	 */
	private static final long serialVersionUID = -1369757100192032435L;
	/**
     * 部门/群组管理接口
     */
    @Resource
    private IGroup groupService;
    private GroupInfo group;
	private String groupMsg;
	
	private String userMsg;
	
	private String groupName;
	
	private String groupCode;
	

    // 用户
    private UserInfo user;
    
    private String birthDay;
    
    private String workNos;
    
    private boolean deleteFlag = false;
	
	  /**
     * 用户信息
     */
    @Resource(name = "userService")
    IUser userService;
	
	/**
     * 同步话务部门信息
     */
    public void synchGroupInfo(){
    	GroupInfo groupInfo = null;
    	try{
    		Gson gson = new Gson();
    		groupInfo=gson.fromJson(groupMsg, GroupInfo.class);
    		String hql = "";
    		if(StringUtils.isNoneBlank(groupInfo.getGroupCode())){
    			
    		}
    		List<GroupInfo> groupList=groupService.findAll(" isDelete=0  and groupCode = '"+groupInfo.getGroupCode()+"'");
           	List<GroupInfo> parentGroupList=groupService.findAll(" isDelete=0 and groupArea = '"+groupInfo.getGroupArea()+"'");
           	GroupInfo groupParent =null; 
           	if(parentGroupList!=null && parentGroupList.size()>0){
           		groupParent=parentGroupList.get(0);
           	}
           	if(groupList!=null && groupList.size()>0){//修改
           		GroupInfo groupOld=groupList.get(0);
    	    	groupOld.setOrderIndex(groupInfo.getOrderIndex());
    	    	groupOld.setGroupName(groupInfo.getGroupName());
    	    	if(StringUtils.isNoneBlank(groupInfo.getPhone())){
    	    		groupOld.setPhone(groupInfo.getPhone());
    	    	}
    	    	groupOld.setParentId(groupInfo.getParentId());
    	    	groupOld.setDirectorId(groupInfo.getDirectorId());
    	    	groupOld.setAssistantId(groupInfo.getAssistantId());
    	    	groupOld.setTopDirectorId(groupInfo.getTopDirectorId());
    	    	groupOld.setTopChangeId(groupInfo.getTopChangeId());
    	    	groupOld.setFunctions(groupInfo.getFunctions());
    	    	groupOld.setLastUpdateTime(new Date());
    	    	groupOld.setGroupArea(groupInfo.getGroupArea());
    	    	groupOld.setGroupCode(groupInfo.getGroupCode());
    	    	if(groupParent==null){
    	    		groupOld.setParentId(0);
    	    		groupOld.setPath(String.valueOf(groupInfo.getGroupId()));
    	    		groupOld.setGrade(0);
    	    	}else{
    	        	//得到父对象
    	    		if(groupParent!=null){
    	    			//设置路径
    	    			String pathParent = groupParent.getPath();
    	    			if(pathParent!=null){
    	    				groupOld.setPath(pathParent+","+groupInfo.getGroupId());
    	    			}
    	            	//设置级别
    	    			Integer gradeParent=groupParent.getGrade();
    	    			if(gradeParent!=null){
    	    				groupOld.setGrade(gradeParent+1);
    	    			}
    	    			groupOld.setParentId(groupParent.getGroupId());
    	    		}
    	    	}
    	    	groupService.addGroup(groupOld);
    	    	logger.info("同步更新部门成功!!!》groupName="+groupOld.getGroupName()+"groupId="+groupOld.getGroupId()+",parentId="+groupOld.getParentId());
    	    	ajax("1");
           	}else{//新增
           		groupInfo.setGroupId(null);
            	groupInfo.setIsDelete(0);
            	groupInfo.setExtension("3");//默认临床
            	groupInfo.setLastUpdateTime(new Date());
            	groupInfo.setPath(null);
            	groupInfo.setParentId(0);
            	if(groupParent==null){
            		groupInfo.setParentId(0);
            		groupInfo.setGrade(0);
            	}else{
                	//得到父对象
            		if(groupParent!=null){
                    	//设置级别
            			Integer gradeParent=groupParent.getGrade();
            			if(gradeParent!=null){
            				groupInfo.setGrade(gradeParent+1);
            			}
            			groupInfo.setParentId(groupParent.getGroupId());
            		}
            	}
            	if(groupInfo.getOrderIndex()==null){
            		int maxOrder = groupService.getMaxOrderIndex(groupInfo.getCompanyId(), groupInfo.getParentId(),GroupInfo.DEPT);
            		groupInfo.setOrderIndex(maxOrder+1);
            	}
            	groupService.saveOrUpdate(groupInfo);
            	if(groupInfo.getParentId()==0){
            		groupInfo.setPath(String.valueOf(groupInfo.getGroupId()));
            	}else{
            		GroupInfo groupNewParent=groupService.findOne(groupInfo.getParentId());
            		if(groupNewParent!=null){
            			String pathParent = groupNewParent.getPath();
    	    			if(pathParent!=null){
    	    				groupInfo.setPath(pathParent+","+groupInfo.getGroupId());
    	    			}
            		}
            	}
            	groupService.saveOrUpdate(groupInfo);
            	logger.info("同步添加部门成功!!!》groupName="+groupInfo.getGroupName()+"groupId="+groupInfo.getGroupId()+",parentId="+groupInfo.getParentId());
                ajax("1");
           	}
    	}catch(Exception e){
    		logger.info("同步部门失败!!!groupMsg="+groupMsg+",失败原因："+e.getMessage());
    	}
       	
    }
    
    
    /**
     * 删除部门/群组
     * @return
     */
    public String synchDeleteGroup()
    {
        // 得到是否有子组
    	String hql = "isDelete=0  and groupName = '"+groupName+"'";
    	if(StringUtils.isNoneBlank(groupCode)){
    		hql = "isDelete=0  and groupCode = '"+groupCode+"'";
    	}
    	List<GroupInfo> groupList=groupService.findAll(hql);
    	GroupInfo group =groupList.get(0);
    	boolean isHasChild = groupService.isHasChild(group.getGroupId());
        //GroupInfo group = groupService.findOne(groupId);
        if (isHasChild)
        {
            ajax(2);
            return null;
        }
        //如果是部门则判断下面是否含有成员
        if(group.getGroupType().intValue() == GroupInfo.DEPT){
        	// 判断部门下是否含有成员
        	boolean isHasGroupUser = groupService.isHasUsers(group.getCompanyId(),group.getGroupId());
        	
        	if (isHasGroupUser)
        	{
        		logger.info("删除部门失败,该部门下存在人员");
        		ajax(3);
        		return null;
        	}
        }
        groupService.deleteGroup(group.getGroupId());
        ajax("1");
        return null;
    }
    
    
    
    
    /**
     * 同步话务人员信息
     */
    public void synchUserInfo(){
    	Gson gson = new Gson();
    	UserInfo userInfo= null;
    	try {
    		userInfo = gson.fromJson(userMsg, UserInfo.class);
    		UserInfo userOld = userService.findOne(" workNo='"+userInfo.getWorkNo()+"'");
        	List<GroupInfo> groupList=groupService.findAll(" isDelete=0 and groupCode = '"+groupCode+"'");
        	Integer groupId=null;
        	if(groupList!=null && groupList.size()>0){
        		GroupInfo groupInfo=groupList.get(0);
        		groupId=groupInfo.getGroupId();
        	}else{
        		GroupInfo group=groupService.findOne("groupName ='健康信息中心'");
        		groupId=group.getGroupId();
        	}
           	if(userOld!=null){//修改
           		userOld.setGroupId(groupId);
           		userOld.setUserName(userInfo.getUserName());
                userOld.setWorkNo(userInfo.getWorkNo());
                userOld.setLoginName(userInfo.getWorkNo());
                userOld.setOrderIndex(userInfo.getOrderIndex());
                userOld.setAlterName(userInfo.getAlterName());
                userOld.setSex(userInfo.getSex());
                if(StringUtils.isNotBlank(birthDay)){
            		try {
            			userOld.setBirthDay(DateUtils.parseDate(birthDay, "yyyy-MM-dd"));
        			} catch (ParseException e) {
        				e.printStackTrace();
        			}
            	}else{
            		userOld.setBirthDay(null);
            	}
                userOld.setPhone(userInfo.getPhone());
                userOld.setPhonePublic(userInfo.getPhonePublic());
                userOld.setPhone2(userInfo.getPhone2());
                userOld.setOfficeTel(userInfo.getOfficeTel());
                userOld.setHomeTel(userInfo.getHomeTel());
                userOld.setEmail(userInfo.getEmail());
                userOld.setSignType(userInfo.getSignType());
                userOld.setSinWidget(userInfo.getSinWidget());
                userOld.setOfficeWidget(userInfo.getOfficeWidget());
                userOld.setTaoDa(userInfo.getTaoDa());
                userOld.setPhoto(userInfo.getPhoto());
                userOld.setJob(userInfo.getJob());
                userOld.setTitle(userInfo.getTitle());
                if(userInfo.getSignUrl()!=null && !userInfo.getSignUrl().equals("")){
                	userOld.setSignUrl(userInfo.getSignUrl());
                }
                if(userInfo.getNtkoUrl()!=null && !userInfo.getNtkoUrl().equals("")){
                	userOld.setNtkoUrl(userInfo.getNtkoUrl());
                }

                String py = Pinyin4jUtil.getPinYinHeadChar(userInfo.getUserName());
                userOld.setPy(py);
                //获得全拼拼音 带空格
                String fullPy = Pinyin4jUtil.getPinYinWithBlank(userInfo.getUserName());
                userOld.setFullPy(fullPy);
                //获得用户姓名全拼对应九宫格按键
                String formattedNumber = FormatPinyinTo9Data.getFormattedNumber(fullPy);
                userOld.setFormattedNumber(formattedNumber);
                userOld.setLastUpdateTime(new Date());
                userService.addOrUpdateUserWithOrder(userOld);
                logger.info("同步人员修改成功!!!userMsg="+userMsg);
           	}else{//新增
           	// 登录密码默认123456
    	        MD5 md5 = new MD5();
    	        String pass = md5.encrypt("123456");
    	        if(StringUtils.isNotBlank(birthDay)){
    	    		try {
    	    			userInfo.setBirthDay(DateUtils.parseDate(birthDay, "yyyy-MM-dd"));
    				} catch (ParseException e) {
    					e.printStackTrace();
    				}
    	    	}
    	        userInfo.setIsDelete(0);
    	        userInfo.setIsDefault(1);
    	        userInfo.setLoginPass(pass);
    	        userInfo.setPartitionCompanyId(userInfo.getCompanyId()%10);
    	        userInfo.setSkinLogo(1);
    	        userInfo.setCreateTime(new Date());
    	        userInfo.setLastUpdateTime(new Date());
    	        String py = Pinyin4jUtil.getPinYinHeadChar(userInfo.getUserName());
    	        userInfo.setPy(py);
    	        //获得全拼拼音 带空格
    	        String fullPy = Pinyin4jUtil.getPinYinWithBlank(userInfo.getUserName());
    	        userInfo.setFullPy(fullPy);
    	        //获得用户姓名全拼对应九宫格按键
    	        String formattedNumber = FormatPinyinTo9Data.getFormattedNumber(fullPy);
    	        userInfo.setFormattedNumber(formattedNumber);
    	        userInfo.setGroupId(groupId);	        	        
    	        //设置用户排序,如果用户排序号为空，则取最大值+1
    	        if(userInfo.getOrderIndex()==null){
    	        	int maxOrder = userService.getMaxOrderIndex(userInfo.getCompanyId(), groupId);
    	        	userInfo.setOrderIndex(maxOrder+1);
    	        }
    	        //add by jiayq,添加的用户默认就是登录用户，用户名是手机号码，密码是123456
    	        userInfo.setUserState(UserInfo.USERSTATE_LOGIN);
    	        //user.setLoginName(user.getPhone());
    	        userInfo.setLoginName(userInfo.getWorkNo());
    	        userInfo.setRegisterTime(new Date());
    	        userService.addOrUpdateUserWithOrder(userInfo);
    	        logger.info("同步人员添加成功!!!userMsg="+userMsg);
           	}
           	ajax("1");
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("同步人员失败!!!userMsg="+userMsg+",失败原因:"+e.getMessage());
			ajax(2);
		}
       	
    }
    
    
    /**
     * 删除人员
     * @return
     */
    public String synchDeleteUser()
    {
        if (StringUtils.isNoneBlank(workNos))
        {
        	workNos=removeSymbol(workNos);
            String[] wprkArray=workNos.split(",");
            if(wprkArray.length>0){
            	workNos="";
            	for(int i=0;i<wprkArray.length;i++){
            		workNos+="'"+wprkArray[i]+"',";
            	}
            }
            workNos=removeSymbol(workNos);
            List<UserInfo> list = userService.findAll(" workNo in ("+workNos+") and isDelete=0" );
            String userIds="";
            Integer companyId=1;
            if(list!=null && list.size()>0){
            	for(UserInfo userInfo :list){
            		userIds+=userInfo.getUserId()+",";
            		companyId=userInfo.getCompanyId();
            	}
            }
            if (userIds.endsWith(","))
            {
            	userIds = userIds.substring(0, userIds.length() - 1);
            }
            if (userIds.startsWith(","))
            {
            	userIds = userIds.substring(1, userIds.length());
            }
            userService.deleteUserByIds(userIds, deleteFlag,companyId);
            ajax("success");
            return null;
        }
        else
        {
            ajax("请选择要删除的人员！");
            return null;
        }
    }

    /**
     * 去除都逗号
     * @return 
     */
    private static String removeSymbol(String str){
    	if (str.endsWith(","))
        {
    		str = str.substring(0, str.length() - 1);
        }
        if (str.startsWith(","))
        {
        	str = str.substring(1, str.length());
        }
        return str;
    }
    
    
	public GroupInfo getGroup() {
		return group;
	}
	public void setGroup(GroupInfo group) {
		this.group = group;
	}
	public String getGroupMsg() {
		return groupMsg;
	}
	public void setGroupMsg(String groupMsg) {
		this.groupMsg = groupMsg;
	}


	public String getGroupName() {
		return groupName;
	}


	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}


	public UserInfo getUser() {
		return user;
	}


	public void setUser(UserInfo user) {
		this.user = user;
	}


	public String getBirthDay() {
		return birthDay;
	}


	public void setBirthDay(String birthDay) {
		this.birthDay = birthDay;
	}


	public String getUserMsg() {
		return userMsg;
	}


	public void setUserMsg(String userMsg) {
		this.userMsg = userMsg;
	}


	public String getWorkNos() {
		return workNos;
	}


	public void setWorkNos(String workNos) {
		this.workNos = workNos;
	}


	public String getGroupCode() {
		return groupCode;
	}


	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}
    
     
    
}
