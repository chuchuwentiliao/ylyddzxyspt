/**
 * 
 */
package cn.com.qytx.hemei.location.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.com.qytx.hemei.location.dao.GroupLocationDao;
import cn.com.qytx.hemei.location.domain.GroupLocation;
import cn.com.qytx.hemei.location.service.IGroupLocation;
import cn.com.qytx.platform.base.service.impl.BaseServiceImpl;

/**
 * 功能: 
 * 版本: 1.0
 * 开发人员: 王刚
 * 创建日期: 2017年4月20日
 * 修改日期: 2017年4月20日
 * 修改列表: 
 */
@Service("groupLocationService")
@Transactional
public class GroupLocationImpl extends BaseServiceImpl<GroupLocation> implements IGroupLocation {
	@Autowired
	private GroupLocationDao groupLocationDao;

	
	@Override
	public List<GroupLocation> findList(Integer groupId) {
		// TODO Auto-generated method stub
		return groupLocationDao.findList(groupId);
	}
	
	
}
