/**
 * 
 */
package cn.com.qytx.hemei.report.repair.action;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import cn.com.qytx.cbb.org.util.ExportExcel;
import cn.com.qytx.hemei.report.service.IRepairReport;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.org.domain.UserInfo;

/**
 * 功能: 维修事项前十数据
 * 版本: 1.0
 * 开发人员: 王刚
 * 创建日期: 2017年5月10日
 * 修改日期: 2017年5月10日
 * 修改列表: 
 */
public class RepairTopTenAction extends BaseActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7952833194429841150L;
	
	@Autowired
	private IRepairReport repairReportService;
	
	
	private String beginTime;//开始时间
	private String endTime;//结束时间
	
	  /**
     * 获得维修事项前十数据
     */
    public void findRepairTopTen(){
    	UserInfo user= getLoginUser();
    	if(user!=null){
    		List<Object[]> list = repairReportService.findRepairTopTen(beginTime,endTime);
    		List<Map<String,Object>> listMap= new ArrayList<Map<String,Object>>();
    		List<String> name = new ArrayList<String>(); 
    		List<Integer> number = new ArrayList<Integer>(); 
    		if(list!=null && list.size()>0){
    			int i=1;
    			Integer count=0;
    			for(Object[] obj:list){
    				name.add(obj[0].toString());
    				number.add(Integer.valueOf(obj[1].toString()));
    				Map<String,Object> map = new HashMap<String, Object>();
    				map.put("no", i);
    				map.put("name", obj[0].toString());
    				map.put("num", obj[1]==null?0:Integer.valueOf(obj[1].toString()));
    				count+=obj[1]==null?0:Integer.valueOf(obj[1].toString());
    				listMap.add(map);
    				i++;
    			}
    			Map<String,Object> map = new HashMap<String, Object>();
    			map.put("no", "合计");
    			map.put("name", "");
    			map.put("num", count);
    			listMap.add(map);
    		}
    		String[]nameArray= new String[name.size()];
    		name.toArray(nameArray);
    		Integer[]numArray= new Integer[number.size()];
    		number.toArray(numArray);
    		Map<String,Object> mapData =  new HashMap<String, Object>();
    		mapData.put("aData",listMap );
    		mapData.put("nameArray",nameArray );
    		mapData.put("numArray",numArray );
    		ajax(mapData);
    	}
    }
    /**
     * 导出维修事项前十
     */
    public  void exportRepairTopTen(){
    	HttpServletResponse response = this.getResponse();
        response.setContentType("application/vnd.ms-excel");
        OutputStream outp = null;
		try{
			UserInfo userInfo=this.getLoginUser();
			if(userInfo!=null){
				List<Object[]> list = repairReportService.findRepairTopTen(beginTime,endTime);
				List<Map<String,Object>> listMap= new ArrayList<Map<String,Object>>();
				if(list!=null && list.size()>0){
	    			int i=0;
	    			for(Object[] obj:list){
	    				Map<String,Object> map = new HashMap<String, Object>();
	    				map.put("no", i);
	    				map.put("name", obj[0].toString());
	    				map.put("num", obj[1]==null?0:Integer.valueOf(obj[1].toString()));
	    				listMap.add(map);
	    				i++;
	    			}
	    		}
				String fileName = URLEncoder.encode("维修事项前十.xls", "UTF-8");
		        // 把联系人信息填充到map里面
		        response.addHeader("Content-Disposition",
		                "attachment;filename=" + fileName);// 解决中文
				outp = response.getOutputStream();
	            ExportExcel exportExcel = new ExportExcel(outp, getExportHeadList(), listMap, getExportKeyList());
	            exportExcel.exportWithSheetName("维修事项前十");
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(outp!=null){
				try {
					outp.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}  
    }
    
    private List<String> getExportHeadList(){
        List<String> headList = new ArrayList<String>();
        headList.add("序号");
        headList.add("维修事项");
        headList.add("数量");
        return headList;
    }
    
    private List<String> getExportKeyList(){
        List<String> headList = new ArrayList<String>();
        headList.add("no");
        headList.add("name");
        headList.add("num");
        return headList;
    }
    
    
    
	public String getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}





}
