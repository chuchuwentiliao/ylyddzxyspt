package cn.com.qytx.hemei.report.participant.action;

import java.io.OutputStream;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import cn.com.qytx.cbb.org.util.ExportExcel;
import cn.com.qytx.hemei.report.participant.impl.service.IParticipantReport;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.org.domain.UserInfo;

/**
 * 参与人报表
 * @author dell
 *
 */
public class ParticipantReportAction extends BaseActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5495872896198124747L;
	
	
	
    @Autowired
    private IParticipantReport participantReportService;
	
	private String groupName;
	
	private String userName;
	
	private String beginTime;
	
	private String endTime;
	/**
	 * 参与人报表
	 * @return
	 */
	public String findParticipantReport(){
		try {
			UserInfo user=getLoginUser();
			if(user!=null){
				List<Object[]> list=participantReportService.findParticipantReportList(groupName, userName,beginTime,endTime);
				List<Map<String,Object>> listMap= getMapList(list);
				ajax(listMap);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	/**
	 * 封装数据
	 * @param list
	 * @return
	 */
	public List<Map<String,Object>> getMapList(List<Object[]> list){
		List<Map<String,Object>> listMap= new ArrayList<Map<String,Object>>();
		if(list!=null && list.size()>0){
			int num=1;
			for(Object[] obj:list){
				Map<String,Object> map= new HashMap<String, Object>();
					Integer completeNum=(Integer)obj[0];
					Integer total=(Integer)obj[1];
					map.put("completeNum", completeNum);
					map.put("total", total);
					map.put("noCompleteNum", total-completeNum);
					map.put("no",num);
					map.put("userName",(String)obj[2]);
					map.put("groupName",(String)obj[3]);
					num++;
					listMap.add(map);
			}
		}
		return listMap;
	}
	
	
	/*
	 * 
	 * 导出
	 */
		public void exportReport(){
			HttpServletResponse response = this.getResponse();
	        response.setContentType("application/vnd.ms-excel");
	        OutputStream outp = null;
			try {
				UserInfo user= getLoginUser();
				if(user!=null){
					if(StringUtils.isNoneBlank(userName)){
						userName=URLDecoder.decode(userName, "UTF-8");
					}
					if(StringUtils.isNoneBlank(groupName)){
						groupName=URLDecoder.decode(groupName, "UTF-8");
					}
					List<Object[]> list=participantReportService.findParticipantReportList(groupName, userName,beginTime,endTime);
					List<Map<String,Object>> listMap= getMapList(list);
					String fileName = URLEncoder.encode("参与人报表.xls", "UTF-8");
			        // 把联系人信息填充到map里面
			        response.addHeader("Content-Disposition",
			                "attachment;filename=" + fileName);// 解决中文
					outp = response.getOutputStream();
		            ExportExcel exportExcel = new ExportExcel(outp, getExportHeadList(), listMap, getExportKeyList());
		            exportExcel.exportWithSheetName("参与人报表");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
	   private List<String> getExportHeadList(){
	        List<String> headList = new ArrayList<String>();
	        headList.add("序号");
	        headList.add("参与人");
	        headList.add("所属科室");
	        headList.add("参与数量");
	        headList.add("完工数量");
	        headList.add("未完工数量");
	        return headList;
	    }
		    
	    private List<String> getExportKeyList(){
	        List<String> headList = new ArrayList<String>();
	        headList.add("no");
	        headList.add("userName");
	        headList.add("groupName");
	        headList.add("total");
	        headList.add("completeNum");
	        headList.add("noCompleteNum");
	        return headList;
	    }
	
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getBeginTime() {
		return beginTime;
	}


	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}


	public String getEndTime() {
		return endTime;
	}


	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	
	
	

}
