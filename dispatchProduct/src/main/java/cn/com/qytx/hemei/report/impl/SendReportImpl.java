/**
 * 
 */
package cn.com.qytx.hemei.report.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.com.qytx.hemei.defect.domain.DefectApply;
import cn.com.qytx.hemei.report.dao.SendReportDao;
import cn.com.qytx.hemei.report.service.ISendReport;
import cn.com.qytx.platform.base.service.impl.BaseServiceImpl;

/**
 * 功能: 
 * 版本: 1.0
 * 开发人员: 
 * 创建日期: 2017年5月15日
 * 修改日期: 2017年5月15日
 * 修改列表: 
 */
@Service
@Transactional
public class SendReportImpl extends BaseServiceImpl<DefectApply> implements ISendReport {
   
	@Autowired
	private SendReportDao sendReportDao;
	
	
	@Override
	public List<Object[]> findStaff(String beginTime, String endTime,
			String userName, String departmentName,Integer type) {
		// TODO Auto-generated method stub
		return sendReportDao.findStaffList(beginTime, endTime, userName, departmentName,type);
	}
	@Override
	public List<Object[]> findMedicalDepartmenList(String beginTime,
			String endTime,String departmentName,Integer type) {
		// TODO Auto-generated method stub
		return sendReportDao.findMedicalDepartmenList(beginTime, endTime,departmentName,type);
	}
	@Override
	public List<Object[]> findSatisfaction(String beginTime, String endTime, String userName, Integer type) {
		return sendReportDao.findSatisfaction(beginTime,endTime,userName,type);
	}
	@Override
	public Map<Integer, String> findMap(String beginTime, String endTime,
			Integer type,String instanceIds) {
		// TODO Auto-generated method stub
		return sendReportDao.findListMap(beginTime, endTime, type,instanceIds);
	}
	@Override
	public List<Object[]> findTakeWorkList(String beginTime, String endTime,Integer type) {
		// TODO Auto-generated method stub
		return sendReportDao.findTakeWorkList(beginTime, endTime, type);
	}


}
