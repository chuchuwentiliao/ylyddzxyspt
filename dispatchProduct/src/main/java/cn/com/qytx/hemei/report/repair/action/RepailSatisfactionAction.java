/**
 * 
 */
package cn.com.qytx.hemei.report.repair.action;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import cn.com.qytx.cbb.org.util.ExportExcel;
import cn.com.qytx.hemei.report.service.IRepairReport;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.org.domain.UserInfo;

/**
 * 功能:后勤员工维修满意度
 * 版本: 1.0
 * 开发人员: 王刚
 * 创建日期: 2017年5月10日
 * 修改日期: 2017年5月10日
 * 修改列表: 
 */
public class RepailSatisfactionAction extends BaseActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4194918737348807943L;
	
	@Autowired
	private IRepairReport repairReportService;
	
	
	private String beginTime;//开始时间
	private String endTime;//结束时间
	private String employeeName;//員工
	
	/**
	 * 获得维修人员满意度统计
	 */
    public void findRepairSatisfiedList(){
    	UserInfo user =getLoginUser();
    	if(user!=null){
    		List<Object[]> list = repairReportService.findSatisfactionList(beginTime, endTime,employeeName);
    		List<Map<String,Object>> listMap= new ArrayList<Map<String,Object>>();
    		List<String> name = new ArrayList<String>(); 
    		List<Integer> verySatisfiedNum = new ArrayList<Integer>(); 
    		List<Integer> satisfiedNum = new ArrayList<Integer>(); 
    		List<Integer> kindNum = new ArrayList<Integer>(); 
    		List<Integer> dissatisfiedNum = new ArrayList<Integer>(); 
    		if(list!=null && list.size()>0){
    			int i=1;
    			Integer sumVerySatisfied=0;
    			Integer sumSatisfied=0;
    			Integer sumKind=0;
    			Integer sumDissatisfied=0;
    			for(Object[] obj:list){
    				name.add(obj[0].toString());
    				verySatisfiedNum.add(obj[2]==null?0:Integer.valueOf(obj[2].toString()));
    				satisfiedNum.add(obj[3]==null?0:Integer.valueOf(obj[3].toString()));
    				kindNum.add(obj[4]==null?0:Integer.valueOf(obj[4].toString()));
    				dissatisfiedNum.add(obj[5]==null?0:Integer.valueOf(obj[5].toString()));
    				Map<String,Object> map = new HashMap<String, Object>();
    				map.put("no", i);
    				map.put("name", obj[0].toString());
    				map.put("departmentName",obj[1]==null?"":obj[1].toString());
    				map.put("verySatisfied",obj[2]==null?0:Integer.valueOf(obj[2].toString()));
    				map.put("satisfied",obj[3]==null?0:Integer.valueOf(obj[3].toString()));
    				map.put("kind",obj[4]==null?0:Integer.valueOf(obj[4].toString()));
    				map.put("dissatisfied", obj[5]==null?0:Integer.valueOf(obj[5].toString()));
    				sumVerySatisfied+=obj[2]==null?0:Integer.valueOf(obj[2].toString());
    				sumSatisfied+=obj[3]==null?0:Integer.valueOf(obj[3].toString());
    				sumKind+=obj[4]==null?0:Integer.valueOf(obj[4].toString());
    				sumDissatisfied+=obj[5]==null?0:Integer.valueOf(obj[5].toString());
    				listMap.add(map);
    				i++;
    			}
    			Map<String,Object> map = new HashMap<String, Object>();
    			map.put("no", "合计");
    			map.put("name", "");
				map.put("departmentName","");
				map.put("verySatisfied",sumVerySatisfied);
				map.put("satisfied",sumSatisfied);
				map.put("kind",sumKind);
				map.put("dissatisfied",sumDissatisfied);
				listMap.add(map);
    		}
    		String[] typeName={"满意","不满意"};
    		String[]nameArray= new String[name.size()];
    		name.toArray(nameArray);
    		Integer[]verySatisfiedArray= new Integer[verySatisfiedNum.size()];
    		verySatisfiedNum.toArray(verySatisfiedArray);
    		Integer[]satisfiedArray= new Integer[satisfiedNum.size()];
    		satisfiedNum.toArray(satisfiedArray);
    		Integer[]kindArray= new Integer[kindNum.size()];
    		kindNum.toArray(kindArray);
    		Integer[]dissatisfiedArray= new Integer[dissatisfiedNum.size()];
    		dissatisfiedNum.toArray(dissatisfiedArray);
    		Map<String,Object> mapData =  new HashMap<String, Object>();
    		mapData.put("aData",listMap );
    		mapData.put("nameArray",nameArray );
    		mapData.put("verySatisfiedArray",verySatisfiedArray );
    		mapData.put("satisfiedArray",satisfiedArray );
    		mapData.put("kindArray",kindArray );
    		mapData.put("dissatisfiedArray",dissatisfiedArray );
    		mapData.put("typeName",typeName );
    		ajax(mapData);
    	}
    }
	
    
    /**
     * 导出维修人员满意度
     */
    public  void exportRepairSatisfied(){
    	HttpServletResponse response = this.getResponse();
        response.setContentType("application/vnd.ms-excel");
        OutputStream outp = null;
		try{
			UserInfo userInfo=this.getLoginUser();
			if(userInfo!=null){
				employeeName=URLDecoder.decode(employeeName, "UTF-8");
				List<Object[]> list = repairReportService.findSatisfactionList(beginTime,endTime,employeeName);
				List<Map<String,Object>> listMap= new ArrayList<Map<String,Object>>();
				if(list!=null && list.size()>0){
	    			int i=0;
	    			for(Object[] obj:list){
	    				Map<String,Object> map = new HashMap<String, Object>();
	    				map.put("no", i);
	    				map.put("name", obj[0].toString());
	    				map.put("departmentName",obj[1].toString());
	    				map.put("verySatisfied",obj[2]==null?0:Integer.valueOf(obj[2].toString()));
	    				map.put("satisfied",obj[3]==null?0:Integer.valueOf(obj[3].toString()));
	    				map.put("kind",obj[4]==null?0:Integer.valueOf(obj[4].toString()));
	    				map.put("dissatisfied", obj[5]==null?0:Integer.valueOf(obj[5].toString()));
	    				listMap.add(map);
	    				i++;
	    			}
	    		}
				String fileName = URLEncoder.encode("处理结果满意度.xls", "UTF-8");
		        // 把联系人信息填充到map里面
		        response.addHeader("Content-Disposition",
		                "attachment;filename=" + fileName);// 解决中文
				outp = response.getOutputStream();
	            ExportExcel exportExcel = new ExportExcel(outp, getExportHeadList(), listMap, getExportKeyList());
	            exportExcel.exportWithSheetName("处理结果满意度");
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(outp!=null){
				try {
					outp.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}  
    }
    
    private List<String> getExportHeadList(){
        List<String> headList = new ArrayList<String>();
        headList.add("序号");
        headList.add("姓名");
        headList.add("所属部门");
       /* headList.add("非常满意");*/
        headList.add("满意");
        /*headList.add("一般");*/
        headList.add("不满意");
        return headList;
    }
    
    private List<String> getExportKeyList(){
        List<String> headList = new ArrayList<String>();
        headList.add("no");
        headList.add("name");
        headList.add("departmentName");
        /*headList.add("verySatisfied");*/
        headList.add("satisfied");
        /*headList.add("kind");*/
        headList.add("dissatisfied");
        return headList;
    }
    
    
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public String getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}


	public String getEmployeeName() {
		return employeeName;
	}


	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	
	
	
	

}
