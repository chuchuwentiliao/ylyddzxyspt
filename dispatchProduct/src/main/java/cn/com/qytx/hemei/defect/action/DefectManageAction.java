package cn.com.qytx.hemei.defect.action;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import cn.com.qytx.cbb.capital.domain.Capital;
import cn.com.qytx.cbb.dict.service.IDict;
import cn.com.qytx.cbb.file.domain.Attachment;
import cn.com.qytx.cbb.file.service.IAttachment;
import cn.com.qytx.cbb.myapply.domain.MyProcessed;
import cn.com.qytx.cbb.myapply.service.IMyProcessed;
import cn.com.qytx.cbb.notify.service.IWapNotify;
import cn.com.qytx.cbb.org.util.ExportExcel;
import cn.com.qytx.hemei.defect.domain.DefectApply;
import cn.com.qytx.hemei.defect.domain.SearchVo;
import cn.com.qytx.hemei.defect.domain.UserCapital;
import cn.com.qytx.hemei.defect.domain.WorkOrderLog;
import cn.com.qytx.hemei.defect.service.IDefectApply;
import cn.com.qytx.hemei.defect.service.ISelectPeople;
import cn.com.qytx.hemei.defect.service.IUserCapital;
import cn.com.qytx.hemei.util.EventForAddWorkOrderLog;
import cn.com.qytx.hemei.util.SystemConfig;
import cn.com.qytx.hemei.util.WorkOrderType;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;
import cn.com.qytx.platform.base.query.Sort;
import cn.com.qytx.platform.base.query.Sort.Direction;
import cn.com.qytx.platform.org.domain.GroupInfo;
import cn.com.qytx.platform.org.domain.RoleInfo;
import cn.com.qytx.platform.org.domain.UserInfo;
import cn.com.qytx.platform.org.service.IGroup;
import cn.com.qytx.platform.org.service.IRole;
import cn.com.qytx.platform.org.service.IUser;
import cn.com.qytx.platform.utils.DateUtils;
import cn.com.qytx.platform.utils.spring.SpringUtil;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * PC端Action
 * @ClassName: DefectManageAction   
 * @author: WANG
 * @Date: 2016年10月11日 上午11:21:37   
 *
 */
public class DefectManageAction extends BaseActionSupport{
	private static final long serialVersionUID = 1L;
	@Autowired
	private IDefectApply defectApplyImpl;
	@Autowired
	private IMyProcessed MyProcessImpl;
	@Resource(name="dictService")
	private IDict dictService;
	@Autowired
	private SystemConfig systemConfig;
	@Resource
	private IUser userService;
	@Resource
	private IGroup groupService;
	@Resource(name="attachmentService")
	private IAttachment attachmentService;
	@Resource(name="roleService")
	private IRole roleService;
	
	@Resource(name="userCapitalService")
	private IUserCapital userCapitalService;
	
	@Autowired
	private ISelectPeople selectPeopleImpl;
	
	@Resource
	private IWapNotify wapNotifyService;
	
	
	private Integer userId;
	private DefectApply apply;
	private String instanceId;
	private String approveResult;
	private String advice;
	private String taskName;
	private Integer nextUserId;
	private SearchVo searchVo;
	private String type;
	private Integer processType;
	private Integer achievement;
	private String evaluate;//评价
	
	private Integer num;
	private String beginTime;
	private String endTime;
	
	private String equipments;
	
	private String isOut;//是否话务系统 1 是
	
	private Integer isForkGroup;//是否有部门权限 值为1 则只能查看本部门工单
	
	/**
	 * 消缺上报
	 * @Title: apply   
	 */
	public void apply(){
		try{
			UserInfo userInfo=this.getLoginUser();
			if(userInfo!=null){
				if(StringUtils.isNotBlank(apply.getInstanceId())){
					DefectApply defectApply=defectApplyImpl.findByInstanceId(apply.getInstanceId());
					GroupInfo createGroup = groupService.findOne(defectApply.getCreateGroupId());
					/**
					 * 修改工单日志start
					 */
					WorkOrderLog workOrderLog = new WorkOrderLog();
					workOrderLog.setCompanyId(userInfo.getCompanyId());
					workOrderLog.setOperUser(userInfo);
					workOrderLog.setType(WorkOrderType.WORK_ORDER_UPDATE);
					workOrderLog.setInstanceId(defectApply.getInstanceId());
					workOrderLog.setContent("修改工单，原内容：[上报科室："+(createGroup!=null?createGroup.getGroupName():"--")+"，维修地点："+defectApply.getDescribe()+"，备注："+defectApply.getMemo()+"]");
					/**
					 * 添加工单日志end
					 */
					if(defectApply.getType()!=0){
						defectApply.setEquipmentName(apply.getEquipmentName());
					}
					defectApply.setAttachmentIds(apply.getAttachmentIds());
					defectApply.setAudioIds(apply.getAudioIds());
					defectApply.setDescribe(apply.getDescribe());
					defectApply.setAudioTime(apply.getAudioTime());
					defectApply.setGrade(apply.getGrade());
					defectApply.setDefectTime(apply.getDefectTime());
					defectApply.setMemo(apply.getMemo());
					defectApply.setCreateGroupId(apply.getCreateGroupId());
					defectApplyImpl.saveOrUpdate(defectApply);
					/*if(defectApply!=null){
						if(defectApply.getState().equals("0")||defectApply.getState().equals("6")){
							defectApplyImpl.delete(defectApply.getId(), false);
						}else{
							ajax("3");
							return;
						}
					}*/
					SpringUtil.getApplicationContext().publishEvent(new EventForAddWorkOrderLog(workOrderLog));
				}else{
					
					Integer checkNum = defectApplyImpl.findWaitCheckNum(userInfo.getUserId(), apply.getCreateGroupId(), userInfo.getCompanyId());
					if(checkNum > 0 ){
						ajax("2");
						return;
					}
					
					apply.setTurnNum(0);
					apply.setReminderNum(0);
					if(apply.getCreateUserId()!=null){
						String dispatchWorkNo = (String)this.getRequest().getSession().getAttribute("dispatchWorkNo");
						apply.setDispatchWorkNo(dispatchWorkNo);
					}
					
					List<Integer> capitalIdList = new ArrayList<Integer>();
					if(StringUtils.isNoneBlank(equipments)){
						Gson gson = new Gson();
						capitalIdList = gson.fromJson(equipments,new TypeToken<List<Integer>>(){}.getType());
					}
					apply=defectApplyImpl.apply((apply.getCreateUserId()!=null?apply.getCreateUserId():userInfo.getUserId()), apply,capitalIdList);
					WorkOrderLog workOrderLog = new WorkOrderLog();
					workOrderLog.setCompanyId(userInfo.getCompanyId());
					workOrderLog.setOperUser(userInfo);
					workOrderLog.setType(WorkOrderType.WORK_ORDER_ADD);
					workOrderLog.setInstanceId(apply.getInstanceId());
					workOrderLog.setContent("添加工单");
					SpringUtil.getApplicationContext().publishEvent(new EventForAddWorkOrderLog(workOrderLog) );
				}
				ajax("1");//上报成功
			}
		}catch(Exception e){
			e.printStackTrace();
			ajax("2");
		}
	}
	/**
	 * 删除上报
	 * @Title: delete   
	 */
	public void delete(){
		try{
			UserInfo userInfo=this.getLoginUser();
			if(userInfo!=null&&instanceId!=null){
				DefectApply defectApply=defectApplyImpl.findByInstanceId(instanceId);
				if(defectApply!=null){
					if(defectApply.getState().equals("0")||defectApply.getState().equals("6")){
						defectApplyImpl.delete(defectApply.getId(), false);
						ajax("1");
						return;
					}else{
						ajax("3");
						return;
					}
				}
			}
			ajax("0");
		}catch(Exception e){
			e.printStackTrace();
			ajax("2");
		}
	}
	/**
	 * 处理操作
	 * @Title: approve   
	 */
	public void approve(){
		try{
			UserInfo userInfo=this.getLoginUser();
			if(userInfo!=null){
				DefectApply defectApply=defectApplyImpl.findByInstanceId(instanceId);
				if("2".equals(taskName)){
					if("1".equals(defectApply.getState())){
						ajax("3");
						return;
					}
				}
				List<Integer> capitalIdList = new ArrayList<Integer>();
				if(defectApply.getType()!=0&&StringUtils.isNoneBlank(equipments)){
					Gson gson = new Gson();
					capitalIdList = gson.fromJson(equipments,new TypeToken<List<Integer>>(){}.getType());
				}
				if(capitalIdList!=null && capitalIdList.size()==0 && "3".equals(taskName)){
					List<UserCapital> uclist = userCapitalService.findUserCapitalListByInstanceId(instanceId);
					if(uclist!=null&&uclist.size()>0){
						for(UserCapital uc:uclist){
							Capital capital = uc.getCapital();
							if(capital!=null){
								capitalIdList.add(capital.getId());
							}
						}
					}
				}
				int res=defectApplyImpl.approve(userInfo.getUserId(), instanceId, approveResult, advice, taskName, nextUserId,processType,achievement,evaluate,capitalIdList,null);
				if(res==1){
					ajax("1");
				}else{
					ajax("0");
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			ajax("2");
		}
	}
	
	/**
	 * 再次评价
	 */
	public void updateEvaluate(){
		try{
			UserInfo userInfo=this.getLoginUser();
			if(userInfo!=null){
				DefectApply defectApply=defectApplyImpl.findByInstanceId(instanceId);
				defectApply.setAchievement(achievement);
				defectApply.setEvaluate(evaluate);
				defectApplyImpl.saveOrUpdate(defectApply);
				ajax("1");
			}
		}catch(Exception e){
			e.printStackTrace();
			ajax("2");
		}
	}
	
	
	public void returnCapital(){
		try {
			
			apply.setTurnNum(0);
			apply.setReminderNum(0);
			if(apply.getCreateUserId()!=null){
				String dispatchWorkNo = (String)this.getRequest().getSession().getAttribute("dispatchWorkNo");
				apply.setDispatchWorkNo(dispatchWorkNo);
			}
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 申请详情
	 * @Title: view   
	 */
	public void view(){
		try{
			UserInfo userInfo=this.getLoginUser();
			if(userInfo!=null){
				DefectApply apply=defectApplyImpl.findByInstanceId(instanceId);
				UserInfo applyUser = userService.findOne(apply.getCreateUserId());
				apply.setCreateUserName(applyUser!=null?applyUser.getUserName():"");
				apply.setCreateUserPhone(applyUser!=null?applyUser.getPhone():"");
				apply.setCreateUserWorkNo(applyUser!=null?applyUser.getWorkNo():"");
				if(apply.getCreateGroupId()!=null){
					GroupInfo applyGroup = groupService.findOne(apply.getCreateGroupId());
					apply.setDefectDepartmentVo(applyGroup!=null?applyGroup.getGroupName():"");
					apply.setCreateGroupName(applyGroup!=null?applyGroup.getGroupName():"");
				}else{
					GroupInfo applyGroup = groupService.findOne(applyUser!=null?applyUser.getGroupId():0);
					apply.setDefectDepartmentVo(applyGroup!=null?applyGroup.getGroupName():"");
					apply.setCreateGroupId(applyUser!=null?applyUser.getGroupId():null);
					apply.setCreateGroupName(applyGroup!=null?applyGroup.getGroupName():"");
				}
				/*String groupPath = "";
				if(applyGroup!=null){
					String path = applyGroup.getPath();
					String pathArr[] = path.split(",");
					for(String tempPath:pathArr){
						groupService.findOne(Integer.valueOf(tempPath));
					}
					
				}*/
				
				SimpleDateFormat sdfYYYYMMDDHHMM = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				apply.setDefectTimeStr(sdfYYYYMMDDHHMM.format(apply.getDefectTime()));
				apply.setCreateTimeStr(sdfYYYYMMDDHHMM.format(apply.getCreateTime()));
				Map<String,Object> resMap=new HashMap<String,Object>();
				if(apply!=null){
					Integer achievement=apply.getAchievement();
					if(achievement!=null){
						if(achievement==1){
							apply.setAchievementStr("非常满意");
						}else if(achievement==2){
							apply.setAchievementStr("满意");
						}else if(achievement==3){
							apply.setAchievementStr("一般");
						}else{
							apply.setAchievementStr("不满意");
						}
					}
					long hour = (apply.getDefectTime().getTime()-System.currentTimeMillis())/(1000*60*60);
					Integer outTimeState = 0;
					if(hour<0&&(!"3".equals(apply.getState())&&!"4".equals(apply.getState()))){
						hour = Math.abs(hour);
						outTimeState = 1;
					}
					if("3".equals(apply.getState())||"4".equals(apply.getState())){
						outTimeState = 2;
					}
					String timeStr = "";
					if(hour>24){
						timeStr = hour/24+"天";
					}else{
						timeStr = hour+"小时";
					}
					resMap.put("hour", timeStr);
					resMap.put("outTimeState", outTimeState);
					
					String processUserId = "";//处理人Id
					String processGroupName = "";//处理科室
					String processGroupPhone = "";//处理科室电话
					String processUserName = "";//处理人
					String processUserPhone = "";//处理人电话
					String processTime = "";
					String applyState=apply.getState();
					String state="";
					if(applyState.equals("0")||applyState.equals("6")){
						state="待接收";
					}else if(applyState.equals("1")||applyState.equals("7")||applyState.equals("10")){
						state="待处理";
						String delayState = "";
						if(apply.getIsDelay()!=null){
							if(apply.getIsDelay()==1){
								delayState = "(待延期)";
							}else if(apply.getIsDelay()==2){
								delayState = "(延期中)";
							}else if(apply.getIsDelay()==3){
								delayState = "(延期驳回)";
							}
						}
						apply.setDelayStateVo(delayState);
						UserInfo processUser = userService.findOne(apply.getProcessId());
						if(processUser!=null){
							processUserId = processUser.getUserId()+"";
							processUserName = processUser.getUserName();
							processUserPhone = processUser.getPhone();
							GroupInfo processUserGroup = groupService.findOne(processUser.getGroupId());
							if(processUserGroup!=null){
								processGroupName = processUserGroup.getGroupName();
								processGroupPhone = processUserGroup.getPhone();
							}
						}
					}else if(applyState.equals("2")||applyState.equals("8")){
						state="待验收";
					}else if(applyState.equals("3")){
						//state="待归档";
						state="已完结";
					}else if(applyState.equals("4")){
						state="已归档";
					}else if(applyState.equals("5")){
						state="待作废";
					}else if(applyState.equals("9")){
						state="已作废";
					}else if(applyState.equals("-1")){
						state="待调度";
					}
					apply.setStateVo(state);
					String grade=apply.getGrade();
					if("1".equals(grade)){
						apply.setGradeVo("紧急");
					}else{
						apply.setGradeVo("一般");
					}
					
					//添加设备记录
					if(apply.getType().intValue()!=0){
						List<UserCapital> ucList = userCapitalService.findUserCapitalListByInstanceId(instanceId);
						if(ucList!=null&&ucList.size()>0){
							List<Map<String,Object>> equipmentList = new ArrayList<Map<String,Object>>();
							for(UserCapital userCapital:ucList){
								Map<String,Object> equipmentMap = new HashMap<String, Object>();
								Capital capital = userCapital.getCapital();
								equipmentMap.put("id",capital.getId() );
								equipmentMap.put("name", capital.getName());
								equipmentMap.put("capitalNo", StringUtils.isNotBlank(capital.getCapitalNo())?capital.getCapitalNo():"");
								equipmentMap.put("capitalModel", StringUtils.isNotBlank(capital.getCapitalModel())?capital.getCapitalModel():"");
								equipmentMap.put("capitalType", 1);
								equipmentList.add(equipmentMap);
							}
							resMap.put("equipmentList", equipmentList);
						}
					}
					
					//处理历史
					List<MyProcessed> historyList= MyProcessImpl.findByInstanceId(instanceId);
					for(MyProcessed myProcessed:historyList){
						UserInfo user=userService.findOne(myProcessed.getProcesserId());
						String processStr = "";
						if(user!=null){
							myProcessed.setProcesserPhone(StringUtils.isNoneBlank(user.getPhone())?user.getPhone():"电话不详");
							GroupInfo groupInfo = groupService.findOne(user.getGroupId());
							processStr = (groupInfo!=null?groupInfo.getGroupName():"")+"    "+user.getUserName()+"("+user.getWorkNo()+")";
							myProcessed.setProcesserDealName(processStr);
						}
						/*if(myProcessed.getNextProcesserId()!=null){
							Integer nextProcessType = myProcessed.getNextProcessType();
							if(nextProcessType!=null&&nextProcessType.intValue()==2){
								GroupInfo nextGroup = groupService.findOne(myProcessed.getNextProcesserId());
								if(nextGroup!=null){
									myProcessed.setNextProcesserName(nextGroup.getGroupName());
									myProcessed.setNextProcesserPhone(nextGroup.getPhone());
								}
							}else{
								UserInfo nextUser=userService.findOne(myProcessed.getNextProcesserId());
								if(nextUser!=null){
									myProcessed.setNextProcesserName(nextUser.getUserName());
									myProcessed.setNextProcesserPhone(nextUser.getPhone());
								}
							}
							
						}*/
						if("3".equals(myProcessed.getTaskName())&&(applyState.equals("2")||applyState.equals("8"))){
							UserInfo processUser = userService.findOne(myProcessed.getProcesserId());
							if(processUser!=null){
								processUserId = processUser.getUserId()+"";
								processUserName = processUser.getUserName();
								processUserPhone = processUser.getPhone();
								processTime = sdfYYYYMMDDHHMM.format(myProcessed.getEndTime());
								GroupInfo processUserGroup = groupService.findOne(processUser.getGroupId());
								if(processUserGroup!=null){
									processGroupName = processUserGroup.getGroupName();
									processGroupPhone = processUserGroup.getPhone();
								}
							}
						}
						if("3".equals(myProcessed.getTaskName())&&applyState.equals("3")){
							UserInfo processUser = userService.findOne(myProcessed.getProcesserId());
							if(processUser!=null){
								processUserName = processUser.getUserName();
								processUserPhone = processUser.getPhone();
								processTime = sdfYYYYMMDDHHMM.format(myProcessed.getEndTime());
								GroupInfo processUserGroup = groupService.findOne(processUser.getGroupId());
								if(processUserGroup!=null){
									processGroupName = processUserGroup.getGroupName();
									processGroupPhone = processUserGroup.getPhone();
								}
							}
						}
					}
					apply.setHistoryList(historyList);
					resMap.put("apply", apply);
					resMap.put("processUserId", processUserId);
					resMap.put("processUserName", processUserName);
					resMap.put("processUserPhone", processUserPhone);
					resMap.put("processGroupName", processGroupName);
					resMap.put("processGroupPhone", processGroupPhone);
					resMap.put("processTime", processTime);
					if(StringUtils.isNoneBlank(apply.getDispatchWorkNo())){
						UserInfo dispatchUser = userService.findOne(" workNo=?",apply.getDispatchWorkNo());
						resMap.put("dispatchUserName",dispatchUser!=null?dispatchUser.getUserName():"--");
						resMap.put("dispatchWorkNo", apply.getDispatchWorkNo());
					}else{
						resMap.put("dispatchUserName","");
						resMap.put("dispatchWorkNo", "");
					}
					//系统配置
					if(systemConfig!=null){
						//String phone=systemConfig.getTurnPeople();
						//List<UserInfo> list=userService.findUsersByPhone(phone);
						//list!=null&&!list.isEmpty()?list.get(0):userInfo
						resMap.put("turnUser",userInfo );
						resMap.put("turnNum", systemConfig.getTurnNum());
						resMap.put("rejectNum", systemConfig.getRejectNum());
					}
					//待作废次数
					int count= defectApplyImpl.getUnApprove(userInfo.getUserId(),userInfo.getCompanyId(),apply.getInstanceId());
					if(count>=systemConfig.getRejectNum()){
						apply.setIsDraft(1);
					}
					
					/*if(StringUtils.isNotBlank(apply.getAudioIds())){
						List<Attachment> audoList= attachmentService.getAttachmentsByIds(apply.getAudioIds());
						resMap.put("audioList", audoList);
					}*/
					
					//上传图片
					if(StringUtils.isNotBlank(apply.getAttachmentIds())){
						List<Attachment> attList= attachmentService.getAttachmentsByIds(apply.getAttachmentIds());
						resMap.put("attList", attList);
					}
					
					//处理图片
					if(StringUtils.isNotBlank(apply.getCheckAttachmentIds())){
						List<Attachment> attList= attachmentService.getAttachmentsByIds(apply.getCheckAttachmentIds());
						resMap.put("checkAttList", attList);
					}
					
				}
				Gson gson=new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm").create();
				ajax(gson.toJson(resMap));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * 我的申请列表
	 * @Title: myStarted   
	 */
	public void myStarted(){
		try{
			UserInfo userInfo=this.getLoginUser();
			if(userInfo!=null){
				
				//如果userId为空，则是领导查看全部申请，不为空则是查看我的申请
				Sort sort=new Sort(Direction.DESC,"createTime");
				Pageable pageable=this.getPageable(sort);
				Integer userId=userInfo.getUserId();
				Map<String,Integer> peopleMap = null;
				if("view".equals(type)){
					List<RoleInfo> roleList =roleService.getRoleByUser(userId); //根据人员Id获取角色列表
					if(roleList!=null&&roleList.size()>0){
						for(RoleInfo roleInfo:roleList){
							if("service".equals(roleInfo.getRoleCode())){
								searchVo.setProcessGroupId(userInfo.getGroupId());
								break;
							}
						}
					}
					peopleMap = selectPeopleImpl.getPeopleMap(userInfo.getCompanyId());
					userId = null;
				}else if("allEvent".equals(type)){
					userId = null;
				}else if("delay".equals(type)){
					searchVo.setDelayUserId(userId+"");
					userId = null;
				}else if("delayView".equals(type)){
					searchVo.setIsDelay(1);
					userId = null;
				}
				if(isForkGroup!=null&&isForkGroup.intValue()==1){
					searchVo.setGroupId(userInfo.getGroupId());
					userId = null;
				}
				Page<DefectApply> page=defectApplyImpl.myStarted(pageable,userId ,searchVo);
				List<DefectApply> list=page.getContent();
				List<Map<String,Object>> mapList=new ArrayList<Map<String,Object>>();
				if(list!=null&&list.size()>0){
					Map<String,Integer> processFinishMap = defectApplyImpl.getProcessFinishMap(searchVo);
					SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm");
					Map<Integer,String> groupMap = groupService.findGroupMap(userInfo.getCompanyId(), 1);
					for(DefectApply apply:list){
						Map<String,Object> map=new HashMap<String,Object>();
						String instanceId=apply.getInstanceId()==null?"":apply.getInstanceId();
						String grade=apply.getGrade()==null?"":apply.getGrade();
						String describe=apply.getDescribe()==null?"":apply.getDescribe();
						String equipmentNumber=apply.getEquipmentNumber()==null?"":apply.getEquipmentNumber();
						String defectNumber=apply.getDefectNumber()==null?"":apply.getDefectNumber();
						String equipmentName=apply.getEquipmentName()==null?"":apply.getEquipmentName();
						/*String equipmentState=apply.getEquipmentState()==null?"":apply.getEquipmentState();
						 * String equipmentUnit=apply.getEquipmentUnit()==null?"":apply.getEquipmentUnit();
						String defectDepartment=apply.getDefectDepartment()==null?"":apply.getDefectDepartment();
						String defectProfessional=apply.getDefectProfessional()==null?"":apply.getDefectProfessional();*/
						Integer workType=apply.getType();
						map.put("type", workType);
						String defectTime="";
						if(apply.getDefectTime()!=null){
							defectTime=sdf.format(apply.getDefectTime());
						}
						String createTime="";
						if(apply.getCreateTime()!=null){
							createTime=sdf.format(apply.getCreateTime());
						}
						long hour = (apply.getDefectTime().getTime()-System.currentTimeMillis())/(1000*60*60);
						Integer outTimeState = 0;
						if(hour<0&&(!"3".equals(apply.getState())&&!"4".equals(apply.getState()))){
							hour = Math.abs(hour);
							outTimeState = 1;
						}
						if("3".equals(apply.getState())||"4".equals(apply.getState())){
							outTimeState = 2;
						}
						String timeStr = "";
						if(hour>24){
							timeStr = hour/24+"天";
						}else{
							timeStr = hour+"小时";
						}
						String updateTime="";
						if(apply.getUpdateTime()!=null){
							updateTime=sdf.format(apply.getUpdateTime());
						}
						String state="";//状态
						String processName=apply.getProcessName();//当前操作人
						String applyState=apply.getState();
						String delayState = "";
						if(applyState.equals("0")||applyState.equals("6")){
							state="待接收";
						}else if(applyState.equals("1")||applyState.equals("7")){
							state="待处理";
							if(apply.getIsDelay()!=null){
								if(apply.getIsDelay()==1){
									delayState = "(待延期)";
								}else if(apply.getIsDelay()==2){
									delayState = "(延期中)";
								}else if(apply.getIsDelay()==3){
									delayState = "(延期驳回)";
								}
							}
						}else if(applyState.equals("2")||applyState.equals("8")){
							state="待验收";
						}else if(applyState.equals("3")){
							//state="待归档";
							state="已完结";
						}else if(applyState.equals("4")){
							state="已归档";
						}else if(applyState.equals("5")){
							state="待作废";
						}else if(applyState.equals("9")){
							state="已作废";
						}else if(applyState.equals("-1")){
							state="待调度";
						}
						if("view".equals(type)){
							if(StringUtils.isNoneBlank(apply.getDispatchWorkNo())){
								UserInfo dispatchUser = userService.findOne(" workNo=?",apply.getDispatchWorkNo());
								map.put("dispatchUserName",dispatchUser!=null?dispatchUser.getUserName():"--");
							}else{
								map.put("dispatchUserName","--");
							}
							UserInfo creater = userService.findOne(apply.getCreateUserId());
							map.put("createUserName", creater!=null?creater.getUserName():"--");
							if(apply.getCreateGroupId()!=null){
								map.put("groupName", groupMap.containsKey(apply.getCreateGroupId())?groupMap.get(apply.getCreateGroupId()):"--");
							}else{
								map.put("groupName", creater!=null?(groupMap.containsKey(creater.getGroupId())?groupMap.get(creater.getGroupId()):"--"):"--");
							}
							
							if(peopleMap!=null&&peopleMap.containsKey(instanceId)){
								map.put("isSelectPeople", 1);
							}else{
								map.put("isSelectPeople", 0);
							}
							
						}
						
						Integer process_id = apply.getProcessId();
						String processGroupName = "--";
						String processUserName = "--";
						if(!"9".equals(applyState)){
							if(applyState.equals("2")||applyState.equals("8")){
								UserInfo processUser = userService.findOne(apply.getPreviousProcessId());
								processUserName = processUser!=null?processUser.getUserName():"--";
								processGroupName = processUser!=null?(groupMap.containsKey(processUser.getGroupId())?groupMap.get(processUser.getGroupId()):"--"):"--";
							}else if(applyState.equals("3")){
								Integer prevId = processFinishMap.get(apply.getInstanceId());
								if(prevId!=null){
									UserInfo processUser = userService.findOne(prevId);
									processUserName = processUser!=null?processUser.getUserName():"--";
									processGroupName = processUser!=null?(groupMap.containsKey(processUser.getGroupId())?groupMap.get(processUser.getGroupId()):"--"):"--";
								}
							}else{
								if(apply.getProcessType().intValue()==2){//部门操作
									processGroupName = groupMap.containsKey(process_id)?groupMap.get(process_id):"--";
								}else{
									UserInfo processUser = userService.findOne(process_id);
									processUserName = processUser!=null?processUser.getUserName():"--";
									processGroupName = processUser!=null?(groupMap.containsKey(processUser.getGroupId())?groupMap.get(processUser.getGroupId()):"--"):"--";
								}
							}
						}
						map.put("applyPhone", StringUtils.isNoneBlank(apply.getApplyPhone())?apply.getApplyPhone():"--");
						map.put("processGroupName", processGroupName);//当前处理部门
						map.put("processUserName", processUserName);//当前处理人
						map.put("delayState", delayState);
						map.put("outTimeState", outTimeState);
						map.put("instanceId", instanceId);
						map.put("grade", grade);
						map.put("describe", describe);
						map.put("defectTime", defectTime);
						map.put("updateTime", updateTime);
						map.put("createTime", createTime);
						map.put("hour", timeStr);
						map.put("stateInt", apply.getState());
						map.put("state", state);
						map.put("processName", processName);
						map.put("defectNumber", defectNumber);
						map.put("equipmentNumber", equipmentNumber);
						map.put("equipmentName", equipmentName);
						map.put("reminderNum", apply.getReminderNum());
						mapList.add(map);
					}
				}
				Map<String, Object> jsonMap = new HashMap<String, Object>();
				jsonMap.put("iTotalRecords", page.getTotalElements());
				jsonMap.put("iTotalDisplayRecords", page.getTotalElements());
				jsonMap.put("aaData", mapList);
				Gson gson = new Gson();
				ajax(gson.toJson(jsonMap));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void mydList(){
		try{
			UserInfo userInfo=this.getLoginUser();
			if(userInfo!=null){
				//如果userId为空，则是领导查看全部申请，不为空则是查看我的申请
				Sort sort=new Sort(Direction.DESC,"createTime");
				Pageable pageable=this.getPageable(sort);
				Map<String,Integer> peopleMap = null;
				
				if(searchVo!=null){
					if(StringUtils.isNoneBlank(searchVo.getStartTime())){
						searchVo.setStartTime(searchVo.getStartTime()+" 00:00:00");
					}
					if(StringUtils.isNoneBlank(searchVo.getEndTime())){
						searchVo.setEndTime(searchVo.getEndTime()+" 23:59:00");
					}
				}
				
				Page<DefectApply> page=defectApplyImpl.mydList(pageable,null ,searchVo);
				List<DefectApply> list=page.getContent();
				List<Map<String,Object>> mapList=new ArrayList<Map<String,Object>>();
				if(list!=null&&list.size()>0){
					Map<String,Integer> processFinishMap = defectApplyImpl.getProcessFinishMap(searchVo);
					SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm");
					Map<Integer,String> groupMap = groupService.findGroupMap(userInfo.getCompanyId(), 1);
					for(DefectApply apply:list){
						Map<String,Object> map=new HashMap<String,Object>();
						String instanceId=apply.getInstanceId()==null?"":apply.getInstanceId();
						String grade=apply.getGrade()==null?"":apply.getGrade();
						String describe=apply.getDescribe()==null?"":apply.getDescribe();
						String equipmentNumber=apply.getEquipmentNumber()==null?"":apply.getEquipmentNumber();
						String defectNumber=apply.getDefectNumber()==null?"":apply.getDefectNumber();
						String equipmentName=apply.getEquipmentName()==null?"":apply.getEquipmentName();
						Integer workType=apply.getType();
						map.put("type", workType);
						String defectTime="";
						if(apply.getDefectTime()!=null){
							defectTime=sdf.format(apply.getDefectTime());
						}
						String createTime="";
						if(apply.getCreateTime()!=null){
							createTime=sdf.format(apply.getCreateTime());
						}
						long hour = (apply.getDefectTime().getTime()-System.currentTimeMillis())/(1000*60*60);
						Integer outTimeState = 0;
						if(hour<0&&(!"3".equals(apply.getState())&&!"4".equals(apply.getState()))){
							hour = Math.abs(hour);
							outTimeState = 1;
						}
						if("3".equals(apply.getState())||"4".equals(apply.getState())){
							outTimeState = 2;
						}
						String timeStr = "";
						if(hour>24){
							timeStr = hour/24+"天";
						}else{
							timeStr = hour+"小时";
						}
						String updateTime="";
						if(apply.getUpdateTime()!=null){
							updateTime=sdf.format(apply.getUpdateTime());
						}
						String state="";//状态
						String processName=apply.getProcessName();//当前操作人
						String applyState=apply.getState();
						String delayState = "";
						if(applyState.equals("0")||applyState.equals("6")){
							state="待接收";
						}else if(applyState.equals("1")||applyState.equals("7")){
							state="待处理";
							if(apply.getIsDelay()!=null){
								if(apply.getIsDelay()==1){
									delayState = "(待延期)";
								}else if(apply.getIsDelay()==2){
									delayState = "(延期中)";
								}else if(apply.getIsDelay()==3){
									delayState = "(延期驳回)";
								}
							}
						}else if(applyState.equals("2")||applyState.equals("8")){
							state="待验收";
						}else if(applyState.equals("3")){
							//state="待归档";
							state="已完结";
						}else if(applyState.equals("4")){
							state="已归档";
						}else if(applyState.equals("5")){
							state="待作废";
						}else if(applyState.equals("9")){
							state="已作废";
						}else if(applyState.equals("-1")){
							state="待调度";
						}
						if(StringUtils.isNoneBlank(apply.getDispatchWorkNo())){
							UserInfo dispatchUser = userService.findOne(" workNo=?",apply.getDispatchWorkNo());
							map.put("dispatchUserName",dispatchUser!=null?dispatchUser.getUserName():"--");
						}else{
							map.put("dispatchUserName","--");
						}
						UserInfo creater = userService.findOne(apply.getCreateUserId());
						map.put("createUserName", creater!=null?creater.getUserName():"--");
						if(apply.getCreateGroupId()!=null){
							map.put("groupName", groupMap.containsKey(apply.getCreateGroupId())?groupMap.get(apply.getCreateGroupId()):"--");
						}else{
							map.put("groupName", creater!=null?(groupMap.containsKey(creater.getGroupId())?groupMap.get(creater.getGroupId()):"--"):"--");
						}
						
						if(peopleMap!=null&&peopleMap.containsKey(instanceId)){
							map.put("isSelectPeople", 1);
						}else{
							map.put("isSelectPeople", 0);
						}
							
						
						Integer process_id = apply.getProcessId();
						String processGroupName = "--";
						String processUserName = "--";
						if(!"9".equals(applyState)){
							if(applyState.equals("2")||applyState.equals("8")){
								UserInfo processUser = userService.findOne(apply.getPreviousProcessId());
								processUserName = processUser!=null?processUser.getUserName():"--";
								processGroupName = processUser!=null?(groupMap.containsKey(processUser.getGroupId())?groupMap.get(processUser.getGroupId()):"--"):"--";
							}else if(applyState.equals("3")){
								Integer prevId = processFinishMap.get(apply.getInstanceId());
								if(prevId!=null){
									UserInfo processUser = userService.findOne(prevId);
									processUserName = processUser!=null?processUser.getUserName():"--";
									processGroupName = processUser!=null?(groupMap.containsKey(processUser.getGroupId())?groupMap.get(processUser.getGroupId()):"--"):"--";
								}
							}else{
								if(apply.getProcessType().intValue()==2){//部门操作
									processGroupName = groupMap.containsKey(process_id)?groupMap.get(process_id):"--";
								}else{
									UserInfo processUser = userService.findOne(process_id);
									processUserName = processUser!=null?processUser.getUserName():"--";
									processGroupName = processUser!=null?(groupMap.containsKey(processUser.getGroupId())?groupMap.get(processUser.getGroupId()):"--"):"--";
								}
							}
						}
						map.put("applyPhone", StringUtils.isNoneBlank(apply.getApplyPhone())?apply.getApplyPhone():"--");
						map.put("processGroupName", processGroupName);//当前处理部门
						map.put("processUserName", processUserName);//当前处理人
						map.put("delayState", delayState);
						map.put("outTimeState", outTimeState);
						map.put("instanceId", instanceId);
						map.put("grade", grade);
						map.put("describe", describe);
						map.put("defectTime", defectTime);
						map.put("updateTime", updateTime);
						map.put("createTime", createTime);
						map.put("hour", timeStr);
						map.put("stateInt", apply.getState());
						map.put("state", state);
						map.put("processName", processName);
						map.put("defectNumber", defectNumber);
						map.put("equipmentNumber", equipmentNumber);
						map.put("equipmentName", equipmentName);
						map.put("reminderNum", apply.getReminderNum());
						mapList.add(map);
					}
				}
				Map<String, Object> jsonMap = new HashMap<String, Object>();
				jsonMap.put("iTotalRecords", page.getTotalElements());
				jsonMap.put("iTotalDisplayRecords", page.getTotalElements());
				jsonMap.put("aaData", mapList);
				Gson gson = new Gson();
				ajax(gson.toJson(jsonMap));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * 待我处理的
	 * @Title: myWaitProcess   
	 */
	public void myWaitProcess(){
		try{
			UserInfo userInfo=this.getLoginUser();
			if(userInfo!=null){
				if(num!=null){
					if(num==1){
						beginTime=DateUtils.date2ShortStr(new Date())+" 00:00:00";
						endTime=DateUtils.date2ShortStr(new Date())+" 23:59:59";
					}
					if(num==2){
						beginTime=DateUtils.addOrSubtractTime(7)+" 00:00:00";
						endTime=DateUtils.date2ShortStr(new Date())+" 23:59:59";
					}
					if(num==3){
						beginTime=beginTime+" 00:00:00";
						endTime=endTime+" 23:59:59";
					}
				}
				//如果userId为空，则是领导查看全部申请，不为空则是查看我的申请
				Sort sort=new Sort(Direction.DESC,"createTime");
				Pageable pageable=this.getPageable(sort);
				Integer userId = userInfo.getUserId();
				if("1".equals(isOut)){
					userId = null;
					searchVo.setIsOut(isOut);
				}
				searchVo.setGroupId(userInfo.getGroupId());
				Page<DefectApply> page=defectApplyImpl.myWaitProcess(pageable,userId,taskName,searchVo,beginTime,endTime);
				List<DefectApply> list=page.getContent();
				List<Map<String,Object>> mapList=new ArrayList<Map<String,Object>>();
				if(list!=null&&list.size()>0){
					//Map<String, Map<String, String>> mapDict= dictService.findAllDict();
					SimpleDateFormat sdf=new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
					Map<Integer,String> groupMap = groupService.findGroupMap(userInfo.getCompanyId(), 1);
					for(DefectApply apply:list){
						Map<String,Object> map=new HashMap<String,Object>();
						String instanceId=apply.getInstanceId()==null?"":apply.getInstanceId();
						String grade=apply.getGrade()==null?"":apply.getGrade();
						String describe=apply.getDescribe()==null?"":apply.getDescribe();
						String equipmentNumber=apply.getEquipmentNumber()==null?"":apply.getEquipmentNumber();
						String defectNumber=apply.getDefectNumber()==null?"":apply.getDefectNumber();
						//String equipmentState=apply.getEquipmentState()==null?"":apply.getEquipmentState();
						String equipmentName=apply.getEquipmentName()==null?"":apply.getEquipmentName();
						//String equipmentUnit=apply.getEquipmentUnit()==null?"":apply.getEquipmentUnit();
						//String defectDepartment=apply.getDefectDepartment()==null?"":apply.getDefectDepartment();
						//String defectProfessional=apply.getDefectProfessional()==null?"":apply.getDefectProfessional();
						String defectTime="";
						if(apply.getDefectTime()!=null){
							defectTime=sdf.format(apply.getDefectTime());
						}
						String createTime="";
						if(apply.getCreateTime()!=null){
							createTime=sdf.format(apply.getCreateTime());
						}
						long hour = (apply.getDefectTime().getTime()-System.currentTimeMillis())/(1000*60*60);
						Integer outTimeState = 0;
						if(hour<0&&(!"3".equals(apply.getState())&&!"4".equals(apply.getState()))){
							hour = Math.abs(hour);
							outTimeState = 1;
						}
						if("3".equals(apply.getState())||"4".equals(apply.getState())){
							outTimeState = 2;
						}
						String timeStr = "";
						if(hour>24){
							timeStr = hour/24+"天";
						}else{
							timeStr = hour+"小时";
						}
						String state="";//状态
						String processName=apply.getProcessName();//当前操作人
						String applyState=apply.getState();
						String delayState = "";
						if(applyState.equals("0")||applyState.equals("6")){
							state="待接收";
						}else if(applyState.equals("1")||applyState.equals("7")){
							state="待处理";
							if(apply.getIsDelay()!=null){
								if(apply.getIsDelay()==1){
									delayState = "(待延期)";
								}else if(apply.getIsDelay()==2){
									delayState = "(延期中)";
								}else if(apply.getIsDelay()==3){
									delayState = "(延期驳回)";
								}
							}
						}else if(applyState.equals("2")||applyState.equals("8")){
							state="待验收";
						}else if(applyState.equals("3")){
							//state="待归档";
							state="已完结";
						}else if(applyState.equals("4")){
							state="已归档";
						}else if(applyState.equals("5")){
							state="待作废";
						}else if(applyState.equals("9")){
							state="已作废";
						}else if(applyState.equals("-1")){
							state="待调度";
						}
						map.put("delayState", delayState);
						String updateTime="";
						if(apply.getUpdateTime()!=null){
							updateTime=sdf.format(apply.getUpdateTime());
						}
						UserInfo creater = userService.findOne(apply.getCreateUserId());
						map.put("createUserId", apply.getCreateUserId());
						map.put("createUserName", creater!=null?creater.getUserName():"--");
						if(apply.getCreateGroupId()!=null){
							map.put("createGroupName", groupMap.containsKey(apply.getCreateGroupId())?groupMap.get(apply.getCreateGroupId()):"--");
						}else{
							map.put("createGroupName", creater!=null?(groupMap.containsKey(creater.getGroupId())?groupMap.get(creater.getGroupId()):"--"):"--");
						}
						map.put("createGroupId", apply.getCreateGroupId());
						map.put("outTimeState", outTimeState);
						map.put("hour", timeStr);
						map.put("type", apply.getType());
						map.put("createTime", createTime);
						map.put("instanceId", instanceId);
						map.put("updateTime", updateTime);
						map.put("grade", grade);
						map.put("describe", describe);
						map.put("defectTime", defectTime);
						map.put("state", state);
						map.put("applyState", applyState);
						map.put("processName", processName);
						map.put("defectNumber", defectNumber);
						map.put("equipmentNumber", equipmentNumber);
						map.put("applyPhone", StringUtils.isNoneBlank(apply.getApplyPhone())?apply.getApplyPhone():"--");
						/*map.put("equipmentState", equipmentStateVo);*/
						map.put("equipmentName", equipmentName);
						/*map.put("equipmentUnit", equipmentUnitVo);
						map.put("defectDepartment", defectDepartmentVo);
						map.put("defectProfessional", defectProfessionalVo);*/
						map.put("reminderNum", apply.getReminderNum());
						mapList.add(map);
					}
				}
				Map<String, Object> jsonMap = new HashMap<String, Object>();
				jsonMap.put("iTotalRecords", page.getTotalElements());
				jsonMap.put("iTotalDisplayRecords", page.getTotalElements());
				jsonMap.put("aaData", mapList);
				Gson gson = new Gson();
				ajax(gson.toJson(jsonMap));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	/**
	 * 	
	 * @Title: myProcessed   
	 */
	public void myProcessed(){
		try{
			UserInfo userInfo=this.getLoginUser();
			if(userInfo!=null){
				//如果userId为空，则是领导查看全部申请，不为空则是查看我的申请
				Sort sort=new Sort(Direction.DESC,"createTime");
				Pageable pageable=this.getPageable(sort);
				Page<DefectApply> page=defectApplyImpl.myProcessed(pageable, userInfo.getUserId(),taskName,searchVo);
				List<DefectApply> list=page.getContent();
				List<Map<String,Object>> mapList=new ArrayList<Map<String,Object>>();
				if(list!=null&&list.size()>0){
					SimpleDateFormat sdf=new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
					Map<Integer,String> groupMap = groupService.findGroupMap(userInfo.getCompanyId(), 1);
					for(DefectApply apply:list){
						Map<String,Object> map=new HashMap<String,Object>();
						String instanceId=apply.getInstanceId()==null?"":apply.getInstanceId();
						String grade=apply.getGrade()==null?"":apply.getGrade();
						String describe=apply.getDescribe()==null?"":apply.getDescribe();
						String defectNumber=apply.getDefectNumber()==null?"":apply.getDefectNumber();
						String equipmentNumber=apply.getEquipmentNumber()==null?"":apply.getEquipmentNumber();
						String equipmentName=apply.getEquipmentName()==null?"":apply.getEquipmentName();
						String defectTime="";
						if(apply.getDefectTime()!=null){
							defectTime=sdf.format(apply.getDefectTime());
						}
						String createTime="";
						if(apply.getCreateTime()!=null){
							createTime=sdf.format(apply.getCreateTime());
						}
						
						long hour = (apply.getDefectTime().getTime()-System.currentTimeMillis())/(1000*60*60);
						Integer outTimeState = 0;
						if(hour<0&&(!"3".equals(apply.getState())&&!"4".equals(apply.getState()))){
							hour = Math.abs(hour);
							outTimeState = 1;
						}
						if("3".equals(apply.getState())||"4".equals(apply.getState())){
							outTimeState = 2;
						}
						String state="";//状态
						String processName=apply.getProcessName();//当前操作人
						String applyState=apply.getState();
						String delayState = "";
						if(applyState.equals("0")||applyState.equals("6")){
							state="待接收";
						}else if(applyState.equals("1")||applyState.equals("7")){
							state="待处理";
							if(apply.getIsDelay()!=null){
								if(apply.getIsDelay()==1){
									delayState = "(待延期)";
								}else if(apply.getIsDelay()==2){
									delayState = "(延期中)";
								}else if(apply.getIsDelay()==3){
									delayState = "(延期驳回)";
								}
							}
						}else if(applyState.equals("2")||applyState.equals("8")){
							state="待验收";
						}else if(applyState.equals("3")){
							//state="待归档";
							state="已完结";
						}else if(applyState.equals("4")){
							state="已归档";
						}else if(applyState.equals("5")){
							state="待作废";
						}else if(applyState.equals("9")){
							state="已作废";
						}else if(applyState.equals("-1")){
							state="待调度";
						}
						map.put("delayState", delayState);
						String equipmentStateVo="";
						
						String updateTime="";
						if(apply.getUpdateTime()!=null){
							updateTime=sdf.format(apply.getUpdateTime());
						}
						UserInfo creater = userService.findOne(apply.getCreateUserId());
						map.put("createUserName", creater!=null?creater.getUserName():"--");
						if(apply.getCreateGroupId()!=null){
							map.put("groupName", groupMap.containsKey(apply.getCreateGroupId())?groupMap.get(apply.getCreateGroupId()):"--");
						}else{
							map.put("groupName", creater!=null?(groupMap.containsKey(creater.getGroupId())?groupMap.get(creater.getGroupId()):"--"):"--");
						}
						map.put("hour", hour);
						map.put("outTimeState", outTimeState);
						map.put("type", apply.getType());
						map.put("updateTime", updateTime);
						map.put("instanceId", instanceId);
						map.put("grade", grade);
						map.put("describe", describe);
						map.put("defectTime", defectTime);
						map.put("createTime", createTime);
						map.put("state", state);
						map.put("applyState", applyState);
						map.put("processName", processName);
						map.put("defectNumber", defectNumber);
						map.put("equipmentNumber", equipmentNumber);
						map.put("equipmentState", equipmentStateVo);
						map.put("equipmentName", equipmentName);
						mapList.add(map);
					}
				}
				Map<String, Object> jsonMap = new HashMap<String, Object>();
				jsonMap.put("iTotalRecords", page.getTotalElements());
				jsonMap.put("iTotalDisplayRecords", page.getTotalElements());
				jsonMap.put("aaData", mapList);
				Gson gson = new Gson();
				ajax(gson.toJson(jsonMap));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	/**
	 * 待处理数量
	 * @Title: delete   
	 */
	public void getUnHandleCount(){
		try{
			UserInfo userInfo=this.getLoginUser();
			if(userInfo!=null){
				Integer isAdmin=0;
				if(num!=null){
					if(num==1){
						beginTime=DateUtils.date2ShortStr(new Date())+" 00:00:00";
						endTime=DateUtils.date2ShortStr(new Date())+" 23:59:59";
					}
					if(num==2){
						beginTime=DateUtils.addOrSubtractTime(7)+" 00:00:00";
						endTime=DateUtils.date2ShortStr(new Date())+" 23:59:59";
					}
					if(num==3){
						beginTime=beginTime+" 00:00:00";
						endTime=endTime+" 23:59:59";
					}
				}
				Map<String, Object> map=defectApplyImpl.getWaitHandle(userInfo.getGroupId(),userInfo.getUserId(), userInfo.getCompanyId(),beginTime,endTime);
				if(userInfo.getIsDefault()==0){
					isAdmin=1;
				}else{
					List<RoleInfo> list= roleService.findRolesByUserId(userInfo.getUserId(),1);
					if(list!=null&&list.size()>0){
						for(RoleInfo role:list){
							if(role.getRoleCode().equals("manager_role")){
								isAdmin=1;
								break;
							}
						}
					}
				}
				map.put("isAdmin", isAdmin);
				ajax(new Gson().toJson(map));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	/**
	 * 统计报表
	 * @Title: report   
	 */
	public void report(){
		try{
			UserInfo userInfo=this.getLoginUser();
			if(userInfo!=null){
				Map<String,Object> map=defectApplyImpl.getReport(searchVo.getStartTime(), searchVo.getEndTime(), searchVo.getEquipmentUnit(), searchVo.getDefectDepartment(),searchVo.getDefectProfessional());
				if(map!=null){
					Gson gson=new Gson();
					ajax(gson.toJson(map));
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * 功能：催单操作
	 * @return
	 */
	public String pushReminder(){
		try{
			UserInfo userInfo = this.getLoginUser();
			if(userInfo!=null){
				defectApplyImpl.pushReminder(userInfo,instanceId);
				ajax("0");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	
	/**
	 * 功能：获取申请人
	 * @return
	 */
	public String getApplyUser(){
		try{
			UserInfo currUserInfo = userService.findOne(userId);
			if(currUserInfo!=null){
				Gson gson = new Gson();
				ajax(gson.toJson(currUserInfo));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	
	public String waitProcessNum(){
		try{
			UserInfo userInfo = this.getLoginUser();
			int num = defectApplyImpl.waitProcessCount(userInfo.getUserId(), taskName);
			ajax(num);
		}catch(Exception e){
			ajax("");
		}
		return null;
	}
	
	
	/**
	 * 事务提醒 红点
	 * @Title: delete   
	 */
	public void getWaitProcessCount(){
		try{
			Map<String, Object> map = new HashMap<String, Object>();
			UserInfo user=this.getLoginUser();
			if(user!=null){
				map = defectApplyImpl.getWaitHandle(user.getGroupId(),user.getUserId(), user.getCompanyId(),null,null);
				int notifyUnReadCount = wapNotifyService.getNotifyUnReadCount(35, user.getUserId(), user.getCompanyId());
				map.put("notifyUnReadCount", notifyUnReadCount);
			}
			Gson gson=new Gson();
			ajax(gson.toJson(map));
		}catch(Exception e){
			e.printStackTrace();
			ajax("102||程序异常");
		}
	}
	
	/**
	 * 导出
	 * @Title: export   
	 */
	public void export(){
		HttpServletResponse response = this.getResponse();
        response.setContentType("application/vnd.ms-excel");
        OutputStream outp = null;
		try{
			UserInfo userInfo=this.getLoginUser();
			if(userInfo!=null){
				//如果userId为空，则是领导查看全部申请，不为空则是查看我的申请
				Sort sort=new Sort(Direction.DESC,"createTime");
				this.setIDisplayLength(999999999);
				Pageable pageable=this.getPageable(sort);
				Integer userId=userInfo.getUserId();
				if("view".equals(type)){
					userId=null;
				}else if("delay".equals(type)){
					searchVo.setDelayProcessId(userId+"");
					userId = null;
				}else if("delayView".equals(type)){
					searchVo.setIsDelay(1);
					userId = null;
				}
				if(StringUtils.isNoneBlank(searchVo.getSearchName())){
					searchVo.setSearchName(URLDecoder.decode(searchVo.getSearchName(),"utf-8"));
				}
				if(StringUtils.isNoneBlank(searchVo.getDispatchUserName())){
					searchVo.setDispatchUserName(URLDecoder.decode(searchVo.getDispatchUserName(),"utf-8"));
				}
				if(StringUtils.isNoneBlank(searchVo.getUserName())){
					searchVo.setUserName(URLDecoder.decode(searchVo.getUserName(),"utf-8"));
				}
				if(StringUtils.isNoneBlank(searchVo.getCreateGroupName())){
					searchVo.setCreateGroupName(URLDecoder.decode(searchVo.getCreateGroupName(),"utf-8"));
				}
				if(StringUtils.isNoneBlank(searchVo.getProcessUserName())){
					searchVo.setProcessUserName(URLDecoder.decode(searchVo.getProcessUserName(),"utf-8"));
				}
				if(StringUtils.isNoneBlank(searchVo.getProcessUserGroup())){
					searchVo.setProcessUserGroup(URLDecoder.decode(searchVo.getProcessUserGroup(),"utf-8"));
				}
				Page<DefectApply> page=defectApplyImpl.myStarted(pageable,userId ,searchVo);
				List<DefectApply> list=page.getContent();
				List<Map<String,Object>> mapList=new ArrayList<Map<String,Object>>();
				if(list!=null&&list.size()>0){
					Map<String,Integer> processFinishMap = defectApplyImpl.getProcessFinishMap(searchVo);
					Map<String,Map<String,Timestamp>> processTimeMap = defectApplyImpl.getProcessTimeMap(searchVo);
					SimpleDateFormat sdf=new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
					SimpleDateFormat sdfYYYYMMDDHHMMSS=new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");
					Map<Integer,String> groupMap = groupService.findGroupMap(userInfo.getCompanyId(), 1);
					for(DefectApply apply:list){
						Map<String,Object> map=new HashMap<String,Object>();
						String instanceId=apply.getInstanceId()==null?"":apply.getInstanceId();
						String grade=apply.getGrade()==null?"":apply.getGrade();
						String describe=apply.getDescribe()==null?"":apply.getDescribe();
						String equipmentNumber=apply.getEquipmentNumber()==null?"":apply.getEquipmentNumber();
						String defectNumber=apply.getDefectNumber()==null?"":apply.getDefectNumber();
						String equipmentName=apply.getEquipmentName()==null?"":apply.getEquipmentName();
						UserInfo createUser=userService.findOne(apply.getCreateUserId());
						String createName="";
						if(createUser!=null){
							createName=createUser.getUserName();
						}
						String createTime="";
						if(apply.getCreateTime()!=null){
							createTime=sdf.format(apply.getCreateTime());
						}
						String defectTime="";
						if(apply.getDefectTime()!=null){
							defectTime=sdf.format(apply.getDefectTime());
						}
						String state="";//状态
						String processName=apply.getProcessName();//当前操作人
						String applyState=apply.getState();
						String delayState = "";
						String dispatchTime = "--";
						String acceptTime = "--";
						String handleTime = "--";
						String approveTime = "--";
						Map<String,Timestamp> processMap = processTimeMap.get(instanceId);
						dispatchTime = processMap.get("0_4")!=null?sdfYYYYMMDDHHMMSS.format(processMap.get("0_4")):"--";
						acceptTime = processMap.get("2_1")!=null?sdfYYYYMMDDHHMMSS.format(processMap.get("2_1")):"--";
						handleTime = processMap.get("3_1")!=null?sdfYYYYMMDDHHMMSS.format(processMap.get("3_1")):"--";
						approveTime = processMap.get("4_1")!=null?sdfYYYYMMDDHHMMSS.format(processMap.get("4_1")):"--";
						if(applyState.equals("0")||applyState.equals("6")){
							state="待接收";
						}else if(applyState.equals("1")||applyState.equals("7")){
							state="待处理";
							if(apply.getIsDelay()!=null){
								if(apply.getIsDelay()==1){
									delayState = "(待延期)";
								}else if(apply.getIsDelay()==2){
									delayState = "(延期中)";
								}else if(apply.getIsDelay()==3){
									delayState = "(延期驳回)";
								}
							}
							
						}else if(applyState.equals("2")||applyState.equals("8")){
							state="待验收";
						}else if(applyState.equals("3")){
							//state="待归档";
							state="已完结";
						}else if(applyState.equals("4")){
							state="已归档";
						}else if(applyState.equals("5")){
							state="待作废";
						}else if(applyState.equals("9")){
							state="已作废";
						}else if(applyState.equals("-1")){
							state="待调度";
						}
						map.put("delayState", delayState);
						String updateTime="";
						if(apply.getUpdateTime()!=null){
							updateTime=sdf.format(apply.getUpdateTime());
						}
						
						Integer process_id = apply.getProcessId();
						String processGroupName = "--";
						String processUserName = "--";
						if(!"9".equals(applyState)){
							if(applyState.equals("2")||applyState.equals("8")){
								UserInfo processUser = userService.findOne(apply.getPreviousProcessId());
								processUserName = processUser!=null?processUser.getUserName():"--";
								processGroupName = processUser!=null?(groupMap.containsKey(processUser.getGroupId())?groupMap.get(processUser.getGroupId()):"--"):"--";
							}else if(applyState.equals("3")){
								Integer prevId = processFinishMap.get(apply.getInstanceId());
								if(prevId!=null){
									UserInfo processUser = userService.findOne(prevId);
									processUserName = processUser!=null?processUser.getUserName():"--";
									processGroupName = processUser!=null?(groupMap.containsKey(processUser.getGroupId())?groupMap.get(processUser.getGroupId()):"--"):"--";
								}
							}else{
								if(apply.getProcessType().intValue()==2){//部门操作
									processGroupName = groupMap.containsKey(process_id)?groupMap.get(process_id):"--";
								}else{
									UserInfo processUser = userService.findOne(process_id);
									processUserName = processUser!=null?processUser.getUserName():"--";
									processGroupName = processUser!=null?(groupMap.containsKey(processUser.getGroupId())?groupMap.get(processUser.getGroupId()):"--"):"--";
								}
							}
						}
						map.put("processGroupName", processGroupName);//当前处理部门
						map.put("processUserName", processUserName);//当前处理人
						map.put("delayState", state+delayState);
						Integer type = apply.getType();
						map.put("type", type.intValue()==1?"配送":(type.intValue()==2?"归还":"维修"));
						UserInfo creater = userService.findOne(apply.getCreateUserId());
						map.put("createUserName", creater!=null?creater.getUserName():"--");
						if(apply.getCreateGroupId()!=null){
							map.put("groupName", groupMap.containsKey(apply.getCreateGroupId())?groupMap.get(apply.getCreateGroupId()):"--");
						}else{
							map.put("groupName", creater!=null?(groupMap.containsKey(creater.getGroupId())?groupMap.get(creater.getGroupId()):"--"):"--");
						}
						
						if(StringUtils.isNoneBlank(apply.getDispatchWorkNo())){
							UserInfo dispatchUser = userService.findOne(" workNo=?",apply.getDispatchWorkNo());
							map.put("dispatchUserName",dispatchUser!=null?dispatchUser.getUserName():"--");
						}else{
							map.put("dispatchUserName","--");
						}
						map.put("applyPhone", StringUtils.isNoneBlank(apply.getApplyPhone())?apply.getApplyPhone():"--");
						map.put("updateTime", updateTime);
						map.put("instanceId", instanceId);
						map.put("grade", "1".equals(grade)?"紧急":"一般");
						map.put("describe", describe);
						map.put("createTime", createTime);
						map.put("defectTime", defectTime);
						map.put("state", state);
						map.put("processName", processName);
						map.put("defectNumber", defectNumber);
						map.put("equipmentNumber", equipmentNumber);
						map.put("createName", createName);
						map.put("equipmentName", equipmentName);
						map.put("dispatchTime", dispatchTime);
						map.put("acceptTime", acceptTime);
						map.put("handleTime", handleTime);
						map.put("approveTime", approveTime);
						mapList.add(map);
					}
				}
				String fileName = URLEncoder.encode("事件报表.xls", "UTF-8");
		        // 把联系人信息填充到map里面
		        response.addHeader("Content-Disposition",
		                "attachment;filename=" + fileName);// 解决中文
				outp = response.getOutputStream();
	            ExportExcel exportExcel = new ExportExcel(outp, getExportHeadList(), mapList, getExportKeyList());
	            exportExcel.exportWithSheetName("事件记录");
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(outp!=null){
				try {
					outp.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	private List<String> getExportHeadList(){
        List<String> headList = new ArrayList<String>();
        headList.add("事件编号");
        headList.add("工单类型");
        headList.add("处理事项");
        headList.add("紧急程度");
        headList.add("上报电话");
        headList.add("调度人");
        headList.add("上报人");
        headList.add("上报部门 ");
        headList.add("上报地点");
        headList.add("上报时间");
        headList.add("期望完成时间");
        headList.add("工单状态");
        headList.add("处理部门");
        headList.add("处理人");
        headList.add("调度时间");
        headList.add("接收时间");
        headList.add("处理时间");
        headList.add("验收时间");
        return headList;
    }
    
    private List<String> getExportKeyList(){
        List<String> headList = new ArrayList<String>();
        headList.add("defectNumber");
        headList.add("type");
        headList.add("equipmentName");
        headList.add("grade");
        headList.add("applyPhone");
        headList.add("dispatchUserName");
        headList.add("createUserName");
        headList.add("groupName");
        headList.add("describe");
        headList.add("createTime");
        headList.add("defectTime");
        headList.add("delayState");
        headList.add("processGroupName");
        headList.add("processUserName");
        headList.add("dispatchTime");
        headList.add("acceptTime");
        headList.add("handleTime");
        headList.add("approveTime");
        return headList;
    }
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public DefectApply getApply() {
		return apply;
	}
	public void setApply(DefectApply apply) {
		this.apply = apply;
	}
	public IDefectApply getDefectApplyImpl() {
		return defectApplyImpl;
	}
	public void setDefectApplyImpl(IDefectApply defectApplyImpl) {
		this.defectApplyImpl = defectApplyImpl;
	}
	public String getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	public String getApproveResult() {
		return approveResult;
	}
	public void setApproveResult(String approveResult) {
		this.approveResult = approveResult;
	}
	public String getAdvice() {
		return advice;
	}
	public void setAdvice(String advice) {
		this.advice = advice;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public Integer getNextUserId() {
		return nextUserId;
	}
	public void setNextUserId(Integer nextUserId) {
		this.nextUserId = nextUserId;
	}
	public SearchVo getSearchVo() {
		return searchVo;
	}
	public void setSearchVo(SearchVo searchVo) {
		this.searchVo = searchVo;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Integer getProcessType() {
		return processType;
	}
	public void setProcessType(Integer processType) {
		this.processType = processType;
	}
	public Integer getAchievement() {
		return achievement;
	}
	public void setAchievement(Integer achievement) {
		this.achievement = achievement;
	}
	public Integer getNum() {
		return num;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
	public String getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getEquipments() {
		return equipments;
	}
	public void setEquipments(String equipments) {
		this.equipments = equipments;
	}
	public String getIsOut() {
		return isOut;
	}
	public void setIsOut(String isOut) {
		this.isOut = isOut;
	}
	public Integer getIsForkGroup() {
		return isForkGroup;
	}
	public void setIsForkGroup(Integer isForkGroup) {
		this.isForkGroup = isForkGroup;
	}
	public String getEvaluate() {
		return evaluate;
	}
	public void setEvaluate(String evaluate) {
		this.evaluate = evaluate;
	}
	
	
}
