package cn.com.qytx.hemei.defect.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import cn.com.qytx.hemei.defect.domain.WorkOrderLog;
import cn.com.qytx.platform.base.dao.BaseDao;

@Repository("workOrderLogDao")
public class WorkOrderLogDao extends BaseDao<WorkOrderLog, Serializable> {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	
	public List<WorkOrderLog> findWorkOrderLogListByInstanceId(String instanceId){
		String hql = " instanceId=?";
		return super.findAll(hql, instanceId);
	}
}
