/**
 * 
 */
package cn.com.qytx.hemei.report.service;

import java.util.List;
import java.util.Map;

import cn.com.qytx.hemei.defect.domain.DefectApply;
import cn.com.qytx.platform.base.service.BaseService;

/**
 * 功能: 
 * 版本: 1.0
 * 开发人员: 
 * 创建日期: 2017年5月15日
 * 修改日期: 2017年5月15日
 * 修改列表: 
 */
public interface ISendReport extends BaseService<DefectApply>{

	/**
     * 查询部门工作量
     * @return
     */
	Map<Integer,String> findMap(String beginTime,String endTime,Integer type,String instanceIds);
	/**
	 * 员工工作量统计
	 * @return
	 */
	public List<Object[]> findStaff(String beginTime,String endTime,String userName,String departmentName,Integer type);
	/**
	  *医务科室上报统计
	  * @return
	  */
	public List<Object[]> findMedicalDepartmenList(String beginTime,String endTime,String departmentName,Integer type);
	/**
	 * 满意度
	 * @param beginTime
	 * @param endTime
	 * @param userName
	 * @param type
	 * @return
	 */
	List<Object[]> findSatisfaction(String beginTime,String endTime,String userName,Integer type);
	 /**
     * 查询后勤班组应接工量
     * @return
     */
	 public List<Object[]> findTakeWorkList(String beginTime,String endTime,Integer type);
}
