package cn.com.qytx.hemei.util;

public class SystemConfig {
	private String turnPeople;//最后转交人手机号
	private Integer turnNum;//转交次数
	private Integer rejectNum;//拒绝接收次数
	public String getTurnPeople() {
		return turnPeople;
	}
	public void setTurnPeople(String turnPeople) {
		this.turnPeople = turnPeople;
	}
	public Integer getTurnNum() {
		return turnNum;
	}
	public void setTurnNum(Integer turnNum) {
		this.turnNum = turnNum;
	}
	public Integer getRejectNum() {
		return rejectNum;
	}
	public void setRejectNum(Integer rejectNum) {
		this.rejectNum = rejectNum;
	}
	
}
