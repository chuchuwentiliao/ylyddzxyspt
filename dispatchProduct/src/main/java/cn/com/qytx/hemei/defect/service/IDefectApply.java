package cn.com.qytx.hemei.defect.service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import cn.com.qytx.hemei.defect.domain.DefectApply;
import cn.com.qytx.hemei.defect.domain.SearchVo;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;
import cn.com.qytx.platform.base.service.BaseService;
import cn.com.qytx.platform.org.domain.UserInfo;

/**
 * 消缺申请接口
 * @ClassName: IDefectApply   
 * @author: WANG
 * @Date: 2016年10月11日 上午9:39:31   
 *
 */
public interface IDefectApply extends BaseService<DefectApply> {
	
	/**
	 * 查询上报详情
	 * @Title: findByInstanceId   
	 * @param InstanceId
	 * @return
	 */
	public DefectApply findByInstanceId(String instanceId);
	
	/**
	 * 消缺申请
	 * @Title: apply   
	 * @param userId
	 * @param apply
	 * @return
	 */
	public DefectApply apply(Integer userId,DefectApply apply,List<Integer> capitalIdList);
	
	/**
	 * 申请处理
	 * @Title: approve   
	 * @param userId
	 * @param instanceId
	 * @param approveResult 0驳回 1通过 2转交
	 * @param advice
	 * @param taskName 1上报  2审批 3消缺 4验收 5归档
	 * @param nextUserId
	 * @return
	 */
	public int approve(Integer userId ,String instanceId,String approveResult,String advice,String taskName,Integer nextUserId,Integer processType,Integer achievement,String evaluate,List<Integer> capitalIdList,String attachmentIds);
	/**
	 * 我的申请
	 * @Title: myStarted   
	 * @param pageable
	 * @param userId
	 * @param searchVo 
	 * @param iDisplayStart
	 * @param iDisplayLength
	 * @return
	 */
	public Page<DefectApply> myStarted(Pageable pageable,Integer userId, SearchVo searchVo);
	/**
	 * 待我处理的
	 * @Title: myWaitProcess   
	 * @param pageable
	 * @param userId
	 * @param taskName
	 * @return
	 */
	public Page<DefectApply> myWaitProcess(Pageable pageable, Integer userId, String taskName, SearchVo searchVo,String beginTime,String endTime);
	/**
	 * 我已处理的
	 * @Title: myProcessed   
	 * @param pageable
	 * @param userId
	 * @param taskName
	 * @return
	 */
	public Page<DefectApply> myProcessed(Pageable pageable, Integer userId, String taskName, SearchVo searchVo);
	/**
	 * 报表
	 * @Title: getReport   
	 * @param startTime
	 * @param endTime
	 * @param equipmentUnit
	 * @param defectDepartment
	 * @return
	 */
	public Map<String,Object> getReport(String startTime,String endTime,String equipmentUnit,String defectDepartment,String defectProfessional);
	/**
	 * 请求作废次数
	 * @Title: getUnApprove   
	 * @param userId
	 * @param companyId
	 * @return
	 */
	public int getUnApprove(Integer userId, Integer companyId,String instanceId);
	/**
	 * 获取待处理的数量
	 * @Title: getWaitHandle   
	 * @param userId
	 * @param companyId
	 * @return
	 */
	public Map<String,Object> getWaitHandle(Integer groupId,Integer userId, Integer companyId,String beginTime,String endTime);
	
	
	public Map<String,Integer> getProcessFinishMap(SearchVo searchVo);
	
	public Map<String,Map<String,Timestamp>> getProcessTimeMap(SearchVo searchVo);
	
	/**
	 * 功能：获取当前登录人的待办数量
	 * @param userId
	 * @param taskName
	 * @return
	 */
	public int waitProcessCount(Integer userId,String taskName);
	
	public void pushReminder(UserInfo userInfo, String instanceId);
	
	public void pushReminderNew(String content, List<Integer> userIds);

	public Map<String, Object> getWaitHandle_All(Integer companyId);
	/**
	 * 功能：查询本部门 待验收 数量
	 * @param userId
	 * @param groupId
	 * @param companyId
	 * @return
	 */
	public Integer findWaitCheckNum(Integer userId,Integer groupId,Integer companyId);
	
	public Page<DefectApply> mydList(Pageable pageable, Integer userId, SearchVo searchVo);
}
