package cn.com.qytx.hemei.defect.service.impl;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;




import cn.com.qytx.cbb.capital.dao.CapitalDao;
import cn.com.qytx.cbb.capital.dao.CapitalUseLogDao;
import cn.com.qytx.cbb.capital.domain.Capital;
import cn.com.qytx.cbb.capital.domain.CapitalLog;
import cn.com.qytx.cbb.capital.domain.CapitalUseLog;
import cn.com.qytx.cbb.capital.event.EventForAddLog;
import cn.com.qytx.cbb.myapply.service.IMyProcessed;
import cn.com.qytx.cbb.myapply.service.IMyStarted;
import cn.com.qytx.cbb.myapply.service.IMyWaitProcess;
import cn.com.qytx.cbb.myapply.service.MyApplyService;
import cn.com.qytx.cbb.myapply.service.MyApplyService.ModuleCode;
import cn.com.qytx.cbb.myapply.service.MyApplyService.TaskName;
import cn.com.qytx.hemei.defect.dao.DefectApplyDao;
import cn.com.qytx.hemei.defect.dao.UserCapitalDao;
import cn.com.qytx.hemei.defect.domain.DefectApply;
import cn.com.qytx.hemei.defect.domain.SearchVo;
import cn.com.qytx.hemei.defect.domain.UserCapital;
import cn.com.qytx.hemei.defect.domain.WorkOrderLog;
import cn.com.qytx.hemei.defect.service.IDefectApply;
import cn.com.qytx.hemei.util.EventForAddWorkOrderLog;
import cn.com.qytx.hemei.util.WorkOrderType;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;
import cn.com.qytx.platform.base.service.impl.BaseServiceImpl;
import cn.com.qytx.platform.org.dao.UserDao;
import cn.com.qytx.platform.org.domain.GroupInfo;
import cn.com.qytx.platform.org.domain.UserInfo;
import cn.com.qytx.platform.org.service.IGroup;
import cn.com.qytx.platform.utils.spring.SpringUtil;

@Service
@Transactional
public class DefectApplyImpl extends BaseServiceImpl<DefectApply> implements IDefectApply {
	
	@Autowired
	private DefectApplyDao defectApplyDao;
	
	@Resource(name="capitalDao")
	private CapitalDao capitalDao;
	
	@Resource(name="userCapitalDao")
	private UserCapitalDao userCapitalDao;
	
	@Resource(name="capitalUseLogDao")
	private CapitalUseLogDao capitalUseLogDao;
	
	@Resource(name="userDao")
	private UserDao<UserInfo> userDao;
	@Resource
	private IGroup groupService;
	@Resource
	private MyApplyService myApplyService;
	@Resource
	private IMyStarted myStartedService;
	@Resource
	private IMyWaitProcess myWaitProcessService;
	@Resource
	private IMyProcessed myProcessedService;
	
	public DefectApply findByInstanceId(String instanceId) {
		return defectApplyDao.findByInstanceId(instanceId);
	}
	
	public DefectApply apply(Integer userId, DefectApply apply,List<Integer> capitalIdList) {
		UserInfo userInfo=userDao.findOne(userId);
		Integer processType = apply.getProcessType();
		if(userInfo!=null){
			//下一步申请人
			UserInfo nextUser=null;
			GroupInfo nextGroup=null;
			if(apply.getProcessId()!=null){
				if(processType.intValue()==2){
					nextGroup = groupService.findOne(apply.getProcessId());
				}else{
					nextUser=userDao.findOne(apply.getProcessId());
				}
			}
			if(nextUser!=null||nextGroup!=null){
				//生成实例ID
				String instanceId=getInstanceId(userId);
				//生成缺陷编号
				String defectNumber=getDefectNumber();
				apply.setInstanceId(instanceId);
				apply.setPreviousProcessId(userId);
				String taskName = "";
				String taskName2 = "";
				if(processType==2&&"1".equals(nextGroup.getExtension())){
					apply.setState("-1");//待调度
					taskName = TaskName.DISPATCH.getTaskName();
					taskName2 = TaskName.DISPATCH.getTaskName();
				}else{
					apply.setState("0");//待接收
					taskName = TaskName.APPROVE.getTaskName();
					taskName2 = TaskName.APPLY.getTaskName();
				}
				apply.setIsDelay(0);
				if(processType.intValue()==2){
					apply.setProcessName(nextGroup.getGroupName());
				}else{
					apply.setProcessName(nextUser.getUserName());
				}
				apply.setCompanyId(userInfo.getCompanyId());
				if(apply.getCreateUserId()!=null){
					userId = apply.getCreateUserId();
					userInfo = userDao.findOne(userId);
				}else{
					apply.setCreateUserId(userId);
				}
				apply.setUpdateUserId(userId);
				apply.setDefectNumber(defectNumber);
				apply=defectApplyDao.saveOrUpdate(apply);
				
				//构造设备与工单关联
				if(capitalIdList!=null&&capitalIdList.size()>0){
					UserInfo createUser = userDao.findOne(apply.getCreateUserId());
					/*List<UserCapital> uclist = userCapitalDao.findUserCapitalListByInstanceId(instanceId);
					if(uclist!=null&&uclist.size()>0){
						for(UserCapital uc:uclist){
							userCapitalDao.delete(uc, true);
						}
					}*/
					for(Integer capitalId:capitalIdList){
						UserCapital userCapital = new UserCapital();
						Capital capital = capitalDao.findOne(capitalId);
						userCapital.setCreateTime(new Timestamp(System.currentTimeMillis()));
						userCapital.setCreateUser(createUser);
						userCapital.setCapital(capital);
						userCapital.setDefectApply(apply);
						userCapital.setCompanyId(createUser.getCompanyId());
						if(apply.getType().intValue()==2){
							capital.setUseGroup(null);
							capital.setStoreLocation("5号楼1楼");
							capital.setStatus(1);
							capitalDao.saveOrUpdate(capital);
							/* 归还 直接停止使用 */
							CapitalUseLog cul = capitalUseLogDao.getUnEndUseCapitalUseLog(capital.getId());
							Timestamp currTs = new Timestamp(System.currentTimeMillis());
							if(cul!=null){//先停用 然后 重新添加使用中
								cul.setUseEndTime(currTs);
								Long useLongTime=(cul.getUseEndTime().getTime()-cul.getUseStartTime().getTime())/1000;
								cul.setUseLongTime(useLongTime.intValue());
								cul.setStopUser(createUser);
								capitalUseLogDao.saveOrUpdate(cul);
							}
							
							
							/*添加资产日志开始*/
							CapitalLog log = new CapitalLog();
							log.setCapitalGroup(capital.getCapitalGroup());//日志
							log.setCapitalName(capital.getName());
							log.setCapitalNo(capital.getCapitalNo());
							log.setCompanyId(createUser.getCompanyId());
							log.setCreateTime(new Timestamp(System.currentTimeMillis()));
							log.setCreateUser(createUser);
							log.setType(5);
							log.setRemark("设备已归还,停止使用,临时归库");
							SpringUtil.getApplicationContext().publishEvent(new EventForAddLog(log));//发布添加日志广播
						}
						userCapitalDao.saveOrUpdate(userCapital);
					}
				}
				
				String categoryName = "处理申请";
				String title="";
				//保存发起流程记录
				myApplyService.createProcess(userInfo, categoryName, title, instanceId, ModuleCode.DEFECT);
				//保存发起人的待审批记录
				myApplyService.addApprover(categoryName, title, instanceId, userId+"", ModuleCode.DEFECT,TaskName.DISPATCH.getTaskName(),1,apply.getType());
				
				//保存下一个人的审批记录
				myApplyService.approve(ModuleCode.DEFECT, instanceId, userInfo, "上报事件", "0","-1",taskName2,nextUser,nextGroup);
				myApplyService.addApprover(categoryName, title, instanceId, apply.getProcessId()+"", ModuleCode.DEFECT,taskName,processType,apply.getType());
				return apply;
			}
			
		}
		return null;
	}
	
	private String getDefectNumber() {
		String defectNumber=defectApplyDao.getDefectNumber();
		if(StringUtils.isNotBlank(defectNumber)){
			defectNumber=(Long.parseLong(defectNumber)+1)+"";
		}else{
			defectNumber=new SimpleDateFormat("yyyyMMdd").format(new Date())+"001";
		}
		return defectNumber;
	}
	
	
	/**
	 * 申请处理
	 * @Title: approve   
	 * @param userId
	 * @param instanceId
	 * @param approveResult 0驳回 1通过 2转交
	 * @param advice
	 * @param taskName 1上报  2审批 3消缺 4验收 5归档
	 * @param nextUserId
	 * @return
	 */
	public int approve(Integer userId, String instanceId, String approveResult, String advice, String taskName, Integer nextProcessId,Integer processType,Integer achievement,String evaluate,List<Integer> capitalIdList,String attachmentIds) {
		processType = (processType!=null?processType.intValue():1);
		//审批人
		UserInfo approver = userDao.findOne(userId);
		//待审批的实例
		DefectApply apply=defectApplyDao.findByInstanceId(instanceId);
		//构造设备与工单关联
		if(capitalIdList!=null&&capitalIdList.size()>0 && apply.getType().intValue()==1){
			UserInfo createUser = userDao.findOne(apply.getCreateUserId());
			List<UserCapital> uclist = userCapitalDao.findUserCapitalListByInstanceId(instanceId);
			if(uclist!=null&&uclist.size()>0){
				for(UserCapital uc:uclist){
					userCapitalDao.delete(uc, true);
				}
			}
			for(Integer capitalId:capitalIdList){
				UserCapital userCapital = new UserCapital();
				Capital capital = capitalDao.findOne(capitalId);
				userCapital.setCreateTime(new Timestamp(System.currentTimeMillis()));
				userCapital.setCreateUser(createUser);
				userCapital.setCapital(capital);
				userCapital.setDefectApply(apply);
				userCapital.setCompanyId(createUser.getCompanyId());
				if(apply.getType().intValue()==1){
					GroupInfo useGroup = groupService.findOne(apply.getCreateGroupId());
					capital.setUseGroup(useGroup);
					capital.setStatus(2);//状态变更为使用中
					capital.setStoreLocation(apply.getDescribe());
					capitalDao.saveOrUpdate(capital);
					CapitalUseLog cul = capitalUseLogDao.getUnEndUseCapitalUseLog(capital.getId());
					Timestamp currTs = new Timestamp(System.currentTimeMillis());
					if(cul!=null){//先停用 然后 重新添加使用中
						cul.setUseEndTime(currTs); 
						Long useLongTime=(cul.getUseEndTime().getTime()-cul.getUseStartTime().getTime())/1000;
						cul.setUseLongTime(useLongTime.intValue());
						cul.setStopUser(createUser);
						capitalUseLogDao.saveOrUpdate(cul);
						/*添加资产停用日志开始
						CapitalLog log = new CapitalLog();
						log.setCapitalGroup(capital.getCapitalGroup());//日志
						log.setCapitalName(capital.getName());
						log.setCapitalNo(capital.getCapitalNo());
						log.setCompanyId(createUser.getCompanyId());
						log.setCreateTime(new Timestamp(System.currentTimeMillis()));
						log.setCreateUser(createUser);
						log.setType(7);
						log.setRemark("停用成功");
						SpringUtil.getApplicationContext().publishEvent(new EventForAddLog(log));//发布添加日志广播
						*/
						CapitalUseLog culnew = new CapitalUseLog();
						culnew.setCompanyId(approver.getCompanyId());
						culnew.setUseStartTime(currTs);
						culnew.setUseGroup(useGroup);
						culnew.setCreateTime(currTs);
						culnew.setCreateUser(createUser);
						culnew.setIsDelete(0);
						culnew.setCapital(capital);
						capitalUseLogDao.saveOrUpdate(culnew);
					}else{
						cul = new CapitalUseLog();
						cul.setCompanyId(approver.getCompanyId());
						cul.setUseStartTime(currTs);
						cul.setUseGroup(useGroup);
						cul.setCreateTime(currTs);
						cul.setCreateUser(createUser);
						cul.setIsDelete(0);
						cul.setCapital(capital);
						capitalUseLogDao.saveOrUpdate(cul);
					}
					
					/*添加资产日志开始*/
					CapitalLog log = new CapitalLog();
					log.setCapitalGroup(capital.getCapitalGroup());//日志
					log.setCapitalName(capital.getName());
					log.setCapitalNo(capital.getCapitalNo());
					log.setCompanyId(createUser.getCompanyId());
					log.setCreateTime(new Timestamp(System.currentTimeMillis()));
					log.setCreateUser(createUser);
					log.setType(6);
					log.setRemark("配送完成,设备使用中");
					SpringUtil.getApplicationContext().publishEvent(new EventForAddLog(log));//发布添加日志广播
				}
				/*if(apply.getType().intValue()==2){
					capital.setUseGroup(null);
					capital.setStatus(1);
					capitalDao.saveOrUpdate(capital);
					添加资产日志开始
					CapitalLog log = new CapitalLog();
					log.setCapitalGroup(capital.getCapitalGroup());//日志
					log.setCapitalName(capital.getName());
					log.setCapitalNo(capital.getCapitalNo());
					log.setCompanyId(createUser.getCompanyId());
					log.setCreateTime(new Timestamp(System.currentTimeMillis()));
					log.setCreateUser(createUser);
					log.setType(5);
					log.setRemark("设备已取回,已入库");
					SpringUtil.getApplicationContext().publishEvent(new EventForAddLog(log));//发布添加日志广播
				}*/
				userCapitalDao.saveOrUpdate(userCapital);
			}
		}
		
		if(approver!=null&&apply!=null){
			UserInfo nextUser = null;
			GroupInfo nextGroup = null;
			/**
			 *  审批状态 -1 待调度
			 *  0申请上报（待审批）   1审批通过（待消缺）  2消缺完成（待验收）  3验收通过（待归档）  4归档完成
			 *  5审批驳回（待上报）  6消缺转交（待消缺）  7验收驳回（待消缺）  8归档驳回（待验收） 9上报作废
			 */
			String state = "";
			//下一步处理步骤
			String nextTaskName="";
			if(approveResult.equals("0")){
				if(taskName.equals("2")){
					//接收拒绝
					state="5";
				}else if(taskName.equals("4")){
					//验收驳回
					state="7";
					nextTaskName="3";
					apply.setRealDefectTime(null);
				}else if(taskName.equals("5")){
					//归档驳回
					state="8";
					nextTaskName="4";
				}else if(taskName.equals("1")){
					//拒绝作废
					state="0";
					nextTaskName="2";
				}
			}else if(approveResult.equals("1")){
			/*	if(taskName.equals("1")){
					state="0";
					nextTaskName="2";
				}else */
					
				if(taskName.equals("2")){
					state="1";//审批通过
					nextTaskName="3";
				}else if(taskName.equals("3")){
					state="2";//消缺通过
					nextTaskName="4";
				}else if(taskName.equals("4")){
					state="3";//验收通过
					nextTaskName="5";
					if(achievement!=null){
						apply.setAchievement(achievement);
					}
					apply.setEvaluate(evaluate);//意见评价
					apply.setRealDefectTime(new Timestamp(System.currentTimeMillis()));
				}else if(taskName.equals("5")){
					state="4";//归档通过
				}else if(taskName.equals("1")){
					state="9";//同意作废
				}
			}else if(approveResult.equals("2")){
				//只有消缺转交
				state="6";
				nextTaskName="2";
				int turnNum=apply.getTurnNum()==null?0:apply.getTurnNum();
				apply.setTurnNum(turnNum+1);
			}else if(approveResult.equals("3")){
				//处理延期
				if(taskName.equals("6")){
					apply.setDelayProcessId(nextProcessId);
					apply.setIsDelay(1);//申请延期 ,待延期
					apply.setDelayUserId(userId);
					nextTaskName = "7";
				}else if(taskName.equals("7")){
					apply.setIsDelay(2);//延期中
				}else if(taskName.equals("8")){
					apply.setIsDelay(3);//延期被驳回,回到原始状态
				}
				state = "1";
			}else if(approveResult.equals("4")){
				state="0";
				nextTaskName="2";
			}else if(approveResult.equals("5")){
				state="1";//审批通过
				nextTaskName="3";
			}
			//如果是验收或归档驳回或拒绝作废，查找上一步处理人
			if(approveResult.equals("0")&&(taskName.equals("4")||taskName.equals("5")||taskName.equals("1"))){
				if(taskName.equals("4")||taskName.equals("5")){
					nextProcessId=getPreviousProcessId(instanceId,taskName);
					if(nextProcessId==null){
						nextProcessId=apply.getPreviousProcessId();
					}
				}else{
					nextProcessId=apply.getPreviousProcessId();
				}
			}
			//查找下一步操作人
			if(nextProcessId!=null){
				if(processType==2){
					nextGroup = groupService.findOne(nextProcessId);
				}else{
					nextUser = userDao.findOne(nextProcessId);
				}
			}
			//添加处理记录
			if(taskName.equals("6")||taskName.equals("7")||taskName.equals("8")){
				myApplyService.approve(ModuleCode.DEFECTDELAY, instanceId, approver, advice, state,approveResult,taskName,nextUser,nextGroup);
			}else{
				myApplyService.approve(ModuleCode.DEFECT, instanceId, approver, advice, state,approveResult,taskName,nextUser,nextGroup);
			}
			apply.setState(state);
			apply.setPreviousProcessId(userId);
			apply.setUpdateTime(new Timestamp(System.currentTimeMillis()));
			if(processType==2){
				//如果处理通过，添加下一步审批人
				if(nextGroup!=null&&StringUtils.isNotBlank(nextTaskName)){
					if(nextTaskName.equals("7")){
						myApplyService.addApprover("处理延期申请", "", instanceId, nextProcessId+"", ModuleCode.DEFECTDELAY,nextTaskName,processType,apply.getType());
					}else{
						myApplyService.addApprover("处理申请", "", instanceId, nextProcessId+"", ModuleCode.DEFECT,nextTaskName,processType,apply.getType());
					}
					apply.setProcessId(nextGroup.getGroupId());
					apply.setProcessName(nextGroup.getGroupName());
					
				}
			}else{
				//如果处理通过，添加下一步审批人
				if(nextUser!=null&&StringUtils.isNotBlank(nextTaskName)){
					if(nextTaskName.equals("7")){
						myApplyService.addApprover("处理延期申请", "", instanceId, nextUser.getUserId()+"", ModuleCode.DEFECTDELAY,nextTaskName,processType,apply.getType());
					}else{
						myApplyService.addApprover("处理申请", "", instanceId, nextUser.getUserId()+"", ModuleCode.DEFECT,nextTaskName,processType,apply.getType());
					}
					apply.setProcessId(nextUser.getUserId());
					apply.setProcessName(nextUser.getUserName());
				}
			}
			apply.setProcessType(processType);
			if(StringUtils.isNoneBlank(attachmentIds)){
				apply.setCheckAttachmentIds(attachmentIds);
			}
			defectApplyDao.saveOrUpdate(apply);
			
			//addWorkOrderLog(approver, approveResult, taskName, processType, nextUser, nextGroup, advice, achievement, instanceId);
			
			return 1;
		}
		return 0;
	}
	private Integer getPreviousProcessId(String instanceId,String taskName){
		if(taskName.equals("5")){
			taskName="4";
		}else if(taskName.equals("4")){
			taskName="3";
		}
		return myApplyService.getPreviousProcessId(instanceId,taskName);
	}
	
	@SuppressWarnings("unused")
	private static void addWorkOrderLog(UserInfo currUser,String approveResult,String taskName,Integer processType,UserInfo nextUser,GroupInfo nextGroup,String advice,Integer achievement,String instanceId){
		String workOrderConent = "";
		Integer workOrderType = null;
		if("4".equals(approveResult)){
			workOrderConent = "调度工单，指派给"+nextGroup.getGroupName()+"部门接收，电话("+(StringUtils.isNoneBlank(nextGroup.getPhone())?nextGroup.getPhone():"--")+")";
			workOrderType = WorkOrderType.WORK_ORDER_DISPATCH;
		}
		if("1".equals(approveResult)&&"1".equals(taskName)){
			workOrderConent = "作废工单";
			workOrderType = WorkOrderType.WORK_ORDER_DRAFT;
		}
		if("1".equals(approveResult)&&"2".equals(taskName)){
			workOrderConent = "接收工单，指派给"+nextUser.getUserName()+"处理，电话("+nextUser.getPhone()+")";
			workOrderType = WorkOrderType.WORK_ORDER_ACCEPT;
		}
		if("2".equals(approveResult)&&"2".equals(taskName)){
			workOrderConent = "转交工单，转交给"+nextGroup.getGroupName()+"部门接收，电话("+(StringUtils.isNoneBlank(nextGroup.getPhone())?nextGroup.getPhone():"--")+")";
			workOrderType = WorkOrderType.WORK_ORDER_TURN;
		}
		if("1".equals(approveResult)&&"3".equals(taskName)){
			workOrderConent = "处理工单，指派给"+nextGroup.getGroupName()+"部门验收，电话("+(StringUtils.isNoneBlank(nextGroup.getPhone())?nextGroup.getPhone():"--")+")";
			workOrderType = WorkOrderType.WORK_ORDER_DEFECT;
		}
		if("5".equals(approveResult)&&"9".equals(taskName)){
			workOrderConent = "转交工单，指派给"+nextUser.getUserName()+"处理，电话("+nextUser.getPhone()+")";
			workOrderType = WorkOrderType.WORK_ORDER_TURN;
		}
		if("1".equals(approveResult)&&"4".equals(taskName)){
			String achievementStr = "";
			if(achievement==1){
				achievementStr = "非常满意";
			}else if(achievement==2){
				achievementStr = "满意";
			}else if(achievement==3){
				achievementStr = "一般";
			}else{
				achievementStr = "不满意";
			}
			workOrderConent = "验收工单，通过，评价："+achievementStr;
			workOrderType = WorkOrderType.WORK_ORDER_CHECK_ACCEPT;
		}
		if("0".equals(approveResult)&&"4".equals(taskName)){
			workOrderConent = "验收工单，驳回";
			workOrderType = WorkOrderType.WORK_ORDER_CHECK_ACCEPT;
		}
		if("3".equals(approveResult)&&"6".equals(taskName)){
			workOrderConent = "申请工单延期，指派给"+nextUser.getUserName()+"审批，电话("+(StringUtils.isNoneBlank(nextUser.getPhone())?nextUser.getPhone():"--")+")";
			workOrderType = WorkOrderType.WORK_ORDER_APPLY_DELAY;
		}
		if("3".equals(approveResult)&&"7".equals(taskName)){
			workOrderConent = "同意工单延期";
			workOrderType = WorkOrderType.WORK_ORDER_APPROVE_DELAY;
		}
		if("3".equals(approveResult)&&"8".equals(taskName)){
			workOrderConent = "拒绝工单延期";
			workOrderType = WorkOrderType.WORK_ORDER_APPROVE_DELAY;
		}
		workOrderConent += "，意见："+(StringUtils.isNoneBlank(advice)?advice:"无");
		WorkOrderLog workOrderLog = new WorkOrderLog();
		workOrderLog.setCompanyId(currUser.getCompanyId());
		workOrderLog.setOperUser(currUser);
		workOrderLog.setContent(workOrderConent);
		workOrderLog.setType(workOrderType);
		workOrderLog.setInstanceId(instanceId);
		SpringUtil.getApplicationContext().publishEvent(new EventForAddWorkOrderLog(workOrderLog) );
	}
	
	public Page<DefectApply> myStarted(Pageable pageable, Integer userId, SearchVo searchVo) {
		return defectApplyDao.myStarted(pageable,userId,searchVo);
	}
	public Page<DefectApply> myWaitProcess(Pageable pageable, Integer userId, String taskName, SearchVo searchVo,String beginTime,String endTime) {
		return defectApplyDao.myWaitProcess(pageable,userId,taskName,searchVo,beginTime,endTime);
	}
	public Page<DefectApply> myProcessed(Pageable pageable, Integer userId, String taskName, SearchVo searchVo) {
		return defectApplyDao.myProcessed(pageable,userId,taskName,searchVo);
	}
	public Page<DefectApply> mydList(Pageable pageable, Integer userId, SearchVo searchVo){
		return defectApplyDao.mydList(pageable,userId,searchVo);
	}
	public Map<String, Object> getReport(String startTime, String endTime, String equipmentUnit, String defectDepartment,String defectProfessional) {
		return defectApplyDao.getReport_new(startTime, endTime, equipmentUnit, defectDepartment,defectProfessional);
	}
	/**
	 * 生成实例ID
	 * @Title: getInstanceId   
	 * @return
	 */
	private String getInstanceId(Integer createUserId){
		SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
		return createUserId+"_"+sdf.format(new Date());
	}

	@Override
	public int getUnApprove(Integer userId, Integer companyId,String instanceId) {
		return defectApplyDao.getUnApprove(userId, companyId,instanceId);
	}

	@Override
	public Map<String, Object> getWaitHandle(Integer groupId,Integer userId, Integer companyId,String beginTime,String endTime) {
		return defectApplyDao.getWaitHandle(groupId,userId, companyId,beginTime,endTime);
	}

	@Override
	public Map<String, Map<String, Timestamp>> getProcessTimeMap(SearchVo searchVo) {
		Map<String,Map<String,Timestamp>> processMap = new HashMap<String, Map<String,Timestamp>>();
		List<Object[]> processList = defectApplyDao.getProcessTimeList(searchVo);
		if(processList!=null&&processList.size()>0){
			for(Object[] obj:processList){
				String instanceId = (String)obj[0];
				Timestamp endTime = (Timestamp)obj[2];
				String taskName = (String)obj[3];
				String approveResult = (String)obj[4];
				Map<String,Timestamp> taskMap = null;
				if(processMap.containsKey(instanceId)){
					taskMap = processMap.get(instanceId);
				}else{
					taskMap = new HashMap<String, Timestamp>();
				}
				taskMap.put(taskName+"_"+approveResult, endTime);
				processMap.put(instanceId, taskMap);
			}
		}
		return processMap;
	}

	@Override
	public Map<String, Integer> getProcessFinishMap(SearchVo searchVo) {
		Map<String,Integer> processMap = new HashMap<String, Integer>();
		List<Object[]> processList = defectApplyDao.getFinishProcessIdList(searchVo);
		if(processList!=null&&processList.size()>0){
			for(Object[] obj:processList){
				String instanceId = (String)obj[0];
				Integer processId = (Integer)obj[1];
				processMap.put(instanceId, processId);
			}
		}
		return processMap;
	}

	@Override
	public int waitProcessCount(Integer userId, String taskName) {
		return defectApplyDao.waitProcessCount(userId, taskName);
	}

	
	@Override
	public void pushReminder(UserInfo userInfo, String instanceId) {
		DefectApply apply = defectApplyDao.findByInstanceId(instanceId);
		apply.setReminderNum(apply.getReminderNum()+1);//催单次数+1
		//发送推送   -1 待调度  0 待派工 1 待处理 2 待验收
		defectApplyDao.saveOrUpdate(apply);
		myApplyService.pushReminder(apply.getState(),instanceId);
	}
	
	
	

	@Override
	public Map<String, Object> getWaitHandle_All(Integer companyId) {
		return defectApplyDao.getWaitHandle_All(companyId);
	}

	@Override
	public void pushReminderNew(String content, List<Integer> userIds) {
		myApplyService.pushReminderNew(content,userIds);
	}

	@Override
	public Integer findWaitCheckNum(Integer userId, Integer groupId,
			Integer companyId) {
		return defectApplyDao.findWaitCheckNum(userId, groupId, companyId);
	}

	

	
}
