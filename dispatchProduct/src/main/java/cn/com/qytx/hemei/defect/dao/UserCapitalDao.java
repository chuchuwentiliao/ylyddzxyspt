package cn.com.qytx.hemei.defect.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import cn.com.qytx.hemei.defect.domain.UserCapital;
import cn.com.qytx.platform.base.dao.BaseDao;

@Repository("userCapitalDao")
public class UserCapitalDao extends BaseDao<UserCapital, Serializable> {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	
	public List<UserCapital> findUserCapitalListByInstanceId(String instanceId){
		String hql = " defectApply.instanceId=?";
		return super.findAll(hql, instanceId);
	}
}
