package cn.com.qytx.hemei.util.proxy;

/**
 * 登录的结果
 * 
 * @author 严家俊
 *
 */
public class TokenResult
{
	/**
	 * 操作是否成功
	 */
	private boolean success;
	
	/**
	 * 单点登录的消息，如果异常，会在这里返回异常消息
	 */
	private String message;
	
	/**
	 * 错误代码
	 */
	private int errorCode;
	
	/**
	 * 登录用的URL地址
	 */
	private String loginUrl;
	
	/**
	 * 构造函数
	 * @param bSuccess
	 * @param tMessage
	 * @param iCode
	 * @param tUrl
	 */
	TokenResult(boolean bSuccess,String tMessage,int iCode,String tUrl)
	{
		this.success=bSuccess;
		this.message=tMessage;
		this.errorCode=iCode;
		this.loginUrl=tUrl;
	}

	public boolean isSuccess()
	{
		return success;
	}

	public String getMessage()
	{
		return message;
	}

	public int getErrorCode()
	{
		return errorCode;
	}

	public String getLoginUrl()
	{
		return loginUrl;
	}

	/**
	 * 内容显示
	 */
	@Override
	public String toString()
	{
		String tReturn="";
		if(this.success)
		{
			tReturn="令牌获取成功："+this.loginUrl;
		}
		else
		{
			tReturn="令牌获取失败！";
		}
		
		return tReturn;
	}
	
}
