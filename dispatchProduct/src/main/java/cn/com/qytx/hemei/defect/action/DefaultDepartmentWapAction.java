/**
 * 
 */
package cn.com.qytx.hemei.defect.action;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;

import cn.com.qytx.hemei.defect.domain.DefaultDepartment;
import cn.com.qytx.hemei.defect.service.IDefaultDepartment;
import cn.com.qytx.platform.base.action.BaseActionSupport;

/**
 * 功能: 
 * 版本: 1.0
 * 开发人员: 王刚
 * 创建日期: 2017年5月27日
 * 修改日期: 2017年5月27日
 * 修改列表: 
 */
public class DefaultDepartmentWapAction extends BaseActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6566540561703780566L;
	
	@Autowired
	private IDefaultDepartment defaultDepartmentService;
	
	//0維修  1 配送 2归还
	private Integer type;
	
	private Integer userId;
	
	/**
	 * 设置默认处理部门
	 */
	public void findDefaultDepartment(){
		try {
			if(userId!=null){
				Map<String,Object> map= new HashMap<String, Object>();
				if(type!=null){
					DefaultDepartment d=defaultDepartmentService.findModel(type);
					if(d!=null){
				    	map.put("groupId", d.getGroupId());
				    	map.put("groupName", d.getGroupName());
				    	map.put("id", d.getId());
				    }
					Gson gson=new Gson();
					ajax("100||"+gson.toJson(map));
				}else{
					ajax("101||参数缺少");
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			ajax("102||程序异常");
		}
		
	}

	
	
	
	
	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
   
	
	
}
