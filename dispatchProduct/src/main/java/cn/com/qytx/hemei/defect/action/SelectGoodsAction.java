package cn.com.qytx.hemei.defect.action;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import cn.com.qytx.cbb.dict.domain.Dict;
import cn.com.qytx.cbb.dict.service.IDict;
import cn.com.qytx.cbb.goods.domain.Good;
import cn.com.qytx.cbb.goods.service.IGood;
import cn.com.qytx.hemei.defect.domain.SelectGoods;
import cn.com.qytx.hemei.defect.domain.WorkOrderLog;
import cn.com.qytx.hemei.defect.service.ISelectGoods;
import cn.com.qytx.hemei.util.EventForAddWorkOrderLog;
import cn.com.qytx.hemei.util.WorkOrderType;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.org.domain.UserInfo;
import cn.com.qytx.platform.utils.spring.SpringUtil;

public class SelectGoodsAction extends BaseActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3819432110521482321L;
	
	private String instanceId;
	
	private Integer goodId;
	
	private Integer num;
	
	private Integer id;//选择物资的Id
	
	@Autowired
	private ISelectGoods selectGoodsService;
	
	@Autowired
	private IGood goodService;
	
	@Autowired
	private IDict dictService;
	
	/**
	 * 保存所选的物资
	 */
	public  void saveSelectGood(){
		UserInfo user =getLoginUser();
		try {
			if(user!=null){
				if(StringUtils.isNoneBlank(instanceId) && goodId!=null){
					String workOrderContent = "";
					Good good= goodService.findOne(goodId);
					SelectGoods selectGoods = selectGoodsService.findByGoodIdAndInstanceId(goodId, instanceId);
					float price=good.getPrice();
			    	DecimalFormat df = new DecimalFormat("0.00");
				    if(selectGoods!=null){//修改
				    	Integer count = selectGoods.getNum()+num;
				    	selectGoods.setMoney(df.format(price*count));
				    	selectGoods.setNum(count);
				    	selectGoods.setUpdateTime(new Timestamp(System.currentTimeMillis()));
				    	selectGoods.setUpdateUserId(user.getUserId());
				    	selectGoodsService.saveOrUpdate(selectGoods);
				    	workOrderContent = "修改物资，编号："+good.getGoodNo()+"，名称："+good.getName()+"，数量："+count;
				    }else{//新增
				    	selectGoods= new SelectGoods();
				    	selectGoods.setCompanyId(user.getCompanyId());
				    	selectGoods.setUpdateTime(new Timestamp(System.currentTimeMillis()));
				    	selectGoods.setUpdateUserId(user.getUserId());
				    	selectGoods.setCreateTime(new Timestamp(System.currentTimeMillis()));
				    	selectGoods.setCreateUserId(user.getUserId());
				    	selectGoods.setInstanceId(instanceId);
				    	selectGoods.setGood(good);
				    	selectGoods.setIsDelete(0);
				    	selectGoods.setNum(num);
				    	selectGoods.setMoney(df.format(price*num));
				    	selectGoodsService.saveOrUpdate(selectGoods);
				    	workOrderContent = "添加物资，编号："+good.getGoodNo()+",名称："+good.getName()+"，数量："+num;
				    }
				    ajax(0);//成功
				    /**
				     * 添加工单日志
				     */
				    WorkOrderLog workOrderLog = new WorkOrderLog();
					workOrderLog.setCompanyId(user.getCompanyId());
					workOrderLog.setOperUser(user);
					workOrderLog.setType(WorkOrderType.WORK_ORDER_GOODS);
					workOrderLog.setInstanceId(instanceId);
					workOrderLog.setContent(workOrderContent);
					SpringUtil.getApplicationContext().publishEvent(new EventForAddWorkOrderLog(workOrderLog) );
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			ajax(1);//异常
		}
		
	}

	public void findSelectGoodList(){
		UserInfo user =  getLoginUser();
		if(user!=null){
			try {
				List<SelectGoods> list = selectGoodsService.findList(instanceId);
				List<Map<String,Object>> listMap= new ArrayList<Map<String,Object>>();
				if(list!=null && list.size()>0){
					for(SelectGoods s:list){
						Map<String,Object> map = new HashMap<String, Object>();
						Good g= s.getGood();
						map.put("id", s.getId());
						map.put("name",g.getName());//物资名称
						map.put("groupName",g.getGoodGroup().getGroupName());
						map.put("goodNo",g.getGoodNo());
						map.put("type",getGoodType(g.getType()));
						map.put("price",g.getPrice());
						map.put("num",s.getNum());
						map.put("money", s.getMoney());
						listMap.add(map);
					}
				}
				ajax(listMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/*
	 * 获得物资类型	
	 */
	private  String getGoodType(Integer type){
		String typeStr="";
		if(type==null){
			typeStr=" ";
		}else if(type==1){
			typeStr="备件";
		}else if(type==2){
			typeStr="材料";
		}else if(type==3){
			typeStr="设备";
		}else if(type==4){
			typeStr="仪表";
		}else {
			typeStr="其他";
		}
		return typeStr;
	}
	/**
	 * 删除已选择的物资
	 */
	public void delectSelectGoods(){
		UserInfo user = getLoginUser();
		if(user!=null){
			try {
			SelectGoods selectGoods = selectGoodsService.findOne(id);
			Good good = selectGoods.getGood();
		    WorkOrderLog workOrderLog = new WorkOrderLog();
			workOrderLog.setCompanyId(user.getCompanyId());
			workOrderLog.setOperUser(user);
			workOrderLog.setType(WorkOrderType.WORK_ORDER_GOODS);
			workOrderLog.setInstanceId(instanceId);
			workOrderLog.setContent("删除物资，编号："+good.getGoodNo()+"，名称："+good.getName()+"，数量："+selectGoods.getNum());
			selectGoodsService.delete(id, true);
			/**
		     * 添加工单日志
		     */
			SpringUtil.getApplicationContext().publishEvent(new EventForAddWorkOrderLog(workOrderLog) );
			
			ajax(0);//成功
			} catch (Exception e) {
				e.printStackTrace();
			    ajax(1);
			}
		}
	}
	
	
	
	
	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public Integer getGoodId() {
		return goodId;
	}

	public void setGoodId(Integer goodId) {
		this.goodId = goodId;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
    
	
	
	
}
