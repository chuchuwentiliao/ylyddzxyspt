package cn.com.qytx.hemei.defect.domain;

public class SearchVo {
	
	private String equipmentState;
	private String equipmentUnit;
	private String defectDepartment;
	private String defectProfessional;
	private String searchName;
	private String startTime;
	private String endTime;
	private String state;
	
	private Integer timeType;
	
	/**
	 * 延期处理人ID
	 */
	private String delayProcessId;
	private String delayUserId;
	private Integer isDelay;
	private String delayState;
	
	/**
	 * 等级  
	 */
	private String grade;
	
	/**
	 * 工单类型 维修 配送 归还
	 */
	private Integer type;
	
	/**
	 * 上报人
	 */
	private String userName;
	
	/**
	 * 调度人
	 */
	private String dispatchUserName;
	
	private String createGroupName;
	
	private String processUserName;
	
	private String processUserGroup;
	
	private Integer myJoin;
	/**
	 *  部门Id
	 */
	private Integer groupId;
	
	private String isOut;//是否话务对接  1 是
	
	private Integer processGroupId;//处理科室
	
	public String getEquipmentState() {
		return equipmentState;
	}
	public void setEquipmentState(String equipmentState) {
		this.equipmentState = equipmentState;
	}
	public String getEquipmentUnit() {
		return equipmentUnit;
	}
	public void setEquipmentUnit(String equipmentUnit) {
		this.equipmentUnit = equipmentUnit;
	}
	public String getDefectDepartment() {
		return defectDepartment;
	}
	public void setDefectDepartment(String defectDepartment) {
		this.defectDepartment = defectDepartment;
	}
	public String getDefectProfessional() {
		return defectProfessional;
	}
	public void setDefectProfessional(String defectProfessional) {
		this.defectProfessional = defectProfessional;
	}
	public String getSearchName() {
		return searchName;
	}
	public void setSearchName(String searchName) {
		this.searchName = searchName;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getDelayProcessId() {
		return delayProcessId;
	}
	public void setDelayProcessId(String delayProcessId) {
		this.delayProcessId = delayProcessId;
	}
	public Integer getIsDelay() {
		return isDelay;
	}
	public void setIsDelay(Integer isDelay) {
		this.isDelay = isDelay;
	}
	public String getDelayState() {
		return delayState;
	}
	public void setDelayState(String delayState) {
		this.delayState = delayState;
	}
	public Integer getTimeType() {
		return timeType;
	}
	public void setTimeType(Integer timeType) {
		this.timeType = timeType;
	}
	public String getDelayUserId() {
		return delayUserId;
	}
	public void setDelayUserId(String delayUserId) {
		this.delayUserId = delayUserId;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getProcessUserName() {
		return processUserName;
	}
	public String getProcessUserGroup() {
		return processUserGroup;
	}
	public void setProcessUserName(String processUserName) {
		this.processUserName = processUserName;
	}
	public void setProcessUserGroup(String processUserGroup) {
		this.processUserGroup = processUserGroup;
	}
	public String getCreateGroupName() {
		return createGroupName;
	}
	public void setCreateGroupName(String createGroupName) {
		this.createGroupName = createGroupName;
	}
	public Integer getMyJoin() {
		return myJoin;
	}
	public void setMyJoin(Integer myJoin) {
		this.myJoin = myJoin;
	}
	public Integer getGroupId() {
		return groupId;
	}
	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}
	public String getDispatchUserName() {
		return dispatchUserName;
	}
	public void setDispatchUserName(String dispatchUserName) {
		this.dispatchUserName = dispatchUserName;
	}
	public String getIsOut() {
		return isOut;
	}
	public void setIsOut(String isOut) {
		this.isOut = isOut;
	}
	public Integer getProcessGroupId() {
		return processGroupId;
	}
	public void setProcessGroupId(Integer processGroupId) {
		this.processGroupId = processGroupId;
	}
	
	
	
}
