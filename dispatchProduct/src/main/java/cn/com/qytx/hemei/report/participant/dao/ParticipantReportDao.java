package cn.com.qytx.hemei.report.participant.dao;

import java.util.List;

import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import cn.com.qytx.hemei.defect.domain.SelectPeople;
import cn.com.qytx.platform.base.dao.BaseDao;

/**
 * 
 * @author dell
 * 参与人报表
 */
@Repository
public class ParticipantReportDao extends BaseDao<SelectPeople, Integer> {

	/**
	 * 获得参与人报表数据
	 * @return
	 */
	public List<Object[]> findParticipantReportList(String groupName,String userName,String beginTime,String endTime){
		String sql =" select SUM(CASE WHEN b.approveResult=1 and task_name=3 THEN 1 ELSE 0 END)completeNum,COUNT(DISTINCT b.instance_id) total,b.user_name userName,b.group_name groupName from (";
	    sql +=" select v.user_name,v.group_name, p.instance_id,m.approveResult,m.task_name  from tb_cbb_people_select p INNER JOIN tb_cbb_my_processed m";
	    sql+="  ON p.instance_id=m.instance_id  INNER JOIN View_repair_group v ON v.user_id=p.user_id  ";
	    sql+=" where  1=1 ";
	    if(StringUtils.isNoneBlank(beginTime)){
			sql+=" and m.end_time >='"+beginTime+" 00:00:00'";
		}
		if(StringUtils.isNoneBlank(endTime)){
			sql+=" and m.end_time <='"+endTime+" 23:59:59'";
		}
		sql+=" )b";
	    if(StringUtils.isNoneBlank(groupName)){
	            sql+=" and b.group_name like '%"+groupName+"%'";
	    }
	    if(StringUtils.isNoneBlank(userName)){
	    	   sql+=" and b.userName like '%"+userName+"%'";
		}
	    sql+="  GROUP BY b.user_name,b.group_name";
	    Query query = super.entityManager.createNativeQuery(sql);
	    return query.getResultList();
	}
	
}
