package cn.com.qytx.hemei.defect.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.com.qytx.hemei.defect.dao.SelectGoodsDao;
import cn.com.qytx.hemei.defect.dao.SelectPeopleDao;
import cn.com.qytx.hemei.defect.domain.SelectGoods;
import cn.com.qytx.hemei.defect.domain.SelectPeople;
import cn.com.qytx.hemei.defect.service.ISelectGoods;
import cn.com.qytx.hemei.defect.service.ISelectPeople;
import cn.com.qytx.platform.base.service.impl.BaseServiceImpl;

@Service
@Transactional
public class SelectPeopleImpl extends BaseServiceImpl<SelectPeople> implements ISelectPeople {

	@Autowired
	private SelectPeopleDao selectPeopleDao;
	
	@Override
	public SelectPeople findByPeopleIdAndInstanceId(Integer peopleId,
			String instanceId) {
		// TODO Auto-generated method stub
		return selectPeopleDao.findByPeopleIdAndInstanceId(peopleId, instanceId);
	}

	@Override
	public List<SelectPeople> findList(String instanceId) {
		// TODO Auto-generated method stub
		return selectPeopleDao.findList(instanceId);
	}

	@Override
	public Map<String, Integer> getPeopleMap(Integer companyId) {
		return selectPeopleDao.getPeopleMap(companyId);
	}

}
