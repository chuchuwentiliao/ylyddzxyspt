package cn.com.qytx.hemei.report.send.action;

import cn.com.qytx.cbb.org.util.ExportExcel;
import cn.com.qytx.hemei.report.service.ISendReport;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.org.domain.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.*;

/**
 * Created by lilipo on 2017/5/15.
 * 满意度
 */
public class SatisfactionAction extends BaseActionSupport {
    /**
	 * 
	 */
	private static final long serialVersionUID = -7346110650676165787L;


	@Autowired
    private ISendReport sendReportService;


    private String beginTime;//开始时间
    private String endTime;//结束时间
    private Integer type;// 1:配送 2:归还
    private String userName;
    /**
     * 满意度
     */
    public void findSatisfaction(){
        UserInfo user= getLoginUser();
        if(user!=null){
            try {
                List<Object[]> list = sendReportService.findSatisfaction(beginTime, endTime, userName,type);
                List<Map<String,Object>> listMap= getMapList(list);
                ajax(listMap);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * 封装数据
     * @param list
     * @return
     */
    public List<Map<String,Object>> getMapList(List<Object[]> list){
        List<Map<String,Object>> listMap= new ArrayList<Map<String,Object>>();

        if(list!=null && list.size()>0){
            int no=1;
            Integer fcmyNum=0;
            Integer myNum=0;
            Integer ybNum=0;
            Integer bmyNum=0;
            for(Object[] obj:list){
                Map<String,Object> map = new HashMap<String, Object>();
                map.put("no", no);
                map.put("name", obj[0].toString());
                map.put("groupName", obj[1].toString());
                map.put("fcmy", obj[2].toString());
                map.put("my", Integer.valueOf(obj[3].toString()));
                map.put("yb",Integer.valueOf(obj[4].toString()));
                map.put("bmy",Integer.valueOf(obj[5].toString()));
                map.put("userId", Integer.valueOf(obj[6].toString()));
                listMap.add(map);

                fcmyNum+=Integer.valueOf(obj[2].toString());
                myNum+=Integer.valueOf(obj[3].toString());
                ybNum+=Integer.valueOf(obj[4].toString());;
                bmyNum+=Integer.valueOf(obj[5].toString());;
                no++;
            }
            Map<String,Object> map = new HashMap<String, Object>();
            map.put("no", "合计");
            map.put("name", "");
            map.put("groupName", "");
            map.put("fcmy", fcmyNum);
            map.put("my", myNum);
            map.put("yb",ybNum);
            map.put("bmy",bmyNum);
            listMap.add(map);
        }
        return listMap;
    }

    /**
     * 获得图表数据
     */
    public void findStaffChart(){
        UserInfo user = getLoginUser();
        if(user!=null){
            List<Object[]> list = sendReportService.findSatisfaction(beginTime, endTime, userName,type);
            List<String> xname=new ArrayList<String>();
            List<Integer> fcmyList=new ArrayList<Integer>();
            List<Integer> myList=new ArrayList<Integer>();
            List<Integer> ybList=new ArrayList<Integer>();
            List<Integer> bmyList=new ArrayList<Integer>();
            String[] completeName={"满意","不满意"};
            if(list!=null&& list.size()>0){
                for(Object[] obj:list){
                    xname.add(obj[0].toString());
                    fcmyList.add(Integer.parseInt(obj[2].toString()));
                    myList.add(Integer.parseInt(obj[3].toString()));
                    ybList.add(Integer.parseInt(obj[4].toString()));
                    bmyList.add(Integer.parseInt(obj[5].toString()));
                }
            }

            String[] xnameArray= new String[xname.size()];
            xname.toArray(xnameArray);
            Integer[]fcmyArray= new Integer[fcmyList.size()];
            fcmyList.toArray(fcmyArray);
            Integer[]myArray= new Integer[myList.size()];
            myList.toArray(myArray);
            Integer[]ybArray= new Integer[ybList.size()];
            ybList.toArray(ybArray);
            Integer[]bmyArray= new Integer[bmyList.size()];
            bmyList.toArray(bmyArray);
            Map<String,Object> map =new HashMap<String, Object>();
            map.put("name",xname);
            map.put("completeName",completeName);
            map.put("fcmy", fcmyArray);
            map.put("my", myArray);
            map.put("yb", ybArray);
            map.put("bmy", bmyArray);
            ajax(map);
        }
    }

    /**
     * 导出人员工作量
     */
    public  void export(){
        HttpServletResponse response = this.getResponse();
        response.setContentType("application/vnd.ms-excel");
        OutputStream outp = null;
        try{
            UserInfo userInfo=this.getLoginUser();
            if(userInfo!=null){
                String excelName = "";
                if(type==1){
                    excelName = "员工配送满意度";
                }else if(type==2){
                    excelName = "员工归还满意度";
                }
                List<Object[]> list = sendReportService.findSatisfaction(beginTime, endTime, userName,type);
                List<Map<String,Object>> listMap= getMapList(list);
                String fileName = URLEncoder.encode(excelName+".xls", "UTF-8");
                // 把联系人信息填充到map里面
                response.addHeader("Content-Disposition",
                        "attachment;filename=" + fileName);// 解决中文
                outp = response.getOutputStream();
                ExportExcel exportExcel = new ExportExcel(outp, getExportHeadList(), listMap, getExportKeyList());
                exportExcel.exportWithSheetName(excelName);
            }
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            if(outp!=null){
                try {
                    outp.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private List<String> getExportHeadList(){
        List<String> headList = new ArrayList<String>();
        String  rowName = "";
        if(type==1){
            rowName = "配送";
        }else if(type==2){
            rowName = "归还";
        }
        headList.add("序号");
        headList.add("姓名");
        /*headList.add("非常满意");*/
        headList.add("满意");
        /*headList.add("一般");*/
        headList.add("不满意");
        return headList;
    }

    private List<String> getExportKeyList(){
        List<String> headList = new ArrayList<String>();
        headList.add("no");
        headList.add("name");
        /*headList.add("fcmy");*/
        headList.add("my");
        /*headList.add("yb");*/
        headList.add("bmy");
        return headList;
    }

    public String getBeginTime() {
        return beginTime;
    }
    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }
    public String getEndTime() {
        return endTime;
    }
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public Integer getType() {
        return type;
    }
    public void setType(Integer type) {
        this.type = type;
    }

}

