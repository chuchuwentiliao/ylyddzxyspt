/**
 * 
 */
package cn.com.qytx.hemei.event.domain;

import cn.com.qytx.hemei.util.excel.ExcelAnnotation;

/**
 * 功能: 
 * 版本: 1.0
 * 开发人员: 王刚
 * 创建日期: 2017年4月26日
 * 修改日期: 2017年4月26日
 * 修改列表: 
 */
public class EventImporVo {
	@ExcelAnnotation(exportName="事件标题")
	private String eventName;
	
	@ExcelAnnotation(exportName="部门名称")
	private String groupName;
	
	@ExcelAnnotation(exportName="紧急程度")
	private String urgencyLevel;
	
	@ExcelAnnotation(exportName="维修工时")
	private String workHour;

	private String importErrorRecord;
	
	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getUrgencyLevel() {
		return urgencyLevel;
	}

	public void setUrgencyLevel(String urgencyLevel) {
		this.urgencyLevel = urgencyLevel;
	}

	public String getWorkHour() {
		return workHour;
	}

	public void setWorkHour(String workHour) {
		this.workHour = workHour;
	}

	public String getImportErrorRecord() {
		return importErrorRecord;
	}

	public void setImportErrorRecord(String importErrorRecord) {
		this.importErrorRecord = importErrorRecord;
	}
	
	
	
}
