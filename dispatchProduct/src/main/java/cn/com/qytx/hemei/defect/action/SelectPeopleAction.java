package cn.com.qytx.hemei.defect.action;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import cn.com.qytx.hemei.defect.domain.SelectPeople;
import cn.com.qytx.hemei.defect.domain.WorkOrderLog;
import cn.com.qytx.hemei.defect.service.ISelectPeople;
import cn.com.qytx.hemei.util.EventForAddWorkOrderLog;
import cn.com.qytx.hemei.util.WorkOrderType;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.org.domain.GroupInfo;
import cn.com.qytx.platform.org.domain.UserInfo;
import cn.com.qytx.platform.org.service.IGroup;
import cn.com.qytx.platform.org.service.IUser;
import cn.com.qytx.platform.utils.spring.SpringUtil;

public class SelectPeopleAction extends BaseActionSupport {
	private static final long serialVersionUID = -3819432110521482321L;
	
	private String instanceId;
	
	private Integer id;
	
	@Autowired
	private ISelectPeople selectPeopleService;
	
	@Autowired
	private IGroup groupService;
	
	@Autowired
	private IUser userService;
	
	private String userIds;//选择人员Id串
	
	
	
	/**
	 * 保存参与人
	 */
	public  void saveSelectPeople(){
		UserInfo user =getLoginUser();
		try {
			if(user!=null){
				StringBuffer workOrderContent = new StringBuffer();
				List<SelectPeople> list = selectPeopleService.findList(instanceId);
				List<Integer> haveUserIdList = new ArrayList<Integer>();
				if(list!=null&&list.size()>0){
					for(SelectPeople sp:list){
						haveUserIdList.add(sp.getUserInfo().getUserId());
					}
				}
				workOrderContent.append("添加参与人");
				if(StringUtils.isNoneBlank(userIds)){
					workOrderContent.append(",参与人列表：");
					String userArr[] = userIds.split(",");
					for(String userIdStr:userArr){
						Integer userId = Integer.valueOf(userIdStr);
						if(!haveUserIdList.contains(userId)){
							SelectPeople sp = new SelectPeople();
							sp.setCompanyId(user.getCompanyId());
							sp.setCreateTime(new Timestamp(System.currentTimeMillis()));
							sp.setCreateUserId(user.getUserId());
							sp.setInstanceId(instanceId);
							UserInfo peopleUser = userService.findOne(userId);
							sp.setUserInfo(peopleUser);
							selectPeopleService.saveOrUpdate(sp);
							workOrderContent.append("[姓名："+peopleUser.getUserName()+"，工号："+peopleUser.getWorkNo()+"]");
						}
					}
					 /**
				     * 添加工单日志
				     */
				    WorkOrderLog workOrderLog = new WorkOrderLog();
					workOrderLog.setCompanyId(user.getCompanyId());
					workOrderLog.setOperUser(user);
					workOrderLog.setType(WorkOrderType.WORK_ORDER_PEOPLE);
					workOrderLog.setInstanceId(instanceId);
					workOrderLog.setContent(workOrderContent.toString());
					SpringUtil.getApplicationContext().publishEvent(new EventForAddWorkOrderLog(workOrderLog) );
				}
				
				
			}
			ajax("0");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public void findSelectPeopleList(){
		UserInfo user =  getLoginUser();
		if(user!=null){
			try {
				List<SelectPeople> list = selectPeopleService.findList(instanceId);
				List<Map<String,Object>> listMap= new ArrayList<Map<String,Object>>();
				if(list!=null && list.size()>0){
					List<GroupInfo> groupList = groupService.getGroupListByeExtension(user.getCompanyId(), 1,"1,2",null);
					Map<Integer,String> groupMap = new HashMap<Integer, String>();
					if(groupList!=null&& groupList.size()>0){
						for(GroupInfo gi:groupList){
							groupMap.put(gi.getGroupId(), gi.getGroupName());
						}
					}
					for(SelectPeople sp:list){
						Map<String,Object> map = new HashMap<String, Object>();
						UserInfo people = sp.getUserInfo();
						map.put("id", sp.getId());
						map.put("userName",StringUtils.isNoneBlank(people.getUserName())?people.getUserName():"--");//姓名
						map.put("workNo",StringUtils.isNoneBlank(people.getWorkNo())?people.getWorkNo():"--");//工号
						map.put("groupName",groupMap.containsKey(people.getGroupId())?groupMap.get(people.getGroupId()):"--");
						map.put("phone",StringUtils.isNoneBlank(people.getPhone())?people.getPhone():"--");
						map.put("peopleId",people.getUserId());
						listMap.add(map);
					}
				}
				ajax(listMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 删除已选择的参与人员
	 */
	public void deleteSelectPeople(){
		UserInfo user = getLoginUser();
		if(user!=null){
			try {
				SelectPeople sp = selectPeopleService.findOne(id);
				UserInfo peopleUser = sp.getUserInfo();
				selectPeopleService.delete(id,true);
				 /**
			     * 添加工单日志
			     */
			    WorkOrderLog workOrderLog = new WorkOrderLog();
				workOrderLog.setCompanyId(user.getCompanyId());
				workOrderLog.setOperUser(user);
				workOrderLog.setType(WorkOrderType.WORK_ORDER_PEOPLE);
				workOrderLog.setInstanceId(instanceId);
				workOrderLog.setContent("删除参与人，姓名："+peopleUser.getUserName()+"，工号："+peopleUser.getWorkNo());
				SpringUtil.getApplicationContext().publishEvent(new EventForAddWorkOrderLog(workOrderLog) );
				ajax(0);//成功
			} catch (Exception e) {
				e.printStackTrace();
			    ajax(1);
			}
		}
	}
	
	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserIds() {
		return userIds;
	}

	public void setUserIds(String userIds) {
		this.userIds = userIds;
	}
    
	
	
	
}
