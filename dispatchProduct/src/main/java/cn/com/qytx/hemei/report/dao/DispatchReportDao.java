package cn.com.qytx.hemei.report.dao;

import java.util.List;

import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import cn.com.qytx.hemei.defect.domain.DefectApply;
import cn.com.qytx.platform.base.dao.BaseDao;

@Repository
public class DispatchReportDao  extends BaseDao<DefectApply, Integer>{
    
	/**
	 *查询调度的任务
	 * @return
	 */
	public List<Object[]> findList(String beginTime,String endTime,String employeeName){
		String sql="SELECT g.user_name,SUM(CASE	WHEN e.apply_phone != '' THEN 1 ELSE 0 END) phoneReceive,";
		 sql+=" SUM (CASE WHEN e.apply_phone = '' THEN 1 ELSE 0 END)receive FROM View_repair_group g";
		 sql+=" LEFT JOIN View_repair_event e ON g.user_id = e.processer_id AND task_name = 0 AND approveResult = 4";
		 if(StringUtils.isNoneBlank(beginTime)){
			 sql+=" and e.end_time >='"+beginTime+" 00:00:00'";
			}
		if(StringUtils.isNoneBlank(endTime)){
			sql+=" and e.end_time <='"+endTime+" 23:59:59'";
		}
		 sql+=" WHERE g.extension = 1 ";
		 
		 if(StringUtils.isNoneBlank(employeeName)){
		   sql+=" and g.user_name like '%"+employeeName+"%'";
		}
		 sql+=" GROUP BY g.user_name";
		 Query query = super.entityManager.createNativeQuery(sql);
		return query.getResultList();
	}
}
