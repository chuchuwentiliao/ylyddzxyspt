package cn.com.qytx.hemei.report.equipment.impl.service;

import java.util.List;
import java.util.Map;

import cn.com.qytx.cbb.capital.domain.Capital;
import cn.com.qytx.platform.base.service.BaseService;

public interface IEquipment extends BaseService<Capital> {
   

	/**
	 * 获得参与人报表数据
	 * @return
	 */
	public List<Object[]> findReturnEquipmentList(String groupName,String userName,String beginTime,String endTime,Integer type);
	/**
	 * 设备使用科室使用情况
	 * @return
	 */
	public List<Object[]> findUseEquipmentList(String beginTime,String endTime);
	
	public Map<String,Integer> findEquipmentNum(String beginTime,String endTime);
	
}
