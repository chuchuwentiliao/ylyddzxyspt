package cn.com.qytx.hemei.event.action;

import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;

import cn.com.qytx.cbb.org.util.TreeNode;
import cn.com.qytx.hemei.event.domain.Event;
import cn.com.qytx.hemei.event.domain.EventType;
import cn.com.qytx.hemei.event.service.IEvent;
import cn.com.qytx.hemei.event.service.IEventType;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.org.domain.UserInfo;

/**
 * 功能：事件类型维护
 * 版本：1.0
 * 开发人员: pb
 * 创建日期：2017年4月19日
 * 修改日期：2017年4月19日	
 */
public class EventTypeAction extends BaseActionSupport{
	private static final long serialVersionUID = -7588771189909689456L;
	
	
	/**
	 * 业务接口
	 */
	@Resource(name = "eventTypeService")
	private IEventType eventTypeService;
	@Autowired
	private IEvent eventService;
	
	/**
	 * 1 代表左侧树 2 代表 下拉树
	 */
	private Integer treeType;

	private EventType eventType; 
		
	/**
	 * 获取资源组树列表
	 */
	public void eventTypeTreeList() {
		try {
			UserInfo user = this.getLoginUser();
			if (user != null) {
				String contextPath = getRequest().getContextPath();
				List<TreeNode> list = null;
				if(treeType!=null&&treeType.intValue()==1){
					list = eventTypeService
							.getTreeEventTypeList(contextPath,user.getCompanyId());
				}else{
					list = eventTypeService
							.getTreeEventTypeList_Select(contextPath,user.getCompanyId());
				}
				Gson json = new Gson();
				ajax(json.toJson(list));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 事件类型信息修改或新增事件类型 事件类型信息修改 成功：1； 事件类型添加成功：0；
	 */
	public String saveOrUpdateEventType() {
		try {
			UserInfo user = this.getLoginUser();
			if (user != null) {
				if (eventType.getId() != null) {//修改
					EventType oldEventType = eventTypeService.findOne(eventType.getId());
					if(oldEventType!=null){
						String oldGroupName = oldEventType.getTypeName();
						//判断是否存在事件类型名称
				    	if(!oldGroupName.equals(eventType.getTypeName())){
				    		boolean  isSame=eventTypeService.isHasSameEventTypeName(oldEventType.getParentId(), eventType.getTypeName(),user.getCompanyId());
				        	if(isSame){
				                ajax("2");
				                return null;
				        	}
				    	}
				    	oldEventType.setTypeName(eventType.getTypeName());
				    	oldEventType.setOrderIndex(eventType.getOrderIndex());
				    	oldEventType.setUpdateTime(new Timestamp(System.currentTimeMillis()));
				    	oldEventType.setUpdateUserId(user.getUserId());
						eventTypeService.saveOrUpdate(oldEventType);
						ajax("1");
					}
				} else {
						Integer parentId = 0;
						if(eventType.getParentId()!=null){
							parentId = eventType.getParentId();
						}
						//判断是否存在事件类型名称
						boolean isExistGroupName = eventTypeService.isHasSameEventTypeName(parentId, eventType.getTypeName(), user.getCompanyId());
						if(isExistGroupName){
							ajax("2");
							return null;
						}
						eventType.setCompanyId(user.getCompanyId());
						eventType.setCreateTime(new Timestamp(System.currentTimeMillis()));
						eventType.setCreateUserId(user.getUserId());
						eventType.setIsDelete(0);
						eventType.setUpdateTime(new Timestamp(System.currentTimeMillis()));
						eventType.setUpdateUserId(user.getUserId());
						eventTypeService.saveOrUpdate(eventType);
						EventType parentEventType = eventTypeService.findOne(parentId);
						if(parentEventType!=null){
							//设置路径
			    			String pathIdParent = parentEventType.getPath();
			    			if(StringUtils.isNotBlank(pathIdParent)){
			    				eventType.setPath(pathIdParent+eventType.getId()+",");
			    			}
			            	//设置级别
			    			Integer gradeParent=parentEventType.getGrade();
			    			if(gradeParent!=null){
			    				eventType.setGrade(gradeParent+1);
			    			}
						}else{
							eventType.setPath(","+String.valueOf(eventType.getId())+",");
							eventType.setGrade(1);
						}
						eventTypeService.saveOrUpdate(eventType);
						ajax("0_"+eventType.getId());// 添加成功
					}
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
		return null;
	}
	
	
	/**
	 * 功能：删除事件类型
	 * @return
	 */
	public String deleteEventType(){
		UserInfo userInfo = this.getLoginUser();
		try{
			if(userInfo!=null){
				boolean isHasChild = eventTypeService.isHasChildEventType(eventType.getId(),userInfo.getCompanyId());
				if(isHasChild){
					ajax("1");
					return null;
				}
				Event event = eventService.findModel(eventType.getId());
				if(event!=null){
					ajax("2");
					return null;
				}
				
				eventTypeService.delete(eventType.getId(), false);
				ajax("0");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
    

	
	public Integer getTreeType() {
		return treeType;
	}

	public void setTreeType(Integer treeType) {
		this.treeType = treeType;
	}

	public EventType getEventType() {
		return eventType;
	}

	public void setEventType(EventType eventType) {
		this.eventType = eventType;
	}

}
