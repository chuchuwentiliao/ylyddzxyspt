package cn.com.qytx.hemei.cardispatch.service.impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.com.qytx.hemei.cardispatch.dao.CarDao;
import cn.com.qytx.hemei.cardispatch.domain.Car;
import cn.com.qytx.hemei.cardispatch.domain.CarVo;
import cn.com.qytx.hemei.cardispatch.service.ICar;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;
import cn.com.qytx.platform.base.service.impl.BaseServiceImpl;
@Service
@Transactional
public class CarImpl extends BaseServiceImpl<Car> implements ICar {
	
	@Autowired
	private CarDao carDao; 
	@Override
	public Page<Car> findCarList(Pageable pageable, Integer companyId,Integer carType) {
		// TODO Auto-generated method stub
		return carDao.findCarList(pageable, companyId,carType);
	}
	@Override
	public void saveOrUpdateCar(Car car) {
		 carDao.saveOrUpdateCar(car);
	}
	@Override
	public Page<Car> findPageByVo(Pageable pageable, CarVo carVo) {
		return carDao.findPageByVo(pageable,carVo);
	}
	@Override
	public Car findByCarNo(String carNo) {
		return carDao.findByCarNo(carNo);
	}
	@Override
	public int delete(Integer companyId, String ids) {
		try {
			carDao.delete(companyId, ids);
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
            return 0;
		}
	}

}
