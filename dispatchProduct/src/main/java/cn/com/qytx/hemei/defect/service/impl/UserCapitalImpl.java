package cn.com.qytx.hemei.defect.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.com.qytx.hemei.defect.dao.UserCapitalDao;
import cn.com.qytx.hemei.defect.domain.UserCapital;
import cn.com.qytx.hemei.defect.service.IUserCapital;
import cn.com.qytx.platform.base.service.impl.BaseServiceImpl;

@Service("userCapitalService")
@Transactional
public class UserCapitalImpl extends BaseServiceImpl<UserCapital> implements IUserCapital {

	@Resource(name="userCapitalDao")
	private UserCapitalDao userCapitalDao;
	
	@Override
	public List<UserCapital> findUserCapitalListByInstanceId(String instanceId) {
		return userCapitalDao.findUserCapitalListByInstanceId(instanceId);
	}
	

	

	
}
