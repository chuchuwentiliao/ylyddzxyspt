package cn.com.qytx.hemei.event.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.com.qytx.cbb.org.util.TreeNode;
import cn.com.qytx.hemei.event.dao.EventTypeDao;
import cn.com.qytx.hemei.event.domain.EventType;
import cn.com.qytx.hemei.event.service.IEventType;
import cn.com.qytx.platform.base.service.impl.BaseServiceImpl;

/**
 * 功能:接口实现
 * 版本: 1.0
 * 开发人员: panbo
 * 创建日期: 2016年8月10日
 * 修改日期: 2016年8月10日
 * 修改列表: 
 */
@Service("eventTypeService")
@Transactional
public class EventTypeImpl extends BaseServiceImpl<EventType> implements IEventType{

	@Resource(name="eventTypeDao")
	private EventTypeDao eventTypeDao;
	
	@Override
	public List<EventType> findEventTypeList(Integer companyId) {
		return eventTypeDao.findEventTypeList(companyId);
	}

	@Override
	public List<TreeNode> getTreeEventTypeList(String path,Integer companyId) {
		List<EventType> cgList = this.findEventTypeList(companyId);
		List<TreeNode> tnList = new ArrayList<TreeNode>();
		TreeNode firstNode = new TreeNode();
		firstNode.setId("gid_0");// 部门ID前加gid表示类型为部门
		firstNode.setName("事件类型维护");
		firstNode.setPId("gid_-1");
		firstNode.setIcon(path + "/images/company.png");
		firstNode.setOpen(true);
		tnList.add(firstNode);
		if(cgList!=null&&cgList.size()>0){
			for(EventType cg:cgList){
				TreeNode treeNode = new TreeNode();
				treeNode.setId("gid_"+cg.getId());
				treeNode.setName(cg.getTypeName());
				treeNode.setPId("gid_"+cg.getParentId());
				treeNode.setObj(cg.getOrderIndex());// 排序号
				treeNode.setIcon(path + "/images/group.png");
				tnList.add(treeNode);
			}
		}
		return tnList;
	}

	private Map<Integer,Integer> getSubListCountMap(List<EventType> cgList){
		Map<Integer,Integer> map = new HashMap<Integer, Integer>();
		for(EventType cg:cgList){
			Integer parentId = cg.getParentId();
			if(map.containsKey(parentId)){
				map.put(parentId, map.get(parentId)+1);
			}else{
				map.put(parentId, 1);
			}
		}
		
		return map;
	}
	

	@Override
	public List<TreeNode> getTreeEventTypeList_Select(String path,
			Integer companyId) {
		List<EventType> cgList = this.findEventTypeList(companyId);
		Map<Integer,Integer> cgMap = this.getSubListCountMap(cgList);
		List<TreeNode> tnList = new ArrayList<TreeNode>();
		TreeNode firstNode = new TreeNode();
		firstNode.setId("gid_0");// 部门ID前加gid表示类型为部门
		firstNode.setName("事件类型维护");
		firstNode.setPId("gid_-1");
		firstNode.setIcon(path + "/images/company.png");
		firstNode.setOpen(true);
		tnList.add(firstNode);
		if(cgList!=null&&cgList.size()>0){
			for(EventType cg:cgList){
				TreeNode treeNode = new TreeNode();
				if(cgMap!=null&&!cgMap.containsKey(cg.getId())){
					treeNode.setId("uid_"+cg.getId());
				}else{
					treeNode.setId("gid_"+cg.getId());
				}
				treeNode.setName(cg.getTypeName());
				treeNode.setPId("gid_"+cg.getParentId());
				treeNode.setObj(cg.getOrderIndex());// 排序号
				treeNode.setIcon(path + "/images/group.png");
				tnList.add(treeNode);
			}
		}
		return tnList;
	}

	@Override
	public boolean isHasSameEventTypeName(Integer parentId, String groupName,
			int companyId) {
		return eventTypeDao.isHasSameEventTypeName(parentId, groupName, companyId);
	}


	@Override
	public boolean isHasChildEventType(Integer id, int companyId) {
		return eventTypeDao.isHasChildEventType(id,companyId);
	}

	@Override
	public List<EventType> getEventTypeListByIds(String Ids) {
		return eventTypeDao.getEventTypeListByIds(Ids);
	}

}
