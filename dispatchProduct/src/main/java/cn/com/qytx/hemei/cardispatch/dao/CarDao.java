package cn.com.qytx.hemei.cardispatch.dao;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import cn.com.qytx.hemei.cardispatch.domain.Car;
import cn.com.qytx.hemei.cardispatch.domain.CarVo;
import cn.com.qytx.platform.base.dao.BaseDao;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;

/**
 * 功能：车辆持久层
 * 版本：1.0
 * 开发人员: pb
 * 创建日期：2017年9月11日
 * 修改日期：2017年9月11日	
 */
@Repository("carDao")
public class CarDao extends BaseDao<Car,Integer> {
	
	/**
	 * 查询车辆流转列表
	 * @return
	 */
	public Page<Car> findCarList(Pageable pageable,Integer companyId,Integer carType){
		String hql=" isDelete =0 and companyId="+companyId;
		if(carType!=null){
			hql+=" and carType="+carType;
		}
		return super.findAll(hql, pageable);
	}
	/**
	 * 添加
	 * @param car
	 */
	public void saveOrUpdateCar(Car car) {
		super.saveOrUpdate(car);
	}

	/**
	 * 展示列表
	 * @param page
	 * @param carVo
	 * @return
	 */
	public Page<Car> findPageByVo(Pageable page, CarVo carVo) {
		StringBuilder sb = new StringBuilder();
		sb.append(" 1=1 and isDelete=0");
		if (null != carVo) {
			List<Object> param = new ArrayList<Object>();
			Integer type = carVo.getCarType();//车辆类型
			if (null != type) {
				sb.append(" and car_type = ? ");
				param.add(type);
			}
			String carNo = carVo.getCarNo();//车牌号
			if (StringUtils.isNoneBlank(carNo)) {
				sb.append(" and car_no like ? ");
				param.add("%"+carNo+"%");
			}
			 String carBrand = carVo.getCarBrand();//车辆品牌
			if (StringUtils.isNoneBlank(carBrand)) {
				sb.append(" and car_brand like ? ");
				param.add("%"+carBrand+"%");
			}
			String carModel = carVo.getCarModel();//车辆型号
			if (StringUtils.isNoneBlank(carModel)) {
				sb.append(" and car_model like ? ");
				param.add("%"+carModel+"%");
			}
			Integer status = carVo.getStatus();//当前状态
			if (null != status) {
				sb.append(" and status = ? ");
				param.add(status);
			}
			
			Page<Car> findAll = super.dataFilter().findAll(sb.toString(), page, param.size() > 0 ? param.toArray() : null);
			return findAll;
		}
		Page<Car> findAll = super.dataFilter().findAll(sb.toString(), page);
		return findAll;
		
	}
	/**
	 * 根据车牌号判断是否重复
	 * @param carNo
	 * @return
	 */
	public Car findByCarNo(String carNo) {
		String hql = " carNo = ? and isDelete=0 ";
		List<Car> car = super.findAll(hql, carNo);
		if (car != null && car.size() > 0) {
			return car.get(0);
		} 
		return null;
	}
	
	/**
	 * 删除
	 * @param companyId
	 * @param ids
	 * @return
	 */
	public void delete(Integer companyId, String ids) {
		String hql = "update Car set isDelete = 1 where id in ("+ids+")";
		entityManager.createQuery(hql).executeUpdate();
	}
}
