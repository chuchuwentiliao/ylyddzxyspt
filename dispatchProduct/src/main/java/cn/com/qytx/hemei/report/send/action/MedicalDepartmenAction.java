/**
 * 
 */
package cn.com.qytx.hemei.report.send.action;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import cn.com.qytx.cbb.org.util.ExportExcel;
import cn.com.qytx.hemei.report.service.ISendReport;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.org.domain.UserInfo;

/**
 * 功能: 医务科室上报维修量
 * 版本: 1.0
 * 开发人员: 
 * 创建日期: 2017年5月17日
 * 修改日期: 2017年5月17日
 * 修改列表: 
 */
public class MedicalDepartmenAction extends BaseActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8382306994477120260L;

	@Autowired
	private ISendReport sendReportService;
	
	private String beginTime;//开始时间
	private String endTime;//结束时间
	private Integer type;// 1:配送 2:归还
	private String departmentName;
	
	public void findMedicalDepartmenList(){
		UserInfo user=getLoginUser();
		if(user!=null){
	    	if(user!=null){
	    		List<Object[]> list = sendReportService.findMedicalDepartmenList(beginTime, endTime, departmentName,type);
	    		List<Map<String,Object>> listMap= getMapList(list);
	    		ajax(listMap);
	    	}
	    }
	
	}
	
	/**
	 * 封装数据
	 * @param list
	 * @return
	 */
	public List<Map<String,Object>> getMapList(List<Object[]> list){
		List<Map<String,Object>> listMap= new ArrayList<Map<String,Object>>();
		if(list!=null && list.size()>0){
			int i=1;
			Integer reportNum=0;
			Integer finishNum=0;
			Integer noFinishNum=0;
			Double longTime=0.0;
			Double workTime=0.0;
			DecimalFormat df =new DecimalFormat("0.00");
			for(Object[] obj:list){
				Map<String,Object> map = new HashMap<String, Object>();
				map.put("no", i);
				map.put("name", obj[0].toString());
				map.put("reportNum",obj[1]==null?0:Integer.valueOf(obj[1].toString()));//上報
				map.put("finishNum",obj[2]==null?0:Double.valueOf(obj[2].toString()));//完成
				Integer noFinish=0;
				if(obj[1]!=null&&obj[2]!=null){
					noFinish=Integer.valueOf(obj[1].toString())-Integer.valueOf(obj[2].toString());
				}
				map.put("noFinishNum",noFinish);//未完成数
				Double sumTime=Double.valueOf(obj[3].toString());
				map.put("longTime", df.format(sumTime));//耗时
				Double subWorkTime = (obj[4]==null?0.0:Double.valueOf(obj[4].toString()));
				map.put("workTime", df.format(subWorkTime));//工时
				listMap.add(map);
				noFinishNum+=noFinish;
				reportNum+=obj[1]==null?0:Integer.valueOf(obj[1].toString());
				finishNum+=obj[2]==null?0:Integer.valueOf(obj[2].toString());
				longTime+=sumTime;
				workTime+=subWorkTime;
				i++;
			}
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("no", "合计");
			map.put("name", "");
			map.put("reportNum",reportNum);
			map.put("finishNum",finishNum);
			map.put("noFinishNum",noFinishNum);
			map.put("longTime",df.format(longTime));
			map.put("workTime",df.format(workTime));
			listMap.add(map);
		}
		return listMap;
	}
	
	 /**
     * 导出医务科室上报量
     */
    public  void export(){
    	HttpServletResponse response = this.getResponse();
        response.setContentType("application/vnd.ms-excel");
        OutputStream outp = null;
		try{
			UserInfo userInfo=this.getLoginUser();
			if(userInfo!=null){
				List<Object[]> list = sendReportService.findMedicalDepartmenList(beginTime, endTime,departmentName,type);
	    		List<Map<String,Object>> listMap= getMapList(list);
				String fileName = URLEncoder.encode("医务科室上报量统计.xls", "UTF-8");
		        // 把联系人信息填充到map里面
		        response.addHeader("Content-Disposition",
		                "attachment;filename=" + fileName);// 解决中文
				outp = response.getOutputStream();
	            ExportExcel exportExcel = new ExportExcel(outp, getExportHeadList(), listMap, getExportKeyList());
	            exportExcel.exportWithSheetName("医务科室上报量统计");
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(outp!=null){
				try {
					outp.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}  
    }
    
    private List<String> getExportHeadList(){
        List<String> headList = new ArrayList<String>();
        headList.add("序号");
        headList.add("上报科室名称");
        headList.add("上报量");
        headList.add("完工量");
        headList.add("未完工量");
        return headList;
    }
    
    private List<String> getExportKeyList(){
        List<String> headList = new ArrayList<String>();
        headList.add("no");
        headList.add("name");
        headList.add("reportNum");
        headList.add("finishNum");
        headList.add("noFinishNum");
        return headList;
    }
	public String getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}
	
}
