package cn.com.qytx.hemei.cardispatch.service;

import cn.com.qytx.hemei.cardispatch.domain.Car;
import cn.com.qytx.hemei.cardispatch.domain.CarVo;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;
import cn.com.qytx.platform.base.service.BaseService;

public interface ICar extends BaseService<Car> {
	
	/**
	 * 添加车辆
	 */
	public void saveOrUpdateCar(Car car);

	/**
	 * 车辆管理列表
	 * @param pageable
	 * @param carVo
	 * @return
	 */
	public Page<Car> findPageByVo(Pageable pageable, CarVo carVo);
	
	/**
	 * 查询车辆流转列表
	 * @return
	 */
	public Page<Car> findCarList(Pageable pageable,Integer companyId,Integer carType);

	/**
	 * 根据车牌号判断是否重复
	 * @param carNo
	 * @return
	 */
	public Car findByCarNo(String carNo);


	/**
	 * 删除
	 * @param companyId
	 * @param ids
	 * @return
	 */
	public int delete(Integer companyId, String ids);
}
