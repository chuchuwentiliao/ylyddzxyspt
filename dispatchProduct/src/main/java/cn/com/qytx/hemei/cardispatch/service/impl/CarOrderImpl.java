package cn.com.qytx.hemei.cardispatch.service.impl;


import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.com.qytx.cbb.myapply.service.MyApplyService;
import cn.com.qytx.cbb.myapply.service.MyApplyService.ModuleCode;
import cn.com.qytx.hemei.cardispatch.dao.CarDao;
import cn.com.qytx.hemei.cardispatch.dao.CarOrderDao;
import cn.com.qytx.hemei.cardispatch.dao.DriverDao;
import cn.com.qytx.hemei.cardispatch.domain.Car;
import cn.com.qytx.hemei.cardispatch.domain.CarOrder;
import cn.com.qytx.hemei.cardispatch.domain.CarOrderVo;
import cn.com.qytx.hemei.cardispatch.domain.Driver;
import cn.com.qytx.hemei.cardispatch.service.ICarOrder;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;
import cn.com.qytx.platform.base.service.impl.BaseServiceImpl;
import cn.com.qytx.platform.org.dao.GroupDao;
import cn.com.qytx.platform.org.dao.UserDao;
import cn.com.qytx.platform.org.domain.GroupInfo;
import cn.com.qytx.platform.org.domain.UserInfo;
/**
 * 功能：车辆调度工单业务实现
 * 版本：1.0
 * 开发人员: pb
 * 创建日期：2017年9月7日
 * 修改日期：2017年9月7日	
 */
@Service
@Transactional
public class CarOrderImpl extends BaseServiceImpl<CarOrder> implements ICarOrder {

	@Resource(name="userDao")
	private UserDao<UserInfo> userDao;
	@Resource(name="groupDao")
	private GroupDao<GroupInfo> groupDao;
    @Autowired
	private CarOrderDao carOrderDao;
    
    @Autowired
	private CarDao carDao;
    
    @Autowired
	private DriverDao driverDao;
    
    @Resource
	private MyApplyService myApplyService;
    
	@Override
	public String addCarOrder(CarOrder carOrder) {
		Integer processType = carOrder.getProcessType();
		//下一步申请人
		UserInfo nextUser=null;
		GroupInfo nextGroup=null;
		if(carOrder.getProcessId()!=null){
			if(processType.intValue()==2){
				nextGroup = groupDao.findOne(carOrder.getProcessId());
			}else{
				nextUser=userDao.findOne(carOrder.getProcessId());
			}
		}
		
		String instanceId = carOrder.getInstanceId();
		if(StringUtils.isEmpty(instanceId)&&instanceId!="null"){
			String defectNumber=carOrderDao.getCarInstanceId();
			instanceId = getNewInstanceId(defectNumber);
			CarOrder tempCarOrder = carOrderDao.findByInstanceId(instanceId);
			/*再进行一次验证*/
			if(tempCarOrder!=null){
				instanceId = getNewInstanceId(defectNumber);
			}
		}
		carOrder.setInstanceId(instanceId);
		UserInfo userInfo = userDao.findByWorkNo(carOrder.getDispatchWorkNo());
		if(nextUser!=null||nextGroup!=null){
			//生成实例ID
			carOrder.setPreviousProcessId(userInfo.getUserId());
			carOrder.setStatus(1);//待派遣
			if(processType.intValue()==2){
				carOrder.setProcessName(nextGroup.getGroupName());
			}else{
				carOrder.setProcessName(nextUser.getUserName());
			}
			carOrder.setCompanyId(userInfo.getCompanyId());
			carOrder.setCreateUserId(userInfo.getUserId());
			carOrder.setUpdateUserId(userInfo.getUserId());
			Timestamp startTs = new Timestamp(System.currentTimeMillis());
			carOrder.setCreateTime(startTs);
			carOrder.setUpdateTime(startTs);
			carOrder.setIsAccept(0);
			carOrder.setIsDelete(0);
			carOrder.setConfirmFactory(0);
			carOrderDao.saveOrUpdate(carOrder);
			String categoryName = "车辆调度-车辆申请";
			String title="车辆申请";
			//保存发起流程记录
			myApplyService.createProcess(userInfo, categoryName, title, instanceId, ModuleCode.CARDISPATCH);
			//保存发起人的待审批记录
			myApplyService.addCarApprover(categoryName, title, instanceId, userInfo.getUserId()+"", ModuleCode.CARDISPATCH,"0",1);
			//保存下一个人的审批记录
			myApplyService.carApprove(ModuleCode.CARDISPATCH, instanceId, userInfo, "已受理", "0","0","0",nextUser,nextGroup,null);
			myApplyService.addCarApprover(categoryName, title, instanceId, carOrder.getProcessId()+"", ModuleCode.CARDISPATCH,"1",processType);
			/*判断是医联体工单,并向临床科室发送提醒*/
			if(carOrder.getOrderType().intValue()==1&&carOrder.getReferralDepartment()!=null){
				Integer referralDepartment = carOrder.getReferralDepartment();
				List<UserInfo> userList = userDao.findUsersByGroupId(referralDepartment+"");
				if(userList!=null&&!userList.isEmpty()){
					StringBuffer sbStr = new StringBuffer();
					for(UserInfo tempUser:userList){
						sbStr.append(tempUser.getUserId()).append(",");
					}
                    myApplyService.carDispatchPublishPush(carOrder.getCompanyId(), sbStr.toString(), "7", instanceId);
				}
			}
			
		}
		return instanceId;
	}
	
	
	
	public int approve(Integer userId, String instanceId, String approveResult, String advice, String taskName, Integer nextProcessId,Integer processType,Driver driver,Car car) {
		processType = (processType!=null?processType.intValue():1);
		//审批人
		UserInfo approver = userDao.findOne(userId);
		//待审批的实例
		CarOrder carOrder=carOrderDao.findByInstanceId(instanceId);
		if(approver!=null&&carOrder!=null){
			UserInfo nextUser = null;
			GroupInfo nextGroup = null;
			/**
			 *  1 待派遣 2 待发车 3 待回场 4 已回场      
			 */
			Integer state = carOrder.getStatus();
			//下一步处理步骤
			String nextTaskName="";
			Timestamp currTs = new Timestamp(System.currentTimeMillis());
			carOrder.setUpdateTime(currTs);
			carOrder.setUpdateUserId(userId);
			Driver oldDriver = null;
			if("1".equals(taskName)){//派遣，下一步 待发车
				state = 2;
				nextTaskName = "2";
				advice = "已派遣";
				carOrder.setDispatchTime(currTs);//添加派遣时间
				/*添加工单 - 车辆信息*/
				car = carDao.findOne(car.getId());
				car.setUpdateTime(currTs);
				car.setUpdateUserId(userId);
				car.setStatus(2);//待发车
				carOrder.setCar(car);
				carOrder.setCarType(car.getCarType());
				carDao.saveOrUpdate(car);//更新车辆信息
				
				/*添加工单 - 司机信息*/
				UserInfo tempDriver = userDao.findOne(driver.getUserId());
				/*查询原司机是否存在*/
				oldDriver = driverDao.findOne(driver.getUserId());
				if(oldDriver!=null){
					if(StringUtils.isEmpty(driver.getPhone())){
						oldDriver.setPhone(tempDriver.getPhone());
					}
					oldDriver.setUpdateTime(currTs);
					oldDriver.setUpdateUserId(userId);
					oldDriver.setStatus(2);//待发车
					oldDriver.setUserName(tempDriver.getUserName());
					carOrder.setDriver(oldDriver);
					driverDao.saveOrUpdate(oldDriver);
				}else{
					/*添加人员 到 司机表*/
					driver.setUserId(driver.getUserId());
					if(StringUtils.isEmpty(driver.getPhone())){
						driver.setPhone(tempDriver.getPhone());
					}
					driver.setUserName(tempDriver.getUserName());
					driver.setCompanyId(tempDriver.getCompanyId());
					driver.setCreateTime(currTs);
					driver.setCreateUserId(userId);
					driver.setUpdateTime(currTs);
					driver.setUpdateUserId(userId);
					driver.setStatus(2);//待发车
					driverDao.saveOrUpdate(driver);
				}
				/*特殊处理 如果是派遣操作,则将上一步处理人 更新为 部门Id*/
				carOrder.setPreviousProcessId(carOrder.getProcessId());
			}
			
			if("6".equals(taskName)){//派遣驳回，下一步 待派遣
				state = 1;
				nextTaskName = "1";
				advice = "派遣驳回";
				carOrder.setDispatchTime(null);//清除 派遣时间
				car = carDao.findOne(carOrder.getCar().getId());
				car.setStatus(1);
				car.setUpdateTime(currTs);
				car.setUpdateUserId(userId);
				car.setRestSeatNum(car.getSeatNum());
				car.setOccupy(2);
				car.setDepartureTime(null);
				carDao.saveOrUpdate(car);
				carOrder.setCar(null);
				
				oldDriver = driverDao.findOne(carOrder.getDriver().getUserId());
				oldDriver.setUpdateTime(currTs);
				oldDriver.setUpdateUserId(userId);
				oldDriver.setStatus(1);//待发车
				carOrder.setDriver(null);
				driverDao.saveOrUpdate(oldDriver);
				
			}
			
			if("2".equals(taskName)){//发车，下一步 待回场
				state = 3;
				nextTaskName = "7";
				advice = "已发车";
				carOrder.setActualStartTime(new Timestamp(System.currentTimeMillis()));//添加发车时间
				/*更新车辆信息*/
				car = carDao.findOne(carOrder.getCar().getId());
				car.setStatus(3);
				car.setUpdateTime(currTs);
				car.setUpdateUserId(userId);
				car.setHistoryNum(car.getHistoryNum()+1);//任务数+1
				carDao.saveOrUpdate(car);
				/*更新司机信息*/
				oldDriver = driverDao.findOne(carOrder.getDriver().getUserId());
				oldDriver.setUpdateTime(currTs);
				oldDriver.setUpdateUserId(userId);
				oldDriver.setStatus(3);//待发车
				driverDao.saveOrUpdate(oldDriver);
			}
			
			
			if("7".equals(taskName)){//到达，下一步 待回场
				state = 7;
				nextTaskName = "3";
				advice = "已到达";
				carOrder.setArriveTime(new Timestamp(System.currentTimeMillis()));//添加到达时间
			}
			if("3".equals(taskName)){//回场
				state = 4;
				carOrder.setBackFactoryTime(currTs);//添加回场时间
				if(StringUtils.isEmpty(advice)){
					advice = "已回场";
				}
				/*更新车辆信息*/
				car = carDao.findOne(carOrder.getCar().getId());
				car.setStatus(1);
				car.setUpdateTime(currTs);
				car.setUpdateUserId(userId);
				car.setRestSeatNum(car.getSeatNum());
				car.setOccupy(2);
				car.setDepartureTime(null);
				carDao.saveOrUpdate(car);
				/*更新司机信息*/
				oldDriver = driverDao.findOne(carOrder.getDriver().getUserId());
				oldDriver.setUpdateTime(currTs);
				oldDriver.setUpdateUserId(userId);
				oldDriver.setStatus(1);//待派遣
				driverDao.saveOrUpdate(oldDriver);
			}
			
			if("5".equals(taskName)){//确认到场
				carOrder.setConfirmFactory(1);
				advice = "确认到场";
			}
			
			//如果是撤销，查找上一步处理人
			if("6".equals(taskName)){
				processType=2;
				nextProcessId=carOrder.getPreviousProcessId();
			}
			/*派遣 已指定  上一步处理人,此处不做设置*/
			if(!"1".equals(taskName)){
				carOrder.setPreviousProcessId(userId);
			}
			
			//查找下一步操作人
			if(nextProcessId!=null){
				if(processType==2){
					nextGroup = groupDao.findOne(nextProcessId);
				}else{
					nextUser = userDao.findOne(nextProcessId);
				}
			}
			
			/*添加当前处理流程*/
			myApplyService.carApprove(ModuleCode.CARDISPATCH, instanceId, approver, advice, state+"",approveResult,taskName,nextUser,nextGroup,car!=null?car.getId():null);
			
			carOrder.setStatus(state);
			carOrder.setUpdateTime(currTs);
			carOrder.setUpdateUserId(userId);
			
			
			
			if(processType==2){
				//如果处理通过，添加下一步审批人
				if(nextGroup!=null&&StringUtils.isNotBlank(nextTaskName)){
					myApplyService.addCarApprover("处理申请", "", instanceId, nextProcessId+"", ModuleCode.CARDISPATCH,nextTaskName,processType);
					carOrder.setProcessId(nextGroup.getGroupId());
					carOrder.setProcessName(nextGroup.getGroupName());
				}
			}else{
				//如果处理通过，添加下一步审批人
				if(nextUser!=null&&StringUtils.isNotBlank(nextTaskName)){
					myApplyService.addCarApprover("处理申请", "", instanceId, nextUser.getUserId()+"", ModuleCode.CARDISPATCH,nextTaskName,processType);
					carOrder.setProcessId(nextUser.getUserId());
					carOrder.setProcessName(nextUser.getUserName()); 
				}
			}
			carOrder.setProcessType(processType);
			if("1".equals(taskName)&&oldDriver==null){
				carOrder.setDriver(driver);
			}
			carOrderDao.saveOrUpdate(carOrder);
			return 1;
		}
		return 0;
	}
	

	@Override
	public CarOrder findByInstanceId(String instanceId) {
		return carOrderDao.findByInstanceId(instanceId);
	}

	@Override
	public Page<CarOrder> findCarDispatch(Pageable pageable, Integer userId,
			CarOrderVo carOrderVo) {
		// TODO Auto-generated method stub
		return carOrderDao.findCarDispatch(pageable, userId, carOrderVo);
	}
	
	private static synchronized String getNewInstanceId(String defectNumber) {
		if(StringUtils.isNotBlank(defectNumber)){
			String seconds = new SimpleDateFormat("SSS").format(new Date());
			defectNumber=(Long.parseLong(defectNumber)+Long.valueOf(seconds))+"";
		}else{
			defectNumber=new SimpleDateFormat("yyyyMMdd").format(new Date())+"001";
		}
		return defectNumber;
	}



	@Override
	public Page<CarOrder> findMyWaitCarDispatch(Pageable pageable,
			Integer userId, CarOrderVo carOrderVo) {
		// TODO Auto-generated method stub
		return carOrderDao.findMyWaitCarDispatch(pageable, userId, carOrderVo);
	}



	@Override
	public Page<CarOrder> findMyProcessedCarDispatch(Pageable pageable,
			Integer userId, CarOrderVo carOrderVo) {
		// TODO Auto-generated method stub
		return carOrderDao.findMyProcessedCarDispatch(pageable, userId, carOrderVo);
	}



	@Override
	public Map<String, Integer> getCarOrderWaitNum(Integer companyId,
			Integer userId,Integer groupId) {
		return carOrderDao.getCarOrderWaitNum(companyId,userId,groupId);
	}



	@Override
	public Map<Integer, Integer> getCarOrderNum(Integer companyId) {
		// TODO Auto-generated method stub
		return carOrderDao.getCarOrderNum(companyId);
	}



	@Override
	public Map<Integer, Integer> getDriverOrderNum(Integer companyId) {
		// TODO Auto-generated method stub
		return carOrderDao.getDriverOrderNum(companyId);
	}

	public static void main(String[] args) {
		String seconds = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
		System.out.println(Long.parseLong(seconds)+20170923141l);
	}



	@Override
	public String dispatchAddCarOrder(CarOrder carOrder) {
		Integer processType = carOrder.getProcessType();
		//下一步申请人
		UserInfo nextUser=null;
		GroupInfo nextGroup=null;
		if(carOrder.getDriverId()!=null){
			nextUser=userDao.findOne(carOrder.getDriverId());
		}
		
		String instanceId = carOrder.getInstanceId();
		if(StringUtils.isEmpty(instanceId)&&instanceId!="null"){
			String defectNumber=carOrderDao.getCarInstanceId();
			instanceId = getNewInstanceId(defectNumber);
			CarOrder tempCarOrder = carOrderDao.findByInstanceId(instanceId);
			/*再进行一次验证*/
			if(tempCarOrder!=null){
				instanceId = getNewInstanceId(defectNumber);
			}
		}
		carOrder.setInstanceId(instanceId);
		UserInfo userInfo = userDao.findByWorkNo(carOrder.getDispatchWorkNo());
		if(nextUser!=null){
			//生成实例ID
			carOrder.setPreviousProcessId(carOrder.getProcessDepartmentId());
			carOrder.setProcessName(nextUser.getUserName());
			carOrder.setCompanyId(userInfo.getCompanyId());
			carOrder.setCreateUserId(userInfo.getUserId());
			carOrder.setUpdateUserId(userInfo.getUserId());
			Timestamp startTs = new Timestamp(System.currentTimeMillis());
			carOrder.setCreateTime(startTs);
			carOrder.setUpdateTime(startTs);
			carOrder.setIsAccept(0);
			carOrder.setIsDelete(0);
			carOrder.setConfirmFactory(0);
			carOrder.setStatus(2);
			//保存发起流程记录
			myApplyService.createProcess(userInfo, "车辆调度", "车辆申请", instanceId, ModuleCode.CARDISPATCH);
			//保存发起人的待审批记录
			myApplyService.addCarApprover("车辆调度", "车辆申请", instanceId, userInfo.getUserId()+"", ModuleCode.CARDISPATCH,"0",1);
			//保存下一个人的审批记录
			myApplyService.carApprove(ModuleCode.CARDISPATCH, instanceId, userInfo, "已受理", "0","0","0",nextUser,nextGroup,null);
			//保存派遣记录
			myApplyService.carApprove(ModuleCode.CARDISPATCH, instanceId, userInfo, "已派遣", "2","1","1",nextUser,nextGroup,carOrder.getCarId());
			
			//发起发车 记录
			carOrder.setDispatchTime(startTs);//添加派遣时间
			/*添加工单 - 车辆信息*/
			Car car = carDao.findOne(carOrder.getCarId());
			car.setUpdateTime(startTs);
			car.setUpdateUserId(userInfo.getUserId());
			car.setStatus(2);//待发车
			car.setRestSeatNum(carOrder.getRestSeatNum());//剩余座位
			car.setDepartureTime(carOrder.getDepartureTime());//发车时间
			if(!(car.getOccupy()==1)){
				car.setOccupy(carOrder.getOccupy()!=null?carOrder.getOccupy():0);//是否被占用
			}
			carOrder.setCar(car);
			carOrder.setCarType(car.getCarType());
			carDao.saveOrUpdate(car);//更新车辆信息
			
			/*查询原司机是否存在*/
			Driver oldDriver = driverDao.findOne(carOrder.getDriverId());
			if(oldDriver!=null){
				if(StringUtils.isEmpty(oldDriver.getPhone())){
					oldDriver.setPhone(nextUser.getPhone());
				}
				oldDriver.setUpdateTime(startTs);
				oldDriver.setUpdateUserId(userInfo.getUserId());
				oldDriver.setStatus(2);//待发车
				oldDriver.setUserName(nextUser.getUserName());
				carOrder.setDriver(oldDriver);
				driverDao.saveOrUpdate(oldDriver);
			}else{
				/*添加人员 到 司机表*/
				Driver driver = new Driver();
				driver.setUserId(nextUser.getUserId());
				driver.setPhone(nextUser.getPhone());
				driver.setUserName(nextUser.getUserName());
				driver.setCompanyId(userInfo.getCompanyId());
				driver.setCreateTime(startTs);
				driver.setCreateUserId(userInfo.getUserId());
				driver.setUpdateTime(startTs);
				driver.setUpdateUserId(userInfo.getUserId());
				driver.setStatus(2);//待发车
				driverDao.saveOrUpdate(driver);
				carOrder.setDriver(driver);
			}
			myApplyService.addCarApprover("车辆调度", "车辆待发车", instanceId, carOrder.getDriverId()+"", ModuleCode.CARDISPATCH,"2",processType);
			/*判断是医联体工单,并向临床科室发送提醒*/
			if(carOrder.getOrderType().intValue()==1&&carOrder.getReferralDepartment()!=null){
				Integer referralDepartment = carOrder.getReferralDepartment();
				List<UserInfo> userList = userDao.findUsersByGroupId(referralDepartment+"");
				if(userList!=null&&!userList.isEmpty()){
					StringBuffer sbStr = new StringBuffer();
					for(UserInfo tempUser:userList){
						sbStr.append(tempUser.getUserId()).append(",");
					}
                    myApplyService.carDispatchPublishPush(carOrder.getCompanyId(), sbStr.toString(), "7", instanceId);
				}
			}
			
			carOrderDao.saveOrUpdate(carOrder);
			
		}
		return instanceId;
	}
}
