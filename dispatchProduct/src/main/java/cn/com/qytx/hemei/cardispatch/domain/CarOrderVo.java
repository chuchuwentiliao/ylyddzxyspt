package cn.com.qytx.hemei.cardispatch.domain;

public class CarOrderVo {
    //派遣类型
	private Integer businessType;
	//车辆类型
	private Integer carType;
	
	/**
	 * 到达地点
	 */
	private String arriveLocation;
	
	
	/**
	 * 发车时间
	 */
	private String StartTime_begin;
	
	private String StartTime_end;
	
	/**
	 * 车牌号
	 */
	private String carNo;
	
	
	/**
	 * 司机
	 */
	private String driver;
	
	//回厂时间
	private String returnTime_bengin;
	
	//回厂时间
    private String returnTime_end;
	

	/**
	 * 工单状态（1 待派遣（已受理） 2 待发车（已派遣） 3 待回场（已发车） 4 已回场（完结））
	 */
	private Integer status;
	
	

	/**
	 * 工单类别 （1 医联体工单 2车辆派遣 ）
	 */
	private String orderType;

	//接诊科室
	private Integer referralDepartment;
	
	private String company;//转诊单位
	
	private String createTime_begin;
	private String createTime_end;
	

	public Integer getBusinessType() {
		return businessType;
	}


	public void setBusinessType(Integer businessType) {
		this.businessType = businessType;
	}


	public Integer getCarType() {
		return carType;
	}


	public void setCarType(Integer carType) {
		this.carType = carType;
	}


	public String getArriveLocation() {
		return arriveLocation;
	}


	public void setArriveLocation(String arriveLocation) {
		this.arriveLocation = arriveLocation;
	}



	public String getStartTime_begin() {
		return StartTime_begin;
	}


	public void setStartTime_begin(String startTime_begin) {
		StartTime_begin = startTime_begin;
	}


	public String getStartTime_end() {
		return StartTime_end;
	}


	public void setStartTime_end(String startTime_end) {
		StartTime_end = startTime_end;
	}



	public String getCarNo() {
		return carNo;
	}


	public void setCarNo(String carNo) {
		this.carNo = carNo;
	}


	public String getDriver() {
		return driver;
	}


	public void setDriver(String driver) {
		this.driver = driver;
	}




	public String getReturnTime_bengin() {
		return returnTime_bengin;
	}


	public void setReturnTime_bengin(String returnTime_bengin) {
		this.returnTime_bengin = returnTime_bengin;
	}


	public String getReturnTime_end() {
		return returnTime_end;
	}


	public void setReturnTime_end(String returnTime_end) {
		this.returnTime_end = returnTime_end;
	}


	public Integer getStatus() {
		return status;
	}


	public void setStatus(Integer status) {
		this.status = status;
	}


	public String getOrderType() {
		return orderType;
	}


	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}


	public Integer getReferralDepartment() {
		return referralDepartment;
	}


	public void setReferralDepartment(Integer referralDepartment) {
		this.referralDepartment = referralDepartment;
	}


	public String getCreateTime_begin() {
		return createTime_begin;
	}


	public void setCreateTime_begin(String createTime_begin) {
		this.createTime_begin = createTime_begin;
	}


	public String getCreateTime_end() {
		return createTime_end;
	}


	public void setCreateTime_end(String createTime_end) {
		this.createTime_end = createTime_end;
	}


	public String getCompany() {
		return company;
	}


	public void setCompany(String company) {
		this.company = company;
	}
	
	
	
}
