package cn.com.qytx.hemei.report.equipment.dao;

import java.util.List;

import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import cn.com.qytx.cbb.capital.domain.Capital;
import cn.com.qytx.platform.base.dao.BaseDao;
@Repository
public class EquipmentDao extends BaseDao<Capital, Integer> {
    /**
     * 设备归还报表
     * @return
     */
	public List<Object[]> findReturnEquipmentList(String groupName,String userName, String beginTime,String endTime,Integer type){
		String  sql="SELECT c.group_name capitalGroupName,g.group_name,g.user_name,sum(cast(d.equipment_number as int)) num";
		sql+="  FROM View_capital c  INNER JOIN tb_defect_apply d ON c.defect_id=d.id";
		sql+="  INNER JOIN tb_cbb_my_processed m ON d.instance_id=m.instance_id INNER JOIN View_repair_group g ON m.processer_id = g.user_id";
		sql+="  where d.type="+type+" and g.extension=2 and m.approveResult=1 and m.task_name=3";
		if(StringUtils.isNoneBlank(beginTime)){
			sql+=" and m.end_time >='"+beginTime+" 00:00:00'";
		}
		if(StringUtils.isNoneBlank(endTime)){
			sql+=" and m.end_time <='"+endTime+" 23:59:59'";
		}
		if(StringUtils.isNoneBlank(groupName)){
	            sql+=" and g.group_name like '%"+groupName+"%'";
	    }
	    if(StringUtils.isNoneBlank(userName)){
	    	   sql+=" and g.userName like '%"+userName+"%'";
		}
	    sql+=" GROUP BY c.group_name,g.group_name,g.user_name";
	    Query query = super.entityManager.createNativeQuery(sql);
	    return query.getResultList();
	}
	/**
	 * 设备使用科室使用情况
	 * @return
	 */
	public List<Object[]> findUseEquipmentList(String beginTime,String endTime){
		String sql=" SELECT COUNT (DISTINCT capital_id) capitalNum,COUNT (*) useNum,capitalGroupId,use_group_id,";
		sql+=" SUM (ISNULL(use_long_time, 0))totalTimeLong ,group_name,capitalGroupName FROM View_use_capital where extension='3'";
		if(StringUtils.isNoneBlank(beginTime)){
		     sql+=" and user_end_time >='"+beginTime+" 00:00:00'";
		}
        if(StringUtils.isNoneBlank(endTime)){
        	sql+=" and user_end_time <='"+endTime+" 23:59:59'";
		}
        sql+=" GROUP BY capitalGroupId,use_group_id,group_name,capitalGroupName";
        Query query = super.entityManager.createNativeQuery(sql);
	    return query.getResultList();
	}
	
	/**
	 *查询临床科室设备归还数量
	 */
	public List<Object[]> findReturnEquipmentNum(String beginTime,String endTime){
		String sql=" SELECT v.id capitalGroupId,d.create_group_id,sum(CAST(d.equipment_number as INT)) capitalNum";
		sql+=" FROM View_capital v INNER JOIN tb_defect_apply d ON d.id = v.defect_id";
		sql+=" INNER JOIN tb_cbb_my_processed p ON p.instance_id = d.instance_id where p.approveResult=1 and p.task_name=3 and type=2 ";
		if(StringUtils.isNoneBlank(beginTime)){
		     sql+=" and p.end_time >='"+beginTime+" 00:00:00'";
		}
       if(StringUtils.isNoneBlank(endTime)){
       	sql+=" and p.end_time <='"+endTime+" 23:59:59'";
		}
       sql+=" group by v.id,d.create_group_id";
       Query query = super.entityManager.createNativeQuery(sql);
	   return query.getResultList();
	}
	
	
	
	
}
