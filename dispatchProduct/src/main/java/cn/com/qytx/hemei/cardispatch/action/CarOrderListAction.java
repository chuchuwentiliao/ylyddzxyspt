package cn.com.qytx.hemei.cardispatch.action;

import java.io.OutputStream;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;

import cn.com.qytx.cbb.myapply.domain.MyProcessed;
import cn.com.qytx.cbb.myapply.service.IMyProcessed;
import cn.com.qytx.cbb.org.util.ExportExcel;
import cn.com.qytx.hemei.cardispatch.domain.Car;
import cn.com.qytx.hemei.cardispatch.domain.CarOrder;
import cn.com.qytx.hemei.cardispatch.domain.CarOrderVo;
import cn.com.qytx.hemei.cardispatch.domain.Driver;
import cn.com.qytx.hemei.cardispatch.service.ICar;
import cn.com.qytx.hemei.cardispatch.service.ICarOrder;
import cn.com.qytx.hemei.cardispatch.service.IDriver;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;
import cn.com.qytx.platform.base.query.Sort;
import cn.com.qytx.platform.base.query.Sort.Direction;
import cn.com.qytx.platform.org.domain.GroupInfo;
import cn.com.qytx.platform.org.domain.UserInfo;
import cn.com.qytx.platform.org.service.IGroup;
import cn.com.qytx.platform.org.service.IUser;
import cn.com.qytx.platform.utils.DateUtils;

/**
 * 功能：车辆调度工单
 * 版本：1.0
 * 开发人员: pb
 * 创建日期：2017年9月7日
 * 修改日期：2017年9月7日	
 */
public class CarOrderListAction extends BaseActionSupport{
	private static final long serialVersionUID = 6443441678076641166L;

	
	
	@Autowired
	private IGroup groupService;
	@Autowired
	private IUser userService;
	
	@Autowired
	private IDriver driverSerivce; 
	
	@Autowired
	private ICar carService;

	@Autowired
	private ICarOrder  carOrderService;
	
	private CarOrderVo carOrderVo;
	

	private String instanceId;
	
	
	@Autowired
	private IMyProcessed MyProcessImpl;
	
	private Integer carType;
	
	private String serachKey;
	
	/**
	 * 查询车辆流转列表
	 * @return
	 */
	public String  findCarDispatch(){
		try {
			UserInfo user=getLoginUser();
			if(user!=null){
				Sort sort=new Sort(Direction.DESC,"createTime");
				Pageable pageable=this.getPageable(sort);
				//Integer userId=user.getUserId();
				List<Map<String,Object>> mapList=new ArrayList<Map<String,Object>>();
				Page<CarOrder> page =carOrderService.findMyWaitCarDispatch(pageable, null, carOrderVo);
				List<CarOrder> list=page.getContent();
				Map<Integer,String> groupMap=findGroupNameMap(2);//临床科室
				if(list!=null && list.size()>0){
					int i = 1;
					if(pageable!=null){
		    			i = pageable.getPageNumber() * pageable.getPageSize() + 1;
		    		}
					for(CarOrder c:list){
						Map<String,Object> map=new HashMap<String,Object>();
						map.put("no", i);
						map.put("id",c.getId());
						map.put("status", c.getStatus());
						map.put("instanceId", c.getInstanceId());
						map.put("stateStr", c.getStatus()!=null?getStateStr(c.getStatus()):"-");
						map.put("businessType", c.getBusinessType()!=null?findBusinessTypeStr(c.getBusinessType()):"-");//派遣类型
						map.put("referralDepartment",groupMap.containsKey( c.getReferralDepartment())?groupMap.get(c.getReferralDepartment()):"-");//转诊科室
						String carTypeStr="-";
						if(c.getCarType()==2){
							carTypeStr="急诊车";
						}
						if(c.getCarType()==1){
							carTypeStr="转诊车";
						}
						map.put("carTypeStr", carTypeStr);//车类型
						map.put("carType", c.getCarType());//车类型
						map.put("startLocation", StringUtils.isNoneBlank(c.getStartLocation())?c.getStartLocation():"-");//出发地点
						map.put("arriveLocation",StringUtils.isNoneBlank(c.getArriveLocation())?c.getArriveLocation():"-");//到达地点
						map.put("planStartTime", c.getPlanStartTime()==null?"-":DateUtils.date2LongStr(c.getPlanStartTime()));//计划发车时间
						map.put("createTime", c.getCreateTime()==null?"-":DateUtils.date2LongStr(c.getCreateTime()));//登记时间
						map.put("carNo", c.getCar()!=null?c.getCar().getCarNo():"-");//车牌号
						String carPhone="-";
						if(c.getCar()!=null){
							if(StringUtils.isNoneBlank(c.getCar().getCarPhone())){
								carPhone=c.getCar().getCarPhone();
							}
						}
						map.put("carPhone", carPhone);//车辆电话号
						String driverName="-";
						if(c.getDriver()!=null){
							driverName=c.getDriver().getUserName();
						}
						map.put("driverName",driverName );//司机
						map.put("startTime",c.getActualStartTime()==null?"-":DateUtils.date2LongStr(c.getActualStartTime()));// 实际发车时间
						map.put("backFactoryTime",c.getBackFactoryTime()==null?"-":DateUtils.date2LongStr(c.getBackFactoryTime()));// 回厂时间
						map.put("dispatchTime",c.getDispatchTime()==null?"-":DateUtils.date2LongStr(c.getDispatchTime()));// 派遣时间
						map.put("arriveTime",c.getArriveTime()==null?"-":DateUtils.date2LongStr(c.getArriveTime()));// 到达时间
					    if(c.getStatus()==4){
					    	long time=c.getBackFactoryTime().getTime()-c.getActualStartTime().getTime();
					    	map.put("time", DateUtils.formatTime(time/1000));//耗时
					    }
					    i++;
					    mapList.add(map);
					}
				}
				Map<String, Object> jsonMap = new HashMap<String, Object>();
				jsonMap.put("iTotalRecords", page.getTotalElements());
				jsonMap.put("iTotalDisplayRecords", page.getTotalElements());
				jsonMap.put("aaData", mapList);
				Gson gson = new Gson();
				ajax(gson.toJson(jsonMap));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 查询已办车车辆流转列表
	 * @return
	 */
	public String findMyProcessedCarDispatch(){
		try {
			UserInfo user=getLoginUser();
			if(user!=null){
				Sort sort=new Sort(Direction.DESC,"createTime");
				Pageable pageable=this.getPageable(sort);
				//Integer userId=user.getUserId();
				List<Map<String,Object>> mapList=new ArrayList<Map<String,Object>>();
				Page<CarOrder> page =carOrderService.findMyProcessedCarDispatch(pageable, null, carOrderVo);
				List<CarOrder> list=page.getContent();
				Map<Integer,String> groupMap=findGroupNameMap(2);//临床科室
				if(list!=null && list.size()>0){
					int i = 1;
					if(pageable!=null){
		    			i = pageable.getPageNumber() * pageable.getPageSize() + 1;
		    		}
					for(CarOrder c:list){
						Map<String,Object> map=new HashMap<String,Object>();
						map.put("no", i);
						map.put("id",c.getId());
						map.put("instanceId", c.getInstanceId());
						map.put("stateStr", c.getStatus()!=null?getStateStr(c.getStatus()):"-");
						map.put("status", c.getStatus());
						map.put("businessType", c.getBusinessType()!=null?findBusinessTypeStr(c.getBusinessType()):"-");//派遣类型
						map.put("referralDepartment",groupMap.containsKey( c.getReferralDepartment())?groupMap.get(c.getReferralDepartment()):"-");//转诊科室
						String carTypeStr="-";
						if(c.getCarType()==2){
							carTypeStr="急诊车";
						}
						if(c.getCarType()==1){
							carTypeStr="转诊车";
						}
						map.put("carTypeStr", carTypeStr);//车类型
						map.put("carType", c.getCarType());//车类型
						map.put("startLocation", StringUtils.isNoneBlank(c.getStartLocation())?c.getStartLocation():"-");//出发地点
						map.put("arriveLocation",StringUtils.isNoneBlank(c.getArriveLocation())?c.getArriveLocation():"-");//到达地点
						map.put("planStartTime", c.getPlanStartTime()==null?"-":DateUtils.date2LongStr(c.getPlanStartTime()));//计划发车时间
						map.put("createTime", c.getCreateTime()==null?"-":DateUtils.date2LongStr(c.getCreateTime()));//登记时间
						map.put("carNo", c.getCar()!=null?c.getCar().getCarNo():"");//车牌号
						String carPhone="-";
						if(c.getCar()!=null){
							if(StringUtils.isNoneBlank(c.getCar().getCarPhone())){
								carPhone=c.getCar().getCarPhone();
							}
						}
						map.put("carPhone", carPhone);//车辆电话号
						String driverName="-";
						if(c.getDriver()!=null){
							driverName=c.getDriver().getUserName();
						}
						map.put("driverName",driverName );//司机
						map.put("startTime",c.getActualStartTime()==null?"-":DateUtils.date2LongStr(c.getActualStartTime()));// 实际发车时间
						map.put("backFactoryTime",c.getBackFactoryTime()==null?"-":DateUtils.date2LongStr(c.getBackFactoryTime()));// 回厂时间
						map.put("dispatchTime",c.getDispatchTime()==null?"-":DateUtils.date2LongStr(c.getDispatchTime()));// 派遣时间
					    if(c.getStatus()==4){
					    	long time=c.getBackFactoryTime().getTime()-c.getActualStartTime().getTime();
					    	map.put("time", DateUtils.formatHHMMSS(time/1000));//耗时
					    }
					    i++;
					    mapList.add(map);
					}
				}
				Map<String, Object> jsonMap = new HashMap<String, Object>();
				jsonMap.put("iTotalRecords", page.getTotalElements());
				jsonMap.put("iTotalDisplayRecords", page.getTotalElements());
				jsonMap.put("aaData", mapList);
				Gson gson = new Gson();
				ajax(gson.toJson(jsonMap));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	/**
	 * 查询医联体列表
	 * @return
	 */
	public String findConjoinedOrder(){
		try {
			UserInfo user=getLoginUser();
			if(user!=null){
				Sort sort=new Sort(Direction.DESC,"createTime");
				Pageable pageable=this.getPageable(sort);
				if(carOrderVo==null){
					carOrderVo=new CarOrderVo();
				}
				carOrderVo.setReferralDepartment(user.getGroupId());
				Page<CarOrder> page =carOrderService.findCarDispatch(pageable, null, carOrderVo);
				List<CarOrder> list=page.getContent();
				List<Map<String,Object>> mapList=findListMap(list,pageable);
				
				Map<String, Object> jsonMap = new HashMap<String, Object>();
				jsonMap.put("iTotalRecords", page.getTotalElements());
				jsonMap.put("iTotalDisplayRecords", page.getTotalElements());
				jsonMap.put("aaData", mapList);
				Gson gson = new Gson();
				ajax(gson.toJson(jsonMap));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 封装数据
	 * @return
	 */
	private  List<Map<String,Object>> findListMap(List<CarOrder> list,Pageable pageable){
		List<Map<String,Object>> mapList = new ArrayList<Map<String,Object>>();
		if(list!=null && list.size()>0){
			int i = 1;
			if(pageable!=null){
    			i = pageable.getPageNumber() * pageable.getPageSize() + 1;
    		}
			for(CarOrder c:list){
				Map<String,Object> map=new HashMap<String,Object>();
				map.put("no", i);
				map.put("id",c.getId());
				map.put("instanceId", c.getInstanceId());
				map.put("patientName", c.getPatientName()==null?"-":c.getPatientName());
				map.put("stateStr", c.getStatus()!=null?getStateStr(c.getStatus()):"-");
				map.put("status", c.getStatus());
				map.put("confirmFactory", c.getConfirmFactory());
				map.put("businessType", c.getBusinessType()!=null?findBusinessTypeStr(c.getBusinessType()):"-");//派遣类型
				map.put("company",StringUtils.isNoneBlank(c.getCompany())?c.getCompany():"-");//转诊单位
				String carTypeStr="-";
				if(c.getCarType()==2){
					carTypeStr="急诊车";
				}
				if(c.getCarType()==1){
					carTypeStr="转诊车";
				}
				map.put("carTypeStr", carTypeStr);//车类型
				map.put("startLocation", StringUtils.isNoneBlank(c.getStartLocation())?c.getStartLocation():"-");//出发地点
				map.put("arriveLocation",StringUtils.isNoneBlank(c.getArriveLocation())?c.getArriveLocation():"-");//到达地点
				map.put("planStartTime", c.getPlanStartTime()==null?"-":DateUtils.date2LongStr(c.getPlanStartTime()));//计划发车时间
				map.put("createTime", c.getCreateTime()==null?"-":DateUtils.date2LongStr(c.getCreateTime()));//登记时间
				map.put("carNo", c.getCar()!=null?c.getCar().getCarNo():"");//车牌号
				String driverName="-";
				if(c.getDriver()!=null){
					driverName=c.getDriver().getUserName();
				}
				map.put("driverName",driverName );//司机
				map.put("startTime",c.getActualStartTime()==null?"-":DateUtils.date2LongStr(c.getActualStartTime()));// 实际发车时间
				map.put("backFactoryTime",c.getBackFactoryTime()==null?"-":DateUtils.date2LongStr(c.getBackFactoryTime()));// 回厂时间
				map.put("dispatchTime",c.getDispatchTime()==null?"-":DateUtils.date2LongStr(c.getDispatchTime()));// 派遣时间
			    if(c.getStatus()==4){
			    	long time=c.getBackFactoryTime().getTime()-c.getActualStartTime().getTime();
			    	map.put("time", DateUtils.formatTime(time/1000));//耗时
			    }
			    i++;
			    mapList.add(map);
			}
		}
		return mapList;
	}
	
	
	/*
	 * 
	 * 导出
	 */
		public void exportList(){
			HttpServletResponse response = this.getResponse();
	        response.setContentType("application/vnd.ms-excel");
	        OutputStream outp = null;
			try {
				UserInfo user= getLoginUser();
				if(user!=null){
					if(StringUtils.isNoneBlank(carOrderVo.getCompany())){
						carOrderVo.setCompany(URLDecoder.decode(carOrderVo.getCompany(), "UTF-8"));
					}
					setIDisplayLength(Integer.MAX_VALUE);
					Sort sort=new Sort(Direction.DESC,"createTime");
					Pageable pageable=this.getPageable(sort);
					Integer userId=user.getUserId();
					Page<CarOrder> page =carOrderService.findCarDispatch(pageable, userId, carOrderVo);
					List<Map<String,Object>> mapList=findListMap(page.getContent(),pageable);
					String fileName = URLEncoder.encode("医联体工单.xls", "UTF-8");
			        // 把联系人信息填充到map里面
			        response.addHeader("Content-Disposition",
			                "attachment;filename=" + fileName);// 解决中文
					outp = response.getOutputStream();
		            ExportExcel exportExcel = new ExportExcel(outp, getExportHeadList(), mapList, getExportKeyList());
		            exportExcel.exportWithSheetName("医联体工单");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
	   private List<String> getExportHeadList(){
	        List<String> headList = new ArrayList<String>();
	        headList.add("序号");
	        headList.add("工单编号");
	        headList.add("派遣类型");
	        headList.add("患者姓名");
	        headList.add("转诊单位");
	        headList.add("登记时间");
	        headList.add("当前状态");
	        return headList;
	    }
		    
	    private List<String> getExportKeyList(){
	        List<String> headList = new ArrayList<String>();
	        headList.add("no");
	        headList.add("instanceId");
	        headList.add("businessType");
	        headList.add("patientName");
	        headList.add("company");
	        headList.add("createTime");
	        headList.add("stateStr");
	        return headList;
	    }
	
	
	
	
	
	
	/**
	 * 车辆调度详情
	 * @return
	 */
	public String carDispatchView(){
		try {
			UserInfo user=getLoginUser();
			if(user!=null){
				CarOrder carOrder=carOrderService.findByInstanceId(instanceId);
				Map<String,Object> resMap =new HashMap<String, Object>();
				if(carOrder!=null){
					resMap.put("instanceId", carOrder.getInstanceId());//流程Id;
					resMap.put("stateStr", carOrder.getStatus()!=null?getStateStr(carOrder.getStatus()):"-");//状态
					resMap.put("businessType", carOrder.getBusinessType()!=null?findBusinessTypeStr(carOrder.getBusinessType()):"-");//业务类别
					String orderType="-";
					if(carOrder.getOrderType()==1){
						orderType=" 医联体工单"; 
					}else if(carOrder.getOrderType()==2){
						orderType=" 车辆派遣 "; 
					}
					resMap.put("orderType", orderType);//工单类别
					resMap.put("company", StringUtils.isNoneBlank(carOrder.getCompany())?carOrder.getCompany():"-");//转诊单位
					resMap.put("diagnosis", StringUtils.isNoneBlank(carOrder.getDiagnosis())?carOrder.getDiagnosis():"-");//诊断
					String  referralDepartment="-";
					if(carOrder.getProcessDepartmentId()!=null){
						GroupInfo group=groupService.findOne(carOrder.getProcessDepartmentId());
						referralDepartment=group.getGroupName();
					}
					resMap.put("referralDepartment", referralDepartment);//转诊科室
					resMap.put("checkProject", StringUtils.isNoneBlank(carOrder.getCheckProject())?carOrder.getCheckProject():"-");//检查项目
					resMap.put("referralDoctor", StringUtils.isNoneBlank(carOrder.getReferralDoctor())?carOrder.getReferralDoctor():"-");//转诊医师
					resMap.put("doctorPhone", StringUtils.isNoneBlank(carOrder.getDoctorPhone())?carOrder.getDoctorPhone():"-");//联系方式
					resMap.put("referralPurpose", StringUtils.isNoneBlank(carOrder.getReferralPurpose())?carOrder.getReferralPurpose():"-");//转诊目的
					resMap.put("inhospitalTime", carOrder.getInhospitalTime()!=null?DateUtils.date2LongStr(carOrder.getInhospitalTime()):"-");//入院时间
					resMap.put("referralReason", StringUtils.isNoneBlank(carOrder.getReferralReason())?carOrder.getReferralReason():"-");//转诊原因
					resMap.put("patientName", StringUtils.isNoneBlank(carOrder.getPatientName())?carOrder.getPatientName():"-");//患者姓名
					resMap.put("visitTime", carOrder.getVisitTime()!=null?DateUtils.date2LongStr(carOrder.getVisitTime()):"-");//就诊时间
					resMap.put("carContent",StringUtils.isNoneBlank(carOrder.getCarContent())? carOrder.getCarContent():"-");//用车内容
					resMap.put("position",StringUtils.isNoneBlank(carOrder.getPosition())? carOrder.getPosition():"-");//病人体位
					String patientSex="-";
					if(carOrder.getPatientSex()!=null){
						if(carOrder.getPatientSex()==0){
							patientSex="女";
						}
						if(carOrder.getPatientSex()==1){
							patientSex="男";
						}
					}
					resMap.put("patientSex", patientSex);//患者性别
					resMap.put("personLiable", StringUtils.isNoneBlank(carOrder.getPersonLiable())?carOrder.getPersonLiable():"-");//责任人
					resMap.put("patientAge", carOrder.getPatientAge()!=null?carOrder.getPatientAge():"-");//患者年龄
					resMap.put("patientIdNumber", StringUtils.isNoneBlank(carOrder.getPatientIdNumber())?carOrder.getPatientIdNumber():"-");//身份证号
					String carTypeStr="-";
					if(carOrder.getCarType()==2){
						carTypeStr="急诊车";
					}
					if(carOrder.getCarType()==1){
						carTypeStr="转诊车";
					}
					resMap.put("carTypeStr", carTypeStr);//车辆类型
					String  processDepartment="-";
					if(carOrder.getProcessDepartmentId()!=null){
						GroupInfo vehicleGroup=groupService.findOne(carOrder.getProcessDepartmentId());
						processDepartment=vehicleGroup.getGroupName();
					}
					resMap.put("processDepartment", processDepartment);//处理科室
					resMap.put("startLocation", StringUtils.isNoneBlank(carOrder.getStartLocation())?carOrder.getStartLocation():"-");//出发地点
					resMap.put("arriveLocation", StringUtils.isNoneBlank(carOrder.getArriveLocation())?carOrder.getArriveLocation():"-");//到达地点
					resMap.put("planStartTime", carOrder.getPlanStartTime()!=null?DateUtils.date2LongStr(carOrder.getPlanStartTime()):"-");//计划发车时间
					//处理历史
					List<MyProcessed> historyList= MyProcessImpl.findByInstanceId(instanceId);
					for(MyProcessed m:historyList){
						if("1".equals(m.getTaskName())){//已派遣
							UserInfo driverUser=userService.findOne(m.getNextProcesserId());
							m.setDriverName(driverUser.getUserName());
							m.setDriverPhone(driverUser.getPhone()==null?"-":driverUser.getPhone());
							m.setDriverWorkNo(driverUser.getWorkNo()==null?"-":driverUser.getWorkNo());
							if(m.getCarId()!=null){
								Car car=carService.findOne(m.getCarId());
								m.setCarNo(car.getCarNo());
								m.setCarPhone(StringUtils.isNoneBlank(car.getCarPhone())?car.getCarPhone():"-");
								String carType="-";
								if(car.getCarType()==1){
									carType="转诊车";
								}else{
									carType="急诊车";
								}
								m.setCarTypeStr(carType);
							}
							
						}
						UserInfo userInfo=userService.findOne(m.getProcesserId());
						if(userInfo!=null){
							m.setWorkNo(StringUtils.isNoneBlank(userInfo.getWorkNo())?userInfo.getWorkNo():"-");
							m.setProcesserName(userInfo.getUserName());
							String taskName = m.getTaskName();
							String statusName = "";
							if("0".equals(taskName)){
								statusName = "派车工单";
							}
							if("1".equals(taskName)){
								statusName = "已派遣";
							}
							if("2".equals(taskName)){
								statusName = "已发车";
							}
							if("3".equals(taskName)){
								statusName = "已回场";
							}
							if("5".equals(taskName)){
								statusName = "确认到场";
							}
							if("6".equals(taskName)){
								statusName = "派遣撤销";
							}
							if("7".equals(taskName)){
								statusName = "已到达";
							}
							m.setStatusName(statusName);
							m.setEndTimeStr(DateUtils.date2LongStr(m.getEndTime()));
						}
					}
					resMap.put("historyList", historyList);//处理历史
					Car car = carOrder.getCar();
					resMap.put("carNo", car!=null?car.getCarNo():"-");//车牌号
					resMap.put("carPhone", car!=null?car.getCarPhone():"-");//车电话
					String driverStr="-";
					Driver driver = carOrder.getDriver();
					if(driver!=null){
						UserInfo driverUser=userService.findOne(driver.getUserId());
						driverStr=driverUser.getUserName()+"【"+driverUser.getWorkNo()+"】";
					}
					resMap.put("driverStr", driverStr);//驾驶人
					resMap.put("driverPhone", driver!=null?driver.getPhone():"-");//驾驶人电话
					Gson gson=new Gson();
					ajax(gson.toJson(resMap));
				}
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
		
	}
	/*
	 * 获得司机姓名列表
	 */
	public String findDriverNameList(){
		try {
				List<Driver> list =driverSerivce.findDriverName(1, serachKey);
				Map<Integer,Integer> map=carOrderService.getDriverOrderNum(1);
				if(list!=null && list.size()>0){
					for(Driver d:list){
						if(map.containsKey(d.getUserId())){
							Integer num=map.get(d.getUserId());
							if(num>0){
								d.setStatuStr("忙碌");
							}
						}else{
							d.setStatuStr("空闲");
						}
					}
				}
				ajax(list);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	/**
	 * 获得司机列表
	 * @return
	 */
	public String  findDriverList(){
		try {
				Sort sort = new Sort(Direction.ASC,"status");
				Pageable pageable=this.getPageable(sort);
				Page<Driver> page =driverSerivce.findDriver(pageable, 1);
				Map<Integer,Integer> map=carOrderService.getDriverOrderNum(1);
				List<Driver>list=page.getContent();
				if(list!=null && list.size()>0){
					int i = 1;
					if(pageable!=null){
		    			i = pageable.getPageNumber() * pageable.getPageSize() + 1;
		    		}
					for(Driver d:list){
						d.setNo(i);
						if(map.containsKey(d.getUserId())){
							Integer num=map.get(d.getUserId());
							if(num>0){
								d.setStatuStr("忙碌");
							}
						}else{
							d.setStatuStr("空闲");
						}
						i++;
					}
				}
				Map<String, Object> jsonMap = new HashMap<String, Object>();
				jsonMap.put("iTotalRecords", page.getTotalElements());
				jsonMap.put("iTotalDisplayRecords", page.getTotalElements());
				jsonMap.put("aaData", list);
				Gson gson = new Gson();
				ajax(gson.toJson(jsonMap));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	/**
	 * 获得车辆列表
	 * @return
	 */
	public String  findCarList(){
		try {
				Sort sort=new Sort(Direction.ASC,"status");
				Pageable pageable=this.getPageable(sort);
				Map<Integer,Integer> map=carOrderService.getCarOrderNum(1);
				Page<Car> page =carService.findCarList(pageable, 1,carType);
				List<Car>list=page.getContent();
				if(list!=null && list.size()>0){
					int i = 1;
					if(pageable!=null){
		    			i = pageable.getPageNumber() * pageable.getPageSize() + 1;
		    		}
					for(Car c:list){
						c.setNo(i);
						if(map.containsKey(c.getId())){
							Integer num=map.get(c.getId());
							if(num>0){
								c.setStatusStr("忙碌");
							}
						}else{
							c.setStatusStr("空闲");
						}
						if(!StringUtils.isNoneBlank(c.getCarPhone())){
							c.setCarPhone("-");
						}
						i++;
					}
				}
				Map<String, Object> jsonMap = new HashMap<String, Object>();
				jsonMap.put("iTotalRecords", page.getTotalElements());
				jsonMap.put("iTotalDisplayRecords", page.getTotalElements());
				jsonMap.put("aaData", list);
				Gson gson = new Gson();
				ajax(gson.toJson(jsonMap));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	/**
	 *  groupType    1 车管科 2 临床
	 * 获得科室Map
	 * @return
	 */
	public Map<Integer,String> findGroupNameMap(Integer groupType){
		List<GroupInfo> groupList = null;
		if(groupType!=null&&groupType.intValue()==1){
			groupList = groupService.findAll(" companyId=1 and branch='1'");
		}else{
			groupList = groupService.getGroupListByeExtension(1, 1, "3",null);
		}
		Map<Integer,String> map = new HashMap<Integer, String>();
		if(groupList!=null &&groupList.size()>0){
			for(GroupInfo g:groupList){
				map.put(g.getGroupId(), g.getGroupName());
			}
		}
		return map;
	}
	/**
	 * 获得转诊类型
	 * @param businessType
	 * @return
	 */
	private static String findBusinessTypeStr(Integer businessType){
		if(businessType==1){
			return "上转住院";
		}else if(businessType==2){
			return "上转检查";
		}else if(businessType==3){
			return "上转门诊";
		}else if(businessType==4){
			return "下转住院";
		}else if(businessType==5){
			return "急危重抢救";
		}else if(businessType==6){
			return "标本收取 ";
		}else if(businessType==7){
			return "转运病人 ";
		}else if(businessType==8){
			return " 会诊安排";
		}else if(businessType==9){
			return "物资调配";
		}else{
			return "其他";
		}
	}
	/**
	 * 工单状态
	 */
	private static String getStateStr(Integer state){
		if(state==1){
			return "待派遣";
		}else if(state==2){
			return "待发车";
		}else if(state==3){
			return "已发车";
		}else if(state==7){
			return "已到达";
		}else{
			return "已完结";
		}
	}
	
	
	public CarOrderVo getCarOrderVo() {
		return carOrderVo;
	}
	public void setCarOrderVo(CarOrderVo carOrderVo) {
		this.carOrderVo = carOrderVo;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public Integer getCarType() {
		return carType;
	}

	public void setCarType(Integer carType) {
		this.carType = carType;
	}

	public String getSerachKey() {
		return serachKey;
	}

	public void setSerachKey(String serachKey) {
		this.serachKey = serachKey;
	}
	
	
	
	
	
}
