package cn.com.qytx.hemei.cardispatch.service;

import java.util.Map;

import cn.com.qytx.hemei.cardispatch.domain.Car;
import cn.com.qytx.hemei.cardispatch.domain.CarOrder;
import cn.com.qytx.hemei.cardispatch.domain.CarOrderVo;
import cn.com.qytx.hemei.cardispatch.domain.Driver;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;
import cn.com.qytx.platform.base.service.BaseService;

/**
 * 功能：车辆调度工单业务接口
 * 版本：1.0
 * 开发人员: pb
 * 创建日期：2017年9月7日
 * 修改日期：2017年9月7日	
 */
public interface ICarOrder extends BaseService<CarOrder> {

	/**
	 * 功能：车辆申请接口
	 * @param carOrder
	 * @return
	 */
	public String addCarOrder(CarOrder carOrder);
	/**
	 * 功能：车辆申请接口 直接派遣
	 * @param carOrder
	 * @return
	 */
	public String dispatchAddCarOrder(CarOrder carOrder);
	
	
	/**
	 * 功能：车辆审批接口
	 * @param userId 用户Id
	 * @param instanceId 流程实例
	 * @param approveResult 
	 * @param advice 意见
	 * @param taskName 
	 * @param nextProcessId 下一步处理人
	 * @param processType  处理类型
	 * @param driver  司机
	 * @param car   车辆
	 * @return
	 */
	public int approve(Integer userId, String instanceId, String approveResult, String advice, String taskName, Integer nextProcessId,Integer processType,Driver driver,Car car);
	
	
	/**
	 * 查看工单通过实例编号
	 * @Title: findByInstanceId   
	 * @param InstanceId
	 * @return
	 */
	public CarOrder findByInstanceId(String instanceId);
	
	/**
	 * 查询车辆流转列表
	 * @return
	 */
	public Page<CarOrder>  findCarDispatch(Pageable pageable,Integer userId,CarOrderVo carOrderVo);

	/**
	 * 查询待发车车辆流转列表
	 * @return
	 */
	public Page<CarOrder>  findMyWaitCarDispatch(Pageable pageable,Integer userId,CarOrderVo carOrderVo);

	/**
	 * 查询已办车车辆流转列表
	 * @return
	 */
	public Page<CarOrder>  findMyProcessedCarDispatch(Pageable pageable,Integer userId,CarOrderVo carOrderVo);
	
	/**
	 * 功能：车辆工单待办数量
	 * @param companyId
	 * @param userId
	 * @return
	 */
	public Map<String,Integer> getCarOrderWaitNum(Integer companyId,Integer userId,Integer groupId);

	/**
	 * 查询所车正在执行的工单数
	 * @return
	 */
	public Map<Integer,Integer> getCarOrderNum(Integer companyId);
	
	/**
	 * 查询司机正在执行的工单数
	 * @return
	 */
	public Map<Integer,Integer> getDriverOrderNum(Integer companyId);
	
}
