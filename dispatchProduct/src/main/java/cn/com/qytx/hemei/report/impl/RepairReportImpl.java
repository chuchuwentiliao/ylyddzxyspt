/**
 * 
 */
package cn.com.qytx.hemei.report.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.com.qytx.hemei.defect.domain.DefectApply;
import cn.com.qytx.hemei.report.dao.RepairReportDao;
import cn.com.qytx.hemei.report.service.IRepairReport;
import cn.com.qytx.platform.base.service.impl.BaseServiceImpl;
import cn.com.qytx.platform.org.dao.GroupDao;
import cn.com.qytx.platform.org.domain.GroupInfo;

/**
 * 功能: 
 * 版本: 1.0
 * 开发人员: 王刚
 * 创建日期: 2017年5月9日
 * 修改日期: 2017年5月9日
 * 修改列表: 
 */
@Service
@Transactional
public class RepairReportImpl extends BaseServiceImpl<DefectApply> implements IRepairReport {
   
	@Autowired
	private RepairReportDao repairReportDao;
	
   @Autowired
   private GroupDao<GroupInfo> groupDao;
	
	@Override
	public List<Object[]> findRepairTopTen(String beginTime,String endTime) {
		// TODO Auto-generated method stub
		return repairReportDao.findRepairTopTen(beginTime,endTime);
	}

	@Override
	public List<Object[]> findRepairEventType(String beginTime, String endTime) {
		// TODO Auto-generated method stub
		return repairReportDao.findRepairEventType(beginTime, endTime);
	}
	@Override
	public List<Object[]> findSatisfactionList(String beginTime, String endTime,String employeeName) {
		// TODO Auto-generated method stub
		return repairReportDao.findSatisfactionList(beginTime, endTime,employeeName);
	}

	@Override
	public List<Object[]> findMedicalDepartmenList(String beginTime,
			String endTime,String departmentName) {
		// TODO Auto-generated method stub
		return repairReportDao.findMedicalDepartmenList(beginTime, endTime,departmentName);
	}
	@Override
	public List<Object[]> findRepairPeople(String beginTime, String endTime,
			String userName, String departmentName,String instanceIds) {
		// TODO Auto-generated method stub
		return repairReportDao.findRepairPeople(beginTime, endTime, userName, departmentName,instanceIds);
	}

	@Override
	public Map<Integer, String> finshMap(String beginTime, String endTime,String instanceIds) {
		// TODO Auto-generated method stub
		
		Map<Integer,String> findListMap = repairReportDao.findListMap(beginTime, endTime, instanceIds);
		return findListMap;
	}
	

	@Override
	public List<Object[]> findTakeWorkList(String beginTime, String endTime) {
		// TODO Auto-generated method stub
		return repairReportDao.findTakeWorkList(beginTime, endTime);
	}

	
	

}
