<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<jsp:include page="../../common/flatHead.jsp" />
	<link href="../../images/favicon.ico" mce_href="favicon.ico" rel="bookmark" type="image/x-icon" /> 
<link href="../../images/favicon.ico" mce_href="favicon.ico" rel="icon" type="image/x-icon" /> 
<link href="../../images/favicon.ico" mce_href="favicon.ico" rel="shortcut icon" type="image/x-icon" /> 
	<title>郑州市中心医院后勤调度系统</title>
	<link rel="stylesheet" href="../css/style.css">
	<link href="../plugins/datatable/skins/datatable_default.css" rel="stylesheet" type="text/css"/>
	<link href="../css/affairs.css" rel="stylesheet" type="text/css"/>
	<script type="text/javascript" src="../js/jquery.min.js"></script>
	<script type="text/javascript" src="../js/divselect.js"></script>
	
	<script type="text/javascript">
	$(function(){
		$(".bottom_nav nav ul li").hover(function(){
			$(this).find("span").css("background","")
			$(this).find(".second_menu").removeClass("hide");
		},function(){
			$(this).find(".second_menu").addClass("hide");
		})
		$(".be_br").click(function(){
			$(this).css("background","url(../images/top_ico.png) no-repeat 30px center");
			$(".be_busy").removeClass("hide")
			
		})
		$(".be_busy").click(function(){
			$(".be_busy").css("top","20px");
			$(".be_busy").css("background","#0c5bc4 url(../images/down_ico.png) no-repeat 30px center");
		})

		/*计算导航高度*/
		var Wh = $(window).height();
		var Ch = Wh-50;
		$(".center").css({height:Ch+"px"});
		$(".nav").css({height:Ch+"px"});
		/*出现二级菜单*/
		$(".first_menu a").click(function(){
			$(this).siblings(".second_menu").toggle('solw');
			$(this).parent().addClass('active');
			$(this).parent().siblings('li').find(".second_menu").hide();
			$(this).parent().siblings('li').removeClass('active')
			//delayTime($(this));
			
		});
		$.divselect("#divselect","#inputselect");
		
		
		var firstLi = $("#s_menu").children("li").eq(0);
		if(firstLi){
			var firstA = firstLi.children("a");
			firstA.trigger("click");
			if(firstLi.find(".second_menu li").length > 0){
				firstA.css({"background":"url(../../hotlineframe/images/qy_ui_bottom.png) no-repeat 150px"});
				var firstSecond = $(".sencond_menu_list").eq(0);
				firstSecond.addClass("active");
				firstSecond.find(".second_text").trigger("click");
			}
		}
	})
	
	function delayTime(obj){
		setTimeout(function(){
			var style = $(obj).siblings('ul').css("display");
			if(style=="none"){
				$(obj).css("background","url(../images/qy_ui_right_h.png) no-repeat 150px center ");
			}
		},500);
	}
	</script>
</head>
<body>
<input type="hidden" id="workNo" value="${adminUser.workNo}"/>
	<div class="header">
		<div class="top_nav">
			<!-- <img src="../images/logo.png" alt=""> -->
			<img src="../images/logo.png" alt="">
			<div class="box_manage">
				<span class="fr exBot" style="margin-top:15px">
				<em title="事务提醒" class="icon_work"   onclick="openAffairs();" id='affairsEm' style="height:30px;"></em> 
		</span> 
				<span class="peo_name">欢迎您，${adminUser.userName }</span>
				<span class="godead" onclick="javascript:window.location.href='<%=request.getContextPath() %>/s/logout'">退出</span>
			</div>
		</div>
		
	</div>
	<div class="center">
		
		<div class="nav">
			<ul id="s_menu">
				<c:forEach items="${moduleList}" var="m" >
				<c:if test="${m.moduleLevel==1 }">
					<li class="first_menu ${m.moduleName=='首页'||m.moduleName=='通讯录'?'no_bg':''}">
						<a class="first_text" onclick="go('${m.url}')"><i class="qy_ui ${m.moduleClass }"></i>${m.moduleName }</a>
						
						<ul class="second_menu">
						<c:forEach items="${moduleList }" var="n">
							<c:if test="${n.moduleLevel==2 && n.parentId == m.moduleId }">
								<li class="sencond_menu_list ">
									<a class="second_text" onclick="go('${n.url}')">${n.moduleName }</a>
								</li>
							</c:if>
						</c:forEach>
						
					</ul>
					</li>
				</c:if>
				</c:forEach>
				
			</ul>
		</div>
		<iframe src="" id="content" frameborder="0" width="100%" height="100%"></iframe>
		
		<!-- <div class="canverse_right_tap canverse_right_tap_new">
			<span class="active">维修登记</span>
			<span>配送登记</span> 
			<span>归还登记</span>
		</div> -->
		<!-- end .canverse_right_tap -->
		
	</div>
	
	
	<div id="newAffairsAlert" class="news_pop" style="display:none;">
			   <div class="top_news"><h3 id="affairsNum">事务提醒（4）<a href="javascript:void(0);" class="close" onclick="closeLittleDiv(event)"></a></h3></div>
			   <div class="center_news">
			       <p>您有新的提醒，请注意查收！</p>
			       <div class="btnShowMsg"><input name="" type="button" value="点击查看"  onclick="openNewAffairs()"  /></div>
			   </div>
			</div>
	<div id="newMessageDIV" style="display: none"></div>
	<!--底部信息
	<div class="bottom" onclick="jQuery(top.document).find('.menu').hide();" >
		<span class="fr exBot">
			<em title="事务提醒" class="icon_work"   onclick="openAffairs();" id='affairsEm' ></em> 
		</span> 
		版权所有：北京全亚通信技术有限公司
    </div>-->
	
</body>
<script type="text/javascript" src="../js/affairs.js"></script>
<script type="text/javascript">
	function go(url){
		if(url){
			$("#content").attr("src","<%=request.getContextPath()%>"+url);
		}
	}
</script>
</html>