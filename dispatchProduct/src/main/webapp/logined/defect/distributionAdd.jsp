	<%@page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html ng-app="distributionAddApp">
	<head>
		<meta charset="UTF-8">
		<title>配送登记</title>
		<jsp:include page="../../common/flatHead.jsp" />
		<%
			String type = (String)request.getParameter("type");
		%>
		
		<link rel="stylesheet" href="${ctx}flat/plugins/tree/skins/tree_default.css" type="text/css"/>
		<link href="${ctx }flat/plugins/form/skins/form_default.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="${ctx}flat/css/hm_style.css" />
		<link rel="stylesheet" href="${ctx}/css/jquery.autocomplete.css" />
		<style type="text/css">
			.hello{display:block;background-color: red}
			.uploadify-button {background-repeat:no-repeat;background-position:center left;}
			.annex ul li{float:left;margin-right:10px;}
			.txt p a{color: #ff5454 !important;}
			.selectLocation:hover{background-color: #e6e6e6}
			
			.new_right_content.no-padding {
					padding:0;
					background: none;
			}
		</style>
		<script type="text/javascript">
			var basePath = "${ctx}";
			var baseUrl="${baseUrl}";
			$(function(){
				$(".new_right_content").css({"height":$(window).height()});
			});
		</script>
		<script TYPE="text/javascript" src="${ctx}angularframe/lib/angular.min.js"></script>
		<script type="text/javascript" src="${ctx}plugins/tree/ztree/jquery.ztree.all-3.5.min.js?version=${version}"></script>
		<script TYPE="text/javascript" src="${ctx }angularframe/directives/ui-basetree.js"></script>
		<script TYPE="text/javascript" src="${ctx }angularframe/directives/ui-uploadify.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/app/distributionAddApp.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/controller/distributionAddController.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/controller/selectUser.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/filter/commonFilter.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/service/service.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/autocomplete/jquery.autocomplete.js"></script>
	</head>
	<body ng-controller="distributionAddController">
		<input type="hidden" value="<%=request.getParameter("instanceId")%>" id="instanceId">
		<input type="hidden" value="<%=type%>" id="type">
		<form novalidate="novalidate" name="myForm">
		
		<div class="new_right_content no-padding">
			<div class="index_detail pdb8" style="margin-top: 30px;">
				<%-- <p class="new_index_title mb20">
					<%if("1".equals(type)){%>配送上报<%}else{%>归还上报<%} %>
					
				</p> --%>
		<!-- <div>
			<p class="describle_title">缺陷信息</p> -->
				<ul class="describle_info describle_info_two" style="overflow:initial;">
					<!-- <li class="pr fl" style="overflow: inherit;">
						<span class="des_info_title">设备名称：</span>
						<input type="text" class="in_area" ng-model="maintenanceItems" id="maintenanceItems"  style="width: 258px"/>
					</li> -->
					
					<li class="pr fl" style="overflow: inherit;">
						<span class="des_info_title">设备名称：</span>
						<!-- <input type="text" class="in_area ng-pristine ng-untouched ng-valid ng-empty" ng-model="maintenanceItems" id="maintenanceItems" style="width: 258px"> -->
						<div id="divselect3" class="divselect4" style="width: 258px;position: relative;" >
			                <input type="text" class="select_style wd258" ng-model="maintenanceItems" id="maintenanceItems" ng-keyup="refresh()" style="border: #d9d9d9 1px solid;"/>
			                <ul style="display:none;position: absolute;background-color: #fff;width: 255px;z-index:10;border: 1px solid #d9d9d9;border-top:none;height: 200px;overflow: auto" id="locationListUl">
			                    <li ng-repeat="capitalGroup in capitalGroupList" style="height: 20px;padding:0 5px;cursor: pointer" class="selectLocation" ng-click="selectP(capitalGroup.id,capitalGroup.groupName)" ><a href="javascript:;" selectid="{{capitalGroup.groupName}}"  style="color: #333;font-size: 14px;">{{capitalGroup.groupName}}</a></li>
			                </ul>
           				 </div>
           				 <input type="hidden"  ng-model="capitalGroupId"/>
					</li>
					
					<li class="pr fl" style="overflow: inherit;">
						<span class="des_info_title">设备数量：</span>
						<input type="text" class="in_area"  id="equipmentNumber" style="width: 258px" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"/>
					</li>
					<li class="pr"  >
						<span class="des_info_title">处理部门：</span>
						<input type="hidden" id="processId" value="61"/>
						<input type="hidden" id="showType" value="1"/>
						<input type="hidden" id="processType" value="2">
						<input type="hidden" id="extension" value="1,2"/>
			  			<input class="in_area" type="text" id="processName" readonly="readonly" style="width: 258px" value="调度中心">
			  			<span class="addMember"><a
							class="icon_add" href="#" id="userSelect" fieldId="processId" fieldName="processName" fileType="processType" showType="showType" extension="extension">选择</a></span>
					</li>
					<!-- <li class="pr percent33">
						<span class="des_info_title">事件等级：</span>
						<select name="grade" class="defect_select wd258" ng-model="apply.grade" required ng-change="changeGrade();">
							<option value="">请选择</option>
							<option value="{{grade.value}}" ng-repeat="grade in gradeList">{{grade.name}}</option>
						</select>
						<p style="color:red;" class="error" ng-show="myForm.grade.$error.required&&(!myForm.grade.$pristine||submitted)">请选择事件等级</p>
					</li> -->
					<li class="pr" >
						<span class="des_info_title"><!-- <em class="requireField">*</em> -->上报科室：</span>
						<input type="hidden" id="createGroupId" value="${sessionScope.groupInfo.groupId }"/>
						<input type="hidden" id="showType" value="1"/>
			  			<input class="in_area" type="text" id="createGroupName" readonly="readonly" style="width: 258px" value="${sessionScope.groupInfo.groupName }">
			  			<span class="addMember"><a
							class="icon_add" href="#" id="createGroupSelect" fieldId="createGroupId" fieldName="createGroupName"  showType="showType" >选择</a></span>
					</li>
					<li class="pr">
						<span class="des_info_title">使用地点：</span>
						<input type="hidden" id="repaireId" value="61"/>
						<input type="hidden" id="showType_repair" value="5"/>
			  			<input class="in_area" type="text" id="describe"  style="width: 258px">
			  			<span class="addMember"><a
							class="icon_add" href="#" id="repaireSelect" fieldId="repaireId" fieldName="describe" showType="showType_repair">选择</a></span>
					</li> 
					<li class="pr">
						<span class="des_info_title">备注：</span>
						<textarea class="defect_inword" ng-model="apply.memo" style="font-family: -webkit-body;width: 50%"></textarea>
					</li>
	                <!-- <li class="pr" style="display: none">
						<span class="des_info_title">图片：</span>
						<span class="get_photo">
							<uploadfs class="add_file" config="uploadConfig" outfile="outfile" fs="fs" ></uploadfs>
						</span>
					</li> -->
					<li class="pr">
						<span class="des_info_title">紧急程度：</span>
						<label ng-repeat="a in gradeList" style="display: block;float: left;margin-top: 8px;margin-right:40px;">
						   <input  ng-model="apply.grade"  name="eventLevel" type="radio" value="{{a.value}}"  ng-click="chageRadio(a.value)"/>&nbsp;{{a.name}} 
						</label>
					</li>
					<li class="pr" style="display: none"> 
						<span class="des_info_title"><em class="requireField">*</em>完工时间：</span> 
						<!-- <input class="in_area percent30" type="text" name="defectTimeStr" value="" placeholder="" ng-model="apply.defectTimeStr" ng-readonly="true" required>
						<input type="text" name="defectTime" value="" placeholder="" ng-model="apply.defectTime" ng-show="1==0"> -->
						  <input type="text" name="defectTimeStr" id="defectTime" onFocus="WdatePicker({skin:'default',dateFmt:'yyyy-MM-dd HH:mm:ss'})" class="in_area Wdate" style="width:258px;background-position-x:230px;cursor:pointer;"/>
						<!-- <p style="color:red;" class="error" ng-show="myForm.defectTimeStr.$error.required&&(!myForm.defectTimeStr.$pristine||submitted)">请输入期望消缺时间</p> -->
					 </li> 
	
				<!-- 	<li class="pr">
						<span class="des_info_title">是否大修：</span>
						<div class="raido_area">
							<label><input type="radio" name="isRepair" value="1" ng-model="apply.isRepair"> 是</label>
							<label><input type="radio" name="isRepair" value="0" ng-model="apply.isRepair"> 否</label>
						</div>
					</li>
					 -->
					
				</ul>
			<!-- <p class="describle_title">设备信息</p>
			<ul class="describle_info">
				<li class="pr">
					<span class="des_info_title">设备名称：</span>
					<input class="in_area percent30" type="text" name="equipmentName" value="" placeholder="" ng-model="apply.equipmentName" required>
					<p style="color:red;" class="error" ng-show="myForm.equipmentName.$error.required&&(!myForm.equipmentName.$pristine||submitted)">请输入设备名称</p>
				</li>
				<li class="pr">
					<span class="des_info_title">设备编号：</span>
					<input class="in_area percent30" type="text" name="equipmentNumber" value="" placeholder="" ng-model="apply.equipmentNumber" required>
					<p style="color:red;" class="error" ng-show="myForm.equipmentNumber.$error.required&&(!myForm.equipmentNumber.$pristine||submitted)">请输入设备编号</p>
				</li>
				<li class="pr">
					<span class="des_info_title">设备状态：</span>
					<select name="equipmentState" class="defect_select percent30" ng-model="apply.equipmentState" required>
						<option value="">请选择</option>
						<option value="{{equipmentState.value}}" ng-repeat=" equipmentState in equipmentStateList">{{equipmentState.name}}</option>
					</select>
					<p style="color:red;" class="error" ng-show="myForm.equipmentState.$error.required&&(!myForm.equipmentState.$pristine||submitted)">请选择设备状态</p>
				</li>
				
			</ul>
			<p class="describle_title">其他信息</p>
			<ul class="describle_info">
				<li class="pr">
					<span class="des_info_title">消缺人员：</span>
					<button class="select_peo" ng-click="toggleSelectUser()" ng-init="showSelectUserValue=2">{{node1 | selectUserNames}}</button>
		      		<ul id="tree1" class="ztree leibie" basetree tree-data="data1" tree-set="set1" check-node="node1" select-node="node" default-select="defaultSelectUser" ng-show="showSelectUserValue==1" style="display: block;height:200px !important;width:200px !important;" ng-mouseleave="showSelectUserValue=2">
					</ul>
				</li>
				<li class="pr">
					<span class="des_info_title">期望消缺时间：</span>
					<input class="in_area percent30" type="text" name="defectTimeStr" value="" placeholder="" ng-model="apply.defectTimeStr" ng-readonly="true" required>
					<input type="text" name="defectTime" value="" placeholder="" ng-model="apply.defectTime" ng-show="1==0">
					<p style="color:red;" class="error" ng-show="myForm.defectTimeStr.$error.required&&(!myForm.defectTimeStr.$pristine||submitted)">请输入期望消缺时间</p>
				</li>
				<li class="pr">
					<span class="des_info_title">备注：</span>
					<input class="in_area percent30" type="text" name="" value="" placeholder="" ng-model="apply.memo">
				</li>
			</ul> -->
			<!-- <div class="btn_area">
				<span class="sure" ng-click="addApply(myForm.$valid)" style="cursor:pointer">上报</span>
			</div> -->
			<div class="btn_container01" style="margin-top: 30px;">
				<span class="btn_inline_new hm_btn_blue"  ng-click="addApply(myForm.$valid)">{{instanceId?'提交':'提交'}}</span>
			</div>
		</div>
		</div>
		</form>
	</body>
	<script type="text/javascript">
	  	$(function(){
	  		$(document).click(function(){
	  			$("#locationListUl").hide();
	  		});
	  	});
  </script>
</html>
