	<%@page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html ng-app="dispatchAddApp">
	<head>
		<meta charset="UTF-8">
		<title>调度缺陷</title>
		<jsp:include page="../../common/flatHead.jsp" />
		<link rel="stylesheet" href="${ctx}flat/plugins/tree/skins/tree_default.css" type="text/css"/>
		<link href="${ctx }flat/plugins/form/skins/form_default.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="${ctx}flat/css/hm_style.css" />
		<link rel="stylesheet" href="${ctx}/css/jquery.autocomplete.css" />
		<style type="text/css">
			.hello{display:block;background-color: red}
			.uploadify-button {background-repeat:no-repeat;background-position:center left;}
			.annex ul li{float:left;margin-right:10px;}
			.txt p a{color: #ff5454 !important;}
			.selectLocation:hover{background-color: #e6e6e6}
		</style>
		<script type="text/javascript">
			var basePath = "${ctx}";
			var baseUrl="${baseUrl}";
			$(function(){
				$(".new_right_content").css({"height":$(window).height()});
			});
		</script>
		<script TYPE="text/javascript" src="${ctx}angularframe/lib/angular.min.js"></script>
		<script type="text/javascript" src="${ctx}plugins/tree/ztree/jquery.ztree.all-3.5.min.js?version=${version}"></script>
		<script TYPE="text/javascript" src="${ctx }angularframe/directives/ui-basetree.js"></script>
		<script TYPE="text/javascript" src="${ctx }angularframe/directives/ui-uploadify.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/app/dispatchAddApp.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/controller/dispatchAddController.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/controller/selectUser.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/filter/commonFilter.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/service/service.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/autocomplete/jquery.autocomplete.js"></script>
	</head>
	<body ng-controller="dispatchAddController">
		<input type="hidden" value="<%=request.getParameter("instanceId")%>" id="instanceId">
		<form novalidate="novalidate" name="myForm">
		
		<div class="new_right_content">
			<div class="index_detail pdb8 wd1362">
				<p class="new_index_title mb20">
					调度上报
				</p>
		<!-- <div>
			<p class="describle_title">缺陷信息</p> -->
				<ul class="describle_info" style="overflow:initial;">
				
					<li class="pr percent30 fl" style="overflow: inherit;">
						<span class="des_info_title">上报人工号：</span>
						<div id="divselect_user" class="divselect4" style="width: 258px;position: relative;" ng-mouseleave="hideUserUrl()">
			                <input type="text" class="select_style wd258" ng-model="loginName" id="loginName" ng-keyup="dispatchRefresh()" style="border: #d9d9d9 1px solid;"/>
			                <ul style="display:none;position: absolute;background-color: #fff;width: 255px;z-index:10;border: 1px solid #d9d9d9;border-top:none;height: 100px;overflow: auto">
			                    <li ng-repeat="user in userList" style="height: 20px;padding:0 5px;cursor: pointer" class="selectLocation" ng-click="selectUser(user.userId,user.loginName,user.userName,user.groupId,user.groupName)" ><a href="javascript:;" selectid="{{user.userId}}"  style="color: #333;font-size: 14px;">{{user.loginName}}({{user.userName}})</a></li>
			                </ul>
           				 </div>
					</li>
					<li class="pr percent30 fl" >
						<span class="des_info_title" style="width: 120px;" >上报人姓名：</span>
						<span  style="width: 120px;float:left; margin-top: 4px" id="dispatchUserName">--</span>
						<input type="hidden" id="userId"/>
			  			<!-- <input class="in_area" type="text" id="dispatchUserName" readonly="readonly" style="width: 258px" value="调度中心"> -->
					</li>
					
					<li class="pr percent30 fl" >
						<span class="des_info_title" style="width: 120px;" >上报人部门：</span>
						<span  style="width: 120px;float:left; margin-top: 4px" id="dispatchGroupName">--</span>
			  			<!-- <input class="in_area" type="text" id="dispatchGroupName" readonly="readonly" style="width: 258px" value="调度中心"> -->
					</li>
				
					
					
					<li class="pr fl" style="overflow: inherit;">
						<span class="des_info_title">维修事项：</span>
						<!-- <input style="padding:0;text-indent:10px;outline:none;"   ng-model="apply.maintenance"  id="maintenanceItems" class="in_area" type="text" name="maintenance" value=""  required>
						<p style="color:red;" class="error" ng-show="myForm.maintenance.$error.required&&(!myForm.maintenance.$pristine||submitted)">请输入维修事项</p> -->
						<div id="divselect3" class="divselect4" style="width: 258px;position: relative;" ng-mouseleave="hideLocationUrl()">
			                <input type="text" class="select_style wd258" ng-model="maintenanceItems" id="maintenanceItems" ng-keyup="refresh()" style="border: #d9d9d9 1px solid;"/>
			                <ul style="display:none;position: absolute;background-color: #fff;width: 255px;z-index:10;border: 1px solid #d9d9d9;border-top:none;height: 100px;overflow: auto">
			                    <li ng-repeat="event in eventList" style="height: 20px;padding:0 5px;cursor: pointer" class="selectLocation" ng-click="selectP(event.id,event.eventName,event.urgencyLevel,event.completeTimeType,event.groupId,event.groupName)" ><a href="javascript:;" selectid="{{event.id}}"  style="color: #333;font-size: 14px;">{{event.eventName}}</a></li>
			                </ul>
           				 </div>
						
						
					</li>
					<li class="pr fl" style="width: 36%!important;margin-right: 3%;" >
						<span class="des_info_title" style="width: 120px;" >维修科室/人员：</span>
						<input type="hidden" id="processId" value="61"/>
						<input type="hidden" id="processType" value="2"/>
						<input type="hidden" id="showType" value="4"/>
						<input type="hidden" id="extension" value="1,2"/>
			  			<input class="in_area" type="text" id="processName" readonly="readonly" style="width: 258px" value="调度中心">
			  			<span class="addMember"><a
							class="icon_add" href="#" id="userSelect" fieldId="processId" fieldName="processName" fileType="processType" showType="showType" extension="extension">选择</a></span>
					</li>
					<!-- <li class="pr percent33">
						<span class="des_info_title">事件等级：</span>
						<select name="grade" class="defect_select wd258" ng-model="apply.grade" required ng-change="changeGrade();">
							<option value="">请选择</option>
							<option value="{{grade.value}}" ng-repeat="grade in gradeList">{{grade.name}}</option>
						</select>
						<p style="color:red;" class="error" ng-show="myForm.grade.$error.required&&(!myForm.grade.$pristine||submitted)">请选择事件等级</p>
					</li> -->
					<li class="pr">
						<span class="des_info_title">维修地点：</span>
						<!-- <input class="in_area" type="text" name="describe" value=""  ng-model="apply.describe" required>
						<p style="color:red;" class="error" ng-show="myForm.describe.$error.required&&(!myForm.describe.$pristine||submitted)">请输入维修地点</p> -->
						<input type="hidden" id="repaireId" value="61"/>
						<input type="hidden" id="showType_repair" value="5"/>
			  			<input class="in_area" type="text" id="describe"  style="width: 258px">
			  			<span class="addMember"><a
							class="icon_add" href="#" id="repaireSelect" fieldId="repaireId" fieldName="describe" showType="showType_repair">选择</a></span>
						
					</li> 
					<li class="pr">
						<span class="des_info_title">备注：</span>
						<textarea class="defect_inword" ng-model="apply.memo" style="font-family: -webkit-body;"></textarea>
					</li>
	                <li class="pr">
						<span class="des_info_title">报修图片：</span>
						<!-- <em class="add_pic"></em> -->
						<span class="get_photo">
							<uploadfs class="add_file" config="uploadConfig" outfile="outfile" fs="fs" ></uploadfs>
						</span>
					</li>
					<li class="pr">
						<span class="des_info_title">紧急程度：</span>
						<label ng-repeat="a in gradeList">
						   <input  ng-model="apply.grade"  name="eventLevel" type="radio" value="{{a.value}}"  ng-click="chageRadio(a.value)"/>&nbsp;{{a.name}} 
						</label>
						<!-- <input name="eventLevel" type="radio" value="2" ng-click="chageRadio(2)"/>&nbsp;一般 
						<input name="eventLevel" type="radio" value="3" ng-click="chageRadio(3)"/>&nbsp;普通 -->
						
					</li>
					<!-- <li class="pr" style="height:42px;overflow:visible;">
						<span class="des_info_title">接收人员：</span>
						<input type="hidden" id="processId"/>
						<input type="hidden" id="processType"/>
			  			<input class="in_area" type="text" id="processName" readonly="readonly" style="width: 258px">
			  			<span class="addMember"><a
							class="icon_add" href="#" id="userSelect" fieldId="processId" fieldName="processName" fileType="processType">选择</a></span>
					</li> -->
	
					<li class="pr">
						<span class="des_info_title">期望完成时间：</span>
						<!-- <input class="in_area percent30" type="text" name="defectTimeStr" value="" placeholder="" ng-model="apply.defectTimeStr" ng-readonly="true" required>
						<input type="text" name="defectTime" value="" placeholder="" ng-model="apply.defectTime" ng-show="1==0"> -->
						 <input type="text" name="defectTimeStr" id="defectTime" onFocus="WdatePicker({skin:'default',dateFmt:'yyyy-MM-dd HH:mm:ss'})" class="in_area Wdate" style="width:258px;background-position-x:230px;cursor:pointer;"/>
						<!-- <p style="color:red;" class="error" ng-show="myForm.defectTimeStr.$error.required&&(!myForm.defectTimeStr.$pristine||submitted)">请输入期望消缺时间</p> -->
					</li>
	
				<!-- 	<li class="pr">
						<span class="des_info_title">是否大修：</span>
						<div class="raido_area">
							<label><input type="radio" name="isRepair" value="1" ng-model="apply.isRepair"> 是</label>
							<label><input type="radio" name="isRepair" value="0" ng-model="apply.isRepair"> 否</label>
						</div>
					</li>
					 -->
					
				</ul>
			<!-- <p class="describle_title">设备信息</p>
			<ul class="describle_info">
				<li class="pr">
					<span class="des_info_title">设备名称：</span>
					<input class="in_area percent30" type="text" name="equipmentName" value="" placeholder="" ng-model="apply.equipmentName" required>
					<p style="color:red;" class="error" ng-show="myForm.equipmentName.$error.required&&(!myForm.equipmentName.$pristine||submitted)">请输入设备名称</p>
				</li>
				<li class="pr">
					<span class="des_info_title">设备编号：</span>
					<input class="in_area percent30" type="text" name="equipmentNumber" value="" placeholder="" ng-model="apply.equipmentNumber" required>
					<p style="color:red;" class="error" ng-show="myForm.equipmentNumber.$error.required&&(!myForm.equipmentNumber.$pristine||submitted)">请输入设备编号</p>
				</li>
				<li class="pr">
					<span class="des_info_title">设备状态：</span>
					<select name="equipmentState" class="defect_select percent30" ng-model="apply.equipmentState" required>
						<option value="">请选择</option>
						<option value="{{equipmentState.value}}" ng-repeat=" equipmentState in equipmentStateList">{{equipmentState.name}}</option>
					</select>
					<p style="color:red;" class="error" ng-show="myForm.equipmentState.$error.required&&(!myForm.equipmentState.$pristine||submitted)">请选择设备状态</p>
				</li>
				
			</ul>
			<p class="describle_title">其他信息</p>
			<ul class="describle_info">
				<li class="pr">
					<span class="des_info_title">消缺人员：</span>
					<button class="select_peo" ng-click="toggleSelectUser()" ng-init="showSelectUserValue=2">{{node1 | selectUserNames}}</button>
		      		<ul id="tree1" class="ztree leibie" basetree tree-data="data1" tree-set="set1" check-node="node1" select-node="node" default-select="defaultSelectUser" ng-show="showSelectUserValue==1" style="display: block;height:200px !important;width:200px !important;" ng-mouseleave="showSelectUserValue=2">
					</ul>
				</li>
				<li class="pr">
					<span class="des_info_title">期望消缺时间：</span>
					<input class="in_area percent30" type="text" name="defectTimeStr" value="" placeholder="" ng-model="apply.defectTimeStr" ng-readonly="true" required>
					<input type="text" name="defectTime" value="" placeholder="" ng-model="apply.defectTime" ng-show="1==0">
					<p style="color:red;" class="error" ng-show="myForm.defectTimeStr.$error.required&&(!myForm.defectTimeStr.$pristine||submitted)">请输入期望消缺时间</p>
				</li>
				<li class="pr">
					<span class="des_info_title">备注：</span>
					<input class="in_area percent30" type="text" name="" value="" placeholder="" ng-model="apply.memo">
				</li>
			</ul> -->
			<!-- <div class="btn_area">
				<span class="sure" ng-click="addApply(myForm.$valid)" style="cursor:pointer">上报</span>
			</div> -->
			<div class="btn_container01 mt20 mb20" >
				<span class="btn_inline_new hm_btn_blue"  ng-click="addApply(myForm.$valid)">上报</span>
			</div>
		</div>
		</div>
		</form>
	</body>
</html>
