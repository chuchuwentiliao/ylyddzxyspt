<%@page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html ng-app="viewApp">
	<head>
		<meta charset="UTF-8">
		<title>上报缺陷</title>
		<jsp:include page="../../common/flatHead.jsp" />
		<link rel="stylesheet" href="${ctx}flat/plugins/tree/skins/tree_default.css" type="text/css"/>
		<link href="${ctx }flat/plugins/form/skins/form_default.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="${ctx}flat/css/hm_style.css" />
		<link rel="stylesheet" href="css/screen.css" />
		<link rel="stylesheet" href="css/step.css" />
		<link rel="stylesheet" href="${ctx}plugins/ngDialog/css/ngDialog.css">
	    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,400italic' rel='stylesheet' type='text/css'>
	    <link rel="stylesheet" href="${ctx}plugins/ngDialog/css/ngDialog-theme-default.css">
	    <link rel="stylesheet" href="${ctx}plugins/ngDialog/css/ngDialog-theme-plain.css">
	    <link rel="stylesheet" href="${ctx}plugins/ngDialog/css/ngDialog-custom-width.css">
		<script type="text/javascript">
			var basePath = "${ctx}";
			var baseUrl = "${baseUrl}";
			
			function printPage(){
				var instanceId = $("#instanceId").val();
				var url = basePath+"logined/defect/printWork.jsp?instanceId="+instanceId+"&workType=0";
				art.dialog.open(url,{
					id : "printWork",
				    title : "打印工单",
				    width : 800,
				    height : 520,
				    lock : true,
				    opacity: 0.1,// 透明度
				    button: [{
				    	name: '确定',
				    	focus: true,
				    	callback:function(){
				    		var iframe = this.iframe.contentWindow;
				    		iframe.printPage();
				    		return false;
				    	}
				    	},
				        {name: '关闭'}]
				});
			}
			
		</script>
		<script TYPE="text/javascript" src="${ctx}angularframe/lib/angular.min.js"></script>
		<script type="text/javascript" src="${ctx}plugins/tree/ztree/jquery.ztree.all-3.5.min.js?version=${version}"></script>
		<script TYPE="text/javascript" src="${ctx }angularframe/directives/ui-basetree.js"></script>
		<script src="${ctx}plugins/ngDialog/js/ngDialog.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/app/viewApp.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/controller/viewController.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/controller/opendSelectGood.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/controller/selectPeople.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/filter/commonFilter.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/service/service.js"></script>
		<style>
			.ngdialog.ngdialog-theme-default .ngdialog-content{padding:0px;padding-bottom:1em;}
			.describle_title{border-bottom:1px solid #ff9933;}
			.in_area{width:100%;}
			.ngdialog-buttons{padding:0 10px;}
			.des_photos_box{position:relative;overflow:initial;}
			.des_photos_box img:hover{transform:scale(10,10);position:absolute;top:0px;left:150px;background:#fff;z-index:999;box-shadow: 0 0 5px #333;}
			.paizhao{overflow:initial !important;min-height:76px;}
			.ng-cloak{display:none;}
			
			.ScoreTableBoxContainer {padding-left: 30px; margin-top: 20px;}
			.ScoreTableBoxContainer .score_table_box {padding: 0 120px;}
			.ScoreTableBoxContainer .TableTitle {
				font-size: 14px;
				position: absolute;
			    top: -8px;
			    left: 0;
			    width: 129px;
			    text-align: right;
			    line-height: 30px;
			}
		</style>
	</head>
	<body ng-controller="viewController">
		<input type="hidden" id="oper" value="<%=request.getParameter("oper") %>">
		<input type="hidden" id="instanceId" value="<%=request.getParameter("instanceId") %>">
		<input type="hidden" id="isOut" value="<%=request.getParameter("isOut") %>">
		<input type="hidden" id="isForkGroup" value="<%=request.getParameter("isForkGroup") %>">
		<input type="hidden" id="iDisplayStart" value="<%=request.getParameter("iDisplayStart") %>">
		
		<!--右侧描述信息标题及列表-->
		<!-- <div>
			<p class="describle_title">缺陷信息</p> -->
			
		<div class="new_right_content">
			<div class="index_detail pdb8">
				<div class="new_index_title mb20 fixed">
					维修工单详情（事件编号：<span class="des_info_decration" ng-cloak style="display: inline-block;">{{apply.defectNumber}}</span>）
					<div ng-if="oper=='apply'&&(apply.state==-1||apply.state==0||apply.state==6||apply.state==1||apply.state==2)" class="btn_container01 AbsoluteBtnContainer mb20" style="padding-left: 155px;"> 
						<span class="btn_inline_new hm_btn_yellow_new " ng-click="pushReminder()" ng-if="apply.state!=3&&apply.state!=5&&apply.state!=9">催单</span>
						<span class="btn_inline_new hm_btn_white " ng-click="openDialog(2);" ng-if="apply.state==2">验收通过</span>
						<span class="btn_inline_new hm_btn_yellow_new ml15" ng-click="openDialog(3);" ng-if="apply.state==2">验收驳回</span>
						<span class="btn_inline_new hm_btn_yellow_new" ng-click="editDefect()" ng-if="apply.state==0||apply.state==-1">修改</span>
						<span class="btn_inline_new hm_btn_red_new ml15" ng-click="draftDefect();"> &nbsp;作废&nbsp; </span>
						<span class="btn_inline_new hm_btn_white" onClick="printPage(1);"> &nbsp;打印&nbsp; </span>
						<span class="btn_inline_new hm_btn_white ml15" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div>
					<div ng-if="oper=='apply'&&apply.state==5" class="btn_container01 AbsoluteBtnContainer mb20" style="padding-left: 155px;">
						<span class="btn_inline_new hm_btn_yellow_new " ng-click="agree();">同意</span>
						<span class="btn_inline_new hm_btn_red_new ml15" ng-click="rejuest();"> &nbsp;拒绝&nbsp; </span>
						<span class="btn_inline_new hm_btn_white ml15" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div> 
					<div ng-if="oper=='dispatch'&&apply.state==-1" class="btn_container01 AbsoluteBtnContainer mb20" style="padding-left: 155px;">    
					<span class="btn_inline_new hm_btn_yellow_new " ng-click="pushReminder()" >催单</span>
						<span class="btn_inline_new hm_btn_yellow_new " ng-click="openDialog(9)">调度</span>
						<span class="btn_inline_new hm_btn_yellow_new " onclick="addGoods()">添加物资</span>
						<span class="btn_inline_new hm_btn_yellow_new " onclick="selectPeople()">添加参与人</span>
						<!-- <span class="btn_inline_new hm_btn_red_new ml15" ng-click="applyDefect();" ng-if="apply.isDraft==0"> &nbsp;请求作废&nbsp; </span> -->
						<span class="btn_inline_new hm_btn_white" onClick="printPage(1);"> &nbsp;打印&nbsp; </span>
						<span class="btn_inline_new hm_btn_white ml15" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div>
					<div ng-if="oper=='dispatch'&&apply.state!=-1" class="btn_container01 AbsoluteBtnContainer mb20" style="padding-left: 155px;">    
						<span class="btn_inline_new hm_btn_white" onClick="printPage(1);"> &nbsp;打印&nbsp; </span>
						<span class="btn_inline_new hm_btn_white ml15" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div>
					<div ng-if="oper=='approve'&&(apply.state==0||apply.state==6)" class="btn_container01 AbsoluteBtnContainer mb20" style="padding-left: 155px;">    
						<span class="btn_inline_new hm_btn_yellow_new " ng-click="pushReminder()" >催单</span>
						<span class="btn_inline_new hm_btn_yellow_new " ng-click="openDialog(6)">接收</span>
						<span class="btn_inline_new hm_btn_red_new ml15" ng-click="openDialog(0);" ng-if="apply.turnNum<turnNum"> &nbsp;转交&nbsp; </span>
						<span class="btn_inline_new hm_btn_yellow_new " onclick="addGoods()">添加物资</span>
						<span class="btn_inline_new hm_btn_yellow_new " onclick="selectPeople()">添加参与人</span>
						<!-- <span class="btn_inline_new hm_btn_red_new ml15" ng-click="applyDefect();" ng-if="apply.isDraft==0"> &nbsp;请求作废&nbsp; </span> -->
						<span class="btn_inline_new hm_btn_white" onClick="printPage(1);"> &nbsp;打印&nbsp; </span>
						<span class="btn_inline_new hm_btn_white ml15" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div>
					<div ng-if="oper=='approve'&&apply.state!=0&&apply.state!=6" class="btn_container01 AbsoluteBtnContainer mb20">    
						<span class="btn_inline_new hm_btn_white" onClick="printPage(1);"> &nbsp;打印&nbsp; </span>
						<span class="btn_inline_new hm_btn_white ml15" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div>
					
					<div ng-if="oper=='defect'&&(apply.state==1 || apply.state==7)" class="btn_container01 AbsoluteBtnContainer mb20" style="padding-left: 155px;">
						<span class="btn_inline_new hm_btn_yellow_new " ng-click="openDialog(1);">处理完成</span>
						<span class="btn_inline_new hm_btn_red_new " ng-click="openDialog(10);">转交</span>
						<span class="btn_inline_new hm_btn_yellow_new " onclick="addGoods()">添加物资</span>
						<span class="btn_inline_new hm_btn_yellow_new " onclick="selectPeople()">添加参与人</span>
						<span class="btn_inline_new hm_btn_yellow_new ml15" ng-if="apply.isDelay==0||apply.isDelay==3" ng-click="openDialog(7);">申请延期</span> 
						<span class="btn_inline_new hm_btn_white" onClick="printPage(1);"> &nbsp;打印&nbsp; </span>
						<span class="btn_inline_new hm_btn_white ml15" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div>
					<div ng-if="oper=='defect'&&apply.state!=1&&apply.state!=7" class="btn_container01 AbsoluteBtnContainer mb20" style="padding-left: 155px;">    
						<span class="btn_inline_new hm_btn_white" onClick="printPage(1);"> &nbsp;打印&nbsp; </span>
						<span class="btn_inline_new hm_btn_white ml15" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div>
					<div ng-if="oper=='accept'&&apply.state==2" class="btn_container01 AbsoluteBtnContainer mb20" style="padding-left: 155px;">
						<span class="btn_inline_new hm_btn_yellow_new " ng-click="openDialog(2);">验收通过</span>
						<span class="btn_inline_new hm_btn_yellow_new ml15" ng-click="openDialog(3);">驳回</span>
						<span class="btn_inline_new hm_btn_white" onClick="printPage(1);"> &nbsp;打印&nbsp; </span>
						<span class="btn_inline_new hm_btn_white ml15" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div>
					<div ng-if="oper=='accept'&&apply.state!=2" class="btn_container01 AbsoluteBtnContainer mb20" style="padding-left: 155px;">    
						<span class="btn_inline_new hm_btn_white" onClick="printPage(1);"> &nbsp;打印&nbsp; </span>
						<span class="btn_inline_new hm_btn_white ml15" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div>
					<div ng-if="oper=='archive'" class="btn_container01 AbsoluteBtnContainer mb20" style="padding-left: 155px;">
						<span class="btn_inline_new hm_btn_yellow_new " ng-click="openDialog(4);">归档</span>
						<!-- <span class="btn_inline_new hm_btn_yellow_new ml15" ng-click="openDialog(5);">驳回验收</span> -->
						<span class="btn_inline_new hm_btn_white" onClick="printPage(1);"> &nbsp;打印&nbsp; </span>
						<span class="btn_inline_new hm_btn_white ml15" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div>
					<div ng-if="oper=='delay'" class="btn_container01 AbsoluteBtnContainer mb20" style="padding-left: 155px;">
						<span class="btn_inline_new hm_btn_yellow_new " ng-if="apply.isDelay==0||apply.isDelay==3" ng-click="openDialog(7);">申请延期</span>
						<span class="btn_inline_new hm_btn_white" onClick="printPage(1);"> &nbsp;打印&nbsp; </span>
						<span class="btn_inline_new hm_btn_white ml15" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div>
					<div ng-if="oper=='approveDelay'" class="btn_container01 AbsoluteBtnContainer mb20" style="padding-left: 155px;">
						<span class="btn_inline_new hm_btn_yellow_new " ng-click="agreeDelay()" ng-if="apply.isDelay==1">同意延期</span>
						<span class="btn_inline_new hm_btn_yellow_new ml15" ng-click="rejuestDelay()" ng-if="apply.isDelay==1">拒绝延期</span>
						<span class="btn_inline_new hm_btn_white" onClick="printPage(1);"> &nbsp;打印&nbsp; </span>
						<span class="btn_inline_new hm_btn_white ml15" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div>
					<div ng-if="oper=='myJoin'||oper=='view'||(oper=='apply'&&apply.state!=-1&&apply.state!=0&&apply.state!=5&&apply.state!=6&&apply.state!=2&&apply.state!=1)||oper=='delayView'" class="btn_container01 AbsoluteBtnContainer mb20" style="padding-left: 155px;">
						<!-- 特殊处理 -->
						<span class="btn_inline_new hm_btn_yellow_new " ng-click="openDialog(10);" ng-if="apply.state==1 || apply.state==7">转交处理</span>
						<span class="btn_inline_new hm_btn_white" onClick="printPage(1);"> &nbsp;打印&nbsp; </span>
						<span class="btn_inline_new hm_btn_white" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div>
				</div>
				<!--startprint-->
<ul class="describle_info special">
	<li class="pr">
		<span class="des_info_title">维修事项：</span>
		<span class="des_info_decration" ng-cloak>{{apply.equipmentName}}</span>
	</li>
	<li class="pr percent30 fl">
		<span class="des_info_title">事件状态：</span>
		<span class="des_info_decration" ng-cloak><span class="{{apply.stateVo|stateColor}}">{{apply.stateVo}}{{apply.delayStateVo}}</span></span>
	</li>
	<li class="pr percent30 fl">
		<span class="des_info_title">紧急程度：</span>
		<span class="des_info_decration {{apply.grade==1?'color_red':''}}" ng-cloak> {{apply.gradeVo}}</span>
	</li>
	<li class="pr percent30 fl">
		<span class="des_info_title">事件类型：</span>
		<span class="des_info_decration" ng-cloak>维修</span>
	</li>
	<li class="pr fl percent30">
		<span class="des_info_title">上报电话：</span>
		<span class="des_info_decration" ng-cloak>{{apply.applyPhone?apply.applyPhone:'--'}}{{apply.applyPhoneName?'('+apply.applyPhoneName+')':''}}<span class="tel" ng-show="apply.applyPhone&&isOut==1" ng-click="outCall(apply.applyPhone,'','')"></span></span>
	</li>

	<li class="pr fl percent30">
		<span class="des_info_title">上报科室：</span>
		<span class="des_info_decration" ng-cloak>{{apply.defectDepartmentVo?apply.defectDepartmentVo:'--'}}</span>
	</li>
	<li class="pr fl percent30">
		<span class="des_info_title">上报地点：</span>
		<span class="des_info_decration" ng-cloak>{{apply.describe}}</span>
	</li>
	
    <li class="pr percent30 fl">
		<span class="des_info_title">上报人：</span>
		<span class="des_info_decration" ng-cloak>{{apply.createUserName?apply.createUserName:'--'}}<span style="color: #888;">（{{apply.createUserPhone?apply.createUserPhone:'--'}}）</span><span class="tel" ng-show="apply.createUserPhone&&isOut==1" ng-click="outCall(apply.createUserPhone,'','')"></span></span>
	</li>

	<li class="pr fl percent30">
		<span class="des_info_title">工号：</span>
		<span class="des_info_decration" ng-cloak>{{apply.createUserWorkNo?apply.createUserWorkNo:'--'}}</span>
	</li>
	<li class="pr percent30 fl">
		<span class="des_info_title">上报时间：</span>
		<span class="des_info_decration" ng-cloak>{{apply.createTimeStr?apply.createTimeStr:'--'}}</span>
	</li>
	
	<!-- <li class="pr percent30">
		<span class="des_info_title" >所属部门：</span>
		<span class="des_info_decration" ng-cloak>{{apply.defectDepartmentVo}}</span>
	</li> -->
					
	<li class="pr percent30 fl">
		<span class="des_info_title">处理科室：</span>
		<span class="des_info_decration" ng-cloak >{{processGroupName?processGroupName:"--"}}</span>
	</li>
	<li class="pr percent30 fl">
		<span class="des_info_title">处理人：</span>
		<span class="des_info_decration" ng-cloak >{{processUserName?processUserName:"--"}}</span>
	</li>
	<li class="pr percent30 fl">
		<span class="des_info_title">完工时间：</span>
		<span class="des_info_decration" ng-cloak >{{processTime?processTime:"--"}}</span>
	</li>
	<li class="pr percent30 fl" ng-show="dispatchUserName!=''">
		<span class="des_info_title">调度人：</span>
		<span class="des_info_decration" ng-cloak >{{dispatchUserName}}</span>
	</li>
	<li class="pr percent30 fl" ng-show="dispatchWorkNo!=''">
		<span class="des_info_title">调度人工号：</span>
		<span class="des_info_decration" ng-cloak >{{dispatchWorkNo}}</span>
	</li>
	<li class="pr percent30 fl" ng-show="dispatchUserName!=''">
		<span class="des_info_title">调度时间：</span>
		<span class="des_info_decration" ng-cloak >{{apply.createTimeStr?apply.createTimeStr:'--'}}</span>
	</li>
       		<li class="pr fl" style="width:66.66%">
				<span class="des_info_title">备注：</span>
				<span class="des_info_decration" ng-cloak>{{apply.memo?apply.memo:'--'}}</span>
			</li>
			<li class="pr percent30 fl" >
				<span class="des_info_title">催单次数：</span>
				<span class="des_info_decration" ng-cloak >{{apply.reminderNum}}</span>
			</li>
       		<li class="pr">
               	<span class="des_info_title">录音：</span>
                <span class="des_info_decration" ng-show="apply.audioIds" >
                	<audio controls="controls" id="myVideo" preload="preload" src="{{audioSrc}}">    
					</audio>
				</span>
				<span class="des_info_decration" ng-show="!apply.audioIds">
                	--
				</span>	
         			 </li>
         	<li class="pr {{attachmentIds.length==0?'':'paizhao2'}}">
	            <span class="des_info_title">上报图片：</span>
	            <div class="des_photos_box" ng-repeat="attachmentId in attachmentIds">
	                <img src="${baseUrl}fileServer/download?fileId={{attachmentId}}" />
	            </div>
	            <span class="des_info_decration" ng-show="attachmentIds.length==0">--</span>
       		</li>
       		<li class="pr {{checkAttachmentIds.length==0?'':'paizhao2'}}">
	            <span class="des_info_title">处理图片：</span>
	            <div class="des_photos_box" ng-repeat="attachmentId in checkAttachmentIds">
	                <img src="${baseUrl}fileServer/download?fileId={{attachmentId}}" />
	            </div>
	            <span class="des_info_decration" ng-show="checkAttachmentIds.length==0">--</span>
       		</li>
       		<!-- <li class="pr percent30 fl">
				<span class="des_info_title">事件编号：</span>
				<span class="des_info_decration" ng-cloak>{{apply.defectNumber}}</span>
			</li>  -->
			
			
			<li class="pr" ng-if="apply.achievementStr">
				<span class="des_info_title">满意度评价：</span>
				<span class="des_info_decration" ng-cloak>{{apply.achievementStr}}</span>
			</li>
		</ul>
			<!-- 	<div class="ScoreTableBoxContainer pr">
				    <p class="TableTitle">所需物资：</p>
					<div class="score_table_box">
						<table class="score_table">
							<thead>
								<tr>
								    <th width="20%">物资名称</th>
									<th width="15%">物资组</th>
									<th width="15%">编号</th>
									<th width="10%">类型</th>
									<th width="10%">单价</th>
									<th width="10%">数量</th>
									<th width="10%">总价</th>
									<th>操作</th>
								</tr>
							</thead>
							<tbody id="goodsTable">
							</tbody>
						</table>
					</div>
				</div> -->
				
				<!-- <div class="ScoreTableBoxContainer pr">
				    <p class="TableTitle">参与人：</p>
					<div class="score_table_box">
						<table class="score_table">
							<thead>
								<tr>
								    <th width="25%">姓名</th>
									<th width="20%">工号</th>
									<th width="20%">科室</th>
									<th width="25%">联系方式</th>
									<th width="10%">操作</th>
								</tr>
							</thead>
							<tbody id="peopleTable">
							</tbody>
						</table>
					</div>
				</div> -->
				<input type="hidden" value="{{processUserId}}" id="processUserId"/>
				<!-- <div class="step_box" style="padding-left: 150px; position: relative;">
				    <span class="des_info_title" style="left: 29px; top: -10px;">处理流程：</span>
					<ul class="defect_step">
						<li ng-repeat="history in historyList" ng-cloak>
							<font>{{history.endTime}}</font>
							<span>{{history.processerName}}</span>({{history.processerPhone}}<span class="tel" ng-show="history.processerPhone!=null&&isOut==1" ng-click="outCall(history.processerPhone,'','')"></span>){{history.taskName|getHistoryDetail:history.approveResult:history.nextProcesserName:history.nextProcesserPhone:history.advice}}<span class="tel" ng-show="history.nextProcesserPhone!=null&&isOut==1" ng-click="outCall(history.nextProcesserPhone,'','')"></span>
							<em class="liucheng_checked"></em>
							<p class="renyuan">{{history.taskName|getHistoryTitle}}：{{history.processerName}}({{history.processerPhone}})
								<span class="liucheng_time"></span>
							</p>
							<span class="liucheng_beizhu">{{history.advice}}</span>
						</li>
					</ul>
				</div> -->
				<!--endprint-->
					 
				<!-- <div class="btn_container01 mb20">
					<span class="btn_inline_new hm_btn_white">返回</span>
					<span class="btn_inline_new hm_btn_red_new ml15">作废</span>
					<span class="btn_inline_new hm_btn_yellow_new ml15">修改</span>
				</div> -->
				
				<div class="TabReviseContainer">
				    <div class="tab-box">
				        <ul>
				        	<li><span class="tab current">处理流程</span></li>
				        	<li><span class="tab">参与人</span></li>
				        	<li><span class="tab">所需物资</span></li>
				        </ul>
				    </div>
				    <!-- end .tab-box -->
				    <div class="tab-revise-content">
				        <!-- <ul class="defect_step">
							<li ng-repeat="history in historyList" ng-cloak>
								<font>{{history.endTime}}</font>
								<span>{{history.processerName}}</span>({{history.processerPhone}}<span class="tel" ng-show="history.processerPhone!=null&&isOut==1" ng-click="outCall(history.processerPhone,'','')"></span>){{history.taskName|getHistoryDetail2:history.approveResult}}<span class="tel" ng-show="history.nextProcesserPhone!=null&&isOut==1" ng-click="outCall(history.nextProcesserPhone,'','')"></span>
								<em class="liucheng_checked"></em>
								<p class="renyuan">{{history.taskName|getHistoryTitle}}：{{history.processerName}}({{history.processerPhone}})
									<span class="liucheng_time"></span>
								</p>
								<span class="liucheng_beizhu">{{history.advice}}</span>
							</li>
						</ul> -->
						
						
						<ul class="event-status-list">
							<li class="" ng-repeat="history in historyList">
								<div class="info-box">
									<p class="title">
										<span class="time">{{history.endTime}}</span>
										<span class="style">{{history.taskName|getHistoryDetail2:history.approveResult}}</span>
									 	{{history.processerDealName}} 
									</p>
								</div>
								<p class="prompt" ng-if="history.advice">备注：{{history.advice}}</p>
								<!--end .info-box-->
							</li>
						</ul>
						
						
						
						
				    </div>
				    <!-- end .tab-revise-content -->
				    <div class="tab-revise-content hide">
				          <table class="score_table">
							<thead>
								<tr>
								    <th width="25%">姓名</th>
									<th width="20%">工号</th>
									<th width="20%">科室</th>
									<th width="25%">联系方式</th>
									<th width="10%">操作</th>
								</tr>
							</thead>
							<tbody id="peopleTable">
							</tbody>
						</table>
				    </div>
				    <!-- end .tab-revise-content -->
				    <div class="tab-revise-content hide" >
				         <table class="score_table">
							<thead>
								<tr>
								    <th width="20%">物资名称</th>
									<th width="15%">物资组</th>
									<th width="15%">编号</th>
									<th width="10%">类型</th>
									<th width="10%">单价</th>
									<th width="10%">数量</th>
									<th width="10%">总价</th>
									<th>操作</th>
								</tr>
							</thead>
							<tbody id="goodsTable">
							</tbody>
						</table>
				    </div>
				    <!-- end .tab-revise-content -->
				</div>
				<!-- end .TabReviseContainer -->
			</div>
		</div>
		<script type="text/ng-template" id="modalDialogId">
		<div class="ngdialog-message">
            <p class="describle_title">{{dialogInfo.theme}}</p>
			<ul class="describle_info">
				

				<li class="pr" ng-if="dialogInfo.userTitle!=''" style="height:42px;overflow:visible;">
					<span class="des_info_title">{{dialogInfo.userTitle}}：</span>
					 <div class="divselect12  divselect13 ">
			      		<cite class="cite12 text_ellipsis cite13" style="width:192px"  ng-click="toggleSelectUser()" ng-show="!flag">{{node1 | selectUserNames}}</cite>
						<cite class="cite12 text_ellipsis cite13" style="width:192px;{{flag?'background:url()':''}}"  ng-show="flag">{{node1 | selectUserNames}}</cite>
			      		<ul id="tree2" class="ztree leibie" basetree tree-data="data1" tree-set="set1" check-node="node1" select-node="node" default-select="defaultSelectUser" ng-show="showSelectUserValue==1" style="display: block;height:200px !important;width:200px !important;z-index:20000;position:absolute;top:28px;left:0px;border:1px solid #d5d5d5;" ng-mouseleave="showSelectUserValue=2">
						</ul>
		  			</div>
				</li>
				<li class="pr" ng-if="dialogInfo.adviceTitle!=''">
					<span class="des_info_title">{{dialogInfo.adviceTitle}}：</span>
					<input class="in_area percent30" type="text" name="" value="" placeholder="" ng-model="advice">
				</li>
			</ul>
        </div>
        <div class="ngdialog-buttons">
            <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog('button')">取消</button>
            <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm({'advice':($$childTail.advice),'userId':($$childHead.node1|selectUserIds)})">确定</button>
        </div>
    	</script>
    	<script type="text/javascript">
    	 function outCall(called, hotline, source) {
    	        if (typeof(exec_obj) == 'undefined') {
    	            exec_obj = document.createElement('iframe');
    	            exec_obj.name = 'tmp_frame';
    	            exec_obj.src = 'http://192.168.10.100:8181/zxyy/outCall.html?called=' + called +
    	                "&hotline=" + hotline + "&source=" + source + "&token=" + Math.random();
    	            exec_obj.style.display = 'none';
    	            document.body.appendChild(exec_obj);
    	        } else {
    	            exec_obj.src = 'http://192.168.10.100:8181/zxyy/outCall.html?called=' + called +
    	                "&hotline=" + hotline + "&source=" + source + "&token=" + Math.random();
    	        }
    	    }
    	
    	</script>
    	<script>
	    	$(".tab-box ul li .tab").click(function(){
	    	    if($(this).hasClass("current"))
	    	    return;
	    			 
	    	    var index = $(".tab-box ul li .tab").index($(this));
	    	    $(".tab-box ul li .tab").removeClass("current");
	    	    $(".tab-box ul li .tab").eq(index).addClass("current");
	    			  
	    	    $(".tab-revise-content").addClass("hide");
	    	    $(".tab-revise-content").eq(index).removeClass("hide");
	    	})
    	</script>
	</body>
	
	
</html>
