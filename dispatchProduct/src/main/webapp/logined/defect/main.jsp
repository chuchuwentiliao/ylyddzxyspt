<%@page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html ng-app="mainApp">
	<head>
		<meta charset="UTF-8">
		<title>首页</title>
		<jsp:include page="../../common/flatHead.jsp" />
		<link rel="stylesheet" href="${ctx}flat/plugins/tree/skins/tree_default.css" type="text/css"/>
		<link href="${ctx }flat/plugins/form/skins/form_default.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="${ctx}flat/css/hm_style.css" />
		<link href="${ctx}flat/plugins/form/skins/form_default.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/plugins/datatable/skins/datatable_default.css" rel="stylesheet" type="text/css" />
		<style type="text/css">
			.hello{display:block;background-color: red}
			.Wdate{background-image: none; }
		</style>
		<script type="text/javascript">
			var basePath = "${ctx}";
			$(function(){
				$(".new_right_content").css({"height":$(window).height()});
			});
		</script>
		<script TYPE="text/javascript" src="${ctx}angularframe/lib/angular.min.js"></script>
		<script type="text/javascript" src="${ctx}plugins/My97DatePicker/WdatePicker.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/app/mainApp.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/controller/mainController.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/filter/commonFilter.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/service/service.js"></script>
	</head>
<body ng-controller="mainController">
		<!--右侧内容-->
		<div class="new_right_content" style="overflow:auto;width:100%;">
			<!-- <div class="index_detail pdb8 wd1362">
				<div style="overflow:hidden;">
					<p class="new_index_title fl" >
						<span>{{dutyInfo}}</span>					
					</p>
					<div class="time_search fl" style="display: inline-block;width: 220px;padding:1px;margin-left:25px;">
						<input type="text" class="formText Wdate" id="dutyDate"  ng-model="dutyDate" onfocus="WdatePicker({skin:'default',dateFmt:'yyyy-MM-dd'})">
						<em class="select_ico"></em>
						<span class="time_btn01" ng-click="search();">查询</span>
					</div>
				</div>
				
				
				<div class="qiandao_block">
					<button class="{{isDuty==1?'qiandao_btn':'qiantui_btn'}}" ng-click="toSign()">{{isDuty==1?'值班签退':'值班签到'}}</button>
				</div>
				<ul class="new_zhiban_list">
					<li ng-repeat="duty in allList">
						<span class="new_bumen">{{duty.dutyDepartment}}</span>
						<div style="float:left;background-color:#fff;">
							<ul class="renyuan_list">
								<li ng-repeat="dutyUser in duty.dutyList" title="{{dutyUser.dutyPersons}}{{dutyUser.dutyPhone}}" style="{{dutyUser.isOwn==1?'color:#ff5454':''}}">
									<span class="renyuan_name">{{(dutyUser.dutyPersons|getLength)>6?(dutyUser.dutyPersons|cutstr:6):dutyUser.dutyPersons }}</span>
									<span>{{dutyUser.dutyPhone}}</span>
								</li>
								<p class="no_person_tishi" ng-show="{{duty.dutyList==null}}">暂无值班</p>
							</ul>
						</div>
					</li>
				</ul>
			</div> -->
		<div class="wd1362">
			<div class="index_detail pdb8 wd1362 fl">
					<p class="new_index_title" style="display:inline-block; border: none;">
						待办事项
					</p>
					<div class="time_search " style="display: inline-block;padding:1px;margin-left:25px;">
						<select class="wd90" style="height:28px;border:1px solid #d9d9d9" ng-model="selectTime" >
						   <option value="">全部</option>
						   <option value="1">今天</option>
						   <option value="2">七日内</option>
						   <option value="3">自定义</option>
						</select>
						<div ng-if="selectTime==3" style="display:inline-block;vertical-align: top;">
							<input type="text" class="formText Wdate" id="beginTime"   onfocus="WdatePicker({skin:'default',dateFmt:'yyyy-MM-dd'})">
							<em class="select_ico"></em>~
							<input type="text" class="formText Wdate" id="endTime"   onfocus="WdatePicker({skin:'default',dateFmt:'yyyy-MM-dd'})">
							<em class="select_ico"></em>
						</div>
						<span class="time_btn01" ng-click="find()">查询</span>
					</div>
				<ul class="shixiang_list">
				<li style="cursor: pointer" ng-click="toWait('dispatch')">
						<em class="waitdiaodu"></em>
						<p class="shixiang_tishi">待调度</p>
						<span class="shixiang_num">{{dispatchCount}}</span>
					</li>
					<li style="cursor: pointer" ng-click="toWait('approve')">
						<em class="recieve"></em>
						<p class="shixiang_tishi">待接收</p>
						<span class="shixiang_num">{{approveCount}}</span>
					</li>
					<li style="cursor: pointer" ng-click="toWait('defect')">
						<em class="xiaoque"></em>
						<p class="shixiang_tishi">待处理</p>
						<span class="shixiang_num">{{defectCount}}</span>
					</li>
					<li style="cursor: pointer" ng-click="toWait('approveDelay')">
						<em class="pizhun"></em>
						<p class="shixiang_tishi">待批准</p>
						<span class="shixiang_num">{{delayCount}}</span>
					</li>
					<li style="cursor: pointer" ng-click="toWait('accept')">
						<em class="yanshou"></em>
						<p class="shixiang_tishi">待验收</p>
						<span class="shixiang_num">{{acceptCount}}</span>
					</li>
				</ul>
			</div>
			<!-- <div class="index_detail pdb8 wd365 fl ml15" >
				<p class="new_index_title">
					查看事件
				</p>
				<ul class="new_quexian_list">
					<li class="shangbao" ng-click="lookDefect('my')" style="cursor: pointer">我上报的</li>
					<li class="all" ng-click="lookDefect('all')" style="cursor: pointer">全部事件</li>
					<li class="tongji" ng-click="lookDefect('statistics')" style="cursor: pointer">事件统计</li>
				</ul>
				
			</div> -->
			
			</div>
			
			
			
			
			<div class="wd1362">
			<div class="index_detail pdb8 wd1362 fl">
					<p class="new_index_title" style="display:inline-block;border: none;">
						最新公告
					</p>
					<div class="time_search " style="display: inline-block;padding:1px;">
						<span class="time_btn01" style="margin-top:10px" ng-click="moreNotify()">更多公告</span>
					</div>
			<table cellpadding="0" cellspacing="0"  class="pretty dataTable" style="font-size: 14px;margin-left: 35px;" id="myTable">
				<thead>
				<tr>
		        	<th id="subject" class="longTxt">标题</th>
		        	<th id="typename" style="width:100px;">类型</th>
		        	<th id="username" style="width:90px;">发布人</th>
		       		<th id="approveDate" style="width:150px;">发布时间</th>
		        	<th id="totalCount" style="width:70px;" class="data_r">浏览次数</th>
	     	   </tr>
				</thead>
			<tbody>
		     		<tr ng-repeat="notify in aaData">
			        	<td class="tdCenter" ng-cloak ng-show="notify.isTop==1">
			        		<a title="{{notify.subject}}" style="text-decoration:none;color:#04578D;text-align:left;"  href="${ctx }logined/notify/view.jsp?notifyId={{notify.notifyId}}&columnId=35&returnType=1"><font color="red"><b>[置顶]&nbsp;&nbsp;</b></font>{{notify.subject}}</a>
			        	</td>
			        	<td class="tdCenter" ng-cloak ng-show="notify.isTop!=1">
			        		<a title="{{notify.subject}}" style="text-decoration:none;color:#04578D;text-align:left;"  href="${ctx }logined/notify/view.jsp?notifyId={{notify.notifyId}}&columnId=35&returnType=1">{{notify.subject}}</a>
			        	</td>
			        	<td class="tdCenter" ng-cloak>{{notify.typename?notify.typename:"--"}}</td>
			        	<td class="tdCenter" ng-cloak>{{notify.username}}</td>
			        	<td class="tdCenter" ng-cloak>{{notify.approveDate}}</td>
			        	<td class="tdCenter" ng-cloak>{{notify.totalCount}}</td>
		        	</tr>
		        	<tr ng-show="iTotalRecords==0">
			        	<td class="tdCenter" ng-cloak colspan="5">没有检索到数据</td>
		        	</tr>
			    </tbody>
			</table>
			</div>
			
			</div>
			
		</div>
		
		
		
		
		
	</body>
</html>
