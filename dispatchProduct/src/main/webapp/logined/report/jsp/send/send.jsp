<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>
		 <jsp:include page="../../../../common/flatHead.jsp" />
		<link rel="stylesheet" href="../../css/style.css">			
		<link href="../../css/reset.css" rel="stylesheet" type="text/css" />
		<link href="../../css/main.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="${ctx}flat/plugins/tree/skins/tree_default.css" type="text/css"/>
		<link href="${ctx}flat/plugins/Accormenus/skins/Accormenus_default.css" rel="stylesheet" type="text/css" />
		<link href="../../css/datatable_default.css" rel="stylesheet" type="text/css" />
		<style type="text/css">
			.inputTable th{width:82px;}
			.searchArea{padding:19px 0 12px 0;}
  			.dataTables_wrapper{
			 padding: 0 20px;
			}
			.canverse_right_tap{
			 height:59px;
			}
			.canverse_right_tap span{
			 padding:6px 10px;
			}
			/* .canverse_tap_area{
			 margin:12px 10px;
			} */
		</style>
			
		<script type="text/javascript" src="${ctx}flat/plugins/datatable/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="${ctx}flat/plugins/datatable/selecedForDatatablePagination.js"></script>
		<script type="text/javascript" src="${ctx}js/placeholder.js"></script>
		<script type="text/javascript" src="${ctx}flat/js/base.js"></script>
		<script type="text/javascript" src="${ctx}flat/plugins/tree/skins/jquery.ztree.all-3.2.min.js"></script>
		<script type="text/javascript" src="${ctx}/plugins/workview/echarts.js"></script>
        <script type="text/javascript" src="${ctx}js/logined/report/send/send.js"></script>
        <script type="text/javascript" src="${ctx}js/logined/report/send/teamSend.js"></script>
        <script type="text/javascript" src="${ctx}js/logined/report/send/staffSend.js"></script>
       <%--  <script type="text/javascript" src="${ctx}js/logined/report/send/equipmentSend.js"></script>
        <script type="text/javascript" src="${ctx}js/logined/report/send/medicalDepartment.js"></script> --%>
        <script type="text/javascript" src="${ctx}js/logined/report/send/sendSatisfaction.js"></script>
	
	</head>
	<body> 
			<div class="converse_right">
				<div class="canverse_right_two">
					<div class="canverse_right_tap">
						<span id="callLogDetailSpan" class="active">班组配送工作量统计</span>
						<span>员工配送工作量统计</span> 
						<%-- <span>配送设备统计</span>
						<span>医务科室上报量统计</span> --%>
						<span>处理结果满意度统计</span>
					</div>					
					<div class="canverse_tap_area">
						<!-- 班组配送 -->
                        <jsp:include page="teamSend.jsp"/>	
                         <!-- 员工配送工作量-->
						 <jsp:include page="staffSend.jsp"/>  
						<%-- <!-- 配送设备-->
						<jsp:include page="equipmentSend.jsp"/>
						<!-- 医务科室上报量 -->
						<jsp:include page="medicalDepartment.jsp"/> --%>
                        <!-- 处理结果满意度-->
						<jsp:include page="sendSatisfaction.jsp"/>
					</div>	
				</div>
			</div>
	</body>
</html>