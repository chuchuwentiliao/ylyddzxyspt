<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <jsp:include page="../../../../common/flatHead.jsp" />
    <link rel="stylesheet" href="../../css/style.css">
    <link href="../../css/reset.css" rel="stylesheet" type="text/css" />
    <link href="../../css/main.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="${ctx}flat/plugins/tree/skins/tree_default.css" type="text/css"/>
    <link href="${ctx}flat/plugins/Accormenus/skins/Accormenus_default.css" rel="stylesheet" type="text/css" />
    <link href="../../css/datatable_default.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .inputTable th{width:82px;}
        .searchArea{padding:19px 0 12px 0;}
        .dataTables_wrapper{
            padding: 0 20px;
        }
        .canverse_right_tap{
            height:59px;
        }
        .canverse_right_tap span{
            padding:6px 10px;
        }
        /* .canverse_tap_area{
            margin:12px 10px;
        } */
    </style>

    <script type="text/javascript" src="${ctx}flat/plugins/datatable/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="${ctx}flat/plugins/datatable/selecedForDatatablePagination.js"></script>
    <script type="text/javascript" src="${ctx}js/placeholder.js"></script>
    <script type="text/javascript" src="${ctx}flat/js/base.js"></script>
    <script type="text/javascript" src="${ctx}flat/plugins/tree/skins/jquery.ztree.all-3.2.min.js"></script>
    <script type="text/javascript" src="${ctx}/plugins/workview/echarts.js"></script>

    <script type="text/javascript" src="${ctx}js/logined/report/escheat/escheat.js"></script>

    <script type="text/javascript" src="${ctx}js/logined/report/escheat/classEscheat.js"></script>
    <script type="text/javascript" src="${ctx}js/logined/report/escheat/employeeEscheat.js"></script>
    <%-- <script type="text/javascript" src="${ctx}js/logined/report/escheat/returnDevice.js"></script>
    <script type="text/javascript" src="${ctx}js/logined/report/escheat/medicalEscheat.js"></script> --%>
    <script type="text/javascript" src="${ctx}js/logined/report/escheat/processResults.js"></script>


</head>
<body>
<div class="converse_right">
    <div class="canverse_right_two">
        <div class="canverse_right_tap">
            <span id="callLogDetailSpan" class="active">班组归还工作量统计</span>
            <span>员工归还工作量统计</span>
            <%-- <span>归还设备统计</span>
            <span>医务科室上报量统计</span> --%>
            <span>处理结果满意度统计</span>
        </div>
        <div class="canverse_tap_area">
            <!-- 班组归还工作量统计 -->
            <jsp:include page="classEscheat.jsp"/>
            <!-- 员工归还工作量统计-->
            <jsp:include page="employeeEscheat.jsp"/>
           <%--  <!-- 归还设备统计-->
            <jsp:include page="returnDevice.jsp"/>
            <!-- 维修事件类型 -->
            <jsp:include page="medicalEscheat.jsp"/> --%>
            <!-- 处理结果满意度统计 -->
            <jsp:include page="processResults.jsp"/>

        </div>
    </div>
</div>
</body>
</html>