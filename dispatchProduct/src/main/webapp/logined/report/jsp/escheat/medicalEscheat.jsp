<%--
  Created by IntelliJ IDEA.
  User: lilipo
  Date: 2017/5/13
  Time: 12:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--医务科室上报量统计--%>
<div class="canverse_tap_box hide" >
     <div class="searchArea">
        <table cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
                <td class="right">
                    <label style="padding-left:20px;color:#999;font-size:14px;">时间：</label>
                    <input type="text" onfocus="WdatePicker({maxDate:'#F{$dp.$D(\'endDate_medical\')}',skin:'default',dateFmt:'yyyy-MM-dd'})" id="begDate_medical" class="in_something2 pr Wdate" name=""/>
                    -
                    <input type="text" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'begDate_medical\')}',skin:'default',dateFmt:'yyyy-MM-dd'})" id="endDate_medical" class="in_something2 pr Wdate" name=""/>
                    <label style="padding-left:20px;color:#999;font-size:14px;">员工：</label>
                    <input type="text"  id="medicalName" class="input_css" name=""/>
                    <button class="btn_jinhu_add" onclick="query_medical()">查询</button>
                    <button class="btn_jinhu_add" onclick="report_export_medical()">导出</button>
                </td>
               <%-- <td style="width:110px;">
                    <button class="btn_jinhu_add" onclick="change_class(this,0)">图表视图</button>
                    <button class="btn_jinhu_add" id="list_button_c" onclick="change_class(this,-1)">列表视图</button>
                </td>--%>
            </tr>
            </tbody>
        </table>
    </div>
    <%--<div class="IllegalsNum" id="view_class" style="width: 100%;height:60%;"></div>--%>
    <div class="list_IllegalsNum" id="list_medical">
        <div id="table_wrapper" class="dataTables_wrapper" role="grid">
            <div id="myTable_Squadron_processing" class="dataTables_processing" style="visibility: hidden;">正在加载数据...</div>
            <table id="tables" class="pretty dataTable" width="100%" cellspacing="0" cellpadding="0" border="0">
                <thead>
                <tr>
                    <th >序号</th>
                    <th >上报科室名称</th>
                    <th >上报量</th>
                    <th >完工量</th>
                    <th >未完工量</th>
                </tr>
                </thead>
                <tbody id="tbody_medical">

                </tbody>
            </table>
        </div>
    </div>
</div>