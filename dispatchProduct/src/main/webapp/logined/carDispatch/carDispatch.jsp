<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>
		<jsp:include page="../../common/flatHead.jsp" />
		<link rel="stylesheet" href="css/style.css">			
		<link href="css/reset.css" rel="stylesheet" type="text/css" />
		<link href="css/main.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="${ctx}flat/plugins/tree/skins/tree_default.css" type="text/css"/>
		<link href="${ctx}flat/plugins/Accormenus/skins/Accormenus_default.css" rel="stylesheet" type="text/css" />
		<link href="css/datatable_default.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="${ctx}flat/css/hm_style.css" />
		<style type="text/css">
			.inputTable th{width:82px;}
			.searchArea{padding:19px 0 12px 0;}
  			.dataTables_wrapper{
			 padding: 0 20px;
			}
			.canverse_right_tap{
			 height:59px;
			}
			.canverse_right_tap span{
			 padding:6px 10px;
			}
			.canverse_tap_area{
			 /* margin:12px 10px; */
			}
		</style>
			
		<script type="text/javascript" src="${ctx}flat/plugins/datatable/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="${ctx}flat/plugins/datatable/selecedForDatatablePagination.js"></script>
		<script type="text/javascript" src="${ctx}js/placeholder.js"></script>
		<script type="text/javascript" src="${ctx}flat/js/base.js"></script>
		<script type="text/javascript" src="${ctx}flat/plugins/tree/skins/jquery.ztree.all-3.2.min.js"></script>
		<script type="text/javascript" src="${ctx}/plugins/workview/echarts.js"></script>
        <script type="text/javascript" src="${ctx}js/logined/carDispatch/carDispatch.js"></script>
	
	</head>
	<body> 
	<input type="hidden" id="currentTab" value="${param.currentTab}"><!-- 详情页面点击返回传的参数,用于显示当前列表 -->
			<div class="converse_right">
				<div class="canverse_right_two">
					<div class="canverse_right_tap">
						<span id="waitDispatchList" class="active" onclick="waitDispatchList(this);">待派遣</span>
						<span id="waitStartList" onclick="waitStartList(this);">待发车</span> 
						<span id="startList" onclick="startList(this);">已发车</span>
						<span id="arriveList" onclick="arriveList(this);">已到达</span>
						<span id="carBackList" onclick="carBackList(this);">已回场</span>
					</div>					
					<div class="canverse_tap_area">
						<!-- 待派遣-->
                        <jsp:include page="waitDispatch.jsp"/>	
                       	<!-- 待发车-->
						<jsp:include page="waitStart.jsp"/>
						<!-- 已发车-->
						<jsp:include page="start.jsp"/>
						<!-- 已到达-->
						<jsp:include page="arrive.jsp"/>
						<!-- 已回场 -->	
						<jsp:include page="carBack.jsp"/>
					</div>	
				</div>
			</div>
	</body>
</html>