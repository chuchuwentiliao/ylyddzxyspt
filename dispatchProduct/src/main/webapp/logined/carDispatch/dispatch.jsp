<%@page pageEncoding="UTF-8" %>
<%@ page import="cn.com.qytx.platform.org.domain.UserInfo" %>
<%
	UserInfo userInfo = (UserInfo)session.getAttribute("adminUser");
	String currUserId = "";
	String currUserName = "";
	if(userInfo!=null){
		currUserId = userInfo.getUserId().toString();
		currUserName = userInfo.getUserName();
	}
	request.setAttribute("currUserId", currUserId);
	request.setAttribute("currUserName", currUserName);
%>
<!DOCTYPE html>
<html ng-app="approve2App">
	<head>
		<meta charset="UTF-8">
		<title>审批</title>
		<jsp:include page="../../common/flatHead.jsp" />
		<link href="${ctx}flat/css/reset.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="${ctx}flat/css/hm_style.css" />
		<link href="${ctx }flat/plugins/form/skins/form_default.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="${ctx}flat/plugins/tree/skins/tree_default.css" type="text/css"/>
		<script type="text/javascript">
			var basePath = "${ctx}";
		</script>
		<script type="text/javascript" src="${ctx}plugins/tree/ztree/jquery.ztree.all-3.5.min.js?version=${version}"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/carDispatch/dispatchDiag.js"></script>
		<style type="text/css">
			#driverUl li:hover {
				background: #f0f0f0;
			}
		</style>
	</head>
	<body  style="background-color:#fff;">
	<input type="hidden" id="instanceId" value="${param.instanceId}" />
	<input type="hidden" id="carTypes" value="${param.carType}" />
	<!-- <input type="hidden" id="driverName" value="" />
	<input type="hidden" id="driverPhone" value="" />
	<input type="hidden" id="driverId" value="" />
	<input type="hidden" id="driverStatus" value="" />
	<input type="hidden" id="driverName" value="" />
	<input type="hidden" id="driverPhone" value="" />
	<input type="hidden" id="driverId" value="" />
	<input type="hidden" id="driverStatus" value="" /> -->
		<div class="elasticFrame formPage">
			<form name="myForm" novalidate="novalidate">
			  <table width="100%" cellspacing="0" cellpadding="0" border="0" class="inputTable" style="padding-left: 30px;">
			    <tbody>
			     <tr>
			        <th><label>车辆类型：</label></th>
			        <td>
			            <input type="radio" name="carType" checked="checked" value="1"> 转诊车
			            <input type="radio" name="carType"  value="2" style="margin-left: 10px;"> 急诊车
			        </td>
			      </tr>
			      <tr>
			        <th><label>车牌号：</label></th>
			        <td>
			        	<input class="formText" type="text"  id="carNo" value="" style="width:200px" readonly="readonly">
			        	<span class="addMember" ng-show="!flag"><a
							class="icon_add" href="#" onclick="openCarList();">选择</a></span>
			        </td>
			      </tr>
			        <tr>
			        <th><label>驾驶员：</label></th>
			        <td>
		                <input type="text" class="select_style" id="driverName" onkeyup="findDriver()" style="width: 200px; border: #d9d9d9 1px solid;border-radius: 0;height: 28px;line-height: 28px;"/>
		                <ul style="display:none;position: absolute;background-color: #fff;width: 198px;z-index:10;border: 1px solid #d9d9d9;border-top:none;height: 90px;overflow: auto" id="driverUl">
		                    
		                </ul>
		                <span class="addMember" ng-show="!flag"><a class="icon_add" href="#" onclick="openDriverList();">选择</a></span>
			        	
			        </td>
			      </tr>
			      <tr>
			      <th><label>车辆电话号：</label></th>
			        <td>
			        	<input type="text" id="phone" value="" style="width: 200px;height: 28px;border: 1px solid #ccc;padding: 0 10px;box-sizing: border-box;">
			        </td>
			      </tr>
			    </tbody>
			  </table>
		  </form>
		</div>
	</body>
</html>
