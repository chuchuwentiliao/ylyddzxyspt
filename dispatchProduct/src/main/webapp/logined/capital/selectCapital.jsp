<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<html>
<meta charset="utf-8">
<title>选择设备</title>
<jsp:include page="../../common/flatHead.jsp" />
<link href="${ctx}flat/css/reset.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/css/hm_style.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/css/main.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/plugins/tree/skins/tree_default.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}plugins/datatable/jquery.dataTables.min.js"></script>
<script language="javascript" type="text/javascript" src="${ctx}plugins/My97DatePicker/WdatePicker.js"></script>
<link href="${ctx}flat/plugins/Accormenus/skins/Accormenus_default.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/flat/plugins/datatable/skins/datatable_default.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}plugins/tree/ztree/jquery.ztree.all-3.5.min.js?version=${version}"></script>
<script type="text/javascript" src="${ctx}js/common/treeNode.js"></script>
<script type="text/javascript" src="${ctx}js/logined/capital/selectCapital.js"></script>
<script type="text/javascript">
</script>
<style type="text/css">
	.dataTables_info {
	    width: 30%;
	    height: 26px;
	    margin-top: 15px;
	    float: left;
	    color: rgb(136, 136, 136);
	    position:inherit;
	    bottom: 15px;
	}
	.dataTables_paginate {
	    float: right;
	    text-align: right;
	    position: inherit;
	    bottom: 15px;
	    right: 5px;
	}
	.paging_full_numbers {
    width: 500px;
    height: 26px;
    line-height: 26px;
    margin-top: 0px;
}
</style>
<body>
<div class="mainpage_r" style="padding-right: 0px;height:458px">
<input id="type" type="hidden" value="${param.type}">
<input id="selectGroupId" type="hidden">
	<!-- 左侧树 -->
	<div class="leftMenu" style="left:0px">
		<div class="service-menu">
			<h1>资产组</h1>
			 <div class="zTreeDemoBackground">
				<ul id="myTree" class="ztree"></ul>
			</div> 
		</div>
	</div>
	<!-- 右侧列表 -->
	<div class="list overf" style="overflow:visible;">
		<div class="searchArea overf" style="overflow:visible;">
			<table cellspacing="0" cellpadding="0">
				<tbody>
					<tr>
						<td class="right"  >
						     <input style="float:right;" onClick="serch()" value="查询" class="searchButton" type="button" />
			                <div style="float:right;">
								<label>关键字	：</label>
			                	<input id="keyWord" type="text" class="formText" style="width: 220px;border-radius: 3px;height: 30px;" placeholder="名称/设备编号"/>
							</div>
							<div style="float:right;display: none">
			                	<label>设备状态：</label>
				                <select id="capitalStatus" style="height: 30px;width: 130px;border-radius:3px;" >
				                	<option value="1">在库</option>
				                </select>
			                </div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="center_content" style="width: 747px;height:420px">
			<table id="capitalTable" cellpadding="0" cellspacing="0"  class="pretty dataTable" style="font-size: 14px; width:auto;min-width:850px;height: auto;" >
				<thead>
					<tr>
			       	 	<th style="width:42px;">序号</th>
			        	<th style="width:80px;">设备编号</th>
			        	<th style="width:120px;">名称</th>
			        	<th style="width:80px;">设备型号</th>
			        	<th style="width:80px;">设备组</th>
			        	<th style="width:90px;">当前位置</th>
			        	<th style="width:50px;">操作</th>
			     	</tr>
				</thead>
			</table>
		</div>
	</div>
</div>

<div class="modal-add-doctor" style="width: 850px;padding-left:242px">
	<div class="modal-body">
		<!--end .screen-result-area-->
		<div class="add-name-box">
			<span class="btn-add" onclick="addCapital()">添加>></span>
			<div class="name-area" id="capitalDiv">
			</div>
			<!--end .name-area-->
		</div>
		<!--end .add-name-box-->
	</div>
</div>



</body>
</html>