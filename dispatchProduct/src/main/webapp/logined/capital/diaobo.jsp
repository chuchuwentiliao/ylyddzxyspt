<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>资产调拨</title>
    <jsp:include page="../../common/flatHead.jsp"/>
    <link href="${ctx }flat/css/reset.css" rel="stylesheet" type="text/css" />
  	<link href="${ctx}flat/css/main.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="${ctx}flat/plugins/tree/skins/tree_default.css" type="text/css"/>
	<link href="${ctx }flat/plugins/form/skins/form_default.css" rel="stylesheet" type="text/css" />
<style >
	 body{background-color: #fff;}
  	.formPage{width: 100%;height: 100%;padding-top: 0;} 
</style>
<!-- tree -->
<script type="text/javascript" src="${ctx}plugins/tree/ztree/jquery.ztree.all-3.5.min.js?version=${version}"></script>
<script TYPE="text/javascript" src="${ctx }js/logined/capital/diaobo.js"></script>
<script TYPE="text/javascript" src="${ctx }js/logined/capital/commonDetail.js"></script>
</head>
<body>
<input id="type" type="hidden" value="${param.type}">
<div class="formPage">
  <div class="formbg" >
  <form name="form">
    <div class="content_form">
      <table width="100%" border="0" cellpadding="0" cellspacing="0"  class="inputTable">
        <tr>
          <th><label>资产名称：</label></th>
          <td id="goodName"></td>
        </tr>
        <tr>
          <th><label>资产组：</label></th>
          <td id="capitalGroup"></td>
        </tr>
        <tr>
          <th><label>资产编号：</label></th>
          <td id="capitalNo"></td>
        </tr>
        <tr>
          <th><label>入库日期：</label></th>
          <td id="storageTime"></td>
        </tr>
        <tr>
          <th><label>资产品牌：</label></th>
          <td id="capitalBrand"></td>
        </tr>
        <tr>
          <th><label>资产型号：</label></th>
          <td id="capitalModel"></td>
        </tr>
        <tr>
          <th><label>资产类型：</label></th>
          <td id="capitalTypeStr">固定资产</td>
        </tr>
        <tr>
          <th><label>使用部门：</label></th>
          <td id="useGroup"></td>
        </tr>
        <tr>
          <th><label>使用人：</label></th>
          <td id="usePeople"></td>
        </tr>
        <tr>
           <th><label>调拨人：</label></th>
           <td>
	           <div class="divselect12  divselect13 " onmouseleave="javascript:$('#userUserTree').hide()">
               		<input type="hidden" id="useUserIds"/>
			      	<cite class="cite12 text_ellipsis cite13"  onClick="toggleSelectUser()" id="useUserText"></cite>
		      		<ul id="userUserTree" class="ztree leibie" style="margin-top:0.5px;display: none;height:200px !important;width:200px !important;" >
					</ul>
			  	</div>
			</td> 
        </tr>
        </table>
    </div>
    </form>
</div>
</div>
</body>
</html>