<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>资产入库</title>
    <jsp:include page="../../common/flatHead.jsp"/>
    <link href="${ctx }flat/css/reset.css" rel="stylesheet" type="text/css" />
	<link href="${ctx }flat/plugins/form/skins/form_default.css" rel="stylesheet" type="text/css" />
<style >
 .textarea{width:100% ; height: 74px;border: 1px solid #e4e4e4;}
  .formPage .textarea:hover , .formPage .formTextarea:hover{border:1px solid #cecece;}
.formPage .textarea:focus , .formPage .formTextarea:focus{border:1px solid #459fd2;}  
</style>
<!--angularjs类库-->
<script TYPE="text/javascript"
	src="${ctx }angularframe/lib/angular.min.js"></script>
<script TYPE="text/javascript"
	src="${ctx }angularframe/lib/angular-animate.min.js"></script>
<script TYPE="text/javascript"
	src="${ctx }angularframe/lib/angular-sanitize.min.js"></script>
<!-- My97 -->
<script type="text/javascript" src="${ctx }angularframe/directives/ui-datePicker.js"></script>
<!-- 分页 -->
<script TYPE="text/javascript" src="${ctx }angularframe/directives/ui-basepage.js"></script>
<!--bootstrapUI控件的angularjs版本-->
<script TYPE="text/javascript"
	src="${ctx }angularframe/lib/ui-bootstrap-tpls-1.3.3.min.js"></script>
	
</head>
<body>
<form action="#" id="userForm">
<div class="formPage">
  <div class="formbg">
    <div class="big_title">资产信息</div>     
    <div class="content_form">
      <table width="100%" border="0" cellpadding="0" cellspacing="0"  class="inputTable">
        <tr>
          <th><em class="requireField">*</em><label>资产名称：</label></th>
          <td><input type="text" class="formText" /></td>
          <th><em class="requireField">*</em><label>资产组：</label></th>
           <td><select ng-model="meetingId"  ng-options="meetingRoom.id as meetingRoom.name for meetingRoom in meetingRoomList" class="formText"></select></td>       
        </tr>
        <tr>
          <th><label>资产编码：</label></th>
          <td><input type="text" class="formText"  /></td>
          <th><label>资产品牌：</label></th>
          <td><input type="text" class="formText"  /></td>
               
        </tr>
        <tr>
           <th><label>资产型号：</label></th>
          <td><input type="text" class="formText"  /></td>
          <th><label>计量单位：</label></th>
           <td><select  class="formText">
               <option ng-click="">台</option> 
               <option ng-click="">张</option>
               <option ng-click="">个</option> 
               <option ng-click="">辆</option> 
               <option ng-click="">支</option> 
               <option ng-click="">部</option> 
               <option ng-click="">把</option> 
               <option ng-click="">套</option> 
               <option ng-click="">件</option> 
               <option ng-click="">只</option> 
               <option ng-click="">份</option> 
               <option ng-click="">本</option> 
               <option ng-click="">件</option> 
               <option ng-click="">包</option>  
             </select>
          </td>       
        </tr>
        <tr>
           <th><label>资产单价：</label></th>
          <td><input type="text" class="formText"  /></td>  
          <th><label>审批编号：</label></th>
          <td><input type="text" class="formText"  /></td>      
        </tr>
        <tr>          
           <th><label>存放地点：</label></th>
          <td><input type="text" class="formText"  /></td>
          <th><label>供应商：</label></th>
           <td><input type="text" class="formText"  /></td>
        </tr>
        <tr>
           
           <th><label>采购人：</label></th>
           <td><select ng-model="meetingId"  ng-options="meetingRoom.id as meetingRoom.name for meetingRoom in meetingRoomList" class="formText"></select></td>           
           <th><em class="requireField">*</em><label>购置日期：</label></th>
          <td><input type="text" ng-model="startTime" date-picker config="config" class="Wdate formText"/></td>
        </tr>
        <tr>       
          <th><label>发票号码：</label></th>
          <td><input type="text" class="formText"  /></td>        
        </tr>
        <tr>
        <th><label>备注：</label></th>
         <td colspan="3"><textarea class="textarea" ></textarea></td>     
        </tr>
        </table>
    </div>

  
  <div class="buttonArea">
    <input type="button" class="formButton_grey" value="取消"  id="userInfoUpdate"/>
    <input type="button" class="formButton_green" value="提交入库" id="back" />
  </div>
</div>
</form>
</body>
</html>