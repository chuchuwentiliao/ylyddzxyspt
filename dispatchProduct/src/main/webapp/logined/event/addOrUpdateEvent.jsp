<%@page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html ng-app="approveApp">
	<head>
		<meta charset="UTF-8">
		<title>新增事件</title>
		<jsp:include page="../../common/flatHead.jsp" />
		<link href="${ctx}flat/css/reset.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="${ctx}flat/css/hm_style.css" />
		<link href="${ctx }flat/plugins/form/skins/form_default.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="${ctx}flat/plugins/tree/skins/tree_default.css" type="text/css"/>
		<script type="text/javascript">
			var basePath = "${ctx}";
		</script>
		<script type="text/javascript" src="${ctx }flat/plugins/tree/skins/jquery.ztree.all-3.2.min.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/event/addOrUpdateEvent.js"></script>
		<script type="text/javascript" src="${ctx}js/logined/event/selectGroup.js"></script>
	</head>
	<!-- 维修部门 -->
	<input type="hidden" id="parentId"/>
	<input type="hidden" id="eventId" value="${param.eventId}"/>
	<input type="hidden" id="eventType" value="${param.eventType}"/>
	<body style="background-color:#fff;">
		<div class="elasticFrame formPage">
			<form name="myForm" >
			  <table width="100%" cellspacing="0" cellpadding="0" border="0" class="inputTable">
			    <tbody>
			      <tr >
			        <th><em class="requireField">*</em><label>事件标题：</label></th>
			        <td>
			        	<input class="formText" type="text" name="" value="" placeholder="" id="eventTitle" style="width:250px">
			        </td>
			      </tr>
			       <tr id="updateType">
			        <th><em class="requireField">*</em><label>服务项目分类：</label></th>
			        <td>
			        	<div id="treeDiv" style="z-index:66;position:relative">
							<input id="groupType" type="text" readonly="readonly" class="formText iconTree" style="width: 250px;"/>
							<div id="menuDiv" style="position: absolute;display: none;">
								<ul id="eventTypeTree" class="ztree" style="position: absolute; margin-top:0;width:250px;height:150px;overflow:auto; background: #ffffff;  border: 1px solid #8a9ba5"></ul>
							</div>
						</div>
			        </td>
			      </tr> 
			      <tr>
			        <th><em class="requireField">*</em><label>维修部门：</label></th>
			        <td>
			        	<div id="treeContent" style="z-index:6;position:relative">
							<input id="groupSel" type="text" readonly="readonly" class="formText iconTree" style="width: 250px;"/>
							<div id="menuContent" style="position: absolute;display: none;">
								<ul id="groupTree" class="ztree" style="position: absolute; margin-top:0;width:250px;height:150px;overflow:auto; background: #ffffff;  border: 1px solid #8a9ba5"></ul>
							</div>
						</div>
			        </td>
			      </tr>
			      <tr>
			        <th><label>维修工时：</label></th>
			        <td>
			        	<input class="formText" type="text" name="" value="" placeholder="" id="workingHours" style="width:250px" onkeyup="value=value.replace(/[^\d.]/g,'')">
			        </td>
			      </tr>
			      <tr>
			        <th><label>紧急程度：</label></th>
			        <td>
						<label>
						   <input  name="eventLevel" type="radio" value="1" onclick="chageTime(1)"/>紧急
						</label>
						<label>
						   <input  name="eventLevel" type="radio" value="2" checked="checked" onclick="chageTime(2)"/>一般
						</label>
			        </td>
			      </tr>
			       <tr style="display: none">
			        <th><label>期望完成时间：</label></th>
			        <td class="pr">
			        	 <input type="text" name="defectTimeStr" id="defectTime" onFocus="WdatePicker({skin:'default',dateFmt:'yyyy-MM-dd HH:mm:ss'})" class="in_area Wdate" style="width:258px;background-position-x:230px;cursor:pointer;"/>
			        </td>
			      </tr>
			    </tbody>
			  </table>
		  </form>
		</div>
	</body>
</html>
