<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>事件列表</title>
<jsp:include page="../../common/flatHead.jsp" />
<link href="${ctx}flat/css/reset.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/css/main.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="${ctx}flat/plugins/tree/skins/tree_default.css" type="text/css"/>
<link href="${ctx}flat/plugins/datatable/skins/datatable_default.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/plugins/Accormenus/skins/Accormenus_default.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}plugins/tree/ztree/jquery.ztree.all-3.5.min.js"></script>
<script type="text/javascript" src="${ctx}flat/plugins/datatable/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="${ctx}js/placeholder.js"></script>
<script type="text/javascript" src="${ctx}flat/js/base.js"></script>
<script type="text/javascript" src="${ctx}flat/plugins/tree/skins/jquery.ztree.all-3.2.min.js"></script>
<script type="text/javascript" src="${ctx}plugins/datatable/selecedForDatatablePagination.js"></script>

<script type="text/javascript" src="${ctx}js/logined/event/eventList.js?version=${version}"></script>
<script type="text/javascript" src="${ctx}js/common/validate_form.js?version=${version}"></script>
<script type="text/javascript" src="${ctx}js/common/showError.js?version=${version}"></script>
<script type="text/javascript" src="${ctx}js/logined/user/importuserFather.js?version=${version}"></script>

<script type="text/javascript" src="${ctx}js/common/ajaxfileupload.js?version=${version}"></script>

<script type="text/javascript" src="${ctx}plugins/My97DatePicker/WdatePicker.js?version=${version}"></script>
</head>
<body>
  <form id="userSelectFrom">
    <input type="hidden" id="type" value="${param.type}" />
	<input name="groupId" type="hidden" id="eventType" value="${param.groupId}" />
  </form>

  <div class="list">
    <div class="searchArea">
      <table cellspacing="0" cellpadding="0">
        <tbody>
          <tr>
            <td class="right">
              <label>关键字：</label>
              <span class="position:relative;">
              <input type="text"  placeholder="事件标题" maxlength="30" class="formText searchkey" id="searchName" name="searchName"/>
              </span>
              <input id="searchButton" type="button" value="查询" class="searchButton"/></td>
            <td style="width:370px;">
                <div class="fButton greenBtn" ><span class="add" onclick="toAddOrUpdate()">新增</span> </div>
                <div class="fButton orangeBtn"> <span class="delete" onclick="deleteEvent()">删除</span> </div>
         		<div class="fButton blueBtn"> <span class="export" id="eventExport">导出</span> </div>
         		<div class="fButton greenBtn"> <span class="import" id="importEvent">导入</span> </div>
          </td>
          </tr>
        </tbody>
      </table>
    </div>
    <table cellpadding="0" cellspacing="0"  class="pretty dataTable" id="eventTable">
     <thead>
      <tr>
       <th  class="chk"><input type='checkbox' id='total'/></th>
       <th id="no" class="num">序号</th>
       <th id="eventName" class="longTxt" style="width:20%;">标题</th>
       <th id="urgencyLevelName" class="tdCenter" style="width:100px;">紧急程度</th>
      <!--  <th id="completeTime" class="tdCenter" style="width:20%;">期望完成时间</th> -->
       <th id="repairName" class="tdCenter" style="width:20%;">维修部门</th>
       <th id="workHour" class="tdCenter" style="width:100px;">维修工时</th>
       <th  class="right_bdr0" style="width:70px;">操作</th>
       </tr>	
      </thead>
    </table>
  </div>
<script>funPlaceholder(document.getElementById("searchName"));</script>
</body>
</html>
