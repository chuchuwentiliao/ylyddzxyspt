var currentTab;//用于详情点击返回返回到当前tab列表
$(document).ready(function() {
	waitDispatchList();
	//控住显示tab显示
	currentTab = $("#currentTab").val();
	if(currentTab == "waitDispatchList" || currentTab == undefined || currentTab == null || currentTab == ""){
		$("#waitDispatchList").addClass("active");
		$("#waitDispatchList").siblings().removeClass("active");
		waitDispatchList();
	}
	if(currentTab == "waitStartList"){
		$("#waitStartList").addClass("active");
		$("#waitStartList").siblings().removeClass("active");
		waitStartList();
	}
	if(currentTab == "startList"){
		$("#startList").addClass("active");
		$("#startList").siblings().removeClass("active");
		startList();
	}
	if(currentTab == "arriveList"){
		$("#arriveList").addClass("active");
		$("#arriveList").siblings().removeClass("active");
		arriveList();
	}
	if(currentTab == "carBackList"){
		$("#carBackList").addClass("active");
		$("#carBackList").siblings().removeClass("active");
		carBackList();
	}
	
	$(document).keyup(function(event){
		  if(event.keyCode ==13){
		    $("#searchCarBackList").trigger("click");
		  }
		});
});

/**
 * 待派遣列表
 */
function waitDispatchList(obj){
	if(obj != null || obj != undefined || obj != 'undefined'){
		tabControl(obj);//头部高亮显示
		currentTab = "waitDispatchList";
	}
	//控制其他几部分显示
	$("#waitDispatchDiv").show();
	$("#waitDispatchDiv").siblings().hide();
	var iDisplayStart = 0;
	$('#myWaitDispatchTable').dataTable({
		"bProcessing" : true,
		'bServerSide' : true,
		'fnServerParams' : function(aoData) {
			aoData.push({
				"name" : "carOrderVo.status",
				"value" : 1
			});
		},
		"sAjaxSource" : basePath + "cardispatch/cardispatch_findCarDispatch.action",
		"sServerMethod" : "POST",
		"sPaginationType" : "full_numbers",
		"bPaginate" : true, // 翻页功能
		"bStateSave" : false, // 状态保存
		"bLengthChange" : false, // 改变每页显示数据数量
		"bFilter" : false, // 过滤功能
		"bSort" : false, // 排序功能
		"bInfo" : true,// 页脚信息
		"bAutoWidth" : false,// 自动宽度
		"bDestroy" : true,//用于当要在同一个元素上履行新的dataTable绑订时，将之前的那个数据对象清除掉，换以新的对象设置
		"iDisplayLength" : 15, // 每页显示多少行
		"aoColumns" : [{
					"mDataProp" : "no",//序号
				}, {
					"mDataProp" : "instanceId" //工单编号
				}, {
					"mDataProp" : "businessType", //派遣类型
				}, {
					"mDataProp" : "referralDepartment" //接诊科室
				}, {
					"mDataProp" : "carTypeStr" //车辆类型
				}, {
					"mDataProp" : "startLocation" //出发地点
				}, {
					"mDataProp" : "arriveLocation" //到达地点
				}, {
					"mDataProp" : "planStartTime" //计划发车时间
				}, {
					"mDataProp" : "createTime" //登记时间
				}, {
					"mDataProp" : null, //操作
				}],
		"oLanguage": {
			   "sUrl": basePath+"plugins/datatable/cn.txt" //中文包
		   },
		"fnDrawCallback": function (oSettings) {
			   $('#myWaitDispatchTable tbody  tr td,#myWaitDispatchTable tbody  tr td a').each(function() {
					this.setAttribute('title', $(this).text());
			   });
		   },
		"fnFooterCallback": function( nFoot, aData, iStart, iEnd, aiDisplay ) {
				var Pagecount=aData.length; //在这里这个没有用到。
		   },
		"aoColumnDefs" : [{
			"aTargets" : [9], //操作
			"fnRender" : function(oObj) {
				var instanceId = oObj.aData.instanceId;
				var type = oObj.aData.type;
				var carType = oObj.aData.carType
				return '<a href="javascript:void(0);" onclick="openDispatch(\''+instanceId+'\',\''+carType+'\');">派遣</a><a href="javascript:void(0);" onclick="showView('+instanceId+');">查看</a>';
			}
		}]
	}); 
}

/**
 * 待发车列表
 */
function waitStartList(obj){
	if(obj != null || obj != undefined || obj != 'undefined'){
		tabControl(obj);//头部高亮显示
		currentTab = "waitStartList";
	}
	//控制其他几部分显示
	$("#waitStartdiv").show();
	$("#waitStartdiv").siblings().hide();
	
	var iDisplayStart = 0;
	$('#myWaitStartTable').dataTable({
		"bProcessing" : true,
		'bServerSide' : true,
		'fnServerParams' : function(aoData) {
			aoData.push({
				"name" : "carOrderVo.status",
				"value" : 2
			});
		},
		"sAjaxSource" : basePath + "cardispatch/cardispatch_findCarDispatch.action",
		"sServerMethod" : "POST",
		"sPaginationType" : "full_numbers",
		"bPaginate" : true, // 翻页功能
		"bStateSave" : false, // 状态保存
		"bLengthChange" : false, // 改变每页显示数据数量
		"bFilter" : false, // 过滤功能
		"bSort" : false, // 排序功能
		"bInfo" : true,// 页脚信息
		"bAutoWidth" : false,// 自动宽度
		"bDestroy" : true,//用于当要在同一个元素上履行新的dataTable绑订时，将之前的那个数据对象清除掉，换以新的对象设置
		"iDisplayLength" : 15, // 每页显示多少行
		"aoColumns" : [{
					"mDataProp" : "no", //序号
				}, {
					"mDataProp" : "instanceId" //工单编号
				}, {
					"mDataProp" : "businessType", //派遣类型
				}, {
					"mDataProp" : "carTypeStr" //车辆类型
				}, {
					"mDataProp" : "startLocation" //出发地点
				}, {
					"mDataProp" : "arriveLocation" //到达地点
				}, {
					"mDataProp" : "planStartTime" //计划发车时间
				}, {
					"mDataProp" : null //车牌号
				}, {
					"mDataProp" : "driverName" //驾驶人	
				}, {
					"mDataProp" : "dispatchTime" //派遣时间
				}, {
					"mDataProp" : null, //操作
				}],
		"oLanguage": {
			   "sUrl": basePath+"plugins/datatable/cn.txt" //中文包
		   },
		"fnDrawCallback": function (oSettings) {
			   $('#myWaitStartTable tbody  tr td,#myWaitStartTable tbody  tr td a').each(function() {
					this.setAttribute('title', $(this).text());
			   });
		   },
		"fnFooterCallback": function( nFoot, aData, iStart, iEnd, aiDisplay ) {
				//nFoot.getElementsByTagName('th')[0].innerHTML = "Starting index is "+iStart;
				//alert("aData.length:  "+aData.length); // 打印该页显示多少行记录。
				var Pagecount=aData.length; //在这里这个没有用到。
		   },
		"aoColumnDefs" : [{
			"aTargets" : [10], //操作
			"fnRender" : function(oObj) {
				var instanceId = oObj.aData.instanceId;
				var type = oObj.aData.type;
				return '<a href="javascript:void(0);" onclick="start('+instanceId+');">发车</a><a href="javascript:void(0);" onclick="cancel('+instanceId+');">撤销</a><a href="javascript:void(0);" onclick="showView('+instanceId+');">查看</a> ';
			}
		},{
			"aTargets" : [7], //车辆信息
			"fnRender" : function(oObj) {
				var carNo = oObj.aData.carNo;
				var carPhone = oObj.aData.carPhone;
				return carNo+'('+carPhone+')';
			}
		}]
	}); 
}

/**
 * 已发车列表
 */
function startList(obj){
	if(obj != null || obj != undefined || obj != 'undefined'){
		tabControl(obj);//头部高亮显示
		currentTab = "startList";
	}
	//控制其他几部分显示
	$("#startdiv").show();
	$("#startdiv").siblings().hide();
	
	var iDisplayStart = 0;
	$('#myStartTable').dataTable({
		"bProcessing" : true,
		'bServerSide' : true,
		'fnServerParams' : function(aoData) {
			aoData.push({
				"name" : "carOrderVo.status",
				"value" : 3
			});
		},
		"sAjaxSource" : basePath + "cardispatch/cardispatch_findMyProcessedCarDispatch.action",
		"sServerMethod" : "POST",
		"sPaginationType" : "full_numbers",
		"bPaginate" : true, // 翻页功能
		"bStateSave" : false, // 状态保存
		"bLengthChange" : false, // 改变每页显示数据数量
		"bFilter" : false, // 过滤功能
		"bSort" : false, // 排序功能
		"bInfo" : true,// 页脚信息
		"bAutoWidth" : false,// 自动宽度
		"bDestroy" : true,//用于当要在同一个元素上履行新的dataTable绑订时，将之前的那个数据对象清除掉，换以新的对象设置
		"iDisplayLength" : 15, // 每页显示多少行
		"aoColumns" : [{
					"mDataProp" : "no", //序号
				}, {
					"mDataProp" : "instanceId" //工单编号
				}, {
					"mDataProp" : "businessType", //派遣类型
				}, {
					"mDataProp" : "carTypeStr" //车辆类型
				}, {
					"mDataProp" : "startLocation" //出发地点
				}, {
					"mDataProp" : "arriveLocation" //到达地点
				}, {
					"mDataProp" : "planStartTime" //计划发车时间
				}, {
					"mDataProp" : null//车牌号
				}, {
					"mDataProp" : "driverName" //驾驶人	
				}, {
					"mDataProp" : "startTime" //发车时间
				}, {
					"mDataProp" : null, //操作
				}],
		"oLanguage": {
			   "sUrl": basePath+"plugins/datatable/cn.txt" //中文包
		   },
		"fnDrawCallback": function (oSettings) {
			   $('#myStartTable tbody  tr td,#myStartTable tbody  tr td a').each(function() {
					this.setAttribute('title', $(this).text());
			   });
		   },
		"fnFooterCallback": function( nFoot, aData, iStart, iEnd, aiDisplay ) {
				//nFoot.getElementsByTagName('th')[0].innerHTML = "Starting index is "+iStart;
				//alert("aData.length:  "+aData.length); // 打印该页显示多少行记录。
				var Pagecount=aData.length; //在这里这个没有用到。
		   },
		"aoColumnDefs" : [{
			"aTargets" : [10], //操作
			"fnRender" : function(oObj) {
				var instanceId = oObj.aData.instanceId;
				var type = oObj.aData.type;
				var status = oObj.aData.status;
				var html="";
				if(status==3){
					html+='<a href="javascript:void(0);" onclick="carArrive(\''+instanceId+'\');">到达</a>';
				}
				html+='<a href="javascript:void(0);" onclick="showView('+instanceId+');">查看</a> ';
				return  html;
			}
		},{
			"aTargets" : [7], //车辆信息
			"fnRender" : function(oObj) {
				var carNo = oObj.aData.carNo;
				var carPhone = oObj.aData.carPhone;
				return carNo+'('+carPhone+')';
			}
		}]
	}); 
}


/**
 * 到达列表
 */
function arriveList(obj){
	if(obj != null || obj != undefined || obj != 'undefined'){
		tabControl(obj);//头部高亮显示
		currentTab = "arriveList";
	}
	//控制其他几部分显示
	$("#arrivediv").show();
	$("#arrivediv").siblings().hide();
	
	var iDisplayStart = 0;
	$('#myArriveTable').dataTable({
		"bProcessing" : true,
		'bServerSide' : true,
		'fnServerParams' : function(aoData) {
			aoData.push({
				"name" : "carOrderVo.status",
				"value" : 7
			});
		},
		"sAjaxSource" : basePath + "cardispatch/cardispatch_findCarDispatch.action",
		"sServerMethod" : "POST",
		"sPaginationType" : "full_numbers",
		"bPaginate" : true, // 翻页功能
		"bStateSave" : false, // 状态保存
		"bLengthChange" : false, // 改变每页显示数据数量
		"bFilter" : false, // 过滤功能
		"bSort" : false, // 排序功能
		"bInfo" : true,// 页脚信息
		"bAutoWidth" : false,// 自动宽度
		"bDestroy" : true,//用于当要在同一个元素上履行新的dataTable绑订时，将之前的那个数据对象清除掉，换以新的对象设置
		"iDisplayLength" : 15, // 每页显示多少行
		"aoColumns" : [{
					"mDataProp" : "no", //序号
				}, {
					"mDataProp" : "instanceId" //工单编号
				}, {
					"mDataProp" : "businessType", //派遣类型
				}, {
					"mDataProp" : "carTypeStr" //车辆类型
				}, {
					"mDataProp" : "startLocation" //出发地点
				}, {
					"mDataProp" : "arriveLocation" //到达地点
				}, {
					"mDataProp" : "planStartTime" //计划发车时间
				}, {
					"mDataProp" : null//车牌号
				}, {
					"mDataProp" : "driverName" //驾驶人	
				}, {
					"mDataProp" : "startTime" //发车时间
				}, {
					"mDataProp" : "arriveTime" //到达时间
				}, {
					"mDataProp" : null, //操作
				}],
		"oLanguage": {
			   "sUrl": basePath+"plugins/datatable/cn.txt" //中文包
		   },
		"fnDrawCallback": function (oSettings) {
			   $('#myStartTable tbody  tr td,#myStartTable tbody  tr td a').each(function() {
					this.setAttribute('title', $(this).text());
			   });
		   },
		"fnFooterCallback": function( nFoot, aData, iStart, iEnd, aiDisplay ) {
				//nFoot.getElementsByTagName('th')[0].innerHTML = "Starting index is "+iStart;
				//alert("aData.length:  "+aData.length); // 打印该页显示多少行记录。
				//var Pagecount=aData.length; //在这里这个没有用到。
		   },
		"aoColumnDefs" : [{
			"aTargets" : [7], //车辆信息
			"fnRender" : function(oObj) {
				var carNo = oObj.aData.carNo;
				var carPhone = oObj.aData.carPhone;
				return carNo+'('+carPhone+')';
			}
		},{
			"aTargets" : [11], //操作
			"fnRender" : function(oObj) {
				var instanceId = oObj.aData.instanceId;
				var type = oObj.aData.type;
				var status = oObj.aData.status;
				var html="";
				if(status==7){
					html+='<a href="javascript:void(0);" onclick="carBack(\''+instanceId+'\');">回场</a>';
				}
				html+='<a href="javascript:void(0);" onclick="showView('+instanceId+');">查看</a> ';
				return  html;
			}
		}]
	}); 
}

/**
 * 已回场列表
 */
function carBackList(obj){
	if(obj != null || obj != undefined || obj != 'undefined'){
		tabControl(obj);//头部高亮显示
		currentTab = "carBackList";
	}
	//控制其他几部分显示
//	$("#carBackdiv").removeClass("hide");
//	$("#carBackdiv").siblings(".canverse_tap_box").addClass("hide");
	$("#carBackdiv").show();
	$("#carBackdiv").siblings().hide();
	var iDisplayStart = 0;
	var businessType = $("#businessType").val();//派遣类型
	var carType = $("#carType").val();//车辆类型
	var arriveLocation = $("#arriveLocation").val();//实际到达地点
	var StartTime_begin = $("#StartTime_begin").val();//实际发车开始时间
	var StartTime_end = $("#StartTime_end").val();//实际发车结束时间
	var carNo = $("#carNo").val();//车牌号
	var driver = $("#driver").val();//驾驶人
	var returnTime_bengin = $("#returnTime_bengin").val();//回场开始时间
	var returnTime_end = $("#returnTime_end").val();//回场结束时间
	$('#myCarBackTable').dataTable({
		"bProcessing" : true,
		'bServerSide' : true,
		'fnServerParams' : function(aoData) {
			aoData.push({
				"name" : "carOrderVo.status",
				"value" : 4
			},{
				"name" : "carOrderVo.businessType",
				"value" : businessType
			},{
				"name" : "carOrderVo.carType",
				"value" : carType
			},{
				"name" : "carOrderVo.arriveLocation",
				"value" : arriveLocation
			},{
				"name" : "carOrderVo.StartTime_begin",
				"value" : StartTime_begin
			},{
				"name" : "carOrderVo.StartTime_end",
				"value" : StartTime_end
			},{
				"name" : "carOrderVo.carNo",
				"value" : carNo
			},{
				"name" : "carOrderVo.driver",
				"value" : driver
			},{
				"name" : "carOrderVo.returnTime_bengin",
				"value" : returnTime_bengin
			},{
				"name" : "carOrderVo.returnTime_end",
				"value" : returnTime_end
			});
		},
		"sAjaxSource" : basePath + "cardispatch/cardispatch_findMyProcessedCarDispatch.action",
		"sServerMethod" : "POST",
		"sPaginationType" : "full_numbers",
		"bPaginate" : true, // 翻页功能
		"bStateSave" : false, // 状态保存
		"bLengthChange" : false, // 改变每页显示数据数量
		"bFilter" : false, // 过滤功能
		"bSort" : false, // 排序功能
		"bInfo" : true,// 页脚信息
		"bAutoWidth" : false,// 自动宽度
		"bDestroy" : true,//用于当要在同一个元素上履行新的dataTable绑订时，将之前的那个数据对象清除掉，换以新的对象设置
		"iDisplayLength" : 15, // 每页显示多少行
		"aoColumns" : [{
					"mDataProp" : "no", //序号
				}, {
					"mDataProp" : "instanceId" //工单编号
				}, {
					"mDataProp" : "businessType", //派遣类型
				}, {
					"mDataProp" : "carTypeStr" //车辆类型
				}, {
					"mDataProp" : "startLocation" //出发地点
				}, {
					"mDataProp" : "arriveLocation" //到达地点
				}, {
					"mDataProp" : null //车牌号
				}, {
					"mDataProp" : "driverName" //驾驶人	
				}, {
					"mDataProp" : "startTime" //发车时间
				}, {
					"mDataProp" : "backFactoryTime" //回场时间
				}, {
					"mDataProp" : "time" //任务耗时
				}, {
					"mDataProp" : null, //操作
				}],
		"oLanguage": {
			   "sUrl": basePath+"plugins/datatable/cn.txt" //中文包
		   },
		"fnDrawCallback": function (oSettings) {
			   $('#myCarBackTable tbody  tr td,#myCarBackTable tbody  tr td a').each(function() {
					this.setAttribute('title', $(this).text());
			   });
		   },
		"fnFooterCallback": function( nFoot, aData, iStart, iEnd, aiDisplay ) {
				//nFoot.getElementsByTagName('th')[0].innerHTML = "Starting index is "+iStart;
				//alert("aData.length:  "+aData.length); // 打印该页显示多少行记录。
				var Pagecount=aData.length; //在这里这个没有用到。
		   },
		"aoColumnDefs" : [{
			"aTargets" : [11], //操作
			"fnRender" : function(oObj) {
				var instanceId = oObj.aData.instanceId;
				var type = oObj.aData.type;
				return '<a href="javascript:void(0);" onclick="showView('+instanceId+');">查看</a> ';
			}
		},{
			"aTargets" : [6], //车辆信息
			"fnRender" : function(oObj) {
				var carNo = oObj.aData.carNo;
				var carPhone = oObj.aData.carPhone;
				return carNo+'('+carPhone+')';
			}
		}]
	}); 
}

function tabControl(obj){
	$(obj).addClass("active");
	$(obj).siblings().removeClass("active");
}

function searchCarBackList(){
	carBackList();
}

/**
 * 跳转详情页面
 */
function showView(instanceId){
	window.location.href = basePath + "logined/carDispatch/view.jsp?instanceId=" + instanceId + "&currentTab=" + currentTab;
}

//弹出派遣
function openDispatch(instanceId,carType){
 	art.dialog.data("waitDispatchList",waitDispatchList);
	var url=basePath+"logined/carDispatch/dispatch.jsp?instanceId="+instanceId+"&carType="+carType;
	var artWindow=art.dialog.open(url,{
		title : "派遣",
		width : 480,
		height : 250,
		lock : true,
		drag : true,
		opacity : 0.08,//透明度
		button : [{
			name : '确定',
			focus:true,
			callback : function() {
				return false;
			}
		}, {
			name : '取消',
			callback : function() {
				return true;
			}
		}]
	});
}
/**
 * 撤销
 * @returns
 */
var issbumit_cancel=0;
function cancel(instanceId){
	if(issbumit_cancel==1){
		return;
	}
	art.dialog.confirm("是否撤销?", function () {
		issbumit_cancel=1;
		$.ajax({
			url:basePath+"cardispatch/approve.action",
		    data:{"instanceId":instanceId,"taskName":6},
		    type:'post',
		    dataType:'text',
		    success:function(data){
		    	if(data==1){
		    		issbumit_cancel=0;
		    		art.dialog.tips("操作成功!");
		    		waitStartList();
		    	}else{
		    		isSbumit=0;
					art.dialog.close();
					waitStartList();
					art.dialog.alert(data);
		    	}
		    }
		 })
        
    })
}

/**
 * 发车
 * @returns
 */
var issbumit=0;
function start(instanceId){
	if(issbumit==1){
		return;
	}
	art.dialog.confirm("是否发车?", function () {
		issbumit=1;
		$.ajax({
			url:basePath+"cardispatch/approve.action",
		    data:{"instanceId":instanceId,"taskName":2},
		    type:'post',
		    dataType:'text',
		    success:function(data){
		    	if(data==1){
		    		art.dialog.tips("操作成功!");
		    		issbumit=0;
		    		waitStartList();
		    	}else{
		    		isSbumit=0;
					art.dialog.close();
					waitStartList();
					art.dialog.alert(data);
		    	}
		    }
		 })
        
    })
}


/**
 * 到达
 * @returns
 */
var issbumit=0;
function carArrive(instanceId){
	if(issbumit==1){
		return;
	}
	art.dialog.confirm("是否已到达?", function () {
		issbumit=1;
		$.ajax({
			url:basePath+"cardispatch/approve.action",
		    data:{"instanceId":instanceId,"taskName":7},
		    type:'post',
		    dataType:'text',
		    success:function(data){
		    	if(data==1){
		    		art.dialog.tips("操作成功!");
		    		issbumit=0;
		    		startList();
		    	}else{
		    		isSbumit=0;
					art.dialog.close();
					startList();
					art.dialog.alert(data);
		    	}
		    }
		 })
        
    })
}

/**
 * 回场
 * @returns
 */
var issbumit_back=0;
function carBack(instanceId){
	if(issbumit_back==1){
		return;
	}
	art.dialog.confirm("是否回场?", function () {
		issbumit_back=1;
		$.ajax({
			url:basePath+"cardispatch/approve.action",
		    data:{"instanceId":instanceId,"taskName":3},
		    type:'post',
		    dataType:'text',
		    success:function(data){
		    	if(data==1){
		    		issbumit_back=0;
		    		art.dialog.tips("操作成功!");
		    		startList();
		    	}else{
		    		issbumit_back=0;
					art.dialog.close();
					startList();
					art.dialog.alert(data);
		    	}
		    }
		 })
        
    })
}



