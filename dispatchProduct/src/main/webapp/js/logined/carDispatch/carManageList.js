$(document).ready(function() {
	// 展示列表
	getDataTable();

	// 点击查询
	$("#seach_btn").click(function() {
		getDataTable();
	})
	// 点击添加
	$("#add_btn").click(function() {
		showAddCar();
	})
	
	//添加回车事件
	$(document).keydown(function(event){
		var code=event.which;
		if (code == 13) {
			getDataTable();
		}
	}); 
});
// 列表展示
function getDataTable() {
	$('#carTable')
			.dataTable(
					{
						"bProcessing" : true,
						'bServerSide' : true,
						'fnServerParams' : function(aoData) {
							aoData.push({
								"name" : "carVo.carNo",
								"value" : $("#carNo").val()
							},// 车牌号
							{
								"name" : "carVo.carType",
								"value" : $("#carType").val()
							},// 类型
							{
								"name" : "carVo.carBrand",
								"value" : $("#carBrand").val()
							}
							);
						},
						"sAjaxSource" : basePath + "cars/carList.action",
						"sServerMethod" : "POST",
						"sPaginationType" : "full_numbers",
						"bPaginate" : true, // 翻页功能
						"bStateSave" : false, // 状态保存
						"bLengthChange" : false, // 改变每页显示数据数量
						"bFilter" : false, // 过滤功能
						"bSort" : false, // 排序功能
						"bInfo" : true,// 页脚信息
						"bAutoWidth" : false,// 自动宽度
						"bDestroy" : true,//用于当要在同一个元素上履行新的dataTable绑订时，将之前的那个数据对象清除掉，换以新的对象设置
						"iDisplayLength" : 15, // 每页显示多少行
						"aoColumns" : [ {
							"sTitle" : '序号',
							"mDataProp" : "no",
							"sWidth" : "50",
							"sClass" : "tdCenter"
						}, {
							"sTitle" : '车牌号',
							"mDataProp" : "carNo",
							"sWidth" : "40%",
							"sClass" : "tdCenter"
						},  {
							"sTitle" : '车辆电话号',
							"mDataProp" : "carPhone",
							"sWidth" : "60%",
							"sClass" : "tdCenter"
						}, {
							"sTitle" : '车辆类型',
							"mDataProp" : "carType",
							"sWidth" : "60%",
							"sClass" : "tdCenter"
						}, {
							"sTitle" : '历史任务数',
							"mDataProp" : "historyNum",
							"sWidth" : "60%",
							"sClass" : "tdCenter"
						}, {
							"sTitle" : '当前状态',
							"mDataProp" : "status",
							"sWidth" : "60%",
							"sClass" : "tdCenter"
						}, {
							"sTitle" : '担架',
							"mDataProp" : "havaStretcher",
							"sWidth" : "60%",
							"sClass" : "tdCenter"
						}, {
							"sTitle" : '总座位数',
							"mDataProp" : "seatNum",
							"sWidth" : "60%",
							"sClass" : "tdCenter"
						}, {
							"sTitle" : '操作',
							"mDataProp" : null,
							"sWidth" : "80%",
							"sClass" : "right_bdr0"
						} ],
						"oLanguage" : {
							"sUrl" : basePath + "plugins/datatable/cn.txt" // 中文包
						},
						"fnDrawCallback" : function(oSettings) {
							$("#total").prop("checked", false);
							// 提示
							$('#carTable tbody  tr td').each(function() {
								var t = jQuery(this).text();
								var showText = $(this).text();
								showText = showText.replace(/<[^>].*?>/g, "");
								if (t.indexOf('[') < 0 && showText.length > 3) {
									$(this).attr("title", showText); // 每个TD显示title
								}
							});
						},
						"aoColumnDefs" : [
								{
									"aTargets" : [ 8 ],
									"fnRender" : function(oObj) {
										var carId = oObj.aData.id;
										var statuStr = oObj.aData.status;
										return '<a href="javascript:void(0);" class="view_url" onclick=updateLoginUser("'+carId+'"); id="updateUrl">编辑</a><a id="' + carId + '" href="javascript:void(0);" onclick=deleteClick('+carId+',"'+statuStr+'")>删除</a>';
									}
								} ]
					});
}
function updateLoginUser(carId){
	art.dialog.data('getDataTable', getDataTable);
	var url = basePath +"cars/editCar.action?carVo.id="+carId;
	var titleVal= "修改车辆";
	art.dialog.open(url,{
		lock : true,
		opacity :0,
		title:	titleVal,
		width : 400,
		height : 300,
	});
}
/**
 * 删除
 * 
 * @param delId
 */
function deleteClick(delId,statuStr) {
	if (statuStr != "空闲") {
		art.dialog.alert("请删除空闲的车辆");
		return ;
	}
	var dataParam = {
			'ids' : delId
		};
	art.dialog.confirm("确定要删除吗?", function () {
		$.ajax({
			url : basePath + "cars/deleteCar.action",
		    data:dataParam,
		    type:'post',
		    dataType:'text',
		    success:function(data){
		    	if(data==1){
		    		art.dialog.tips("删除成功!");
		    		getDataTable();
		    	}else{
		    		art.dialog.tips("删除失败!");
		    	}
		    }
		 })
        
    })
}

/**
 * 弹出添加车辆
 */
function showAddCar() {
	art.dialog.data('getDataTable', getDataTable);
	var url = basePath + "logined/carDispatch/carManageAdd.jsp";
	art.dialog.open(url, {
		lock : true,
		opacity :0,
		title : '车辆新增',
		width : 400,
		height : 300,
	});

}
