$(function(){
	initButton();
	position();
	
});

/**
 * 报废
 * */
function change(){
	var id=art.dialog.data("capitalId");
	var param = {
			"capitalChangesVo.type":2,
			"capitalChangesVo.id":id,
			"capitalChangesVo.storeLocation":$("#position").val()
	}
	$.ajax({
		url:basePath+"capital/changes.action",
		type:'post',
		data:param,
		dataType:'text',
		success:function(result){
			if(result==1){
				art.dialog.tips("归还成功");
				setTimeout(function(){
					art.dialog.data("result","success");
					art.dialog.close();
				},500);
			}else{
				art.dialog.alert("归还失败，请稍后重试！");
			}
		}
	});
}

/**
 * 初始化按钮
 */
function initButton(){
	var api = art.dialog.open.api;
	api.button(
			{
				name: '确定',
				callback: function () {
					change();
					return false;
				},
				focus: true
			},
			{
				name: '取消'
			}
		);
}


//加载存放位置下拉选
function position(){
	var param={
		"infoType":"capital_store",
		"sysTag":1
	}
	$.ajax({
		url:basePath+"dict/getDicts.action",
		type:'post',
		data:param,
		dataType:'json',
		success:function(data){
			var html="<option value =''>请选择</option>";
			if(data!=null){
				for(var i=0;i<data.length;i++){
					 html+="<option value ='"+data[i].value+"'>"+data[i].name+"</option>";
				}
			}
			$("#position").html(html);
		}
	})
};


function initUserTree(){
    var useUserIds=$("#useUserIds").val();
    qytx.app.tree.user({
    	id:"userUserTree",
    	type:"check",
    	click:callBackUser
    });
}

function callBackUser(data)
{
    if(data){
    	var userIds = [];
    	var userNames = [];
    	$(data).each(function(i,item){
    		var uId = item.id.indexOf("uid_");
    		if(item.id.indexOf("uid_") != -1){
    			userIds.push(item.id.substring(4));
    			userNames.push(item.name);
        	}
    	});
        $("#useUserText").text(userNames.join(","));
        $("#useUserIds").val(userIds.join(","));
    }
}

function toggleSelectUser(){
	$("#userUserTree").show();
}