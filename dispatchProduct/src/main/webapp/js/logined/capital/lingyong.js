$(function(){
	initButton();
	initUserTree();
})



    /**
	 * 领用
	 * */
  var float =1 ;
   function change(){
	   var id=art.dialog.data("capitalId");
		if(float!=1){
			return;
		}
		var useUserId = $("#useUserIds").val();
		if(!useUserId){//没有选择人员
			art.dialog.alert("请选择人员！");
			return;
		}
		var reg=/^[0-9]\d*$/;
		var isShowCapitalNum=$("#isShowCapitalNum").val();
		if(isShowCapitalNum==1){
			var capitalNum=$("#capitalNum").val();
			if(!capitalNum){
				art.dialog.alert("请选择领用数量！");
				return;
			}
			if(!reg.test(capitalNum)){
		    	art.dialog.alert("领用数量必须为正整数！");
		    	return;
		    }
		}
		var param = {
				"capitalChangesVo.type":1,
				"capitalChangesVo.id":id,
				"capitalChangesVo.useUserId":useUserId,
				"capitalChangesVo.capitalNum":capitalNum
		}
		$.ajax({
			url:basePath+"capital/changes.action",
			type:'post',
			data:param,
			dataType:'text',
			success:function(result){
				if(result==1){
					float =2;
					art.dialog.tips("领用成功");
					setTimeout(function(){
						art.dialog.data("result","success");
						art.dialog.close();
					},500);
				}else if(result==2){
					art.dialog.alert("剩余数量不足，请刷新列表后重试！");
				}else{
					art.dialog.alert("领用失败，请稍后重试！");
				}
			}
		});
	}
	
/**
 * 初始化按钮
 */
function initButton(){
	var api = art.dialog.open.api;
	api.button(
			{
				name: '确定',
				callback: function () {
					change();
					return false;
				},
				focus: true
			},
			{
				name: '取消'
			}
		);
}

function initUserTree(){
    var useUserIds=$("#useUserIds").val();
    qytx.app.tree.user({
    	id:"userUserTree",
    	type:"radio",
    	click:callBackUser
    });
}

function callBackUser(data)
{
    if(data){
    	var userIds = [];
    	var userNames = [];
    	$(data).each(function(i,item){
    		var uId = item.id.indexOf("uid_");
    		if(item.id.indexOf("uid_") != -1){
    			userIds.push(item.id.substring(4));
    			userNames.push(item.name);
        	}
    	});
        $("#useUserText").text(userNames.join(","));
        $("#useUserIds").val(userIds.join(","));
    }
}

function toggleSelectUser(){
	$("#userUserTree").show();
}