$(function(){
	var capitalId=$("#capitalId").val();
	if(capitalId){
		$("#addOrUpdate").val("修改");
		findDetail(capitalId)
	}else{
		$("#addOrUpdate").val("新增");
	}
	loadCapitalGroupTree();
});

function toggleSelectGroup(){
	$("#capitalGroupTree").show();
}

function loadCapitalGroupTree(){
	var capitalGroupId = $("#capitalGroupId").val();
	if(!capitalGroupId){
		capitalGroupId = "";
	}else{
		capitalGroupId = "gid_"+capitalGroupId;
	}
	var url = basePath+"capital/selectCapitalGroup.action";
	var param = {"treeType":1};
	$.ajax({
		url : url,
		type:'post',
		data:param,
		dataType : 'json',
		success : function(data) {
			qytx.app.tree.base({
				id	:	"capitalGroupTree",
				type:"radio",
				defaultSelectId:capitalGroupId,
				data:	data,
				click:	callBackCapitalGroup
			});
		}
	});
}

function callBackCapitalGroup(data)
{
    if(data){
    	var groupIds = [];
    	var groupNames = [];
    	$(data).each(function(i,item){
    		var uId = item.id.indexOf("gid_");
    		if(item.id.indexOf("gid_") != -1){
    			groupIds.push(item.id.substring(4));
    			groupNames.push(item.name);
        	}
    	});
        $("#capitalGroupText").text(groupNames[0]);
        $("#capitalGroupId").val(groupIds[0]);
    }
}

/**
 * 验证编号是否存在
 */
function existsCapitalNo(capitalNo){
	$.ajax({
		url:basePath+"capital/existsCapitalNo.action",
	    data:{"capitalNo":capitalNo},
	    type:'post',
	    dataType:'text',
	    success:function(data){
	    	if(data==1){//有相同的编码
	    		return false;
	    	}else{
	    		return true;
	    	}
	    }
	 })
}


//增加资产
function addCapital(){	
	// 框架校验
	if (!validator(document.getElementById("form1"))) {
		return;
	}
	var capitalId = $("#capitalId").val();
	var capitalName = $("#capitalName").val();
	var capitalNo = $("#capitalNo").val();
	var describe = $("#describe").val();//位置
	var capitalGroupId = $("#capitalGroupId").val();//设备组
	var capitalModel = $("#capitalModel").val();
	var capitalBrand = $("#capitalBrand").val();
	var supplier = $("#supplier").val();
	var capitalNo = $("#capitalNo").val();
	var remark = $("#remark").val();
	var url = basePath+"capital/addCapital.action";
	var param={
			"capital.id":capitalId,	
			"capital.name":capitalName,
			"capital.capitalGroup.id":capitalGroupId,
			"capital.capitalBrand":capitalBrand,
			"capital.supplier":supplier,
			"capital.capitalModel":capitalModel,
			"capital.storeLocation":describe,
			"capital.capitalNo":capitalNo,
			"capital.remark":remark
		}
	$.ajax({
		url:url,
		type:'POST',
		data:param,
		dataType:'json',
		success:function(result){
			if(result==1){
				art.dialog.tips("修改成功!");
				goBack();
			}else if(result==2){
				art.dialog.tips("新增成功!");
				goBack();
			}else if(result==3){
				art.dialog.alert("设备编码不能重复!");
			}else{
				art.dialog.tips("操作失败!");
			}
		}
	});
}


/**
 * 返回
 */
function goBack(){
	var type=$("#type").val();
	window.location.href=basePath+"logined/capital/capitalChanges.jsp?type="+type;
}

function getCapitalView(capitalId,callBack1,callBack2){
	var param = {"capitalId":capitalId};
	$.ajax({
		url:basePath+"capital/getDetailCapital.action",
		type:"POST",
		data:param,
		dataType:"json",
		success:function(result){
			$("#capitalName").val(result.capitalName);
			$("#capitalBlank").val(result.capitalBlank);
			$("#capitalModel").val(result.capitalModel);
			$("#isShowCapitalType").hide();
			$("#isShowCapitalTypeName").show();
			$("#isShowCapitalTypeName").text(result.capitalTypeName);
			$("#capitalType").val(result.capitalType);
			$("#unitId").val(result.unitId);
			$("#capitalPrice").val(result.capitalPrice);
			if(result.capitalType==2){//办公用品
				$("#isShowCapitalNum").show();
				$("#oldNum").text(result.oldNum);
			}
			$("#capitalNum").parent().hide();
			$("#capitalNumAdd").hide();
			$("#capitalNum").val(result.capitalNum);
			$("#isShowCapitalCodeLabel").show();
			$("#capitalCodeValue").show().text(result.capitalNo);;
			$("#approveNo").val(result.approveNo);
			$("#capitalStoreId").val(result.capitalStoreId);
			$("#supplier").val(result.supplier);
			$("#capitalGroupId").val(result.capitalGroupId);
			$("#capitalGroupText").text(result.capitalGroup);
			$("#handleUserId").val(result.handUserId);
			$("#handleUserText").text(result.handUserName);
			$("#buyDate").val(result.buyDate);
			$("#invoiceNumber").val(result.invoiceNumber);
			$("#remark").val(result.remark);
			$("#isShowOper").hide();
			callBack1();
			callBack2();
		}
	});
	
}

function findDetail(capitalId){
	var param={
		"capitalId":capitalId
	}
	$.ajax({
		url:basePath+"capital/getCapitalDetail.action",
		type:"POST",
		data:param,
		dataType:"json",
		success:function(result){
			if(result){
				$("#capitalName").val(result.name);
				$("#capitalNo").val(result.capitalNo);
				$("#capitalGroupId").val(result.capitalGroupId);
				$("#capitalGroupText").html(result.capitalGroupName);
				$("#capitalModel").val(result.capitalModel);
				$("#capitalBrand").val(result.capitalBrand);
				$("#describe").val(result.storeLocation);
				$("#remark").val(result.remark);
				$("#supplier").val(result.supplier);
			}
		}
	})
}
