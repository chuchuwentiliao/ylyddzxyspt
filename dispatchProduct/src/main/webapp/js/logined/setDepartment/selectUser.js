$(function(){
	$("#userSelect").click(function(){
		selectUser(this);
	});
	$("#distributionSelect").click(function(){
		selectUser(this);
	});
	$("#returnSelect").click(function(){
		selectUser(this);
	});
	
});
var userObj;
/**
 * 人员选择
 * REN
 **/
function selectUser(obj)
{
	var processType = $("#processType").val();
    var userId=$("#"+$(obj).attr("fieldId")).val();
    /*if(processType&&processType==2){
    	userId = "gid_"+userId;
    }*/
    var showType = $("#"+$(obj).attr("showType"));
    if(!showType){
    	showType = 3;
    }else{
    	showType = showType.val();
    }
    var extension=$("#extension").val();
    userObj = obj;
    qytx.app.tree.alertUserTree({
    	type:"radio",
    	showType:showType,
    	defaultSelectIds:userId,
    	extension:extension,
    	callback:callBackUser
    });
}


function callBackUser(data)
{
    if(data){
    	var userIds = [];
    	var userNames = [];
    	var types = [];
    	$(data).each(function(i,item){
    		userIds.push(item.id);
    		if(item.pathName){
    			userNames.push(item.pathName);
    		}else{
    			userNames.push(item.name);
    		}
    		types.push(item.type)
    	});
    	$("#"+$(userObj).attr("fieldId")).val(userIds[0]);
    	$("#"+$(userObj).attr("fieldName")).val(userNames[0]);
    	$("#"+$(userObj).attr("fileType")).val(types[0]);
    }
}



