app.controller("myStartController",["$scope","defectService","$filter",function($scope,defectService,$filter){
	/**
	 * 获得数据字典
	 */
	var data={
			"infoType":"equipmentUnit",
			"sysTag":1	
	}
	defectService.getDicts(data,function(result){		
		$scope.equipmentUnitList=result;
	});
	data={
			"infoType":"defectDepartment",
			"sysTag":1	
	}
	defectService.getDicts(data,function(result){		
		$scope.defectDepartmentList=result;
	});
	data={
			"infoType":"defectProfessional",
			"sysTag":1	
	}
	defectService.getDicts(data,function(result){		
		$scope.defectProfessionalList=result;
	});
	
	$scope.searchVo={
			equipmentState:"",
			equipmentUnit:"",
			defectDepartment:"",
			defectProfessional:"",
			state:"",
			searchName:"",
			delayState:"",
			timeType:"",
			type:"",
			userName:"",
			createGroupName:"",
			processUserName:"",
			processUserGroup:"",
			dispatchUserName:""
	};
	/**
	 * apply 我上报的
	 * view 查看所有
	 */
	var type=document.getElementById("type").value;
	$scope.type=type;
	var isOut=document.getElementById("isOut").value;
	$scope.isOut = isOut;
	var isForkGroup=$("#isForkGroup").val();
	if(isForkGroup=="null"){
		isForkGroup = "";
	}
	$scope.isForkGroup = isForkGroup;
	if(type=='apply'){
		if($scope.isForkGroup==1){
			$scope.title = "科室事件";
		}else{
			$scope.title = "我上报的";
		}
	}else if(type=='delay'){
		$scope.title = "申请的";
	}else if(type=='delayView'){
		$scope.title = "延期事件";
	}else{
		$scope.title = "全部事件";
		//$scope.searchVo.timeType = "1";
	}
	
	$scope.iDisplayLength =15;
	var oldIDisplayStart = $("#oldIDisplayStart").val();
	if(oldIDisplayStart!=null && oldIDisplayStart!="null"){
		$scope.iDisplayStart = oldIDisplayStart;
	}else{
		$scope.iDisplayStart = 0;
	}
	$scope.getList = function(){
		var startTime = $("#startTime").val();
		if(!startTime){startTime="";}
		var endTime = $("#endTime").val();
		if(!endTime){endTime="";}
		if(isForkGroup==null||isForkGroup=="null"){isForkGroup="";}
		var params={
				"iDisplayLength":$scope.iDisplayLength,
				"iDisplayStart":$scope.iDisplayStart,
				"searchVo.equipmentUnit":$scope.searchVo.equipmentUnit,
				"searchVo.defectDepartment":$scope.searchVo.defectDepartment,
				"searchVo.defectProfessional":$scope.searchVo.defectProfessional,
				"searchVo.searchName":$scope.searchVo.searchName,
				"searchVo.state":$scope.searchVo.state,
				"searchVo.delayState":$scope.searchVo.delayState,
				"searchVo.timeType":$scope.searchVo.timeType,
				"searchVo.type":$scope.searchVo.type,
				"searchVo.userName":$scope.searchVo.userName,
				"searchVo.createGroupName":$scope.searchVo.createGroupName,
				"searchVo.processUserName":$scope.searchVo.processUserName,
				"searchVo.processUserGroup":$scope.searchVo.processUserGroup,
				"searchVo.dispatchUserName":$scope.searchVo.dispatchUserName,
				"searchVo.startTime":startTime,
				"searchVo.endTime":endTime,
				"type":type,
				"isForkGroup":isForkGroup
			}
		defectService.getMyStartList(params,function(result){
			$scope.aaData = result.aaData;
			$scope.result = result.iTotalRecords;
		});
	}
	$scope.searchList=function(){
		$scope.iDisplayStart = 0;
		$scope.getList();
	}
	//查看详情
	$scope.goView=function(instanceId,type2){
		var isForkGroup = $("#isForkGroup").val();
		if(isForkGroup==null||isForkGroup=="null"){isForkGroup="";}
		alert($scope.iDisplayStart);
		if(type2==0){
			if($scope.type=='view'){
				window.location.href=basePath+"logined/defect/view.jsp?oper=view&instanceId="+instanceId+"&isForkGroup="+isForkGroup+"&iDisplayStart="+$scope.iDisplayStart;
			}else if($scope.type=='delay'){
				window.location.href=basePath+"logined/defect/view.jsp?oper=delay&instanceId="+instanceId+"&isForkGroup="+isForkGroup+"&iDisplayStart="+$scope.iDisplayStart;
			}else{
				window.location.href=basePath+"logined/defect/view.jsp?oper=apply&instanceId="+instanceId+"&isForkGroup="+isForkGroup+"&iDisplayStart="+$scope.iDisplayStart;
			}
		}else{
			if($scope.type=='view'){
				window.location.href=basePath+"logined/defect/view2.jsp?oper=view&instanceId="+instanceId+"&isForkGroup="+isForkGroup+"&iDisplayStart="+$scope.iDisplayStart;
			}else if($scope.type=='delay'){
				window.location.href=basePath+"logined/defect/view2.jsp?oper=delay&instanceId="+instanceId+"&isForkGroup="+isForkGroup+"&iDisplayStart="+$scope.iDisplayStart;
			}else{
				window.location.href=basePath+"logined/defect/view2.jsp?oper=apply&instanceId="+instanceId+"&isForkGroup="+isForkGroup+"&iDisplayStart="+$scope.iDisplayStart;
			}
		}
		
		
	}
	//导出
	$scope.exportApply=function(){
		var startTime = $("#startTime").val();
		if(!startTime){startTime="";}
		var endTime = $("#endTime").val();
		if(!endTime){endTime="";}
		var data="searchVo.delayState="+$scope.searchVo.delayState;
		data+="&searchVo.equipmentUnit="+$scope.searchVo.equipmentUnit;
		data+="&searchVo.defectDepartment="+$scope.searchVo.defectDepartment;
		data+="&searchVo.defectProfessional="+$scope.searchVo.defectProfessional;
		data+="&searchVo.searchName="+encodeURI(encodeURI($scope.searchVo.searchName)) ;
		data+="&searchVo.userName="+encodeURI(encodeURI($scope.searchVo.userName)) ;
		data+="&searchVo.createGroupName="+encodeURI(encodeURI($scope.searchVo.createGroupName)) ;
		data+="&searchVo.processUserName="+encodeURI(encodeURI($scope.searchVo.processUserName)) ;
		data+="&searchVo.processUserGroup="+encodeURI(encodeURI($scope.searchVo.processUserGroup)) ;
		data+="&searchVo.dispatchUserName="+encodeURI(encodeURI($scope.searchVo.dispatchUserName)) ;
		data+="&searchVo.state="+$scope.searchVo.state;
		data+="&searchVo.timeType="+$scope.searchVo.timeType;
		data+="&searchVo.type="+$scope.searchVo.type;
		data+="&type="+$scope.type;
		data+="&searchVo.startTime="+startTime;
		data+="&searchVo.endTime="+endTime;
		data+="&isForkGroup="+$scope.isForkGroup;
		document.location.href=basePath + "defect/export.action?" + data;
	}
	
	
	//作废缺陷
	$scope.draftDefect=function(instanceId){
		art.dialog.confirm("确定要作废该工单吗？",function(){
			$scope.approve(1,1,'同意作废','',instanceId);
		});
	}
	
	
	//处理结果提交
	$scope.approve=function(taskName,approveResult,advice,nextUserId,instanceId){
		var params={
			instanceId:instanceId,
			taskName:taskName,
			approveResult:approveResult,
			advice:advice,
			nextUserId:nextUserId
		}
		var type="approve";
		if(taskName==0){
			type="dispatch";
		}else if(taskName==1){
			type="apply";
		}else if(taskName==2){
			type="approve";
		}else if(taskName==3||taskName==6){
			type="defect";
		}else if(taskName==4){
			type="accept";
		}else if(taskName==5){
			type="archive";
		}
		defectService.approve(params,function(result){
			if(result&&result==1){
				window.location.href=basePath+"/logined/defect/myStartList.jsp?type=view&isOut="+$scope.isOut;
			}
		});
	}
	
}]);