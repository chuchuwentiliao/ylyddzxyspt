app.controller("myProcessController",["$scope","defectService","$filter",function($scope,defectService,$filter){
	/**
	 * 获得数据字典
	 */
	var data={
			"infoType":"equipmentUnit",
			"sysTag":1	
	}
	defectService.getDicts(data,function(result){		
		$scope.equipmentUnitList=result;
	});
	data={
			"infoType":"defectDepartment",
			"sysTag":1	
	}
	defectService.getDicts(data,function(result){		
		$scope.defectDepartmentList=result;
	});
	data={
			"infoType":"defectProfessional",
			"sysTag":1	
	}
	defectService.getDicts(data,function(result){		
		$scope.defectProfessionalList=result;
	});
	/**
	 * defect 处理
	 * accept 验收
	 * archive 归档
	 */
	var type=document.getElementById("type").value;
	$scope.searchVo={
			equipmentState:"",
			equipmentUnit:"",
			defectDepartment:"",
			defectProfessional:"",
			state:"",
			searchName:""
	};
	if(type=="dispatch"){
		$scope.title="已调度";
		$scope.searchVo.taskName="'0'";
	}else if(type=="approve"){
		$scope.title="已接收";
		$scope.searchVo.taskName="'2'";
	}else if(type=="defect"){
		$scope.title="已处理";
		$scope.searchVo.taskName="'3'";
	}else if(type=="accept"){
		$scope.title="已验收";
		$scope.searchVo.taskName="'4'";
	}else if(type=="archive"){
		$scope.title="已归档";
		$scope.searchVo.taskName="'5'";
	}else if(type=="myJoin"){
		$scope.title = "已处理";
	}
	$scope.iDisplayLength = 15;
	$scope.iDisplayStart = 0;
	$scope.getList = function(){
		var startTime = $("#startTime").val();
		if(!startTime){startTime="";}
		var endTime = $("#endTime").val();
		if(!endTime){endTime="";}
		var myJoin = "";
		if(type=="myJoin"){
			myJoin = "1";
		}
		var params={
				"iDisplayLength":$scope.iDisplayLength,
				"iDisplayStart":$scope.iDisplayStart,
				"searchVo.state":$scope.searchVo.state,
				"searchVo.equipmentUnit":$scope.searchVo.equipmentUnit,
				"searchVo.defectDepartment":$scope.searchVo.defectDepartment,
				"searchVo.defectProfessional":$scope.searchVo.defectProfessional,
				"searchVo.searchName":$scope.searchVo.searchName,
				"taskName":$scope.searchVo.taskName,
				"searchVo.type":$scope.searchVo.type,
				"searchVo.startTime":startTime,
				"searchVo.myJoin":myJoin,
				"searchVo.endTime":endTime
			}
		defectService.getMyProcessList(params,function(result){
			$scope.aaData = result.aaData;
			$scope.result = result.iTotalRecords;
		});
	}
	$scope.searchList=function(){
		$scope.iDisplayStart = 0;
		$scope.getList();
	}
	//查看详情
	$scope.goView=function(instanceId,state,type2){
		if((state==0||state==6)&&type=='defect'){
			type="approve";
		}
		if(type2!=0){
			window.location.href=basePath+"logined/defect/view2.jsp?oper=view&instanceId="+instanceId+"&type="+type;
		}else{
			window.location.href=basePath+"logined/defect/view.jsp?oper=view&instanceId="+instanceId+"&type="+type;
		}
	}
}]);