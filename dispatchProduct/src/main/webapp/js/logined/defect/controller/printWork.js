$(function(){
	var instanceId = $("#instanceId").val();
	var workType = $("#workType").val();
	getView(instanceId,workType);
	if(workType==0){
		findGoodList(instanceId);
	}else{
		$("#repairTr").hide();
		$("#otherTr").show();
		$("#goodsTable").hide();
		$("#equipmentTable").show();
	}
});

function getView(instanceId,workType){
	var url = basePath+"defect/view.action";
	$.ajax({
		url:url,
		type:'POST',
		data:{"instanceId":instanceId},
		dataType:'json',
		success:function(result){
			if(result){
				var apply = result.apply;
				var processUserName = result.processUserName;
				var processGroupName = result.processGroupName;
				$("#workNo").text(apply.defectNumber);
				$("#processGroupName").text(processGroupName?processGroupName:"--");
				$("#processUserName").text(processUserName?processUserName:"--");
				$("#workContent").text(apply.equipmentName);
				$("#workState").text(apply.stateVo);
				$("#workGrade").text(apply.gradeVo);
				$("#workType").text(workType==0?"事件维修":(workType==1?"设备配送":"设备归还"));
				var applyPhone = "--";
				if(apply.applyPhone){
					applyPhone = apply.applyPhone;
					if(apply.applyPhoneName){
						applyPhone += "("+apply.applyPhoneName+")";
					}
				}
				$("#applyPhone").text(applyPhone);
				$("#applyGroupName").text(apply.defectDepartmentVo);
				$("#workLocation").text(apply.describe);
				$("#applyUserName").text(apply.createUserName+"("+apply.createUserPhone+")");
				$("#applyWorkNo").text(apply.createUserWorkNo?apply.createUserWorkNo:'--');
				$("#applyTime").text(apply.createTimeStr);
				$("#applyMemo").text(apply.memo?apply.memo:'--');
				if(workType!=0){
					var equipmentMap = new Map();
					var equipmentList = result.equipmentList;
					if(equipmentList){
						var size = equipmentList.length;
						for(var i = 0;i<size;i++){
							var equipment = equipmentList[i];
							var newEquipment = new Equipment();
							newEquipment.id=equipment.id;
							newEquipment.name=equipment.name;
							newEquipment.capitalNo=equipment.name;
							newEquipment.capitalModel=equipment.name;
							equipmentMap.put(equipment.id+"",newEquipment);
						}
					}
					showEquipment(equipmentMap);
				}
			}
		}
	});
}



/**
 * 获得选择的列表
 */
function findGoodList(instanceId){
	$.ajax({
		url:basePath+"defect/findSelectGoodList.action",
		type:"post",
		data:{"instanceId":instanceId},
		dataType:'json',
		success:function(data){
			var html="";
			if(data.length>0){
				for(var i=0;i<data.length;i++){
					html += '<tr><td width="20%">'+data[i].name+'</td><td width="15%">'+data[i].groupName+'</td><td width="10%">'+data[i].num+'</td><td width="10%">'+data[i].price+'</td><td width="10%">'+data[i].money+'</td><td></td></tr>';
					if(i>2){
						break;
					}
				}
				if(data.length==2){
					html += '<tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>';
				}
				if(data.length==1){
					html += '<tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>';
					html += '<tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>';
				}
			}else{
				html += '<tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>';
				html += '<tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>';
				html += '<tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>';
			}
			$(html).insertAfter("#goodsTable");
		}
	})
}

function showEquipment(equipmentMap){
	var size = equipmentMap.size();
	var html = "";
	if(size>0){
		for(var i = 0;i<equipmentMap.size();i++){
			var arr = equipmentMap.arr;
			var tempEquip = arr[i].value;
			html += '<tr><td >'+tempEquip.name+'</td><td >'+tempEquip.capitalModel+'</td><td >'+tempEquip.capitalNo+'</td><td></td><td></td><td></td></tr>';
			if(i>2){
				break;
			}
		}
		if(size==2){
			html += '<tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>';
		}
		if(size==1){
			html += '<tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>';
			html += '<tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>';
		}
		
	}else{
		html += '<tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>';
		html += '<tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>';
		html += '<tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>';
	}
	$(html).insertAfter("#equipmentTable");
}

var Equipment = function(){
	this.id = "";
	this.name = "";
	this.capitalType=1;
	this.capitalNo="";
	this.capitalModel="";
};

