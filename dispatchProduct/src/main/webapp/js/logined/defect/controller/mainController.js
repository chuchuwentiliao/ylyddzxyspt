app.controller("mainController",["$scope","defectService","$filter",function($scope,defectService,$filter){
	var date=new Date();
	var year=date.getFullYear();
	var month=date.getMonth()+1;
	var day=date.getDate();
	if(month<10){
		month="0"+month;
	}
	if(day<10){
		day="0"+day;
	}
	$scope.dutyDate=year+"-"+month+"-"+day;
	$scope.dutyInfo="今日值班";
	//获取待处理记录
	//时间类型
	$scope.find=function(){
		var param={
				"num":$scope.selectTime,
				"beginTime":$("#beginTime").val(),
				"endTime":$("#endTime").val()
			}
			defectService.getUnHandleCount(param,function(data){
				if(data){
					$scope.applyCount=data.applyCount;
					$scope.approveCount=data.approveCount;
					$scope.dispatchCount=data.dispatchCount;
					$scope.defectCount=data.defectCount;
					$scope.acceptCount=data.acceptCount;
					//$scope.archiveCount=data.archiveCount;
					$scope.delayCount=data.delayCount;
					$scope.isAdmin=data.isAdmin;
				}
			});
	}
	$scope.find();
	//查询值班信息
	$scope.page=1;
	$scope.len=5;
	$scope.search=function(){
		var dutyDateStr=document.getElementById("dutyDate").value;
		if(dutyDateStr&&dutyDateStr!=$scope.dutyDate){
			var dateAry=dutyDateStr.split("-");
			if(dateAry[0]==new Date().getFullYear()){
				//$scope.dutyInfo=dateAry[1]+"月"+dateAry[2]+"日值班";
			}else{
				//$scope.dutyInfo=dateAry[0]+"年"+dateAry[1]+"月"+dateAry[2]+"日值班";
			}
			$scope.dutyInfo="当日值班";
		}else{
			$scope.dutyInfo="今日值班";
		}
		//获取当前值班信息
		defectService.getDutyList({"dutyDate":dutyDateStr},function(data){
			if(data){
				$scope.allList=data;
			}
		});
	}
	//$scope.search();
	
	$scope.todayWorkDuty = function(){
		defectService.todayWorkDuty({},function(data){
			if(data){
				$scope.isDuty=data.isDuty;
				$scope.userName = data.userName;
				$scope.userId = data.userId;
			}
		});
	}
	//$scope.todayWorkDuty();
	
	$scope.viewList = function(){
		var param = {
				"columnId":35,
				"iDisplayStart":0,
				"iDisplayLength":5,
				"isShowOut":1,
				"notifyType":0
		};
		defectService.viewList(param,function(data){
			if(data){
				$scope.iTotalRecords=data.iTotalRecords;
				$scope.aaData = data.aaData;
			}
		});
	}
	$scope.viewList();
	
	$scope.moreNotify = function(){
		window.location.href = basePath +"logined/notify/viewList.jsp?id=35";
	}
	$scope.viewList = function(){
		var param = {
				"columnId":35,
				"iDisplayStart":0,
				"iDisplayLength":5,
				"isShowOut":1,
				"notifyType":0
		};
		defectService.viewList(param,function(data){
			if(data){
				$scope.iTotalRecords=data.iTotalRecords;
				$scope.aaData = data.aaData;
			}
		});
	}
	
	/*去签到*/
	$scope.toSign = function(){
		if($scope.isDuty==1){
			art.dialog.confirm("确定要签退吗?",function(){
				defectService.deleteWorkDuty({},function(data){
					if(data=="0"){
						art.dialog.tips("签退成功!");
						$scope.search();
						$scope.todayWorkDuty();
					}
				});
				
			});
		}else{
			var url = basePath+"logined/defect/workDuty.jsp";
			art.dialog.data("userName",$scope.userName);
			art.dialog.open(url,{
				title : "值班设定",
				width : 400,
				height : 150,
				lock : true,
				drag : true,
				opacity : 0.08,//透明度
				close:function(){
					if(art.dialog.data("result")=="success"){
						$scope.search();
						$scope.todayWorkDuty();
					}
					return true;
			   },button:[{name:"确定",focus:true}, {name:"取消", focus:false}]
			});
		}
		
	}
	
	
	
	//下载值班模板
	$scope.down=function(){
		window.location.href=basePath+"down/dutyList.xls";
	}
	//导出值班表
	$scope.exportDuty=function(){
		var dutyDateStr=document.getElementById("dutyDate").value;
		window.location.href=basePath+"duty/export.action";
	}
	/**
	 * 跳转待办
	 */
	$scope.toWait = function(type){
		var beginTime=$("#beginTime").val();
		if(!beginTime){
			beginTime='';
		}
		var endTime=$("#endTime").val();
		if(!endTime){
			endTime='';
		}
		if(!$scope.selectTime){
			$scope.selectTime='';
		}
		var url = basePath+"logined/defect/myWaitList.jsp?type="+type+"&num="+$scope.selectTime+"&beginTime="+beginTime+"&endTime="+endTime;
		window.location.href = url;
	}
	/**
	 * 跳转查看缺陷
	 */
	$scope.lookDefect = function(type){
		var url = "";
		if(type=="all"){
			url = basePath+"logined/defect/myStartList.jsp?type=view";
		}
		if(type=="my"){
			url = basePath+"logined/defect/myStartList.jsp?type=apply";
		}
		if(type=="statistics"){
			url = basePath+"logined/defect/report.jsp";
		}
		window.location.href = url;
	}
}]);



