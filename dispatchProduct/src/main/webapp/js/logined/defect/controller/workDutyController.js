
var parent = art.dialog.parent;
var api = art.dialog.open.api;	

app.controller("workDutyController",["$rootScope","$scope","defectService","$filter",function($rootScope,$scope,defectService,$filter){
	
	$scope.userName = art.dialog.data("userName");
	
	/**
	 * 获得数据字典
	 */
	var data={
			"infoType":"dutyDepartment",
			"sysTag":1	
	}
	defectService.getDicts(data,function(result){		
		$scope.dutyDepartmentList=result;
	});
	
	$scope.saveWorkDuty = function(){
		if(!$scope.myForm.$valid){
			$scope.$apply(function(){
				$scope.submitted=true;
			});
			return false; 
		}
		var param = {
				"dutyDepartmentId":$scope.dutyDepartment
		};
		defectService.saveWorkDuty(param,function(result){	
			if(result=="0"){
				art.dialog.tips("签到成功!");
				setTimeout(function(){
					art.dialog.data("result","success");
					art.dialog.close();
				},1000);
			}else{
				art.dialog.alert("签到失败,请稍后重试!");
			}
			
		});
		
	}
	
	/**
	 * 初始化按钮
	 */
	$scope.initButton = function(){
		api.button(
				{
					name: '确定',
					callback: function () {
						$scope.saveWorkDuty();
						return false;
					},
					focus: true
				},
				{
					name: '取消',
					callback:function(){
						return true;
					}
				}
			);
	}
	
	$scope.initButton();
	
}]);