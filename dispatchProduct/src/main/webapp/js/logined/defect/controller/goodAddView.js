$(function(){
	var treeHeight = $(window).height()-38;
	$(".center_content").height(treeHeight);
	getGoodsGroupTree("gid_0");
	//加载存放位置下拉选
	// 捕获回车事件
    $("html").die().live("keydown", function(event) {
        if (event.keyCode == 13) {
        	getTable();
        	
        }
    });
	serch();
	
	
	$("#goodTable tbody").on('click', 'tr', function () {
		$(this).find("input").attr("checked",true);
	})
	
	
});



//搜索
function serch(){
	getTable();
}


function getTable(){
  var goodType=$("#goodType").val();	
  var goodNo=$("#goodNo").val();
  var goodName=$("#goodName").val();
  var groupId=$("#selectGroupId").val();
	$('#goodTable').dataTable({
		"bProcessing" : true,
		'bServerSide' : true,
		'fnServerParams' : function(aoData) {
			aoData.push({
						"name" : "name",
						"value" : goodName
					},{
						"name":"type",
						"value":goodType
					},{
						"name":"goodNo",
						"value":goodNo
					},{
						"name":"groupId",
						"value":groupId
					}
				);
		},
		"sAjaxSource" : basePath+"goods/findGoodListPage.action",// 获取管理员列表
		"sServerMethod" : "POST",
		"sPaginationType" : "full_numbers",
		"bPaginate" : true, // 翻页功能
		"bLengthChange" : false, // 改变每页显示数据数量
		"bFilter" : false, // 过滤功能
		"bSort" : false, // 排序功能
		"bInfo" : true,// 页脚信息
		"bAutoWidth" : false,// 自动宽度
		"bDestroy" : true,
		"iDisplayLength" : 15, // 每页显示多少行
		"aoColumns" : [{
					"mDataProp" : "no",
				}, {
					"mDataProp" : "name",
				    "sClass" : "longTxt"
				}, {
					"mDataProp" : "goodlNo",
					"sClass" : "longTxt"
				}, {
					"mDataProp" :"goodGroup"
				}, {
					"mDataProp" : "type"
				},{
					"mDataProp" : "specifications"
				},{
					"mDataProp" : "unit"
				},{
					"mDataProp" : "price"
				},{
					"mDataProp" : null
				}],
		"oLanguage" : {
			"sUrl" : basePath + "plugins/datatable/cn.txt" // 中文包
		},
		"fnDrawCallback" : function(oSettings) {
			$('#goodTable tbody  tr td').each(function() {
				this.setAttribute('title', $(this).text());
			});
              
		},
		"fnInitComplete" : function() {
			// 重置iframe高度
		
		},
		"aoColumnDefs" : [ {
			"aTargets" : [8],
			"fnRender" : function(oObj) {
				return '<input name="chk"  value="'+ oObj.aData.id+'" type="radio" />';
			}
			
		}]
	});
}	





function getGoodsGroupTree(defaultSelectId){
	var url = basePath+"goods/selectGoodGroup.action";
	var param = {"treeType":1};
	$.ajax({
		url : url,
		type:'post',
		data:param,
		dataType : 'json',
		success : function(data) {
			qytx.app.tree.base({
				id	:	"myTree",
				defaultSelectId:defaultSelectId,
				data:	data,
				click:	function(nodes){
					onTreeNodeClick(nodes);
				}
			});
			
		}
	});
}


function onTreeNodeClick(nodes){
	if(nodes&&nodes.length > 0){
		var name = nodes[0].name;
		var vid = nodes[0].id;
		var pId = nodes[0].pId;
		var index_code = nodes[0].obj;
		treeNodeClick(name, vid, pId,index_code);
	}
}

function treeNodeClick(name, vid, pid,index_code) {
	var type=$("#type").val();
	if(vid.substring(4)!="0"){
		$("#selectGroupId").val(vid.substring(4));
	}else{
		$("#selectGroupId").val("");
	}
	getTable();
}


function opened(){
	var id=$(":radio[name='chk']:checked").val(); 
	if(!id){
		art.dialog.alert("请选择物资！")
		return;
	}
	art.dialog.data("goodId",id);
	var url=basePath+"logined/defect/selectGoodNum.jsp";
	art.dialog.open(url,{
		title : "选择物资",
		width : 500,
		height : 100,
		lock : true,
		drag : true,
		opacity : 0.08,//透明度
		button : [{
			name : '确定',
			focus:true,
			callback : function() {
					var iframe = this.iframe.contentWindow;
					iframe.addSelectGood();
					return false;
			}
		}, {
			name : '取消',
			callback : function() {
				return true;
			}
		}]
	});

}

