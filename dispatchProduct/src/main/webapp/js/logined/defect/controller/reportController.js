app.controller("reportController",["$scope","defectService","$filter",function($scope,defectService,$filter){
	var data={
			"infoType":"defectDepartment",
			"sysTag":1	
	}
	defectService.getDicts(data,function(result){		
		$scope.defectDepartmentList=result;
	});
	data={
			"infoType":"equipmentUnit",
			"sysTag":1	
	}
	defectService.getDicts(data,function(result){		
		$scope.equipmentUnitList=result;
	});
	data={
			"infoType":"defectProfessional",
			"sysTag":1	
	}
	defectService.getDicts(data,function(result){		
		$scope.defectProfessionalList=result;
	});
	
	// 基于准备好的dom，初始化echarts实例
    var myChart1 = echarts.init(document.getElementById('main1'));
    var myChart2 = echarts.init(document.getElementById('main2'));
    var myChart3 = echarts.init(document.getElementById('main3'));
    var myChart4 = echarts.init(document.getElementById('main4'));
	$scope.searchVo={};
	$scope.getReport=function(){
		var startTime=document.getElementById("startTime").value;
		var endTime=document.getElementById("endTime").value;
		var params={
				"searchVo.startTime":startTime,
				"searchVo.endTime":endTime,
				"searchVo.defectDepartment":$scope.searchVo.defectDepartment,
				"searchVo.equipmentUnit":$scope.searchVo.equipmentUnit,
				"searchVo.defectProfessional":$scope.searchVo.defectProfessional
		}
		defectService.getReport(params,function(result){
			
			var waitDefectWaitDelay='待处理(待批准延期)'+result.waitDefectWaitDelay+'个';
		    var waitDefectDelaying='待处理(延期中)'+result.waitDefectDelaying+'个';
		    var hasDefectDelaying='已处理(延期中)'+result.hasDefectDelaying+'个';
		    legendData=new Array(waitDefectWaitDelay,waitDefectDelaying,hasDefectDelaying);
			seriesData=new Array(
					{value:result.waitDefectWaitDelay, name:waitDefectWaitDelay},
					{value:result.waitDefectDelaying, name:waitDefectDelaying},
					{value:result.hasDefectDelaying, name:hasDefectDelaying});
		    myChart1.setOption(getOption('事件延期',legendData,seriesData));
			
			
			
			// 使用刚指定的配置项和数据显示图表。
			var grade1='一级事件'+result.grade1+'个';
			var grade2='二级事件'+result.grade2+'个';
			var grade3='三级事件'+result.grade3+'个';
			var legendData=new Array(grade1,grade2,grade3);
			var seriesData=new Array(
					{value:result.grade1, name:grade1},
					{value:result.grade2, name:grade2},
					{value:result.grade3, name:grade3});
		    myChart2.setOption(getOption('事件等级分布',legendData,seriesData));
		    
		    
		    
		    
		   /* var defecting='待消缺'+result.defecting+'个';
		    var accepting='待验收'+result.accepting+'个';
		    var defected='已消缺'+result.defected+'个';
		    legendData=new Array(defecting,accepting,defected);
			seriesData=new Array(
					{value:result.defecting, name:defecting},
					{value:result.accepting, name:accepting},
					{value:result.defected, name:defected});
		    myChart2.setOption(getOption('事件状态',legendData,seriesData));*/
		    
		    var waitAccept='待接收'+result.waitAccept+'个';
		    var waitDefect='待处理'+result.waitDefect+'个';
		    var waitCheck='待验收'+result.waitCheck+'个';
		    var waitFile='待归档'+result.waitFile+'个';
		    var hasFile='已归档'+result.hasFile+'个';
		    var hasDelete='已作废'+result.hasDelete+'个';
		    legendData=new Array(waitAccept,waitDefect,waitCheck,waitFile,hasFile,hasDelete);
			seriesData=new Array(
					{value:result.waitAccept, name:waitAccept},
					{value:result.waitDefect, name:waitDefect},
					{value:result.waitCheck, name:waitCheck},
					{value:result.waitFile, name:waitFile},
					{value:result.hasFile, name:hasFile},
					{value:result.hasDelete, name:hasDelete});
		    myChart3.setOption(getOption('事件状态',legendData,seriesData));
//			inTimeDefect: 0, inTimeunDefect: 7, outTimeDefect: 0, outTimeunDefect: 1}
		   /* var outTimeunDefect='延时未消缺'+result.outTimeunDefect+'个';
		    var outTimeDefect='延时消缺'+result.outTimeDefect+'个';
		    var inTimeDefect='及时消缺'+result.inTimeDefect+'个';
		    var inTimeunDefect='正在消缺'+result.inTimeunDefect+'个';
		    legendData=new Array(outTimeunDefect,outTimeDefect,inTimeDefect,inTimeunDefect);
			seriesData=new Array(
					{value:result.outTimeunDefect, name:outTimeunDefect},
					{value:result.outTimeDefect, name:outTimeDefect},
					{value:result.inTimeDefect, name:inTimeDefect},
					{value:result.inTimeunDefect, name:inTimeunDefect});
		    myChart3.setOption(getOption('消缺率',legendData,seriesData));*/
		    seriesData=new Array( 
		    		{name:'一级事件',type:'bar',data:result.grade1Ary},
		    		{name:'二级事件',type:'bar',data:result.grade2Ary},
		    		{name:'三级事件',type:'bar',data:result.grade3Ary});
		    myChart4.setOption(getOption1('年度事件个数统计',seriesData));
		});
	}
	$scope.getReport();
	
}]);

function getOption(text,legendData,seriesData){
	var option = {
		    title : {
		        text: text,
		        x:'center'
		    },
		    tooltip : {
		        trigger: 'item',
		        formatter: "{a} <br/>{b} : {c} ({d}%)",
		        position:function(p){   //其中p为当前鼠标的位置
		            return [p[0]+10, p[1] - 10];
		        }
		    },
		    legend: {
		        orient: 'vertical',
		        left: 'left',
		        data: legendData
		    },
		    series : [
		        {
		            name: text,
		            type: 'pie',
		            radius : '55%',
		            center: ['50%', '60%'],
		            data:seriesData,
		            itemStyle: {
		                emphasis: {
		                    shadowBlur: 10,
		                    shadowOffsetX: 0,
		                    shadowColor: 'rgba(0, 0, 0, 0.5)'
		                }
		            }
		        }
		    ]
		};
	return option;
}

function getOption1(text,seriesData){
	var option = {
			 title : {
			        text: text,
			        x:'left'
			},
		    tooltip : {
		        trigger: 'axis',
		        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
		            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
		        },
		        position:function(p){   //其中p为当前鼠标的位置
		            return [p[0]- 70, p[1] - 10];
		        }
		    },
		    legend: {
		        data:['一级事件','二级事件','三级事件']
		    },
		    grid: {
		        left: '3%',
		        right: '4%',
		        bottom: '3%',
		        containLabel: true
		    },
		    xAxis : [
		        {
		            type : 'category',
		            data : ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月']
		        }
		    ],
		    yAxis : [
		        {
		            type : 'value'
		        }
		    ],
		    series : seriesData
		};
	return option;
}