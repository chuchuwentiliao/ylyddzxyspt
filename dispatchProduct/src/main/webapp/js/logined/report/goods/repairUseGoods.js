$(document).ready(function() {
	getTable_repair();
	//查询列表
	$("#serch_repair").click(function(){
		getTable_repair();
	})
	//导出
	$("#export_repair").click(function(){
		report_export_repair();
    });

    
});
//导出
function report_export_repair(){
	var beginTime=$("#begDate_repair").val();
	var endTime=$("#endDate_repair").val();
	var repairTeam=$("#groupName_repair").val();
	var goodName=$("#goodName_repair").val();
	var groupName=$("#goodGroupName_repair").val();
	var name=$("#name_repair").val();
    var url = basePath + "report/goodReport_exportRepairUseGoods.action?beginTime="+beginTime+"&endTime="+endTime+"&repairTeam="+repairTeam+"&goodName="+goodName+"&groupName="+groupName+"&name="+name;	
	url=encodeURI(encodeURI(url));
	window.open(url);
}
var getTable_repair=function(){
	var param={
			'beginTime':$("#begDate_repair").val(),
            'endTime':$("#endDate_repair").val(),
            "repairTeam":$("#groupName_repair").val(),
            "goodName":$("#goodName_repair").val(),
            "groupName":$("#goodGroupName_repair").val(),//物资组
            "name":$("#name_repair").val()//物资组
	}
	$.ajax({
		type : 'post',
		url : basePath + "report/goodReport_findRepairUseGoods.action",
		data : param,
		dataType : 'json',
		success : function(data) {
				var html="";
				if(data.length>0){
					for(var i=0;i<data.length;i++){
						if(i%2==0){
							html+="<tr class='odd'>";	
						}else{
							html+="<tr class='even'>";
						}
						html+="<td>"+data[i].no+"</td>" ;
						html+="<td>"+data[i].name+"</td>" ;
						html+="<td>"+data[i].groupName+"</td>";
						html+="<td>"+data[i].goodName+"</td>";
						html+="<td>"+data[i].goodGroupName+"</td>";
						html+="<td>"+data[i].num+"</td>";
						html+="<td>"+data[i].price+"</td>";
						html+="<td>"+data[i].money+"</td>";
						html+="</tr>";
					}
				}else{
					html ="<tr><td colspan='8'>暂无数据</td></tr>";
				}
				$("#repairUseGoodsList").html(html);
		}
	});
}

