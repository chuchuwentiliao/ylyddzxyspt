$(document).ready(function() {
	getTable();
	//查询列表
	$("#serch").click(function(){
		getTable();
	})
	//导出
	$("#export").click(function(){
		report_export();
    });

    
});
//导出
function report_export(){
	var beginTime=$("#begDate_Department").val();
	var endTime=$("#endDate_Department").val();
	var repairTeam=$("#groupName").val();
	var goodName=$("#goodName").val();
	var groupName=$("#goodGroupName").val();
    var url = basePath + "report/goodReport_exportGroupUseGoods.action?beginTime="+beginTime+"&endTime="+endTime+"&repairTeam="+repairTeam+"&goodName="+goodName+"&groupName="+groupName;	
	url=encodeURI(encodeURI(url));
	window.open(url);
}
var getTable=function(){
	var param={
			'beginTime':$("#begDate_Department").val(),
            'endTime':$("#endDate_Department").val(),
            "repairTeam":$("#groupName").val(),
            "goodName":$("#goodName").val(),
            "groupName":$("#goodGroupName").val()//物资组
	}
	$.ajax({
		type : 'post',
		url : basePath + "report/goodReport_findDepartmentUseGoods.action",
		data : param,
		dataType : 'json',
		success : function(data) {
				var html="";
				if(data.length>0){
					for(var i=0;i<data.length;i++){
						if(i%2==0){
							html+="<tr class='odd'>";	
						}else{
							html+="<tr class='even'>";
						}
						html+="<td>"+data[i].no+"</td>" ;
						html+="<td>"+data[i].groupName+"</td>";
						html+="<td>"+data[i].goodName+"</td>";
						html+="<td>"+data[i].goodGroupName+"</td>";
						html+="<td>"+data[i].num+"</td>";
						html+="<td>"+data[i].price+"</td>";
						html+="<td>"+data[i].money+"</td>";
						html+="</tr>";
					}
				}else{
					html ="<tr><td colspan='7'>暂无数据</td></tr>";
				}
				$("#GroupUseGoodList").html(html);
		}
	});
}

