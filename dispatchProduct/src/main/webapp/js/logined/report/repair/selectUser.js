$(function(){
	initTrees(2,3);
})






/*$(function(){
	getGroupTree();
	var i=0;//让其显示0 让其隐藏 1 
	$("#selectUserDiv").click(function(){
		if(i==0){
			i=1;
			$("#userTree").show();
		}else{
			i=0;
			$("#userTree").hide();
		}
	
	})
	
})



*//**
 * 获取維修部門的树结构
 *//*
function getGroupTree() {
	$("#userTree").hide();
    // 组织机构树参数设置
	var _check={};
	_check.enable = true;//默认是没有单选框或者复选框
    var setting = {
    	check : _check,
        view: {dblClickExpand: false},
        data: { simpleData: {enable: true }},
        callback: {onClick: onTreeNodeClick}
    };
    var dataParam = {
            'qunzuType' : 2,
            'showType' :2
	};
    qytx.app.ajax({
		url : basePath+ "report/selectUserOrGroup.action",
		type : 'post',
		dataType :'json',
		data:dataParam,
		success : function(data) {
            $.fn.zTree.init($("#userTree"), setting, data);
			//选择部门
	        getGroup();
		}
	});
}
*//**
 * 得到部门
 *//*
function getGroup() {
	var zTree = $.fn.zTree.getZTreeObj("userTree");
	var nodes = zTree.transformToArray(zTree.getNodes());
	for (var i = 0; i < nodes.length; i++) {
		var gName = nodes[i].name;
		var gId = nodes[i].id;
		var groupId = $("#groupId").val();
		if (gId == "gid_" + groupId) {
			zTree.selectNode(nodes[i]);
			$("#groupSel").val(gName);
		}
	};
}

*//**
 * 群组树调用点击事件
 *//*
function onTreeNodeClick(e, treeId, treeNode) {
	var zTree = $.fn.zTree.getZTreeObj("userTree");
	var nodes = zTree.getSelectedNodes();
	if (nodes.length > 0) {
		if(nodes[nodes.length - 1].id == "gid_0"){
			return;
		}
		var groupName = nodes[nodes.length - 1].name;
		var groupId = nodes[nodes.length - 1].id;
		$("#groupId").val(groupId.substring(4, groupId.length));
		$("#groupSel").val(groupName);
		$("#menuContent").hide();
	}
}
*/




//初始化选择树
	function initTrees(qunzuType,showType){
		/**
		 * selectId 选中的id
		 * type 1 部门人员 2 角色人员 4 群组人员 5 搜索
	     */
	    var param={};
		    param.id="userTree";
		    param.type = "check";
		    param.showType = showType;
		    param.dataParam = {"showType":showType,"qunzuType":2};
		    param.click = function(data,treeNode){
									_result = [];
									_selectId = "";
									if(data&&data.length>0){
										var str = "";
											for(var i=0;i<data.length;i++){
												var node = data[i];
												if(showType == 3){//选择人员
													if(node.id.substr(0,4) == "uid_"){
														str += node.id.substr(4) + ",";
														var obj = {};
															obj.id = node.id.substr(4);
															obj.name = node.name;
														addObj(_result,obj);//返回值
													}
												}else if(showType == 1){
													if(node.id.substr(0,4) == "gid_" && node.id !="gid_0"){
														str += node.id.substr(4) + ",";
														var obj = {};
															obj.id = node.id.substr(4);
															obj.name = node.name;
															if(type==2){
																obj.pathName = (node.obj.indexOf('{1}')>0)?node.obj.split("{1}")[1]:node.obj;
															}
														addObj(_result,obj);//返回值
													}
												}else if(showType == 2){
													if(node.id.substr(0,4) == "rid_"){
														str += node.id.substr(4) + ",";
														var obj = {};
															obj.id = node.id.substr(4);
															c.name = node.name;
														addObj(_result,obj);//返回值
													}
												}else if(showType == 4){
													if(node.id.substr(0,4) == "uid_"){
														str += node.id.substr(4) + ",";
														var obj = {};
															obj.id = node.id.substr(4);
															obj.name = node.name;
															obj.type="1"
														addObj(_result,obj);//返回值
													}
													if(node.id.substr(0,4) == "gid_" && node.id !="gid_0"){
														str += node.id.substr(4) + ",";
														var obj = {};
															obj.id = node.id.substr(4);
															obj.name = node.name;
															obj.type="2"
														addObj(_result,obj);//返回值
													}
												}else if(showType == 5){
													if(node.id.substr(0,4) == "gid_" && node.id !="gid_0"){
														str += node.id.substr(4) + ",";
														var obj = {};
															obj.id = node.id.substr(4);
															obj.name = node.name;
															obj.type="2"
														addObj(_result,obj);//返回值
													}
												}
											}
										if(str.length>0){
											_selectId = str.substr(0,str.length-1);//修改全局选中的人员的id
										}
									}
								art.dialog.data("result",_result);
							};
			param.loadComplete=function(){
		    	if(type != 5){
			    	_result = [];
			        if ("" != _selectId) {
				        var ids = _selectId.split(",");
				        var treeObj = $.fn.zTree.getZTreeObj("groupUserTree");
				        var showType = $("#showType").val();
			
				        for ( var i = 0; i < ids.length; i++) {
					        var node = null;
					        node = treeObj.getNodeByParam("id", "gid_" + ids[i], null);
					        if (null != node) {
					        	var obj = {};
								obj.id = node.id.substr(4);
								obj.name = node.name;
							addObj(_result,obj);//返回值
					        }
					       
				        }
			        }
			        art.dialog.data("result",_result);
		    	}
		    
			};
		 
			param.url= basePath+"report/selectUserOrGroup.action";
			qytx.app.tree.userCheckOrRadio(param);
	}