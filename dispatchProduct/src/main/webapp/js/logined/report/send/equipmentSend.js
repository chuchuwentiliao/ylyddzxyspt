$(document).ready(function() {
	findRepairEventType(1);
	//查询列表
	$("#search_EventType").click(function(){
		if($(".IllegalsNum").hasClass("hide")){
			findRepairEventType(1);
		}else{
			findRepairEventType(2);
		}
	});
	//导出
	$("#report_EventType").click(
			function(){
				export_RepairEventType();
			}
	);
	//查看视图
	$("#showView_EventType").click(
			function(){
				if($("#showList_EventType").hasClass("hide")){
					$(".list_IllegalsNum").addClass("hide");
                    $(".IllegalsNum").removeClass("hide");
					$("#showList_EventType").removeClass("hide");
					$("#showView_EventType").addClass("hide");
					findRepairEventType(2);
				}
			});
	//查看列表
	$("#showList_EventType").click(
			function(){
				if($("#showView_EventType").hasClass("hide")){
					$(".IllegalsNum").addClass("hide");
					$("#showList_EventType").addClass("hide");
					$(".list_IllegalsNum").removeClass("hide");
					$("#showView_EventType").removeClass("hide");
					findRepairEventType(1);
				}
});
    
});
//导出
function export_RepairEventType(){
	var beginTime=$("#begDate_EventType").val();
	var endTime=$("#endDate_EventType").val();
    var url = basePath + "report/repairType_exportRepairEventType.action?beginTime="+beginTime+"&endTime="+endTime;	
	url=encodeURI(url);
	window.open(url);
}

/**
 * type:1 列表 2 视图
 * 获得视图
 */
function findRepairEventType(type){
	var param={
			'beginTime':$("#begDate_EventType").val(),
            'endTime':$("#endDate_EventType").val()	
	}
	$.ajax({
		type : 'post',
		url : basePath + "report/repairType_findRepairEventTypeList.action",
		data : param,
		dataType : 'json',
		success : function(data) {
			if(type==1){
				var html="";
				if(data.length>0){
					for(var i=0;i<data.length;i++){
						if(i%2==0){
							html+="<tr class='odd'>";	
						}else{
							html+="<tr class='even'>";
						}
						html+="<td>"+data[i].no+"</td>" ;
						html+="<td>"+data[i].name+"</td>";
						html+="<td>"+data[i].num+"</td>";
						html+="</tr>";
					}
				}else{
					html ="<tr><td colspan='3'>暂无数据</td></tr>";
				}
				$("#equipmentSendList").html(html);
			}
		}
	});
}
