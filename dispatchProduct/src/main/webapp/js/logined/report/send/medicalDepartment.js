$(document).ready(function() {
	findRepairMedicalDepartment();
	//查询列表
	$("#search_medicalDepartment").click(function(){
		findRepairMedicalDepartment();
		
	});
	//导出
	$("#report_medicalDepartment").click(
			function(){
				report_MedicalDepartment();
			}
	);
});
//导出
function report_MedicalDepartment(){
	var beginTime=$("#begDate_medicalDepartment").val();
	var endTime=$("#endDate_medicalDepartment").val();
	var departmentName=$("#department_name").val();
    var url = basePath + "report/department_export.action?beginTime="+beginTime+"&endTime="+endTime+"&departmentName="+departmentName+"&type=1";	
	url=encodeURI(url);
	window.open(url);
}

/**
 * 获得列表
 */
function findRepairMedicalDepartment(){
	var param={
			'beginTime':$("#begDate_medicalDepartment").val(),
            'endTime':$("#endDate_medicalDepartment").val(),
            'departmentName':$("#department_name").val(),
            'type':1
	}
	$.ajax({
		type : 'post',
		url : basePath + "report/department_findMedicalDepartmenList.action",
		data : param,
		dataType : 'json',
		success : function(data) {
				var html="";
				if(data.length>0){
					for(var i=0;i<data.length;i++){
						if(i%2==0){
							html+="<tr class='odd'>";	
						}else{
							html+="<tr class='even'>";
						}
						html+="<td>"+data[i].no+"</td>" ;
						html+="<td>"+data[i].name+"</td>";
						html+="<td>"+data[i].reportNum+"</td>";
						html+="<td>"+data[i].finishNum+"</td>";
						html+="<td>"+data[i].noFinishNum+"</td>";
						html+="</tr>";
					}
				}else{
					html ="<tr><td colspan='5'>暂无数据</td></tr>";
				}
				$("#medicalDepartmentList").html(html);
		}
	});
}


