$(document).ready(function() {

    getTable_return();
});

//查询
function query_return() {
    /* //判定图表是否隐藏
     if($("#view_process").is(":hidden")) {
     getTable_process()
     }else {
     findChart_process();
     }*/
    getTable_return()
}

/*//切换
 function change_process(obj,type) {
 // 0 图表
 $(obj).hide();
 if(type == 0){
 $(obj).next().show();
 $("#list_process").hide();
 $("#view_process").show();
 findChart_process();
 }else {
 $(obj).prev().show()
 $("#view_process").hide();
 $("#list_process").show();

 }
 }*/

//导出
function report_export_return(){
    var beginTime=$("#begDate_return").val();
    var endTime=$("#begDate_return").val();
    var className = $("#className").val();
    var url = basePath + "report/export.action?beginTime="+beginTime+"&endTime="+endTime+"&className="+className;
    url=encodeURI(url);
    window.open(url);
}


function getTable_return(){
    var param={
        'beginTime':$("#begDate_return").val(),
        'endTime':$("#begDate_return").val(),
        'className':$("#className").val()
    }
    $.ajax({
        type : 'post',
        url : basePath + "report/biaoge.action",
        data : param,
        dataType : 'json',
        success : function(data) {
            var html="";
            if(data.length>0){
                for(var i=0;i<data.length;i++){
                    if(i%2==0){
                        html+="<tr class='odd'>";
                    }else{
                        html+="<tr class='even'>";
                    }
                    html+="<td>"+data[i].no+"</td>" ;
                    html+="<td>"+data[i].name+"</td>";
                    html+="<td>"+data[i].sumNum+"</td>";
                    html+="<td>"+data[i].finish+"</td>";
                    html+="<td>"+data[i].noFinish+"</td>";
                    html+="</tr>";
                }
            }else{
                html ="<tr><td colspan='3'>暂无数据</td></tr>";
            }
            $("#tbody_return").html(html);
        },
        error:function (XMLHttpRequest, textStatus, errorThrown) {
            var html ="<tr><td colspan='3'>暂无数据</td></tr>";
            $("#tbody_return").html(html);
        }
    });
}

/*
 /!**
 * 获得视图
 *!/
 function findChart_return(){
 var param={
 'beginTime':$("#begDate_return").val(),
 'endTime':$("#endDate_return").val(),
 'className':$("#className").val()
 }
 $.ajax({
 type : 'post',
 url : basePath + "report/shitu.action",
 data : param,
 dataType : 'json',
 success : function(data) {
 eachart_return(data);
 },
 error: function (XMLHttpRequest, textStatus, errorThrown) {
 var data = {};

 data.name = [];
 data.fcmy = [];
 data.my = [];
 data.yb = [];
 data.bmy = [];
 data.completeName = ["非常满意","满意","一般","不满意"];
 eachart_return(data);
 }
 });
 }
 //加载柱状图
 function eachart_return(data){
 var myChart = echarts.init(document.getElementById("view_return"));
 option = {
 tooltip : {
 trigger: 'axis',
 axisPointer : {            // 坐标轴指示器，坐标轴触发有效
 type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
 }
 },
 legend: {
 data:data.completeName
 },
 grid: {
 x: 30,
 x2: 10,
 y2: 80,
 },
 xAxis : [
 {
 type : 'category',
 data : data.name
 }
 ],
 yAxis : [
 {
 type : 'value'
 }
 ],
 series : [
 {
 name:'非常满意',
 type:'bar',
 data:data.fcmy
 },
 {
 name:'满意',
 type:'bar',
 data:data.my
 },{
 name:'一般',
 type:'bar',
 data:data.yb
 },
 {
 name:'不满意',
 type:'bar',
 data:data.bmy
 }
 ]
 };

 myChart.setOption(option);
 }
 */

