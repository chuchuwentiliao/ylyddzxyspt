$(document).ready(function() {

    getTable_employee();
    $("#list_button_e").hide();
    $("#view_employee").hide();
});

//查询
function query_employee() {
    //判定图表是否隐藏
    if($("#view_employee").is(":hidden")) {
        getTable_employee()
    }else {
        findChart_employee();
    }
}

//切换
function change_employee(obj,type) {
    // 0 图表
    $(obj).hide();
    if(type == 0){
        $(obj).next().show();
        $("#list_employee").hide();
        $("#view_employee").show();
        findChart_employee();
    }else {
        $(obj).prev().show()
        $("#view_employee").hide();
        $("#list_employee").show();

    }
}

//导出
function report_export_employee(){
    var beginTime=$("#begDate_employee").val();
    var endTime=$("#begDate_employee").val();
    var url = basePath + "report/staff_export.action?beginTime="+beginTime+"&endTime="+endTime+"&userName=&departmentName=&type=2";	
    url=encodeURI(url);
    window.open(url);
}


function getTable_employee(){
	var employName = $("#employName").val();
	if(!employName){
		employName = "";
	}
    var param={
        'beginTime':$("#begDate_employee").val(),
        'endTime':$("#begDate_employee").val(),
        'userName':employName,
        'departmentName':'',
        'type':2
    }
    $.ajax({
        type : 'post',
        url : basePath + "report/staff_findStaffSend.action",
        data : param,
        dataType : 'json',
        success : function(data) {
            var html="";
            if(data.length>0){
                for(var i=0;i<data.length;i++){
                    if(i%2==0){
                        html+="<tr class='odd'>";
                    }else{
                        html+="<tr class='even'>";
                    }
                    html+="<td>"+data[i].no+"</td>" ;
					html+="<td>"+data[i].name+"</td>";
					html+="<td>"+data[i].departmentName+"</td>";
					html+="<td>"+data[i].receiveNum+"</td>";
					html+="<td>"+data[i].finishNum+"</td>";
					html+="<td>"+data[i].equipmentNumber+"</td>";
					html+="<td>"+data[i].noFinishNum+"</td>";
					html+="<td>"+data[i].longTimeSum+"</td>";
					html+="<td>"+data[i].avgTime+"</td>";
                    html+="</tr>";
                }
            }else{
                html ="<tr><td colspan='9'>暂无数据</td></tr>";
            }
            $("#tbody_employee").html(html);
        },
        error:function (XMLHttpRequest, textStatus, errorThrown) {
            var html ="<tr><td colspan='9'>暂无数据</td></tr>";
            $("#tbody_employee").html(html);
        }
    });
}
/**
 * 获得视图
 */
function findChart_employee(){
    var param={
        'beginTime':$("#begDate_employee").val(),
        'endTime':$("#endDate_employee").val(),
        'userName':'',
        'departmentName':'',
        'type':2
    }
    $.ajax({
        type : 'post',
        url : basePath + "report/staff_findStaffChart.action",
        data : param,
        dataType : 'json',
        success : function(data) {
            eachart_employee(data);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            var data = {};
            data.sumArray = [];
            data.name = [];
            data.noFinishArray = [];
            data.finishArray = [];
            data.completeName = ["接工量","完工量","未完工量"];
            eachart_employee(data);
        }
    });
}
//加载柱状图
function eachart_employee(data){
    var myChart = echarts.init(document.getElementById("view_employee"));
    option = {
        tooltip : {
            trigger: 'axis',
            axisPointer : {            // 坐标轴指示器，坐标轴触发有效
                type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
            }
        },
        legend: {
            data:data.completeName
        },
        grid: {
            x: 30,
            x2: 10,
            y2: 80,
        },
        xAxis : [
            {
                type : 'category',
                data : data.name
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'接工量',
                type:'bar',
                data:data.sumArray
            },
            {
                name:'完工量',
                type:'bar',
                data:data.finishArray
            },{
                name:'未完工量',
                type:'bar',
                data:data.noFinishArray
            }
        ]
    };

    myChart.setOption(option);
}

