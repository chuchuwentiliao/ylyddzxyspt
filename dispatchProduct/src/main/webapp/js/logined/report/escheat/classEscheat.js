$(document).ready(function() {

    getTable_class();
    $("#list_button_c").hide();
    $("#view_class").hide();
});

//查询
function query_class() {
    //判定图表是否隐藏
    if($("#view_class").is(":hidden")) {
        getTable_class()
    }else {
        findChart_class();
    }
}

//切换
function change_class(obj,type) {
    // 0 图表
    $(obj).hide();
   if(type == 0){
       $(obj).next().show();
       findChart_class();
       $("#list_class").hide();
       $("#view_class").show();
   }else {
       $(obj).prev().show()
       $("#view_class").hide();
       $("#list_class").show();

   }
}

//导出
function report_export_class(){
    var beginTime=$("#begDate_class").val();
    var endTime=$("#begDate_class").val();
    var url = basePath + "report/send_export.action?beginTime="+beginTime+"&endTime="+endTime+"&type=2";	
    url=encodeURI(url);
    window.open(url);
}


function getTable_class(){
    var param={
        'beginTime':$("#begDate_class").val(),
        'endTime':$("#begDate_class").val(),
        'type':2
    }
    $.ajax({
        type : 'post',
        url : basePath + "report/send_findGroupSend.action",
        data : param,
        dataType : 'json',
        success : function(data) {
            var html="";
            if(data.length>0){
                for(var i=0;i<data.length;i++){
                    if(i%2==0){
                        html+="<tr class='odd'>";
                    }else{
                        html+="<tr class='even'>";
                    }
                    html+="<td>"+data[i].no+"</td>" ;
                    html+="<td>"+data[i].name+"</td>";
                    html+="<td>"+data[i].sumTakeWorkNum+"</td>" ;
                    html+="<td>"+data[i].sumNum+"</td>";
                    html+="<td>"+data[i].finish+"</td>";
                    html+="<td>"+data[i].equipmentNumber+"</td>";
                    html+="<td>"+(data[i].noFinish<0?0:data[i].noFinish)+"</td>";
                    html+="<td>"+data[i].finishPer+"</td>";
                    html+="</tr>";
                }
            }else{
                html ="<tr><td colspan='8'>暂无数据</td></tr>";
            }
            $("#tbody_class").html(html);
        },
        error:function (XMLHttpRequest, textStatus, errorThrown) {
            var html ="<tr><td colspan='8'>暂无数据</td></tr>";
            $("#tbody_class").html(html);
        }
    });
}
/**
 * 获得视图
 */
function findChart_class(){
    var param={
        'beginTime':$("#begDate_class").val(),
        'endTime':$("#endDate_class").val(),
        'type':2
    }
    $.ajax({
        type : 'post',
        url : basePath + "report/send_findGroupChart.action",
        data : param,
        dataType : 'json',
        success : function(data) {
            eachart_class(data);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            var data = {};
            data.sumArray = [];
            data.name = [];
            data.noFinishArray = [];
            data.finishArray = [];
            data.completeName = ["应接工量","接工量","完工量","未完工量"];
            eachart_class(data);
        }
    });
}
//加载柱状图
function eachart_class(data){
    var myChart = echarts.init(document.getElementById("view_class"));
    option = {
        tooltip : {
            trigger: 'axis',
            axisPointer : {            // 坐标轴指示器，坐标轴触发有效
                type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
            }
        },
        legend: {
            data:data.completeName
        },
        grid: {
            x: 30,
            x2: 10,
            y2: 80,
        },
        xAxis : [
            {
                type : 'category',
                data : data.name
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
                  {
                      name:'应接工量',
                      type:'bar',
                      data:data.takeWorkArray
                  },
            {
                name:'接工量',
                type:'bar',
                data:data.sumArray
            },
            {
                name:'完工量',
                type:'bar',
                data:data.finishArray
            },{
                name:'未完工量',
                type:'bar',
                data:data.noFinishArray
            }
        ]
    };

    myChart.setOption(option);
}

