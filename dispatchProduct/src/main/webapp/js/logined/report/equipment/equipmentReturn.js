$(document).ready(function() {
	findequipmentReturnList();
	//查询列表
	$("#search_equipmentReturn").click(function(){
		findequipmentReturnList()
	});
	//导出
	$("#export_equipmentReturn").click(function(){
		export_equipmentReturn();
	});

    
});
//导出
function export_equipmentReturn(){
	var beginTime=$("#begDate_equipmentReturn").val();
	var endTime=$("#endDate_equipmentReturn").val();
    var url = basePath + "report/equipment_exportReport.action?beginTime="+beginTime+"&endTime="+endTime+"&type=2";	
	url=encodeURI(url);
	window.open(url);
}

/**
 * type:1 列表 2 视图
 * 获得视图
 */
function findequipmentReturnList(){
	var param={
			'beginTime':$("#begDate_equipmentReturn").val(),
            'endTime':$("#endDate_equipmentReturn").val(),
            'type':2
	}
	$.ajax({
		type : 'post',
		url : basePath + "report/equipment_findReturnEquipmentList.action",
		data : param,
		dataType : 'json',
		success : function(data) {
				var html="";
				if(data.length>0){
					for(var i=0;i<data.length;i++){
						if(i%2==0){
							html+="<tr class='odd'>";	
						}else{
							html+="<tr class='even'>";
						}
						html+="<td>"+data[i].no+"</td>" ;
						html+="<td>"+data[i].equipmentGroupName+"</td>";
						html+="<td>"+data[i].userName+"</td>";
						html+="<td>"+data[i].groupName+"</td>";
						html+="<td>"+data[i].equipmentNum+"</td>";
						html+="</tr>";
					}
				}else{
					html ="<tr><td colspan='5'>暂无数据</td></tr>";
				}
				$("#equipmentReturnList").html(html);
		}
	});
}
