$(document).ready(function() {
	findequipmentSendList();
	//查询列表
	$("#search_EventType").click(function(){
		findequipmentSendList()
	});
	//导出
	$("#report_EventType").click(
			function(){
				export_equipmentSend();
			}
	);

    
});
//导出
function export_equipmentSend(){
	var beginTime=$("#begDate_EventType").val();
	var endTime=$("#endDate_EventType").val();
    var url = basePath + "report/equipment_exportReport.action?beginTime="+beginTime+"&endTime="+endTime+"&type=1";	
	url=encodeURI(url);
	window.open(url);
}

/**
 * type:1 列表 2 视图
 * 获得视图
 */
function findequipmentSendList(){
	var param={
			'beginTime':$("#begDate_EventType").val(),
            'endTime':$("#endDate_EventType").val(),
            'type':1
	}
	$.ajax({
		type : 'post',
		url : basePath + "report/equipment_findReturnEquipmentList.action",
		data : param,
		dataType : 'json',
		success : function(data) {
				var html="";
				if(data.length>0){
					for(var i=0;i<data.length;i++){
						if(i%2==0){
							html+="<tr class='odd'>";	
						}else{
							html+="<tr class='even'>";
						}
						html+="<td>"+data[i].no+"</td>" ;
						html+="<td>"+data[i].equipmentGroupName+"</td>";
						html+="<td>"+data[i].userName+"</td>";
						html+="<td>"+data[i].groupName+"</td>";
						html+="<td>"+data[i].equipmentNum+"</td>";
						html+="</tr>";
					}
				}else{
					html ="<tr><td colspan='5'>暂无数据</td></tr>";
				}
				$("#equipmentSendList").html(html);
		}
	});
}
