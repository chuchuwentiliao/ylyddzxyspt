$(function(){
	var beginTimeStr = getNowDate();
    var endTimeStr = getNowDate();
    //初始化各页面的时间为当前系统时间
    $("#begDate").val(beginTimeStr);
    $("#endDate").val(endTimeStr);
    getTable();
})




function getTable(){
    var param={
        'beginTime':$("#begDate").val(),
        'endTime':$("#begDate").val(),
        'userName':$("#employee").val()
    }
    $.ajax({
        type : 'post',
        url : basePath + "report/dispatch_findDispatchList.action",
        data : param,
        dataType : 'json',
        success : function(data) {
            var html="";
            if(data.length>0){
                for(var i=0;i<data.length;i++){
                    if(i%2==0){
                        html+="<tr class='odd'>";
                    }else{
                        html+="<tr class='even'>";
                    }
                    html+="<td>"+data[i].no+"</td>" ;
					html+="<td>"+data[i].name+"</td>";
					html+="<td>"+data[i].phoneReceive+"</td>";
					html+="<td>"+data[i].receive+"</td>";
					html+="<td>"+data[i].sum+"</td>";
                    html+="</tr>";
                }
            }else{
                html ="<tr><td colspan='8'>暂无数据</td></tr>";
            }
            $("#dispatchList").html(html);
        },
        error:function (XMLHttpRequest, textStatus, errorThrown) {
            var html ="<tr><td colspan='8'>暂无数据</td></tr>";
            $("#dispatchList").html(html);
        }
    });
}


//导出
function report_export(){
    var beginTime=$("#begDate").val();
    var endTime=$("#begDate").val();
    var userName=$("#employee").val();
    userName= encodeURI(userName); 
    var url = basePath + "report/dispatch_exportReport.action?beginTime="+beginTime+"&endTime="+endTime+"&userName="+userName;	
    url=encodeURI(url);
    window.open(url);
}

//获取当前日期
function getNowDate() {
    var now = new Date();
    return getFormatDate(now);
}
//时间格式转换
function getFormatDate(srcdate) {

    var formatDate = "";
    // 初始化时间
    var Year = srcdate.getFullYear();// ie火狐下都可以
    var Month = srcdate.getMonth() + 1;
    formatDate += Year;
    if (Month >= 10) {
        formatDate += "-" + Month;
    } else {
        formatDate += "-0" + Month;
    }

    var Day = srcdate.getDate();
    if (Day >= 10) {
        formatDate += "-" + Day;
    } else {
        formatDate += "-0" + Day;
    }
    return formatDate;
}
