package cn.com.qytx.pushPlat.push.service;

import java.util.Map;

/**
 * 功能: 推送接口 
 * 版本: 1.0
 * 开发人员: zyf
 * 创建日期: 2015年5月11日
 * 修改日期: 2015年5月11日
 * 修改列表:
 */
public interface PushService {
	
	/**
	 * 功能：推送消息
	 */
	public void pushInfo(Map<String,Object> dbInfo,String platType,String destination,String content,String appId,int companyId,String extra,String title,String jpushPushType,String isOld,String iosDestination,String iosPushType,String androidPushType);
}
