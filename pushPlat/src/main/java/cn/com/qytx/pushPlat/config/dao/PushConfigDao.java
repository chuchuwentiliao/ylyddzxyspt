package cn.com.qytx.pushPlat.config.dao;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 功能: 推送配置 
 * 版本: 1.0
 * 开发人员: zyf
 * 创建日期: 2015年5月11日
 * 修改日期: 2015年5月11日
 * 修改列表:
 */
@Component
public class PushConfigDao {
	
	@Autowired
	private BaseDao baseDao;
	
	final String sql = "select b.appKey,b.secret,b.pushType,b.mqttIpPort,b.surrounding from pushInfoPlat a,appPushInfo b where a.appId = ? and a.companyId = ? and a.pId = b.id";
	final String coungSql = "select count(*) from pushInfoPlat a,appPushInfo b where a.appId = ? and a.companyId = ? and a.pId = b.id";
	public List<Map<String, Object>> getPushConfig(String appId,int companyId){
		Integer count = (Integer)baseDao.findBySqlForCount(coungSql, appId,companyId);
		if(count.intValue()>0){
			return baseDao.findListMapBySql(sql, appId,companyId);
		}
		
		if(companyId == 0){
			return null;
		}else{
			//如果当前单位信息查不到则使用app默认推送配置
			Integer baseCount = (Integer)baseDao.findBySqlForCount(coungSql, appId,0);
			if(baseCount.intValue()>0){
				return baseDao.findListMapBySql(sql, appId,0);
			}else{
				return null;
			}
		}
	}
	
	final String apnsSql = "select appId,apnsPath,surrounding,password from apnsInfo where appId=?  ";
	public Map<String, Object> getApnsInfo(String appId){
		return baseDao.findMapBySql(apnsSql, appId);
	}
}
