package cn.com.qytx.pushPlat.push.apns;

import java.io.InputStream;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import cn.com.qytx.pushPlat.config.service.IConnection;
import cn.com.qytx.pushPlat.utils.ThreadPoolUtils;

import com.dbay.apns4j.IApnsService;
import com.dbay.apns4j.demo.Apns4jDemo;
import com.dbay.apns4j.impl.ApnsServiceImpl;
import com.dbay.apns4j.model.ApnsConfig;
import com.dbay.apns4j.model.Payload;

/**
 * 功能: 苹果ios APNS推送
 * 版本: 1.0
 * 开发人员: zyf
 * 创建日期: 2015年3月23日
 * 修改日期: 2015年3月23日
 * 修改列表:
 */
public class APNSPush implements IConnection{
	 
	 /**
	  * 功能：获得推送服务器
	  * @param path
	  * @param password
	  * @param surrounding
	  * @return
	  */
	 private static IApnsService getApnsService(String path,String password,boolean surrounding) {
		      ApnsConfig config = new ApnsConfig();
//		      InputStream is = Apns4jDemo.class.getClassLoader().getResourceAsStream("aps_developer.p12");
		      InputStream is = Apns4jDemo.class.getClassLoader().getResourceAsStream(path);
		      config.setKeyStore(is);
		      //true 测试环境 false 正式环境
		      config.setDevEnv(surrounding);
//		      config.setPassword("900822");
		      config.setPassword(password);
		      config.setPoolSize(5);
		      return  ApnsServiceImpl.createInstance(config);
		  }
	 
	  /**
	     * 功能：启动线程池推送
	     * @param destination 目的token值
	     * @param message 推送内容
	     */
	    public static void publish(final String destination,final String message,final Map<String,Object> apnsInfo){
	    	ThreadPoolUtils.getInstance().getThreadPool().execute(new Thread( new Runnable() {
				
				@Override
				public void run() {
					try {
						/**
						 * 推送消息 
						 */
						sendMessage(destination, message,apnsInfo);
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}));
	    }
	    
	    /**
	     * 功能：推送apns消息
	     * @param tokens tokens
	     * @param message
	     * @param appId
	     */
	    public static void sendMessage(String tokens, String message,Map<String, Object> apnsInfo){
	    	IApnsService service = getApnsService(apnsInfo.get("apnsPath").toString(), apnsInfo.get("password").toString()
	    			, apnsInfo.get("surrounding").toString().endsWith("1")?true:false);
	    	if(StringUtils.isNotEmpty(tokens)){
	    		String[] tokenArr = tokens.split(",");
	    		for(String token:tokenArr){
	    			if(StringUtils.isNotEmpty(token)){//token为空判断
	    				Payload payload = new Payload();
	    			    payload.setAlert(message);
	    			    payload.setBadge(1);
	    			    payload.setSound("default");
	    			    service.sendNotification(token, payload);
	    			}
	    		}
	    	}
			    
			    // It's a good habit to shutdown what you never use
//			    service.shutdown();
	    }
	 
}
