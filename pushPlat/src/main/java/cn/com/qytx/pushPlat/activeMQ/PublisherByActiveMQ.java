/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.com.qytx.pushPlat.activeMQ;

import java.util.LinkedList;

import javax.annotation.Resource;

import org.fusesource.hawtbuf.AsciiBuffer;
import org.fusesource.hawtbuf.Buffer;
import org.fusesource.hawtbuf.UTF8Buffer;
import org.fusesource.mqtt.client.Future;
import org.fusesource.mqtt.client.FutureConnection;
import org.fusesource.mqtt.client.MQTT;
import org.fusesource.mqtt.client.QoS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * 使用ActiveMQ的MQTT协议推送.
 */
@Service
public class PublisherByActiveMQ {
	private Logger log = LoggerFactory.getLogger(PublisherByActiveMQ.class);
	
	@Resource
	private ActiveMQConfig activeMQConfig;

    public void pushByActiveMQ(String destions,String msgContent) throws Exception {
    	log.info("推送目标:"+destions);
    	log.info("推送内容:"+msgContent);
    	
        String user = activeMQConfig.getUserName();
        String password = activeMQConfig.getPassWord();
        String host = activeMQConfig.getHost();
        int port = activeMQConfig.getPort();

        MQTT mqtt = new MQTT();
        mqtt.setHost(host, port);
        mqtt.setUserName(user);
        mqtt.setPassword(password);

        FutureConnection connection = mqtt.futureConnection();
        try{
	        connection.connect().await(3000, java.util.concurrent.TimeUnit.MILLISECONDS);
	        final LinkedList<Future<Void>> queue = new LinkedList<Future<Void>>();
	        String[] des = destions.split(",");
	        Buffer msg = new AsciiBuffer(msgContent);
	        for(int i=0; i<des.length; i++){
	        	UTF8Buffer topic = new UTF8Buffer(des[i]);
	            queue.add(connection.publish(topic, msg, QoS.AT_LEAST_ONCE, false));
	        }
        while( !queue.isEmpty() ) {
            queue.removeFirst().await();
        }
        }catch(Exception e){
        	
        }finally{
        	connection.disconnect().await();
        }
    }
}