package cn.com.qytx.pushPlat.push.http;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import cn.com.qytx.pushPlat.config.service.PushConfigService;
import cn.com.qytx.pushPlat.push.service.PushService;
import cn.com.qytx.pushPlat.utils.SpringUtil;
import cn.com.qytx.pushPlat.utils.SysUtil;

/**
 * 功能: 推送消息 
 * 版本: 1.0
 * 开发人员: zyf
 * 创建日期: 2015年3月12日
 * 修改日期: 2015年3月12日
 * 修改列表:
 */
public class PushInfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	PushConfigService pushConfigService = (PushConfigService) SpringUtil.getBean("pushConfigService");
	PushService pushService = (PushService) SpringUtil.getBean("pushService");
	
	public void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {			String appId = SysUtil.getStringFromObject(request.getParameter("appId"));//应用id
			String companyId = SysUtil.getStringFromObject(request.getParameter("companyId"));//公司id
			if(companyId.equals("")){
				companyId = "0";
			}
			String destination = SysUtil.getStringFromObject(request.getParameter("destination"));//目的地
			String iosDestination = SysUtil.getStringFromObject(request.getParameter("iosDestination"));//ios目的地
			String content = SysUtil.getStringFromObject(request.getParameter("content"));//消息内容
			String extra = SysUtil.getStringFromObject(request.getParameter("extra"));//附加信息
			String fromUser = SysUtil.getStringFromObject(request.getParameter("fromUser"));//推送人
			String title = request.getParameter("title")==null?"":request.getParameter("title").toString();//标题 暂不启用
			String platType = request.getParameter("platType")==null?"":request.getParameter("platType").toString();//推送目标平台
			String iosPushType = request.getParameter("iosPushType")==null?"":request.getParameter("iosPushType").toString();//ios推送类型
			String androidPushType = request.getParameter("androidPushType")==null?"":request.getParameter("androidPushType").toString();//android推送类型
			String jpushPushType = request.getParameter("jpushPushType")==null?"":request.getParameter("jpushPushType").toString();//jpush推送类型 tag/alias
			String isOld = request.getParameter("isOld")==null?"":request.getParameter("isOld").toString();//是否是老接口 true/false 默认是false
			
			if(StringUtils.isEmpty(appId)||StringUtils.isEmpty(companyId)||StringUtils.isEmpty(destination)){
				SysUtil.toClient("101||参数不正确", response);
				return ;
			}
			
			
			
			/**
			 * 获取单位推送配置信息
			 */
			List<Map<String,Object>> dbInfoList = pushConfigService.getPushConfig(appId, Integer.valueOf(companyId));
			
			if(dbInfoList == null || dbInfoList.size()<=0){
				SysUtil.toClient("102||请初始化应用信息", response);
				return ;
			}
			for(Map<String,Object> dbInfo:dbInfoList){
				pushService.pushInfo(dbInfo, platType.toLowerCase(), destination, content, appId, Integer.valueOf(companyId), extra, title, jpushPushType, isOld, iosDestination, iosPushType,androidPushType);
			}
			SysUtil.toClient("100||推送中..", response);
			return ;
		} catch (Exception e) {
			e.printStackTrace();
			SysUtil.toClient("102||服务器异常", response);	
		}
	}

}
