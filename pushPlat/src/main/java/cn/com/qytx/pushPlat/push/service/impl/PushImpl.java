package cn.com.qytx.pushPlat.push.service.impl;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.qytx.pushPlat.activeMQ.PublisherByActiveMQ;
import cn.com.qytx.pushPlat.config.service.IConnection;
import cn.com.qytx.pushPlat.config.service.PushConfigService;
import cn.com.qytx.pushPlat.push.apns.APNSPush;
import cn.com.qytx.pushPlat.push.jpush.JPushForAndroid;
import cn.com.qytx.pushPlat.push.jpush.JPushForIOS;
import cn.com.qytx.pushPlat.push.mqtt.MqttPush;
import cn.com.qytx.pushPlat.push.service.PushService;
import cn.com.qytx.pushPlat.utils.SysUtil;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * 功能: 推送接口 
 * 版本: 1.0
 * 开发人员: zyf
 * 创建日期: 2015年5月11日
 * 修改日期: 2015年5月11日
 * 修改列表:
 */
@Service("pushService")
public class PushImpl implements PushService {
	
	@Autowired
	private PushConfigService pushConfigService;
	
	@Resource
	private PublisherByActiveMQ publisherByActiveMQ;
	
	/**
	 * 功能：推送消息
	 */
	/* (non-Javadoc)
	 * @see cn.com.qytx.pushPlat.push.service.PushService#pushInfo(java.util.Map, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	public void pushInfo(Map<String,Object> dbInfo,String platType,final String destination,String content,String appId,int companyId,String extra,String title,String jpushPushType,String isOld,String iosDestination,String iosPushType,String androidPushType){
		
		boolean apnsProduction = dbInfo.get("surrounding").toString().endsWith("1")?true:false;
		
		if(platType.equals("")||platType.equals("all")||platType.equals("android")){//安卓推送验证
			if(dbInfo.get("pushType")!=null && IConnection.MQTT_PUSH_TYPE.equals(dbInfo.get("pushType"))){
				//mqtt 协议推送
				MqttPush.publish(destination, content, appId, companyId);
			}else if(dbInfo.get("pushType")!=null && IConnection.JPUSH_PUSH_TYPE.equals(dbInfo.get("pushType"))){
				//极光推送
				String appKey = SysUtil.getStringFromObject(dbInfo.get("appKey"));
				String secret = SysUtil.getStringFromObject(dbInfo.get("secret"));
				Map extraMap = null;
				if(StringUtils.isNotEmpty(extra)){
					extraMap = new Gson().fromJson(extra, new TypeToken<Map>(){}.getType());
				}
				
				new JPushForAndroid(destination, content, extraMap, title, appKey, secret, appId, companyId, jpushPushType, isOld,androidPushType).publish();
			}
		}
		
		if(platType.equals("")||platType.equals("all")||platType.equals("ios")){
			if(!iosPushType.equals("message")){//ios推送默认使用通知
				if(StringUtils.isNotEmpty(iosDestination)){
					Map<String, Object> apnsInfo = pushConfigService.getApnsInfo(appId);
					if(apnsInfo!=null){
						/**
						 * apns推送
						 */
						APNSPush.publish(iosDestination, content, apnsInfo);
					}
				}
			}
			
			//兼容ios的apns推送
			if(dbInfo != null){
				String appKey = SysUtil.getStringFromObject(dbInfo.get("appKey"));
				String secret = SysUtil.getStringFromObject(dbInfo.get("secret"));
				Map extraMap = null;
				if(StringUtils.isNotEmpty(extra)){
					extraMap = new Gson().fromJson(extra, new TypeToken<Map>(){}.getType());
				}
				new JPushForIOS(destination, content, extraMap, title, appKey, secret, appId, companyId, iosPushType, jpushPushType, apnsProduction).publish();;
			}
		}
		
		/*
		 * add by jiaq,2015.11.13
		 *使用ActiveMQ推送 
		 */
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					publisherByActiveMQ.pushByActiveMQ(destination, "imimim");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}).start();
	}
}
