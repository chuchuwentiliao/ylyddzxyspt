package cn.com.qytx.pushPlat.push.mqtt;

import java.util.UUID;

import org.apache.commons.lang3.StringUtils;

import cn.com.qytx.pushPlat.config.service.IConnection;
import cn.com.qytx.pushPlat.domain.SysConfig;
import cn.com.qytx.pushPlat.utils.ThreadPoolUtils;

import com.ibm.mqtt.MqttClient;
import com.ibm.mqtt.MqttException;
import com.ibm.mqtt.MqttNotConnectedException;
import com.ibm.mqtt.MqttPersistenceException;
import com.ibm.mqtt.MqttSimpleCallback;

/**
 * 功能: 初始化服务器连接
 * 版本: 1.0
 * 开发人员: zyf
 * 创建日期: 2015年3月9日
 * 修改日期: 2015年3月9日
 * 修改列表:  
 */
public class MqttPush implements IConnection {
    private final static boolean CLEAN_START = false;
    private final static short KEEP_ALIVE = 30;
    
	/**
	 * 功能：连接mqtt服务器
	 * @return
	 * @throws MqttException
	 */
    private MqttClient getConnection() {
    	MqttClient connection = null;
    	try{
    		String connection_string = "tcp://"+SysConfig.getInstance().getMqttServerIp()+":"+SysConfig.getInstance().getMqttServerPort();
//    		String connection_string = "tcp://10.200.10.170:1883";
    		String client_id = UUID.randomUUID().toString().substring(0,22);

    		//设置服务端的ip
    		connection = new MqttClient(connection_string);
//    		SimpleCallbackHandler simpleCallbackHandler = new SimpleCallbackHandler();
//    		connection.registerSimpleHandler(simpleCallbackHandler);
    		connection.connect(client_id, CLEAN_START, KEEP_ALIVE);
    	}catch (MqttException e) {
			e.printStackTrace();
		}
    	return connection;
    }
    
    /**
     * 功能：推送消息 到topic 可以多条topic 以','分割
     * @param topic
     * @param message
     * @throws MqttException 
     * @throws IllegalArgumentException 
     * @throws MqttPersistenceException 
     * @throws InterruptedException 
     * @throws MqttNotConnectedException 
     */
    private static void sendMessage(String topics,String message,String appId,int companyId) throws MqttPersistenceException, IllegalArgumentException, MqttException, InterruptedException{
    	try{
    		if(StringUtils.isNotEmpty(topics)){
	    		if(message.isEmpty()){
	    			message="";
	    		}
	    		/**
	    		 * 打开一个新的连接
	    		 */
	    		MqttClient connection = new MqttPush().getConnection();
	    		if(connection!=null){
	    			String[] list = topics.split(",");
	    			for(String topic:list){
	    				if(StringUtils.isNotEmpty(topic)){
	    					topic = appId+"_"+companyId+"_"+topic;
	    					int result = connection.publish(topic, message.getBytes(), 1, true);
	    					Thread.currentThread().sleep(1);
	    				}
	    			}
	    		}
	    		/**
	    		 * 关闭连接
	    		 */
	    		connection.disconnect();
	    	}
    	}catch(MqttNotConnectedException e){
    		e.printStackTrace();
    	}
    }
    
    /**
     * 功能：启动线程池推送
     * @param topics
     * @param message
     */
    public static void publish(final String topics,final String message,final String appId,final int companyId){
    	ThreadPoolUtils.getInstance().getThreadPool().execute(new Thread( new Runnable() {
			
			@Override
			public void run() {
				try {
					/**
					 * 推送消息 
					 */
					sendMessage(topics, message,appId,companyId);
				} catch (MqttPersistenceException e) {
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (MqttException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		}));
    }
    
    /**
     * 功能: 封装mqtt连接回调函数
     * 版本: 1.0
     * 开发人员: zyf
     * 创建日期: 2015年3月9日
     * 修改日期: 2015年3月9日
     * 修改列表:
     */
    class SimpleCallbackHandler implements MqttSimpleCallback {
    	 
        @Override
        public void connectionLost() throws Exception {
            System.out.println("客户机和broker已经断开");
        }
 
        @Override
        public void publishArrived(String topicName, byte[] payload, int Qos,
                boolean retained) throws Exception {
//            System.out.println("订阅主题: " + topicName);
//            System.out.println("消息数据: " + new String(payload));
//            System.out.println("消息级别(0,1,2): " + Qos);
//            System.out.println("是否是实时发送的消息(false=实时，true=服务器上保留的最后消息): " + retained);
        }
 
    }
}
