package cn.com.qytx.pushPlat.push.jpush;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import cn.com.qytx.pushPlat.utils.ThreadPoolUtils;
import cn.jpush.api.JPushClient;
import cn.jpush.api.common.resp.APIConnectionException;
import cn.jpush.api.common.resp.APIRequestException;
import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.Message;
import cn.jpush.api.push.model.Platform;
import cn.jpush.api.push.model.PushPayload;
import cn.jpush.api.push.model.audience.Audience;
import cn.jpush.api.push.model.notification.Notification;

/**
 * 功能: 极光推送消息  通过别名
 * 版本: 1.0
 * 开发人员: zyf
 * 创建日期: 2015年3月9日
 * 修改日期: 2015年3月9日
 * 修改列表:
 */
public class JPushForAndroid {
	private String topics;
	private String message;
	private Map extra;
	private String title;
	private String appKey;
	private String secret;
	private String appId;
	private Integer companyId;
	private String jpushPushType;
	private String isOld;
	private String androidPushType;
	
	public JPushForAndroid(String topics,String message,Map extra,String title,String appKey,String secret,String appId,int companyId,String jpushPushType,String isOld,String androidPushType){
		this.topics = topics; 
		this.message = message;
		
		if(extra!=null){
			Map<String, String> map = new HashMap<String, String>();
			for (Object key : extra.keySet()) {
				map.put(key.toString(), extra.get(key.toString()).toString());
			  }
			this.extra = map;
		}else{
			this.extra = new HashMap<String, String>();
		}
		this.title = title;
		this.appKey = appKey;
		this.secret = secret;
		this.appId = appId;
		this.companyId = companyId;
		this.jpushPushType = jpushPushType;
		this.isOld = isOld;
		this.androidPushType = androidPushType;
	}
	
	/**
     * 功能：启动线程池推送
     * @param topics
     * @param message
     */
    public void publish(){
    	ThreadPoolUtils.getInstance().getThreadPool().execute(new Thread( new Runnable() {
			
			@Override
			public void run() {
				JPushClient jpush = new JPushClient(secret, appKey,3);
				/**
				 * 推送消息 
				 */
				if(jpushPushType.equals("tag")){
					sendMessage(jpush,20);
				}else{
					sendMessage(jpush,1000);
				}
			}
		}));
    }
    
    /**
     * 功能：极光推送消息
     * @param jpush
     * @param topics
     * @param message
     */
    private void sendMessage(JPushClient jpushClient,int limit){
    	if(StringUtils.isNotEmpty(topics)){
    		if(message.isEmpty()){
    			message="";
    		}
    		if(topics.startsWith(",")){
    			topics = topics.substring(1);
    		}
    		if(topics.endsWith(",")){
    			topics = topics.substring(0,topics.length()-1);
    		}
    		if(StringUtils.isNotEmpty(topics)){
    			String[] list = topics.split(",");
    			
    			if(list!=null && list.length>limit){//别名推送一次只能推送1000 tag一次只能推送20
    				String[] arr = new String[limit];
    				int j=0;
    				for(int i=0;i<list.length;i++){
    					if(list[i]!=null){
    						arr[j] = list[i];
    						j++;
    					}
    					
    					if(j%1000 == 0 || i == (list.length-1)){
    						PushPayload payload = buildPushObject_android(arr, message,extra,title,jpushPushType,isOld);
    						if(androidPushType.equals("notify")){
    							payload = buildPushObject_android_notify(arr, message,extra,title,jpushPushType,isOld);
    						}
    		    			try {
    		    				PushResult result = jpushClient.sendPush(payload);
    		    			} catch (APIConnectionException e) {
    		    				e.printStackTrace();
    		    			} catch (APIRequestException e) {
    		    				e.printStackTrace();
    		    			}
    	    				j=0;
    	    				arr = new String[limit];
    					}
    				}
    			}else{//不满一千不需要走上面的流程
    				PushPayload payload = buildPushObject_android(list, message,extra,title,jpushPushType,isOld);
    				if(androidPushType.equals("notify")){
						payload = buildPushObject_android_notify(list, message,extra,title,jpushPushType,isOld);
					}
        			try {
        				PushResult result = jpushClient.sendPush(payload);
        			} catch (APIConnectionException e) {
        				//e.printStackTrace();
        				System.out.println("链接失败");
        			} catch (APIRequestException e) {
        				//e.printStackTrace();
        				System.out.println("推送目标不存在");
        			}
    			}
    		}
    	}
    }
    
    /**
     * 功能：构建安卓 ios推送负载
     * @return
     */
    private static PushPayload buildPushObject_android(String[] tags,String message,Map extra,String title,String jpushPushType,String isOld) {
    	PushPayload.Builder builder = PushPayload.newBuilder()
    			.setPlatform(Platform.android());
    	if(jpushPushType.equals("tag")){
    		builder.setAudience(Audience.tag(tags));
    	}else{
    		builder.setAudience(Audience.alias(tags));
    	}
    	
    	if(isOld.equals("true")){
    		return builder.setMessage(Message.newBuilder().setMsgContent(message).addExtras(extra).setTitle(title).build())
    				.build();
    	}
    	return builder.setMessage(Message.newBuilder().setContentType("99").setMsgContent(message).addExtras(extra).setTitle(title).build())
    			.build();
    	
    }
    
    /**
     * 功能：构建安卓 ios推送负载
     * @return
     */
    private static PushPayload buildPushObject_android_notify(String[] tags,String message,Map extra,String title,String jpushPushType,String isOld) {
    	PushPayload.Builder builder = PushPayload.newBuilder()
    			.setPlatform(Platform.android());
    	if(jpushPushType.equals("tag")){
    		builder.setAudience(Audience.tag(tags));
    	}else{
    		builder.setAudience(Audience.alias(tags));
    	}
    	
		return builder.setNotification(Notification.android(message, title, extra)).build();
    }
    
	public String getTopics() {
		return topics;
	}

	public void setTopics(String topics) {
		this.topics = topics;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAppKey() {
		return appKey;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public String getJpushPushType() {
		return jpushPushType;
	}

	public void setJpushPushType(String jpushPushType) {
		this.jpushPushType = jpushPushType;
	}

	public String getIsOld() {
		return isOld;
	}

	public void setIsOld(String isOld) {
		this.isOld = isOld;
	}

	public Map getExtra() {
		return extra;
	}

	public void setExtra(Map extra) {
		this.extra = extra;
	}
	
}
