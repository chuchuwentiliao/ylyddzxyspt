$(function(){
	findDetail();
})

function findDetail(){
	var goodId=art.dialog.data("goodId");
	$.ajax({
		url:basePath+"goods/findDetail.action",
	    type:'post',
	    data:{"id":goodId},
	    dataType:'json',
	    success:function(result){
	    	if(result){
	    		$("#name").html(result.name);
	    		$("#goodNo").html(result.goodNo);
	    		$("#goodGroupName").html(result.goodGroupName);
	    		$("#type").html(result.type);
	    		$("#specifications").html(result.specifications);
	    		$("#unit").html(result.unit);
	    		$("#price").html(result.price);
	    	}
	    }
	})
}