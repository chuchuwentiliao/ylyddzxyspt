$(function(){
	initButton();
	position();
	selectResult(1)
});
//资产状态1 正常 2 维修 3报废 4 损耗
var capitalNumType=1;
/**
 * 盘点
 * */
function change(){
	var id=art.dialog.data("capitalId");
	var repairTime =$("#repairTime").val();
	var reg=/^[0-9]\d*$/;
	var capitalNum=$("#capitalNum").val();
	if(capitalNumType==4){
	    if(!reg.test(capitalNum)){
	    	art.dialog.alert("剩余数量必须为正整数！");
	    	return;
	    }
	}
    var repairMoney=$("#repairMoney").val();
    if(capitalNumType==2){
    	if(!reg.test(repairMoney)){
        	art.dialog.alert("维修费用必须为正整数！");
        	return;
        }
    }
	if(repairTime!=null&&repairTime!=""){
		repairTime = repairTime+":00.000";
	}
	var repairCompany=$("#repairCompany").val();
	var repairResult=$("#repairResult").val();
	var approveRecord=$("#approveRecord").val();
	var param = {
			"capitalChangesVo.type":6,
			"capitalChangesVo.id":id,
			"capitalChangesVo.capitalNum":capitalNum,
			"capitalChangesVo.pandianResult":capitalNumType,
			"capitalChangesVo.repairTime":repairTime,
			"capitalChangesVo.repairMoney":repairMoney,
			"capitalChangesVo.repairResult":repairResult,
			"capitalChangesVo.approveRecord":approveRecord,
			"capitalChangesVo.storeLocation":$("#position").val(),
			"capitalChangesVo.repairCompany":repairCompany
	}
	$.ajax({
		url:basePath+"capital/changes.action",
		type:'post',
		data:param,
		dataType:'text',
		success:function(result){
			if(result==1){
				art.dialog.tips("盘点成功");
				setTimeout(function(){
					art.dialog.data("result","success");
					art.dialog.close();
				},500);
			}else{
				art.dialog.alert("盘点失败，请稍后重试！");
			}
		}
	});
}


/**
 * 初始化按钮
 */
function initButton(){
	var api = art.dialog.open.api;
	api.button(
			{
				name: '确定',
				callback: function () {
					change();
					return false;
				},
				focus: true
			},
			{
				name: '取消'
			}
		);
}


//加载存放位置下拉选
function position(){
	var param={
		"infoType":"capital_store",
		"sysTag":1
	}
	$.ajax({
		url:basePath+"dict/getDicts.action",
		type:'post',
		data:param,
		dataType:'json',
		success:function(data){
			var html="<option value =''>请选择</option>";
			if(data!=null){
				for(var i=0;i<data.length;i++){
					 html+="<option value ='"+data[i].value+"'>"+data[i].name+"</option>";
				}
			}
			$("#position").html(html);
		}
	})
};

/**
 * 选择盘点结果
 */
function selectResult(type){
	capitalNumType=type;
	if(type==2){
		$("#time").removeClass('hide');
		$("#money").removeClass('hide');
		$("#company").removeClass('hide');
		$("#result").removeClass('hide');
		$("#num").addClass('hide');
		$("#approveresult").addClass('hide');
		$("#cunfang").addClass('hide');
	}else if(type==3){
		$("#approveresult").removeClass('hide');
		$("#cunfang").removeClass('hide');
		$("#time").addClass('hide');
		$("#money").addClass('hide');
		$("#company").addClass('hide');
		$("#result").addClass('hide');
		$("#num").addClass('hide');
	}else if(type==4){
		$("#num").removeClass('hide');
		$("#approveresult").addClass('hide');
		$("#cunfang").addClass('hide');
		$("#time").addClass('hide');
		$("#money").addClass('hide');
		$("#company").addClass('hide');
		$("#result").addClass('hide');
	}else{
		$("#num").addClass('hide');
		$("#approveresult").addClass('hide');
		$("#cunfang").addClass('hide');
		$("#time").addClass('hide');
		$("#money").addClass('hide');
		$("#company").addClass('hide');
		$("#result").addClass('hide');
	}
}
