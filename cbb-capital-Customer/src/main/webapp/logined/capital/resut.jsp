<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <title>资产入库成功</title>
    <jsp:include page="../../common/flatHead.jsp"/>
    <link href="${ctx }flat/css/reset.css" rel="stylesheet" type="text/css" />
  	<link href="${ctx}flat/css/main.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="${ctx}flat/plugins/tree/skins/tree_default.css" type="text/css"/>
	<link href="${ctx }flat/plugins/form/skins/form_default.css" rel="stylesheet" type="text/css" />
<script TYPE="text/javascript" src="${ctx }js/logined/capital/resut.js"></script>
<style type="text/css">
	body{background:#fff;}
	.aui_buttons button{
	width:120px
	}
	.content_form{padding:0px}
	.ts_message{border:none;}
</style>

</head>
<body>
<div class="formPage" style="width:500px;" ng-cloak>
  <div class="formbg">
        <div class="content_form">
              <div class="ts_message" >
                   <div style="height: 52px;background:url(../../images/call_right.png) no-repeat center;"></div>
                   <span style="display:block;margin-top:20px;font-size:16px;text-align:center;">已成功入库<span id="capitalNum">0</span>个资产！</span>
                   <span style="display:block;margin-top:20px;font-size:16px;text-align:center;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><a style="color:#4d4d4d; text-decoration:none;" href="#" id="capitalCode"></a></span>
              </div>
        </div>
   </div>   
</div>
</body>
</html>