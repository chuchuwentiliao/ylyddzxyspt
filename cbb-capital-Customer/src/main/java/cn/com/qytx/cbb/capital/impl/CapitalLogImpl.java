package cn.com.qytx.cbb.capital.impl;


import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.com.qytx.cbb.capital.dao.CapitalLogDao;
import cn.com.qytx.cbb.capital.domain.CapitalLog;
import cn.com.qytx.cbb.capital.service.ICapitalLog;
import cn.com.qytx.cbb.capital.vo.CapitalVo;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;
import cn.com.qytx.platform.base.service.impl.BaseServiceImpl;

/**
 * 功能:资产日志业务实现
 * 版本: 1.0
 * 开发人员: panbo
 * 创建日期: 2016年8月12日
 * 修改日期: 2016年8月12日
 * 修改列表: 
 */
@Service("capitalLogService")
@Transactional
public class CapitalLogImpl extends BaseServiceImpl<CapitalLog> implements ICapitalLog{

	@Resource(name="capitalLogDao")
	private CapitalLogDao capitalLogDao;

	@Override
	public Page<CapitalLog> findCapitalLogPage(Pageable pageable,
			CapitalVo capitalVo) {
		return capitalLogDao.findCapitalLogPage(pageable, capitalVo);
	}

	

	
	

}
