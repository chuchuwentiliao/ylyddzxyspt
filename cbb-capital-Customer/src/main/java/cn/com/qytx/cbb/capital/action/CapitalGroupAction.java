package cn.com.qytx.cbb.capital.action;

import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;

import cn.com.qytx.cbb.capital.domain.CapitalGroup;
import cn.com.qytx.cbb.capital.service.ICapitalGroup;
import cn.com.qytx.cbb.org.util.TreeNode;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.org.domain.UserInfo;

/**
 * 功能:资产组管理
 * 版本: 1.0
 * 开发人员: panbo
 * 创建日期: 2016年8月12日
 * 修改日期: 2016年8月12日
 * 修改列表: 
 */
public class CapitalGroupAction extends BaseActionSupport {
	private static final long serialVersionUID = 1L;
	/**
	 * 资产组业务接口
	 */
	@Resource(name = "capitalGroupService")
	private ICapitalGroup capitalGroupService;
	/**
	 * 1 代表左侧树 2 代表 下拉树
	 */
	private Integer treeType;

	private CapitalGroup capitalGroup;
	
	private String capitalGroupName;
	
	/**
	 * 获取资源组树列表
	 */
	public void capitalGroupTreeList() {
		try {
			UserInfo user = this.getLoginUser();
			if (user != null) {
				String contextPath = getRequest().getContextPath();
				List<TreeNode> list = null;
				if(treeType!=null&&treeType.intValue()==1){
					list = capitalGroupService
							.getTreeCapitalGroupList(contextPath,user.getCompanyId());
				}else{
					list = capitalGroupService
							.getTreeCapitalGroupList_Select(contextPath,user.getCompanyId());
				}
				Gson json = new Gson();
				ajax(json.toJson(list));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 获取资源组列表
	 */
	public void capitalGroupList() {
		try {
			UserInfo user = this.getLoginUser();
			if (user != null) {
				List<CapitalGroup> list = capitalGroupService.findCapitalGroupList(user.getCompanyId(), capitalGroupName);
				ajax(list);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 资产组信息修改或新增资产组 资产组信息修改 成功：1； 资产组添加成功：0；
	 */
	public String saveOrUpdateCapitalGroup() {
		try {
			UserInfo user = this.getLoginUser();
			if (user != null) {
				if (capitalGroup.getId() != null) {//修改
					CapitalGroup oldCapitalGroup = capitalGroupService.findOne(capitalGroup.getId());
					if(oldCapitalGroup!=null){
						String oldGroupName = oldCapitalGroup.getGroupName();
						String oldCapitalCode = oldCapitalGroup.getCapitalCode();
						//判断是否存在资产组名称
				    	if(!oldGroupName.equals(capitalGroup.getGroupName())){
				    		boolean  isSame=capitalGroupService.isHasSameGroupName(oldCapitalGroup.getParentId(), capitalGroup.getGroupName(),user.getCompanyId());
				        	if(isSame){
				                ajax("2");
				                return null;
				        	}
				    	}
				    	//判断是否存在资产组编码重复
				    	if(!oldCapitalCode.equals(capitalGroup.getCapitalCode())){
				    		boolean isExistCapitalCode = capitalGroupService.isHasSameCapitalCode(oldCapitalGroup.getParentId(), capitalGroup.getCapitalCode(), user.getCompanyId());
							if(isExistCapitalCode){
								ajax("3");
								return null;
							}
				    	}
				    	oldCapitalGroup.setCapitalCode(capitalGroup.getCapitalCode());
				    	oldCapitalGroup.setGroupName(capitalGroup.getGroupName());
				    	oldCapitalGroup.setOrderIndex(capitalGroup.getOrderIndex());
				    	oldCapitalGroup.setUpdateTime(new Timestamp(System.currentTimeMillis()));
				    	oldCapitalGroup.setUpdateUserId(user.getUserId());
						capitalGroupService.saveOrUpdate(oldCapitalGroup);
						ajax("1");
					}
				} else {
						Integer parentId = 0;
						if(capitalGroup.getParentId()!=null){
							parentId = capitalGroup.getParentId();
						}
						//判断是否存在资产组名称
						boolean isExistGroupName = capitalGroupService.isHasSameGroupName(parentId, capitalGroup.getGroupName(), user.getCompanyId());
						if(isExistGroupName){
							ajax("2");
							return null;
						}
						//判断是否存在资产组编码
						boolean isExistCapitalCode = capitalGroupService.isHasSameCapitalCode(parentId, capitalGroup.getCapitalCode(), user.getCompanyId());
						if(isExistCapitalCode){
							ajax("3");
							return null;
						}
						capitalGroup.setCompanyId(user.getCompanyId());
						capitalGroup.setCreateTime(new Timestamp(System.currentTimeMillis()));
						capitalGroup.setCreateUserId(user.getUserId());
						capitalGroup.setIsDelete(0);
						capitalGroup.setUpdateTime(new Timestamp(System.currentTimeMillis()));
						capitalGroup.setUpdateUserId(user.getUserId());
						capitalGroupService.saveOrUpdate(capitalGroup);
						CapitalGroup parentCapitalGroup = capitalGroupService.findOne(parentId);
						if(parentCapitalGroup!=null){
							//设置路径
			    			String pathIdParent = parentCapitalGroup.getPathId();
			    			if(StringUtils.isNotBlank(pathIdParent)){
			    				capitalGroup.setPathId(pathIdParent+capitalGroup.getId()+",");
			    			}
			            	//设置级别
			    			Integer gradeParent=parentCapitalGroup.getGrade();
			    			if(gradeParent!=null){
			    				capitalGroup.setGrade(gradeParent+1);
			    			}
						}else{
							capitalGroup.setPathId(","+String.valueOf(capitalGroup.getId())+",");
							capitalGroup.setGrade(1);
						}
						capitalGroupService.saveOrUpdate(capitalGroup);
						ajax("0_"+capitalGroup.getId());// 添加成功
					}
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
		return null;
	}
	
	
	/**
	 * 功能：删除资产组
	 * @return
	 */
	public String deleteCapitalGroup(){
		UserInfo userInfo = this.getLoginUser();
		try{
			if(userInfo!=null){
				boolean isHasChild = capitalGroupService.isHasChildCapitalGroup(capitalGroup.getId(),userInfo.getCompanyId());
				if(isHasChild){
					ajax("1");
					return null;
				}
				capitalGroupService.delete(capitalGroup.getId(), false);
				ajax("0");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public Integer getTreeType() {
		return treeType;
	}

	public void setTreeType(Integer treeType) {
		this.treeType = treeType;
	}

	public CapitalGroup getCapitalGroup() {
		return capitalGroup;
	}

	public void setCapitalGroup(CapitalGroup capitalGroup) {
		this.capitalGroup = capitalGroup;
	}
	public String getCapitalGroupName() {
		return capitalGroupName;
	}
	public void setCapitalGroupName(String capitalGroupName) {
		this.capitalGroupName = capitalGroupName;
	}

	
	
	
}
