package cn.com.qytx.cbb.capital.vo;

import java.sql.Timestamp;


/**
 * 功能:资产变更Vo类 
 * 版本: 1.0
 * 开发人员: wsg
 * 创建日期: 2016年8月12日
 * 修改日期: 2016年8月12日
 * 修改列表: 
 */
public class CapitalChangesVo {
	
	/**
	 * 操作类型
	 * 1领用； 2归还； 3调拨； 4维修； 5报废； 6盘点； 
	 */
	private Integer type;
	
	/**
	 * 资产ID
	 */
	private Integer id;
	
	/**
	 * 使用人
	 */
	private Integer useUserId;
	
	/**
	 * 领用数量
	 */
	private Integer capitalNum;
	
	/**
	 * 维修时间
	 */
	private Timestamp repairTime;
	/**
	 * 维修费用
	 */
	private Float repairMoney;
	/**
	 *维修单位
	 */
	private String repairCompany;
	/**
	 *维修结果
	 */
	private String repairResult;
	/**
	 *批准记录
	 */
	private String approveRecord;
	/**
	 *存放位置
	 */
	private Integer storeLocation;
	
	/**
	 * 盘点结果 1 正常 2 维修 3 报废 4 损耗（低值易耗）
	 */
	private Integer pandianResult;
	
	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUseUserId() {
		return useUserId;
	}

	public void setUseUserId(Integer useUserId) {
		this.useUserId = useUserId;
	}

	public Integer getCapitalNum() {
		return capitalNum;
	}

	public void setCapitalNum(Integer capitalNum) {
		this.capitalNum = capitalNum;
	}

	public Timestamp getRepairTime() {
		return repairTime;
	}

	public void setRepairTime(Timestamp repairTime) {
		this.repairTime = repairTime;
	}

	public Float getRepairMoney() {
		return repairMoney;
	}

	public void setRepairMoney(Float repairMoney) {
		this.repairMoney = repairMoney;
	}

	public String getRepairCompany() {
		return repairCompany;
	}

	public void setRepairCompany(String repairCompany) {
		this.repairCompany = repairCompany;
	}

	public String getRepairResult() {
		return repairResult;
	}

	public void setRepairResult(String repairResult) {
		this.repairResult = repairResult;
	}

	public String getApproveRecord() {
		return approveRecord;
	}

	public void setApproveRecord(String approveRecord) {
		this.approveRecord = approveRecord;
	}

	public Integer getStoreLocation() {
		return storeLocation;
	}

	public void setStoreLocation(Integer storeLocation) {
		this.storeLocation = storeLocation;
	}

	public Integer getPandianResult() {
		return pandianResult;
	}

	public void setPandianResult(Integer pandianResult) {
		this.pandianResult = pandianResult;
	}
	
	
}
