package cn.com.qytx.cbb.capital.dao;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import cn.com.qytx.cbb.capital.domain.CapitalUseLog;
import cn.com.qytx.cbb.capital.vo.CapitalVo;
import cn.com.qytx.platform.base.dao.BaseDao;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;

/**
 * 功能:资产使用日志持久层操作
 * 版本: 1.0
 * 开发人员: panbo
 * 创建日期: 2016年8月12日
 * 修改日期: 2016年8月12日
 * 修改列表: 
 */
@Repository("capitalUseLogDao")
public class CapitalUseLogDao extends BaseDao<CapitalUseLog, Integer> {

	public Page<CapitalUseLog> findCapitalUseLogPage(Pageable pageable, CapitalVo capitalVo) {
		StringBuffer sbHql = new StringBuffer();
		sbHql.append("select c from CapitalUseLog c where 1=1");
		if(capitalVo.getCompanyId()!=null){
			sbHql.append(" and c.companyId="+capitalVo.getCompanyId());
		}
		if(StringUtils.isNotBlank(capitalVo.getUseStartTime())){
			sbHql.append(" and c.useStartTime>='"+capitalVo.getUseStartTime()+"'");
		}
		if(StringUtils.isNotBlank(capitalVo.getUseEndTime())){
			sbHql.append(" and c.useStartTime<='"+capitalVo.getUseEndTime()+"'");
		}
		if(StringUtils.isNotBlank(capitalVo.getDisableStartTime())){
			sbHql.append(" and c.useEndTime>='"+capitalVo.getDisableStartTime()+"'");
		}
		if(StringUtils.isNotBlank(capitalVo.getDisableEndTime())){
			sbHql.append(" and c.useEndTime<='"+capitalVo.getDisableEndTime()+"'");
		}
		if(StringUtils.isNotBlank(capitalVo.getUserGroupName())){
			sbHql.append(" and c.useGroup.groupId in (select gi.groupId from GroupInfo gi where gi.groupName like '%"+capitalVo.getUserGroupName()+"%')");
		}
		if(StringUtils.isNotBlank(capitalVo.getKeyWord())||StringUtils.isNoneBlank(capitalVo.getCapitalStatus())||capitalVo.getCapitalGroup()!=null){
			sbHql.append(" and c.capital.id in (select ct.id from Capital ct where 1=1 ");
			if(StringUtils.isNotBlank(capitalVo.getKeyWord())){
				sbHql.append(" and (ct.name like '%"+capitalVo.getKeyWord()+"%' or ct.capitalNo like '%"+capitalVo.getKeyWord()+"%')");
			}
			if(StringUtils.isNoneBlank(capitalVo.getCapitalStatus())){
				sbHql.append(" and ct.status in ("+capitalVo.getCapitalStatus()+")");
			}
			if(capitalVo.getCapitalGroup()!=null){
				sbHql.append(" and (ct.capitalGroup.pathId like '%,"+capitalVo.getCapitalGroup()+",%')");
			}
			sbHql.append(")");
		}
		
		sbHql.append(" order by c.createTime desc");
		return super.findByPage(pageable, sbHql.toString());
	}


	public CapitalUseLog getUnEndUseCapitalUseLog(Integer capitalId){
		String hql = " useEndTime is null and capital.id="+capitalId;
		return super.findOne(hql);
		
	}
	
	/**
	 * 查找最新的使用记录
	 * @return
	 */
	public CapitalUseLog findUseCapitalUseLog(Integer capitalId){
		String hql =" capital.id="+capitalId+" order by createTime desc";
		List<CapitalUseLog> list= super.findAll(hql);
		CapitalUseLog capitalUseLog = null;
		if(list!=null && !list.isEmpty()){
			capitalUseLog= list.get(0);
		}
		return capitalUseLog;
	}
	
	
	
}
