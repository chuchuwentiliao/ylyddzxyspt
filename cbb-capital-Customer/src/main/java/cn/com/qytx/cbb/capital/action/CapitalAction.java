/**
 * 
 */
package cn.com.qytx.cbb.capital.action;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import cn.com.qytx.cbb.capital.domain.Capital;
import cn.com.qytx.cbb.capital.domain.CapitalGroup;
import cn.com.qytx.cbb.capital.domain.CapitalLog;
import cn.com.qytx.cbb.capital.domain.CapitalUseLog;
import cn.com.qytx.cbb.capital.event.EventForAddLog;
import cn.com.qytx.cbb.capital.service.ICapital;
import cn.com.qytx.cbb.capital.service.ICapitalGroup;
import cn.com.qytx.cbb.capital.service.ICapitalUseLog;
import cn.com.qytx.cbb.capital.vo.CapitalChangesVo;
import cn.com.qytx.cbb.capital.vo.CapitalVo;
import cn.com.qytx.cbb.dict.service.IDict;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;
import cn.com.qytx.platform.base.query.Sort;
import cn.com.qytx.platform.base.query.Sort.Direction;
import cn.com.qytx.platform.base.query.Sort.Order;
import cn.com.qytx.platform.org.domain.GroupInfo;
import cn.com.qytx.platform.org.domain.UserInfo;
import cn.com.qytx.platform.org.service.IGroup;
import cn.com.qytx.platform.org.service.IUser;
import cn.com.qytx.platform.utils.DateUtils;
import cn.com.qytx.platform.utils.spring.SpringUtil;

import com.google.gson.Gson;

/**
 * 功能:
 * 版本: 1.0
 * 开发人员: 王刚刚
 * 创建日期: 2016年8月10日
 * 修改日期: 2016年8月10日
 * 修改列表: 
 */
public class CapitalAction extends BaseActionSupport {

	/**
	 * 描述含义
	 */
	private static final long serialVersionUID = 7445416973484513385L;
	/**
	 * 资产接口
	 */
	@Autowired	
	ICapital icapital;
	/**
	 * 资产组接口
	 */
	@Autowired
	ICapitalGroup iCapitalGroup;
	/**
	 * 资产使用记录接口
	 */
	@Autowired
	ICapitalUseLog iCapitalUseLog;
	
	private CapitalVo capitalVo;
	private Capital capital;
	private Integer capitalId;
	/**
	 * num 5禁用  1 启用
	 */
	private Integer num;
	
	/**
	 * 库存地点
	 */
	private String describe;
	/**
	 * 开始使用时间
	 */
	private String startUseTime;
	/**
	 * 停止使用时间
	 */
	private String endUseTime;
  /**
   * 
   * 功能：获得资产列表
   */
  public void getCapitalList(){
	  UserInfo user=getLoginUser();
	  try {
		  if(user!=null){
			  capitalVo.setCompanyId(user.getCompanyId());
			  if(capitalVo.getType()!=null){
				  if(capitalVo.getType()==1){//查看本部门
					  //capitalVo.setCurrentGroupId(user.getGroupId());
					 if(StringUtils.isEmpty(capitalVo.getUserGroupName())){
						 GroupInfo groupInfo = (GroupInfo)getSession().getAttribute("groupInfo");
						 capitalVo.setUserGroupName(groupInfo!=null?groupInfo.getGroupName():"");
					 }
				  }
			  }
			  Order order = new Order(Direction.DESC, "createTime");
	          Sort s = new Sort(order);
	          Pageable pageable = getPageable(s);
	          Page<Capital> pageInfo=icapital.findCapitalPage(pageable, capitalVo);
	          List<Capital> list=pageInfo.getContent();
	          List<Map<String,Object>> capitalList=analyzeResult(list, pageable);
	          ajaxPage(pageInfo, capitalList);
		  }
		  
		} catch (Exception e) {
			e.printStackTrace();
		}	  
  }
  
  /**
	 * 
	 * 功能：查询的列表转换成MAp
	 * @param list
	 * @param pageable
	 * @param companyId
	 * @return
	 */
	private List<Map<String,Object>> analyzeResult(List<Capital> list,Pageable pageable){
		List<Map<String,Object>> mapList=new ArrayList<Map<String,Object>>();  
		if(list!=null&&list.size()>0){
			    int i = 1;
				if(pageable!=null){
	    			i = pageable.getPageNumber() * pageable.getPageSize() + 1;
	    		}
				for(Capital capital:list){
					Map<String,Object> map=new HashMap<String, Object>();
					map.put("no", i);
					map.put("id",capital.getId());//ID
					map.put("goodName",capital.getName());//物品名称
					map.put("capitalGroup", capital.getCapitalGroup().getGroupName());//资产组
					map.put("capitalNo", capital.getCapitalNo());//资产编号
					map.put("capitalModel", StringUtils.isNotBlank(capital.getCapitalModel())?capital.getCapitalModel():"-");//资产型号
					map.put("status", capital.getStatus());//资产状态
					map.put("statusStr", capital.getStatus()==null?"-":getStateStr(capital.getStatus()));//资产状态
					map.put("location",capital.getStoreLocation()==null?"-":capital.getStoreLocation());//存放地址
					String useGroupName = "-";
					Integer status = capital.getStatus();
					if(status!=null&&(status==2||status==3||status==6)){
						GroupInfo useGroup = capital.getUseGroup();
						useGroupName = useGroup!=null?useGroup.getGroupName():"-";
					}
					map.put("useGroupName", useGroupName);
					mapList.add(map);
					i++;
				}
		   }
		   return  mapList;
	}
	
/**
 * 获得资产状态	
 */
public String getStateStr(Integer state){
	String str="";
	if(state==1){
		str= "在库";
	}else if(state==2){
		str= "使用中";
	}else if(state==3){
		str= "已停用";
	}else if(state==4){
		str= "待归库";
	}else if(state==5){
		str= "已禁用";
	}else if(state==6){
		str= "已送达";
	}
	return str;
}	
	
	/**
	 * 
	 * 功能：添加资产入库或修改资产信息
	 */
	public void addCapital(){
		UserInfo user=getLoginUser();
		try {
			if(user!=null){
				if(capital.getId()!=null){
					//修改
					Capital ct=icapital.findOne(capital.getId());
					if(capital!=null){				
						ct.setName(capital.getName());
						ct.setCapitalGroup(capital.getCapitalGroup());
						ct.setCapitalBrand(capital.getCapitalBrand());
						ct.setCapitalModel(capital.getCapitalModel());
						ct.setCapitalNo(capital.getCapitalNo());
						ct.setSupplier(capital.getSupplier());
						ct.setStoreLocation(capital.getStoreLocation());
						ct.setRemark(capital.getRemark());	
						ct.setUpdateTime(new Timestamp(System.currentTimeMillis()));
						ct.setUpdateUserId(user.getUserId());
						icapital.saveOrUpdate(ct);
						/*添加日志开始*/
						CapitalLog log = new CapitalLog();
						log.setCapitalGroup(capital.getCapitalGroup());//日志
						log.setCapitalName(capital.getName());
						log.setCapitalNo(ct.getCapitalNo());
						log.setCompanyId(user.getCompanyId());
						log.setCreateTime(new Timestamp(System.currentTimeMillis()));
						log.setCreateUser(user);
						log.setIpAddress(getRequest().getRemoteAddr());
						log.setType(1);
						log.setRemark("修改成功");
						SpringUtil.getApplicationContext().publishEvent(new EventForAddLog(log));//发布添加日志广播
						/*添加日志结束*/
						ajax(1);
					}
				}else{
					//添加
					int count=icapital.getCurrCodeMaxCapitalNum(capital.getCapitalNo(), user.getCompanyId());
					if(count>0){
						ajax(3);
						return;
					}
					capital.setIsDelete(0);
					capital.setCompanyId(user.getCompanyId());
					capital.setStatus(1);
					capital.setCreateTime(new Timestamp(System.currentTimeMillis()));
					capital.setCreateUserId(user.getUserId());
					icapital.saveOrUpdate(capital);
					/*添加日志开始*/
					CapitalLog log = new CapitalLog();
					log.setCapitalName(capital.getName());
					log.setCapitalNo(capital.getCapitalNo());
					log.setCompanyId(user.getCompanyId());
					log.setCreateTime(new Timestamp(System.currentTimeMillis()));
					log.setIpAddress(capital.getStoreLocation());
					log.setCreateUser(user);
					log.setIpAddress(getRequest().getRemoteAddr());
					log.setType(2);
					log.setRemark("新增成功");
					SpringUtil.getApplicationContext().publishEvent(new EventForAddLog(log));//发布添加日志广播
					/*添加日志结束*/
					
				    ajax(2);
			 }
			}	
		} catch (Exception e) {
			e.printStackTrace();
			ajax(0);//返回失败
		}
	}

	/**
	 * 获取资产详情
	 */
	public void getCapitalDetail(){
		try{
			UserInfo user =this.getLoginUser();
			if(user!=null){
				Capital c=icapital.findOne(capitalId);
				Map<String,Object> map=new HashMap<String,Object>();
				//资产名称
				map.put("name", c.getName());
				map.put("id", c.getId());
				//资产组
				CapitalGroup currCg = iCapitalGroup.findOne(c.getCapitalGroup().getId());
				List<CapitalGroup> cgList = iCapitalGroup.getCapitalGroupListByIds(currCg.getPathId());
				int cgSize = cgList.size();
				String capitalGroup="设备组>"+"";
				for(int j=0;j<cgSize;j++){
					CapitalGroup tempCg = cgList.get(j);
					if(j==(cgSize-1)){
					capitalGroup += tempCg.getGroupName();
					}else{
						capitalGroup += tempCg.getGroupName()+">";
					}
				}
				map.put("capitalGroupName",currCg.getGroupName() );
				map.put("capitalGroup",capitalGroup);
				//资产组ID
				map.put("capitalGroupId", c.getCapitalGroup().getId());
				//资产编号
				map.put("capitalNo", c.getCapitalNo());
				map.put("statu",c.getStatus());
				map.put("statusStr",c.getStatus()!=null?getStateStr(c.getStatus()):"-");
				//资产型号
				map.put("capitalModel", StringUtils.isNotBlank(c.getCapitalModel())?c.getCapitalModel():"-");
				map.put("capitalBrand", StringUtils.isNotBlank(c.getCapitalBrand())?c.getCapitalBrand():"-");
				map.put("storeLocation", StringUtils.isNotBlank(c.getStoreLocation())?c.getStoreLocation():"-");
				//供应商
				map.put("supplier", StringUtils.isNotBlank(c.getSupplier())?c.getSupplier():"-");
				
				//备注
				map.put("remark", StringUtils.isNotBlank(c.getRemark())?c.getRemark():"-");
				
				map.put("currUser", user.getUserName());
				GroupInfo useGroup = c.getUseGroup();
				map.put("useGroupName", useGroup!=null?useGroup.getGroupName():"-");
				map.put("useGroupId", useGroup!=null?useGroup.getGroupId():"");
				
				if(c.getStatus()==2){
					CapitalUseLog cul = iCapitalUseLog.getUnEndUseCapitalUseLog(capitalId);
					SimpleDateFormat sdfYYYYMMDDHHMMSS = new SimpleDateFormat("yyyy-MM-dd HH:mm");
					map.put("startUseTime", (cul!=null&&cul.getUseStartTime()!=null)?sdfYYYYMMDDHHMMSS.format(cul.getUseStartTime()):"");
				}
				Gson json=new Gson();
				ajax(json.toJson(map));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * 获得资产详情（修改）
	 */
	public void getDetailCapital(){
		UserInfo user=getLoginUser();
		if(user!=null){
			Capital cpt=icapital.findOne(capitalId);
			Map<String,Object> map=new HashMap<String,Object>();
			if(cpt!=null){
				
				map.put("capitalName", cpt.getName());//名称
				map.put("capitalGroupId",cpt.getCapitalGroup().getId());
				map.put("capitalGroup",cpt.getCapitalGroup().getGroupName());
				map.put("capitalBlank",cpt.getCapitalBrand()==null?"":cpt.getCapitalBrand());//品牌
				map.put("capitalModel",cpt.getCapitalModel()==null?"":cpt.getCapitalModel());//资产型号
				map.put("capitalType",cpt.getCapitalType());//类型
				map.put("capitalTypeName",cpt.getCapitalType()==1?"固定资产":cpt.getCapitalType()==2?"办公用品":cpt.getCapitalType()==3?"车辆":"图书");//类型
				map.put("unitId",cpt.getCapitalUnit());//计量单位
				map.put("capitalPrice",cpt.getPrice()==null?0:cpt.getPrice());
				map.put("capitalNo", cpt.getCapitalNo());
				map.put("approveNo",cpt.getApproveNo()==null?"": cpt.getApproveNo());
				map.put("capitalStoreId",cpt.getStoreLocation()==null?"":cpt.getStoreLocation());
				map.put("supplier",cpt.getSupplier()==null?"": cpt.getSupplier());
				map.put("capitalNum",cpt.getCapitalNum());
				map.put("oldNum",cpt.getOldNum());
				UserInfo handUser = cpt.getHandleUser();
				String handUserId = "";
				String handUserName = "";
				if(handUser!=null){
					handUserId = handUser.getUserId()+"";
					handUserName = handUser.getUserName();
				}
				map.put("handUserId",handUserId);
				map.put("handUserName",handUserName);
				SimpleDateFormat sdfYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
				map.put("buyDate",cpt.getBuyDate()!=null?sdfYYYYMMDD.format(cpt.getBuyDate()):"");
				
				map.put("invoiceNumber",cpt.getInvoiceNumber()==null?"":cpt.getInvoiceNumber());
				map.put("remark",cpt.getRemark()==null?"":cpt.getRemark() );
			}
			ajax(map);
		}
	}
	
	
	
	/**
	 * 
	 * 功能：删除资产信息
	 * @return
	 */
	public void deleteCapital(){
		UserInfo user=getLoginUser();
		int num=0;
		try {
			if(user!=null){
				Capital oldCapital = icapital.findOne(capitalId);
				icapital.delete(capitalId, false);				
				/*添加日志开始*/
				CapitalLog log = new CapitalLog();
				log.setCapitalGroup(oldCapital.getCapitalGroup());//日志
				log.setCapitalType(oldCapital.getCapitalType());
				log.setCapitalName(oldCapital.getName());
				log.setCapitalNo(oldCapital.getCapitalNo());
				log.setCompanyId(user.getCompanyId());
				log.setCreateTime(new Timestamp(System.currentTimeMillis()));
				log.setCreateUser(user);
				log.setIpAddress(getRequest().getRemoteAddr());
				log.setType(1);
				log.setRemark("删除成功");
				SpringUtil.getApplicationContext().publishEvent(new EventForAddLog(log));//发布添加日志广播
				/*添加日志结束*/
			}
		} catch (Exception e) {
			// TODO: handle exception
			num=1;
		}
		ajax(num);
	}
	
	/**
	 * 改变启用、禁用状态
	 */
	public void chageStatu(){
		UserInfo user= getLoginUser();
		try {
			if(user!=null){
				Capital ct = icapital.findOne(capitalId);
				if(ct!=null){
					ct.setStatus(num);
					ct.setUpdateTime(new Timestamp(System.currentTimeMillis()));
					ct.setUpdateUserId(user.getUserId());
					ct.setUseGroup(null);
					icapital.saveOrUpdate(ct);
					/*添加日志开始*/
					CapitalLog log = new CapitalLog();
					log.setCapitalGroup(ct.getCapitalGroup());//日志
					log.setCapitalName(ct.getName());
					log.setCapitalNo(ct.getCapitalNo());
					log.setCompanyId(user.getCompanyId());
					log.setCreateTime(new Timestamp(System.currentTimeMillis()));
					log.setCreateUser(user);
					log.setIpAddress(getRequest().getRemoteAddr());
					if(num==5){
						log.setType(4);
						log.setRemark("禁用成功");
					}if(num==3){
						log.setType(7);
						log.setRemark("停用成功");
					}else{
						log.setType(3);
						log.setRemark("启用成功");
					}
					
					SpringUtil.getApplicationContext().publishEvent(new EventForAddLog(log));//发布添加日志广播
					/*添加日志结束*/
					ajax(1);
				}
			}
		} catch (Exception e) {
			ajax(0);
			e.getStackTrace();
		}
	}
	
	/**
	 * 设备借用
	 */
	public void capitalBorrow(){
		UserInfo user= getLoginUser();
		try {
			if(user!=null){
				String ipAddress = getRequest().getRemoteAddr();
				icapital.capitalUse(ipAddress,user,capital,2,startUseTime,null);
				/*添加日志结束*/
				ajax(1);
			}
		} catch (Exception e) {
			ajax(0);
			e.getStackTrace();
		}
	}
	
	
	/**
	 * 设备停用
	 */
	public void capitalDisable(){
		UserInfo user= getLoginUser();
		try {
			if(user!=null){
				if(user!=null){
					String ipAddress = getRequest().getRemoteAddr();
					icapital.capitalUse(ipAddress,user,capital,3,null,endUseTime);
					/*添加日志结束*/
					ajax(1);
				}
			}
		} catch (Exception e) {
			ajax(0);
			e.getStackTrace();
		}
	}
	
	/**
	 * 设备入库
	 */
	public  void inStore(){
		UserInfo user = getLoginUser();
		try {
			if(user!=null){
				Capital c = icapital.findOne(capitalId);
				if(c!=null){
					c.setStatus(1);
					c.setStoreLocation(describe);
					c.setUpdateTime( new Timestamp(System.currentTimeMillis()));
					c.setUpdateUserId(user.getUserId());
					c.setUseGroup(null);
					icapital.saveOrUpdate(c);
					/*添加日志开始*/
					CapitalLog log = new CapitalLog();
					log.setCapitalGroup(c.getCapitalGroup());//日志
					log.setCapitalName(c.getName());
					log.setCapitalNo(c.getCapitalNo());
					log.setCompanyId(user.getCompanyId());
					log.setCreateTime(new Timestamp(System.currentTimeMillis()));
					log.setCreateUser(user);
					log.setIpAddress(getRequest().getRemoteAddr());
					log.setType(5);
					log.setRemark("入库成功！");
					SpringUtil.getApplicationContext().publishEvent(new EventForAddLog(log));//发布添加日志广播
					ajax(1);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			ajax(0);
		}
	}
	
	
	/**
	 * 设备直接归还
	 */
	public  void returnCapital(){
		UserInfo user = getLoginUser();
		try {
			if(user!=null){
				Capital c = icapital.findOne(capitalId);
				if(c!=null){
					c.setUpdateTime( new Timestamp(System.currentTimeMillis()));
					c.setUpdateUserId(user.getUserId());
					c.setUseGroup(null);
					c.setUseGroup(null);
					c.setStoreLocation("5号楼1楼");
					c.setStatus(1);
					icapital.saveOrUpdate(c);
					/* 归还 直接停止使用 */
					CapitalUseLog cul = iCapitalUseLog.getUnEndUseCapitalUseLog(capitalId);
					Timestamp currTs = new Timestamp(System.currentTimeMillis());
					if(cul!=null){//先停用 然后 重新添加使用中
						cul.setUseEndTime(currTs);
						Long useLongTime=(cul.getUseEndTime().getTime()-cul.getUseStartTime().getTime())/1000;
						cul.setUseLongTime(useLongTime.intValue());
						cul.setStopUser(user);
						iCapitalUseLog.saveOrUpdate(cul);
						
						CapitalLog log = new CapitalLog();
						log.setCapitalGroup(c.getCapitalGroup());//日志
						log.setCapitalName(c.getName());
						log.setCapitalNo(c.getCapitalNo());
						log.setCompanyId(user.getCompanyId());
						log.setCreateTime(new Timestamp(System.currentTimeMillis()));
						log.setCreateUser(user);
						log.setType(7);
						log.setRemark("管理员直接归还,停止使用");
						SpringUtil.getApplicationContext().publishEvent(new EventForAddLog(log));//发布添加日志广播
					}
					/*添加资产日志开始*/
					CapitalLog log = new CapitalLog();
					log.setCapitalGroup(c.getCapitalGroup());//日志
					log.setCapitalName(c.getName());
					log.setCapitalNo(c.getCapitalNo());
					log.setCompanyId(user.getCompanyId());
					log.setCreateTime(new Timestamp(System.currentTimeMillis()));
					log.setCreateUser(user);
					log.setType(5);
					log.setRemark("管理员直接归还,已入库");
					SpringUtil.getApplicationContext().publishEvent(new EventForAddLog(log));//发布添加日志广播
					
					ajax(1);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			ajax(0);
		}
	}
	
	/**
	 * 设备送达
	 */
	public  void capitalService(){
		UserInfo user = getLoginUser();
		try {
			if(user!=null){
				Capital c = icapital.findOne(capital.getId());
				if(c!=null){
					c.setStatus(6);
					c.setUpdateTime( new Timestamp(System.currentTimeMillis()));
					c.setUpdateUserId(user.getUserId());
					c.setStoreLocation(capital.getStoreLocation());
					if(capital.getUseGroup()!=null&&capital.getUseGroup().getGroupId()!=null){
						c.setUseGroup(capital.getUseGroup());
					}
					icapital.saveOrUpdate(c);
					/*添加日志开始*/
					CapitalLog log = new CapitalLog();
					log.setCapitalGroup(c.getCapitalGroup());//日志
					log.setCapitalName(c.getName());
					log.setCapitalNo(c.getCapitalNo());
					log.setCompanyId(user.getCompanyId());
					log.setCreateTime(new Timestamp(System.currentTimeMillis()));
					log.setCreateUser(user);
					log.setIpAddress(getRequest().getRemoteAddr());
					log.setType(8);
					log.setRemark("送达成功！");
					SpringUtil.getApplicationContext().publishEvent(new EventForAddLog(log));//发布添加日志广播
					ajax(1);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			ajax(0);
		}
	}
	
	
	
	public CapitalVo getCapitalVo() {
		return capitalVo;
	}

	public void setCapitalVo(CapitalVo capitalVo) {
		this.capitalVo = capitalVo;
	}

	public Integer getCapitalId() {
		return capitalId;
	}

	public void setCapitalId(Integer capitalId) {
		this.capitalId = capitalId;
	}

	public Capital getCapital() {
		return capital;
	}

	public void setCapital(Capital capital) {
		this.capital = capital;
	}


	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}


	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe;
	}

	public String getStartUseTime() {
		return startUseTime;
	}

	public String getEndUseTime() {
		return endUseTime;
	}

	public void setStartUseTime(String startUseTime) {
		this.startUseTime = startUseTime;
	}

	public void setEndUseTime(String endUseTime) {
		this.endUseTime = endUseTime;
	}
	
	
  
}
