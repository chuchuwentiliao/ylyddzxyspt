/**
 * 
 */
package cn.com.qytx.cbb.capital.domain;

import cn.com.qytx.cbb.capital.util.excel.ExcelAnnotation;


/**
 * 功能: 
 * 版本: 1.0
 * 开发人员: 王刚
 * 创建日期: 2017年4月28日
 * 修改日期: 2017年4月28日
 * 修改列表: 
 */
public class CapitalImportVo {
     
	@ExcelAnnotation(exportName="设备名称")
	private String name;
	
	@ExcelAnnotation(exportName="设备编号")
	private String capitalNo;
	
	@ExcelAnnotation(exportName="当前位置")
	private String storeLocation;
	
	@ExcelAnnotation(exportName="设备组")
	private String capitalGroup;
	
	@ExcelAnnotation(exportName="设备型号")
	private String capitalModel;
	
	@ExcelAnnotation(exportName="品牌信息")
	private String capitalBrand;
	
	@ExcelAnnotation(exportName="厂商信息")
	private String supplier;
	
	private String importErrorRecord;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCapitalNo() {
		return capitalNo;
	}

	public void setCapitalNo(String capitalNo) {
		this.capitalNo = capitalNo;
	}

	public String getStoreLocation() {
		return storeLocation;
	}

	public void setStoreLocation(String storeLocation) {
		this.storeLocation = storeLocation;
	}

	public String getCapitalGroup() {
		return capitalGroup;
	}

	public void setCapitalGroup(String capitalGroup) {
		this.capitalGroup = capitalGroup;
	}

	public String getCapitalModel() {
		return capitalModel;
	}

	public void setCapitalModel(String capitalModel) {
		this.capitalModel = capitalModel;
	}

	public String getCapitalBrand() {
		return capitalBrand;
	}

	public void setCapitalBrand(String capitalBrand) {
		this.capitalBrand = capitalBrand;
	}

	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public String getImportErrorRecord() {
		return importErrorRecord;
	}

	public void setImportErrorRecord(String importErrorRecord) {
		this.importErrorRecord = importErrorRecord;
	}
	
	
	
}
