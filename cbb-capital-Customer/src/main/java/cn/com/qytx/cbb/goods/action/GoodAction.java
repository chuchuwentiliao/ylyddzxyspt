package cn.com.qytx.cbb.goods.action;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import cn.com.qytx.cbb.dict.domain.Dict;
import cn.com.qytx.cbb.dict.service.IDict;
import cn.com.qytx.cbb.goods.domain.Good;
import cn.com.qytx.cbb.goods.domain.GoodGroup;
import cn.com.qytx.cbb.goods.service.IGood;
import cn.com.qytx.cbb.goods.service.IGoodGroup;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;
import cn.com.qytx.platform.base.query.Sort;
import cn.com.qytx.platform.base.query.Sort.Direction;
import cn.com.qytx.platform.base.query.Sort.Order;
import cn.com.qytx.platform.org.domain.UserInfo;

public class GoodAction extends BaseActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = -752755628020485925L;
	
	@Autowired
	private IGood goodService;
	
	@Autowired
	private IGoodGroup goodGroupService;
	
	@Autowired
	private IDict dictService;
	
	private Good good;
	
	private Integer type;
	
	private String goodNo;
	
	private String name;
	
	private Integer id;
	
	private String ids;
	
	
	private Integer groupId;
	/*
	 * 查询物资列表
	 */
	public  void findGoodListPage(){
		UserInfo user=getLoginUser();
		try {
			if(user!=null){
				 Order order = new Order(Direction.DESC, "createTime");
		         Sort s = new Sort(order);
		         Pageable pageable = getPageable(s);
				 Page<Good> pageList = goodService.findGoodPage(pageable, name, goodNo, type, user.getCompanyId(),groupId);
			     List<Good> list = pageList.getContent();
			     List<Map<String,Object>> goodList=analyzeResult(list, pageable);
		         ajaxPage(pageList, goodList);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}

	 /**
	 * 
	 * 功能：查询的列表转换成MAp
	 * @param list
	 * @param pageable
	 * @param companyId
	 * @return
	 */
	private List<Map<String,Object>> analyzeResult(List<Good> list,Pageable pageable){
		List<Map<String,Object>> mapList=new ArrayList<Map<String,Object>>();  
		if(list!=null&&list.size()>0){
			    int i = 1;
				if(pageable!=null){
	    			i = pageable.getPageNumber() * pageable.getPageSize() + 1;
	    		}
				for(Good good:list){
					Map<String,Object> map=new HashMap<String, Object>();
					map.put("no", i);
					map.put("id",good.getId());//ID
					map.put("name",good.getName());//物品名称
					map.put("goodGroup", good.getGoodGroup().getGroupName());//资产组
					map.put("goodlNo", good.getGoodNo());//资产编号
					map.put("type", getGoodType(good.getType()));//资产类型
					map.put("specifications",good.getSpecifications()==null?"--":good.getSpecifications() );
					map.put("unit",good.getUnit()==null?"--":findUnitName(good.getUnit()));
					map.put("price",good.getPrice());
					mapList.add(map);
					i++;
				}
		   }
		   return mapList;
	}
	/*
	 * 获得物资类型	
	 */
	private  String getGoodType(Integer type){
		String typeStr="";
		if(type==null){
			typeStr=" ";
		}else if(type==1){
			typeStr="备件";
		}else if(type==2){
			typeStr="材料";
		}else if(type==3){
			typeStr="设备";
		}else if(type==4){
			typeStr="仪表";
		}else {
			typeStr="其他";
		}
		return typeStr;
	}
	/**
	 * 修改或新增操作	
	 */
	public void addOrUpdate(){
		UserInfo user=getLoginUser();
		if(user!=null){
			try {
				if(good.getId()!=null){//修改
					Good oldGood = goodService.findOne(good.getId());
					if(oldGood!=null){
						oldGood.setName(good.getName());
						oldGood.setGoodGroup(good.getGoodGroup());
						oldGood.setPrice(good.getPrice());
						oldGood.setUnit(good.getUnit());
						oldGood.setSpecifications(good.getSpecifications());
						oldGood.setType(good.getType());
						oldGood.setUpdateTime(new Timestamp(System.currentTimeMillis()));
						oldGood.setUpdateUserId(user.getUserId());
						goodService.saveOrUpdate(oldGood);
						ajax(1);
					}
				}else{  
					    GoodGroup goodGroup=goodGroupService.findOne(good.getGoodGroup().getId());
					    Order order = new Order(Direction.DESC, "createTime");
				        Sort s = new Sort(order);
				        Pageable pageable = getPageable(s);
					    Page<Good> pageList = goodService.findGoodPage(pageable, null, null, null, user.getCompanyId(),goodGroup.getId());
					    List<Good> list= pageList.getContent();
					    String no="0000";
					    Good lastGood = null;
					    if(list!=null && list.size()>0){
					    	lastGood= list.get(0);
					    }
					    int num=0;
					    if(lastGood!=null){
					    	num =lastGood.getNum();
					    }
					    num+=1;
					    if(num>=1 && num<10){
					    	no="0000"+num;
					    }else if(num>=10 && num<100){
					    	no="000"+num;
					    }else if(num>=100 && num<1000){
					    	no="00"+num;
					    }else if(num>=1000&& num<10000){
					    	no="0"+num;
					    }else if(num>=10000){
					    	no=""+num;
					    }
					    good.setGoodNo("WZ"+no);//生成编码
						good.setIsDelete(0);
						good.setCompanyId(user.getCompanyId());
						good.setCreateTime(new Timestamp(System.currentTimeMillis()));
						good.setCreateUserId(user.getUserId());
						good.setUpdateUserId(user.getUserId());
						good.setUpdateTime(new Timestamp(System.currentTimeMillis()));
						good.setNum(num);
						goodService.saveOrUpdate(good);
						ajax(2);
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				ajax(0);
			}
		}
	}
	/**
	 * 跳转到修改页面
	 */
	public String toUpdateView(){
		good = goodService.findOne(id);
		return SUCCESS;
	}
	
	/**
	 * 获得详情
	 * 
	 */
	public void findDetail(){
		UserInfo user = getLoginUser();
		if(user!=null){
			Good good=goodService.findOne(id);
			Map <String,Object> map = new HashMap<String, Object>();
			if(good!=null){
				map.put("name",good.getName());
				map.put("goodNo",good.getGoodNo());
				map.put("goodGroupName",good.getGoodGroup().getGroupName());
				map.put("goodGroupId",good.getGoodGroup().getId());
				map.put("type",getGoodType(good.getType()));
				map.put("typeId",good.getType());
				map.put("specifications",good.getSpecifications()!=null?good.getSpecifications():"--");
				map.put("unit",good.getUnit()!=null?findUnitName(good.getUnit()):"请选择");
				map.put("unitId",good.getUnit()==null?0:good.getUnit());
				map.put("price",good.getPrice());
				ajax(map);
			}
		}
	}
	
	private String findUnitName(Integer unitId){
		List<Dict> list= dictService.findList("goodsType", 1);
		String name="";
		if(list!=null && list.size()>0){
			for(Dict d:list){
				if(unitId==d.getValue()){
					name=d.getName();
				}
			}
		}
		return name;
	}
	
	
	
	/**
	 * 初始化单位
	 * @return
	 */
	public void initUnit(){
		UserInfo user=getLoginUser();
		try {
			if(user!=null){
				List<Dict> list= dictService.findList("goodsType", 1);
				List<Map<String,Object>> mapList=new ArrayList<Map<String,Object>>();
				Map<String,Object> mapInIt=new HashMap<String, Object>();
				mapInIt.put("name","请选择");
				mapInIt.put("value", 0);
				mapList.add(mapInIt);
				if(list!=null && list.size()>0){
					for(Dict d:list){
						Map<String,Object> map =new HashMap<String, Object>();
						map.put("name",d.getName());
						map.put("value",d.getValue());
						mapList.add(map);
					}
				}
				ajax(mapList);
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	
	/**
	 * 删除操作
	 */
	public void deleteGoods(){
		UserInfo user= getLoginUser();
		try {
			if(user!=null){
				goodService.deleteGoodByIds(user.getUserId(), ids);
				ajax(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			ajax(0);
		}
		
	}
	/**
	 * 通过Type name groupid 查找
	 */
	public void findGoodBy(){
		UserInfo user=getLoginUser();
		if(user!=null){
			try {
				Good good=goodService.findModle(type, name, groupId);
				if(good!=null){
					ajax(1);//存在
				}else{
					ajax(0);//不存在
				}
			} catch (Exception e) {
				e.printStackTrace();
				ajax(2);
			}
		}
	}
	public IGood getGoodService() {
		return goodService;
	}
	public void setGoodService(IGood goodService) {
		this.goodService = goodService;
	}
	public Good getGood() {
		return good;
	}
	public void setGood(Good good) {
		this.good = good;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getGoodNo() {
		return goodNo;
	}
	public void setGoodNo(String goodNo) {
		this.goodNo = goodNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}
  
	
}
