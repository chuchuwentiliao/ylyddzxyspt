/**
 * 
 */
package cn.com.qytx.cbb.goods.domain;

import cn.com.qytx.cbb.capital.util.excel.ExcelAnnotation;


/**
 * 功能: 
 * 版本: 1.0
 * 开发人员: 王刚
 * 创建日期: 2017年4月28日
 * 修改日期: 2017年4月28日
 * 修改列表: 
 */
public class GoodImportVo {
     
	@ExcelAnnotation(exportName="物资名称")
	private String name;
	
	@ExcelAnnotation(exportName="物资组")
	private String goodGroup;
	
	@ExcelAnnotation(exportName="类型")
	private String type;
	
	@ExcelAnnotation(exportName="规格")
	private String specifications;
	
	@ExcelAnnotation(exportName="单位")
	private String unit;
	
	@ExcelAnnotation(exportName="单价")
	private String price;
	
	private String importErrorRecord;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGoodGroup() {
		return goodGroup;
	}

	public void setGoodGroup(String goodGroup) {
		this.goodGroup = goodGroup;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSpecifications() {
		return specifications;
	}

	public void setSpecifications(String specifications) {
		this.specifications = specifications;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getImportErrorRecord() {
		return importErrorRecord;
	}

	public void setImportErrorRecord(String importErrorRecord) {
		this.importErrorRecord = importErrorRecord;
	}

	
	
	
}
