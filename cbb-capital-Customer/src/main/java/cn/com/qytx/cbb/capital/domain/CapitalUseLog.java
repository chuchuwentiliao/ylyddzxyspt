package cn.com.qytx.cbb.capital.domain;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import cn.com.qytx.platform.base.domain.DeleteState;
import cn.com.qytx.platform.org.domain.GroupInfo;
import cn.com.qytx.platform.org.domain.UserInfo;

/**
 * 功能:资产使用记录
 * 版本: 1.0
 * 开发人员: panbo
 * 创建日期: 2016年8月9日
 * 修改日期: 2016年8月9日
 * 修改列表: 
 */
@Entity
@Table(name="tb_cbb_capital_use_log")
public class CapitalUseLog implements java.io.Serializable{
	private static final long serialVersionUID = -3806018446633238202L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	
	
	/**
	 * 资产Id
	 */
	@JoinColumn(name="capital_id")
	@ManyToOne(cascade=CascadeType.REFRESH,fetch=FetchType.LAZY)
	private Capital capital;
	
	
	/**
	 * 创建时间
	 */
	@Column(name="create_time")
	private Timestamp createTime;
	
	
	/**
	 * 创建人
	 */
	@JoinColumn(name="create_user_id")
	@ManyToOne(cascade=CascadeType.REFRESH,fetch=FetchType.LAZY)
	private UserInfo createUser;
	
	
	
	@JoinColumn(name="use_group_id")
	@ManyToOne(cascade=CascadeType.REFRESH,fetch=FetchType.LAZY)
	private GroupInfo useGroup;
	
	//停用人
	@JoinColumn(name="stop_user_id")
	@ManyToOne(cascade=CascadeType.REFRESH,fetch=FetchType.LAZY)
	private UserInfo stopUser;
	
	/**
	 * 单位Id
	 */
	@Column(name="company_id")
	private Integer companyId;
	
	
	/**
	 * 使用开始时间
	 */
	@Column(name="user_start_time")
	private Timestamp useStartTime;
	
	
	/**
	 * 使用结束时间
	 */
	@Column(name="user_end_time")
	private Timestamp useEndTime;
	
	/**
	 * 1 已删除 0 未删除
	 */
	@Column(name="is_delete")
	@DeleteState
	private Integer isDelete;
	
	/**
	 * 使用时长
	 */
	@Column(name="use_long_time")
	private Integer useLongTime;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Capital getCapital() {
		return capital;
	}

	

	public GroupInfo getUseGroup() {
		return useGroup;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public Timestamp getUseStartTime() {
		return useStartTime;
	}

	public Timestamp getUseEndTime() {
		return useEndTime;
	}

	public Integer getIsDelete() {
		return isDelete;
	}

	public void setCapital(Capital capital) {
		this.capital = capital;
	}

	
	public UserInfo getCreateUser() {
		return createUser;
	}

	public void setCreateUser(UserInfo createUser) {
		this.createUser = createUser;
	}

	public void setUseGroup(GroupInfo useGroup) {
		this.useGroup = useGroup;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public void setUseStartTime(Timestamp useStartTime) {
		this.useStartTime = useStartTime;
	}

	public void setUseEndTime(Timestamp useEndTime) {
		this.useEndTime = useEndTime;
	}

	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public UserInfo getStopUser() {
		return stopUser;
	}

	public void setStopUser(UserInfo stopUser) {
		this.stopUser = stopUser;
	}

	public Integer getUseLongTime() {
		return useLongTime;
	}

	public void setUseLongTime(Integer useLongTime) {
		this.useLongTime = useLongTime;
	}
   
	
}
