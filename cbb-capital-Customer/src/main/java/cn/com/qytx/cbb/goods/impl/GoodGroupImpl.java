package cn.com.qytx.cbb.goods.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.com.qytx.cbb.capital.dao.CapitalGroupDao;
import cn.com.qytx.cbb.capital.domain.CapitalGroup;
import cn.com.qytx.cbb.capital.service.ICapitalGroup;
import cn.com.qytx.cbb.goods.dao.GoodGroupDao;
import cn.com.qytx.cbb.goods.domain.GoodGroup;
import cn.com.qytx.cbb.goods.service.IGoodGroup;
import cn.com.qytx.cbb.org.util.TreeNode;
import cn.com.qytx.platform.base.service.impl.BaseServiceImpl;

/**
 * 功能:资产组接口实现
 * 版本: 1.0
 * 开发人员: panbo
 * 创建日期: 2016年8月10日
 * 修改日期: 2016年8月10日
 * 修改列表: 
 */
@Service("goodGroupService")
@Transactional
public class GoodGroupImpl extends BaseServiceImpl<GoodGroup> implements IGoodGroup{

	@Resource(name="goodGroupDao")
	private GoodGroupDao goodGroupDao;
	
	@Override
	public List<GoodGroup> findGoodGroupList(Integer companyId) {
		return goodGroupDao.findGoodGroupList(companyId);
	}

	@Override
	public List<TreeNode> getTreeGoodGroupList(String path,Integer companyId) {
		List<GoodGroup> cgList = this.findGoodGroupList(companyId);
		List<TreeNode> tnList = new ArrayList<TreeNode>();
		TreeNode firstNode = new TreeNode();
		firstNode.setId("gid_0");// 部门ID前加gid表示类型为部门
		firstNode.setName("物资组");
		firstNode.setPId("gid_-1");
		firstNode.setIcon(path + "/images/company.png");
		firstNode.setOpen(true);
		tnList.add(firstNode);
		if(cgList!=null&&cgList.size()>0){
			for(GoodGroup cg:cgList){
				TreeNode treeNode = new TreeNode();
				treeNode.setId("gid_"+cg.getId());
				treeNode.setName(cg.getGroupName());
				treeNode.setPId("gid_"+cg.getParentId());
				treeNode.setObj(cg.getOrderIndex()+"{1}"+cg.getGoodCode());// 排序号和编码
				treeNode.setIcon(path + "/images/group.png");
				tnList.add(treeNode);
			}
		}
		return tnList;
	}

	private Map<Integer,Integer> getSubListCountMap(List<GoodGroup> cgList){
		Map<Integer,Integer> map = new HashMap<Integer, Integer>();
		for(GoodGroup cg:cgList){
			Integer parentId = cg.getParentId();
			if(map.containsKey(parentId)){
				map.put(parentId, map.get(parentId)+1);
			}else{
				map.put(parentId, 1);
			}
		}
		
		return map;
	}
	

	@Override
	public List<TreeNode> getTreeGoodGroupList_Select(String path,
			Integer companyId) {
		List<GoodGroup> cgList = this.findGoodGroupList(companyId);
		Map<Integer,Integer> cgMap = this.getSubListCountMap(cgList);
		List<TreeNode> tnList = new ArrayList<TreeNode>();
		TreeNode firstNode = new TreeNode();
		firstNode.setId("gid_0");// 部门ID前加gid表示类型为部门
		firstNode.setName("物资组");
		firstNode.setPId("gid_-1");
		firstNode.setIcon(path + "/images/company.png");
		firstNode.setOpen(true);
		tnList.add(firstNode);
		if(cgList!=null&&cgList.size()>0){
			for(GoodGroup cg:cgList){
				TreeNode treeNode = new TreeNode();
				if(cgMap!=null&&!cgMap.containsKey(cg.getId())){
					treeNode.setId("uid_"+cg.getId());
				}else{
					treeNode.setId("gid_"+cg.getId());
				}
				treeNode.setName(cg.getGroupName());
				treeNode.setPId("gid_"+cg.getParentId());
				treeNode.setObj(cg.getOrderIndex());// 排序号
				treeNode.setIcon(path + "/images/group.png");
				tnList.add(treeNode);
			}
		}
		return tnList;
	}

	@Override
	public boolean isHasSameGroupName(Integer parentId, String groupName,
			int companyId) {
		return goodGroupDao.isHasSameGroupName(parentId, groupName, companyId);
	}

	@Override
	public boolean isHasSameGoodCode(Integer parentId, String capitalCode,
			int companyId) {
		return goodGroupDao.isHasSameGoodCode(parentId, capitalCode, companyId);
	}

	@Override
	public boolean isHasChildGoodGroup(Integer id, int companyId) {
		return goodGroupDao.isHasChildGoodGroup(id,companyId);
	}

	@Override
	public List<GoodGroup> getGoodGroupListByIds(String Ids) {
		return goodGroupDao.getGoodGroupListByIds(Ids);
	}

	@Override
	public GoodGroup findModel(String name, int companyId) {
		// TODO Auto-generated method stub
		return goodGroupDao.findModel(name, companyId);
	}

	

}
