package cn.com.qytx.cbb.goods.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.com.qytx.cbb.goods.dao.GoodDao;
import cn.com.qytx.cbb.goods.domain.Good;
import cn.com.qytx.cbb.goods.service.IGood;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;
import cn.com.qytx.platform.base.service.impl.BaseServiceImpl;

@Service
@Transactional
public class GoodImpl  extends BaseServiceImpl<Good> implements IGood{
  @Autowired
  private GoodDao goodDao;
  
	@Override
	public Page<Good> findGoodPage(Pageable pageable, String name,
			String goodNo, Integer type,Integer companyId,Integer goodGroupId) {
		// TODO Auto-generated method stub
		return goodDao.findGoodPage(pageable, name, goodNo, type,companyId, goodGroupId);
	}

	@Override
	public void deleteGoodByIds(Integer companyId, String ids) {
		// TODO Auto-generated method stub
		 goodDao.deleteGoodByIds(companyId, ids);
	}

	@Override
	public Good findModle(Integer type, String name, Integer goodGroupId) {
		// TODO Auto-generated method stub
		return goodDao.findModle(type, name, goodGroupId);
	}

	

}
