/**
 * 
 */
package cn.com.qytx.cbb.capital.action;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import cn.com.qytx.cbb.capital.domain.Capital;
import cn.com.qytx.cbb.capital.service.ICapital;
import cn.com.qytx.cbb.capital.util.Tool;
import cn.com.qytx.cbb.capital.vo.CapitalVo;
import cn.com.qytx.cbb.dict.domain.Dict;
import cn.com.qytx.cbb.dict.service.IDict;
import cn.com.qytx.cbb.org.util.ExportExcel;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;
import cn.com.qytx.platform.base.query.Sort;
import cn.com.qytx.platform.base.query.Sort.Direction;
import cn.com.qytx.platform.base.query.Sort.Order;
import cn.com.qytx.platform.org.domain.GroupInfo;
import cn.com.qytx.platform.org.domain.UserInfo;
import cn.com.qytx.platform.org.service.IGroup;




/**
 * 功能:
 * 版本: 1.0
 * 开发人员: 王刚刚
 * 创建日期: 2016年8月16日
 * 修改日期: 2016年8月16日
 * 修改列表: 
 */
public class CapitalReportAction extends BaseActionSupport {
 
	/**
	 * 描述含义
	 */
	private static final long serialVersionUID = 2891404351614096576L;

	//资产接口	
	@Autowired	
	ICapital icapital;

  
	@Resource(name = "dictService")
	private  IDict dictService;
	
	 /**
     * 部门,群组管理接口
     */
	@Autowired
    private IGroup groupService;
  
		
	
	//资产显示实体
	private CapitalVo capitalVo;
	 /**
     * 功能：资产列表导出
     */
    public void exporting(){
    	HttpServletResponse response = this.getResponse();
		response.setContentType("application/vnd.ms-excel");
		OutputStream outp=null;
		try {
			UserInfo userInfo =getLoginUser();
			if(userInfo!=null){
				  capitalVo.setCompanyId(userInfo.getCompanyId());
				  Order order = new Order(Direction.DESC, "createTime");
		          Sort s = new Sort(order);
		          super.setIDisplayLength(Integer.MAX_VALUE);
		          Pageable pageable = getPageable(s);
		          Page<Capital> pageInfo=icapital.findCapitalPage(pageable, capitalVo);
		          List<Capital> list=pageInfo.getContent();
		          List<Map<String,Object>> capitalList=analyzeResult(list, pageable);				
				
	            // 把报表信息填充到map里面
				response.addHeader("Content-Disposition", "attachment;filename="
						+ new String("资产统计统计.xls".getBytes(), "iso8859-1"));// 解决中文
				outp = response.getOutputStream();

	            ExportExcel exportExcel = new ExportExcel(outp, getExportHeadList(), capitalList,
						getExportKeyList());
				exportExcel.export();
			}
		} catch(Exception e){
			e.getStackTrace();
		} finally{
			if(outp!=null){
				try {
					outp.close();
				} catch (Exception e) {
					e.getStackTrace();
				}
			}
		}
    }
    
    /**
	 * 
	 * 功能：查询的列表转换成MAp
	 * @param list
	 * @param pageable
	 * @param companyId
	 * @return
	 */
	private List<Map<String,Object>> analyzeResult(List<Capital> list,Pageable pageable ){
		List<Map<String,Object>> mapList=new ArrayList<Map<String,Object>>();  
		if(list!=null&&list.size()>0){
			    int i = 1;
				if(pageable!=null){
	    			i = pageable.getPageNumber() * pageable.getPageSize() + 1;
	    		}
				//数据字典map
				List<Dict> listDict = dictService.findList("capital_store", 1);
				Map<Integer, String> mapDict = new HashMap<Integer, String>();
				for (Dict dict : listDict) {
					mapDict.put(dict.getValue(), dict.getName());
				}
				
				//计量单位
				List<Dict> listUnit = dictService.findList("capital_unit", 1);
				Map<Integer, String> listMap = new HashMap<Integer, String>();
				for (Dict dict : listUnit) {
					listMap.put(dict.getValue(), dict.getName());
				}
				//部门map
				List<GroupInfo> listGroup = groupService.findAll();
				Map<Integer, String> mapGroup = new HashMap<Integer, String>();
				for (GroupInfo groupInfo : listGroup) {
					mapGroup.put(groupInfo.getGroupId(), groupInfo.getGroupName());
				}
				for(Capital capital:list){
					Map<String,Object> map=new HashMap<String, Object>();
					map.put("no", i);
					map.put("goodName",capital.getName());//物品名称
					map.put("capitalGroup", capital.getCapitalGroup().getGroupName());//资产组
					map.put("storageTime",capital.getCreateTime()!=null?Tool.formatDate(capital.getCreateTime()):"--");//入库时间
					map.put("capitalNo", capital.getCapitalNo());//资产编号
					map.put("capitalBrand", StringUtils.isNotBlank(capital.getCapitalBrand())?capital.getCapitalBrand():"--");//资产品牌
					map.put("capitalModel", StringUtils.isNotBlank(capital.getCapitalModel())?capital.getCapitalModel():"--");//资产型号
					map.put("capitalType", getCapitalType(capital.getCapitalType()));//资产类型
					map.put("status",capital.getStatus()==null?"--":getCapitalStatus(capital.getStatus()));//资产状态
					map.put("approveNo", capital.getApproveNo()!=null?capital.getApproveNo():"--");//审批编号
					map.put("checkNum", capital.getCheckNum()!=null?capital.getCheckNum():"--");//盘点次数
					map.put("capitalUnit", capital.getCapitalUnit()!=null?listMap.get(capital.getCapitalUnit()):"--");//计量单位
					map.put("supplier", capital.getSupplier()!=null?capital.getSupplier():"--");//供应商
					map.put("handleUser", capital.getHandleUser()!=null?capital.getHandleUser().getUserName():"--");//采购人
					map.put("buyDate",capital.getBuyDate()!=null?Tool.formatDate(capital.getBuyDate()):"--" );//购置日期
					map.put("invoiceNumber", capital.getInvoiceNumber()!=null?capital.getInvoiceNumber():"--");//发票号码
					map.put("remark", capital.getRemark()!=null?capital.getRemark():"--");//备注
					map.put("price", capital.getPrice()!=null?capital.getPrice():"--");//单价	
					//map.put("usePeople",capital.getUseUser()!=null?capital.getUseUser().getUserName():"--");//使用人
					if(capital.getStatus()==2){
						//使用状态
						if(capital.getCapitalType()==1){//固定资产							
							map.put("useTime",capital.getUseTime()!=null?Tool.formatDate(capital.getUseTime()):"--");
							map.put("usePeople",capital.getUseUser()!=null?capital.getUseUser().getUserName():"--") ;//使用人
							if(capital.getUseUser().getGroupId()!=null){
								GroupInfo useGroupInfo =groupService.findOne(capital.getUseUser().getGroupId());
                                	map.put("useGroupe",useGroupInfo!=null?useGroupInfo.getGroupName():"--") ;//使用人
							}
							//使用部门
						}else{
							map.put("useTime","--");//领用日期
						}
					}					
					map.put("useNum", capital.getUseNum());//1.领用次数
					map.put("repairNum", capital.getRepairNum()!=null?capital.getRepairNum():"--");//维修次数
					
					if(capital.getCapitalType()==1){
						map.put("capitalNum", "--");
						map.put("oldNum", "--");
						map.put("useCount", "--");
					}else{
						map.put("capitalNum", capital.getCapitalNum()!=null?capital.getCapitalNum():"--");//剩余数量
						map.put("oldNum", capital.getOldNum()!=null?capital.getOldNum():"--");//原始数量
						map.put("useCount", capital.getUseCount()!=null?capital.getUseCount():"--");//领用数量
					}
											
					map.put("location",capital.getStoreLocation()!=null?mapDict.get(capital.getStoreLocation()):"--");//存放地址
					map.put("scrapTime", capital.getScrapTime()!=null?Tool.formatDate(capital.getScrapTime()):"--");//报废时间
					map.put("approveRecord", capital.getApproveRecord()!=null?capital.getApproveRecord():"--");//批准记录					
					mapList.add(map);
					i++;
				}
		   }
		   return  mapList;
	}
	
    /**
     * 得到资产类型
     */
	public String getCapitalType(Integer capitalType){
		if(capitalType==1){
			return "固定资产";
		}else{
			return "低值易耗品";
		}
		
	}
	/**
     * 得到资产状态
     */
	public String getCapitalStatus(Integer statu){
		if(statu==1){
			return "在库";
		}else if(statu==2){
			return "使用中";
		}else{
			return "报废";
		}
	}
	
    
    /**
     * 
     * 功能：定义导出的资产表头部信息
     * @return
     */
	private List<String> getExportHeadList(){		
		List<String> headList = new ArrayList<String>();
			headList.add("编号");
			headList.add("资产名称");		
			headList.add("资产组");
			headList.add("入库时间");
			headList.add("资产编号");
			headList.add("资产品牌");
			headList.add("资产型号");
			headList.add("资产类型");
			headList.add("资产状态");
			headList.add("审批编号");		
			headList.add("盘点次数");
			headList.add("计量单位");
			headList.add("供应商");
			headList.add("采购人");
			headList.add("购置日期");
			headList.add("发票号码");
			headList.add("使用人");
			headList.add("使用部门");
			headList.add("资产单价");						
			headList.add("领用次数");
			headList.add("维修次数");
			headList.add("原始数量");
			headList.add("领用数量");
			headList.add("剩余数量");
			headList.add("存放位置");
			headList.add("领用时间");
			headList.add("批准记录");
			headList.add("报废时间");					
			headList.add("备注");
		
		return headList;
	}
	 /**
     * 
     * 功能：定义导出的各个字段属性列表
     * @return
     */
	private List<String> getExportKeyList(){
		List<String> exportKey = new ArrayList<String>();
		exportKey.add("no");
		exportKey.add("goodName");
		exportKey.add("capitalGroup");
		exportKey.add("storageTime");
		exportKey.add("capitalNo");
		exportKey.add("capitalBrand");
		exportKey.add("capitalModel");
		exportKey.add("capitalType");
		exportKey.add("status");
		exportKey.add("approveNo");
		exportKey.add("checkNum");
		exportKey.add("capitalUnit");
		exportKey.add("supplier");
		exportKey.add("handleUser");
		exportKey.add("buyDate");
		exportKey.add("invoiceNumber");
		exportKey.add("usePeople");
		exportKey.add("useGroupe");
		exportKey.add("price");
		exportKey.add("useNum");
		exportKey.add("repairNum");	
		exportKey.add("oldNum");
		exportKey.add("useCount");
		exportKey.add("capitalNum");
		exportKey.add("location");
		exportKey.add("useTime");
		exportKey.add("approveRecord");
		exportKey.add("scrapTime");
		exportKey.add("remark");
		return exportKey;
	}

	public CapitalVo getCapitalVo() {
		return capitalVo;
	}

	public void setCapitalVo(CapitalVo capitalVo) {
		this.capitalVo = capitalVo;
	}
	
}
